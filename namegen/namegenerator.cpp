// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "namegenerator.h"
#include <codecvt>
#include <locale>
#include <iostream>
#include <string>

void NameGenerator::_init() {
}

void NameGenerator::set_seed(const uint64_t p_seed) {
	m_generator.set_seed(p_seed);
}

void NameGenerator::train(const Vector<String> arr) {
	std::vector<std::wstring> data;
	//std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;

	{ // locked scope
		const String *read = arr.ptr();
		for (int i = 0; i < arr.size(); i++) {
			//std::wstring s = converter.from_bytes(read[i].utf8().get_data());
			std::wstring s((const wchar_t *)read[i].ptr()); // TODO
			data.push_back(s);
		}
	} // locked scope
	m_generator.train(data);
}

bool NameGenerator::is_trained() const {
	return m_generator.is_trained();
}

void NameGenerator::_bind_methods() {
	ClassDB::bind_method(D_METHOD("new_word", "min", "max"), &NameGenerator::new_word);
	ClassDB::bind_method(D_METHOD("new_words", "n", "min", "max"), &NameGenerator::new_words);
	ClassDB::bind_method(D_METHOD("train", "arr"), &NameGenerator::train);
	ClassDB::bind_method(D_METHOD("is_trained"), &NameGenerator::is_trained);
}


String NameGenerator::new_word(int min, int max) const {
	return String(m_generator.new_word(min, max).c_str());
}

Vector<String> NameGenerator::new_words(int n, int min, int max) const {
	auto words = m_generator.new_words(n, min, max);
	Vector<String> arr;
	arr.resize(words.size());
	String *write = arr.ptrw();
	for (size_t i = 0; i < words.size(); i++) { //const std::wstring &s : words) {
		write[i] = String(words[i].c_str());
	}
	return arr;
}
