// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include <unordered_map>
#include <algorithm>
#include "core/math/random_pcg.h"

typedef std::unordered_map<std::wstring, std::vector<double>> modelData;

class MarkovModel {
public:
	MarkovModel(const std::vector<std::wstring> &trainData,
		  const int order, double dPrior) :
		m_dPrior(dPrior), m_order(order), m_models(order)
	{
		m_models.resize(m_order);
		p_train(trainData);
	}

	MarkovModel() : m_dPrior(0.0), m_order(0)
	{
	}

	void set_seed(const uint64_t p_seed) {
		pcg = RandomPCG(p_seed);
	}

	inline int order() const {
		return m_order;
	}

	inline bool is_trained() const {
		return !m_models.empty();
	}

	// Return the next wchar_t based on a context/word
	wchar_t generate(const std::wstring &context) const {
		wchar_t res = '#';
		if (!is_trained()) {
			return res;
		}
		int _order = m_order;

		for (int i = m_order; i > 0; i--) {
			std::wstring s = context.substr(context.size() - i, i);
			const modelData &model = get_model(_order);
			auto it = model.find(s);

			if (it != model.cend()) {
				res = m_alphabet[select_index((*it).second)];
				break;
			}
		}
		return res;
	}

	void train(const std::vector<std::wstring> &trainData,
			   const int order = 3, double dPrior = 0.0)
	{
		m_order = order;
		m_models.resize(m_order);
		for (auto &map: m_models) {
			map.clear();
		}
		m_dPrior = dPrior;
		p_train(trainData);
	}

private:
	RandomPCG pcg;

	double m_dPrior;
	int m_order;

	// List of letters in the model
	std::vector<wchar_t> m_alphabet;
	// Katz's back-off model with high order models.
	std::vector<modelData> m_models;

	inline void p_train(const std::vector<std::wstring> &trainData) {
		generate_alphabet(trainData);
		// build the chains of every order
		for (int i = 1; i <= m_order; i++) {
			build_chains(trainData, i);
		}
	}

	modelData& get_model(int order) {
		return m_models[order -1];
	}
	const modelData& get_model(int order) const {
		return m_models[order -1];
	}

	size_t select_index(const std::vector<double> &chain) const {
		double accumulator = 0.0;
		std::vector<double> totals;
		
		for (const double weight : chain) {
			accumulator += weight;
			totals.push_back(accumulator);
		}

		double randRes = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
		double random = randRes * accumulator;

		for (size_t i = 0; i < totals.size(); ++i) {
			if (random < totals[i]) {
				return i;
			}
		}
		return 0;
	}

	// generate the chain for a given order based on the training data,
	// the chain vector must be initialized before calling this function.
	void build_chains(const std::vector<std::wstring> &trainData,
					 const int order)
	{
		// Generate observations (wchar_ts after ecah groups of n=order wchar_ts)
		const std::wstring padding(order, '#');
		std::unordered_map<std::wstring, std::vector<wchar_t>> observations;

		for (std::wstring word : trainData) {
			word = padding + word;
			word += '#';

			for (int i = 0; i < (int)word.length() - order; i++) {
				std::wstring key = word.substr(i, order);

				std::vector<wchar_t> &value = observations[key];
				value.push_back(word.at(i + order));
			}
		}
		// build the chain
		for (auto it : observations) {
			const std::wstring &key = it.first;
			const std::vector<wchar_t> &value = it.second;

			for (const wchar_t prediction : m_alphabet) {
				std::vector<double> &chain = get_model(order)[key];
				int count = 0;
				for (const wchar_t c : value) {
					if (prediction == c) {
						++count;
					}
				}
				chain.push_back(m_dPrior + count);
			}
		}
	}

	// Generate a list of all the wchar_ts in the training data
	void generate_alphabet(const std::vector<std::wstring> &trainData) {
		m_alphabet.resize(0);
		m_alphabet.push_back('#');
		for (const std::wstring &word : trainData) {

			for (const wchar_t c : word) {
				if (std::find(m_alphabet.begin(), m_alphabet.end(), c) == m_alphabet.end()) {
					m_alphabet.push_back(c);
				}
			}
		}
		std::sort(m_alphabet.begin(), m_alphabet.end());
	}
};

class WordGenerator {
public:

	WordGenerator() {
	}

	WordGenerator(const std::vector<std::wstring> &trainData,
		const int order, const double prior) :
		m_model(trainData, order, prior)
	{
	}

	void set_seed(const uint64_t p_seed) {
		m_model.set_seed(p_seed);
	}

	void train(const std::vector<std::wstring> &trainData,
			   const int order = 3, double dPrior = 0.0)
	{
		m_model.train(trainData, order, dPrior);
	}

	inline bool is_trained() const {
		return m_model.is_trained();
	}

	std::wstring new_word(const int minLength, const int maxLength) const {
		std::wstring word;

		if (!is_trained()) {
			return word;
		}
		int i = 0;
	    do {
			word = std::wstring(m_model.order(), '#');
			wchar_t letter = m_model.generate(word);

			while (letter != '#') {
				word += letter;
				letter = m_model.generate(word);
			}

			word.erase(std::remove(word.begin(), word.end(), '#'), word.end());
			i++;
		} while (i < 100 && ((int)word.size() < minLength || (int)word.size() > maxLength));

		return word;
	}

	std::vector<std::wstring> new_words(
		const size_t n,
		const int minLength,
		const int maxLength,
		bool repeat = false) const 
	{
		std::vector<std::wstring> words;

		if (!is_trained()) {
			return words;
		}

		words.reserve(n);

		while (words.size() < n) {
			std::wstring word = new_word(minLength, maxLength);
			words.push_back(word);
		}
		return words;
	}

private:
	MarkovModel m_model;
};
