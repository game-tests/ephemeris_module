// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "core/templates/vector.h"
#include "core/object/ref_counted.h"
#include "markovnames.h"

class NameGenerator : public RefCounted {
	GDCLASS(NameGenerator, RefCounted)

public:
	static void _bind_methods();

	void _init();

	void set_seed(const uint64_t p_seed);

	void train(const Vector<String> arr);

	bool is_trained() const;

	String new_word(int min , int max) const;

	Vector<String> new_words(int n, int min, int max) const;

private:
	WordGenerator m_generator;
};
