// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "register_types.h"
#include "core/object/class_db.h"
#include <influence_curve.h>
#include <stardatagenerator.h>
#include <mappalette.h>
#include <aoi.h>
#include <biome.h>
#include <biomefeature.h>
#include <bf_radial_elevation.h>
#include <bf_base_noise.h>
#include <distributionfeature.h>
#include <ecotope.h>
#include <influence_curve.h>
#include <noisegenerator.h>
#include <simdnoisegenerator.h>
#include <fastnoisegenerator.h>
#include <worldinstance.h>
#include <worldcompositor.h>
#include <noise_graph.h>
#include <noise_editor_nodes.h>
#ifdef TOOLS_ENABLED
#include <generationplugin.h>
#include <visual_noise_editor_plugin.h>
#include <influence_curve_editor_plugin.h>
#endif

void initialize_ephemeris_module(ModuleInitializationLevel p_level) {
if (p_level == MODULE_INITIALIZATION_LEVEL_SCENE) {
        GDREGISTER_CLASS(StarDataGenerator);

        GDREGISTER_CLASS(EcotopePalette);
        GDREGISTER_CLASS(BiomePalette);

        GDREGISTER_CLASS(InfluenceCurve);

        GDREGISTER_ABSTRACT_CLASS(BiomeComponent);
        GDREGISTER_CLASS(AreaOfInterest);
        GDREGISTER_CLASS(Biome);
        GDREGISTER_CLASS(DistributionFeature);
        GDREGISTER_CLASS(Ecotope);

        GDREGISTER_ABSTRACT_CLASS(BiomeFeature);
        GDREGISTER_CLASS(BFRadialElevation);
        GDREGISTER_CLASS(BFBaseNoise);

        GDREGISTER_CLASS(NoiseBuffer);
        GDREGISTER_ABSTRACT_CLASS(NoiseGenerator);
        GDREGISTER_CLASS(SIMDNoiseGenerator);
        GDREGISTER_CLASS(FastNoiseGenerator);

        GDREGISTER_CLASS(CompositionData);
        GDREGISTER_CLASS(WorldInstance);
        GDREGISTER_CLASS(WorldCompositor);

        GDREGISTER_ABSTRACT_CLASS(VisualNoiseNode);
        GDREGISTER_CLASS(VisualNoiseNodeOutput);
        GDREGISTER_CLASS(VisualNoiseNodeInput);
        GDREGISTER_CLASS(VisualNoiseNodeScalarConstant);
        GDREGISTER_CLASS(VisualNoiseNodeNoiseGenerator);
        GDREGISTER_CLASS(VisualNoiseNodeFractalGenerator);
        GDREGISTER_CLASS(VisualNoiseNodeCellNoiseGenerator);
        GDREGISTER_CLASS(VisualNoiseNodeAbs);
        GDREGISTER_CLASS(VisualNoiseNodeBlend);
        GDREGISTER_CLASS(VisualNoiseNodeClamp);
        GDREGISTER_CLASS(VisualNoiseNodeInvert);
        GDREGISTER_CLASS(VisualNoiseNodeThreshold);
        GDREGISTER_CLASS(VisualNoiseNodeNormalize);
        GDREGISTER_CLASS(VisualNoiseNodeScalarOperator);
        GDREGISTER_CLASS(VisualNoiseNodeOperator);
        GDREGISTER_CLASS(NoiseGraph);
	}
#ifdef TOOLS_ENABLED
	if (p_level == MODULE_INITIALIZATION_LEVEL_EDITOR) {
		EditorPlugins::add_by_type<GenerationPlugin>();
                EditorPlugins::add_by_type<VisualNoiseEditorPlugin>();
                EditorPlugins::add_by_type<InfluenceCurveEditorPlugin>();
	}
#endif

}

void uninitialize_ephemeris_module(ModuleInitializationLevel p_level) {

}
