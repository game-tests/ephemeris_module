/*************************************************************************/
/*  material_editor_plugin.cpp                                           */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           GODOT ENGINE                                */
/*                      https://godotengine.org                          */
/*************************************************************************/
/* Copyright (c) 2007-2020 Juan Linietsky, Ariel Manzur.                 */
/* Copyright (c) 2014-2020 Godot Engine contributors (cf. AUTHORS.md).   */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "noise_3d_preview.h"

#include "editor/editor_scale.h"
#include "scene/gui/subviewport_container.h"
#include "scene/resources/particles_material.h"
#include "scene/resources/sky_material.h"
#include "scene/gui/spin_box.h"
#include "scene/main/timer.h"
#include <polygonization.h>

void Noise3dPreview::_notification(int p_what) {

	switch (p_what) {
		case NOTIFICATION_VISIBILITY_CHANGED: {
			if (is_visible()) {
				update_mesh_preview();
			}
		} break;
		case NOTIFICATION_DRAW: {
			Ref<Texture2D> checkerboard = get_theme_icon("Checkerboard", "EditorIcons");
			Size2 size = get_size();

			draw_texture_rect(checkerboard, Rect2(Point2(), size), true);
		} break;
	}
}

void Noise3dPreview::_gui_input(const Ref<InputEvent> &p_event) {
	Ref<InputEventMouseMotion> mm = p_event;
	if (mm.is_valid() && (mm->get_button_mask() & MouseButton::MASK_RIGHT) != MouseButton::NONE) {
		real_t rotation = mesh_instance->get_rotation().y;
		rotation += mm->get_relative().x * 0.01;
		mesh_instance->set_rotation(Vector3(0, rotation, 0));
	}
	Ref<InputEventMouseButton> mb = p_event;
	if (mb.is_valid()) {
		const real_t zoom_val = 2.0;
		if (mb->get_button_index() == MouseButton::WHEEL_UP) {
			if (camera->get_position().z > 10) {
				camera->translate(Vector3(0, 0, -zoom_val));
			}
		} else if (mb->get_button_index() == MouseButton::WHEEL_DOWN) {
			if (camera->get_position().z < 80) {
				camera->translate(Vector3(0, 0, zoom_val));
			}
		}
	}
}

void Noise3dPreview::update_mesh_preview() {
	if (noise_graph.is_valid()) {
		Polygonizer p;
		p.set_chunk_size(32);
		p.set_vertex_distance(2);
		RawMesh rm = p.marching_cubes(noise_graph, Point2i(-16, -16), 0, 456, spin_box->get_value());
		Ref<ArrayMesh> arr_mesh;
		arr_mesh.instantiate();
		if (rm.has_data()) {
			rm.to_indexed_rounded();
			Array arr = rm.get_mesh_array();
			arr_mesh->add_surface_from_arrays(Mesh::PRIMITIVE_TRIANGLES, arr);
		}
		mesh_instance->set_mesh(arr_mesh);
	}
}

void Noise3dPreview::threshold_changed(const double p_val) {
	timer->start(0.05);
}

void Noise3dPreview::set_noise(Ref<NoiseGraph> p_noise) {
	noise_graph = p_noise;
	if (is_visible()) {
		update_mesh_preview();
	}
}

void Noise3dPreview::_bind_methods() {
}

Noise3dPreview::Noise3dPreview() {
	vc = memnew(SubViewportContainer);
	vc->set_stretch(true);
	add_child(vc);
	vc->set_anchors_and_offsets_preset(PRESET_WIDE);
	viewport = memnew(SubViewport);
	Ref<World3D> world_3d;
	world_3d.instantiate();
	viewport->set_world_3d(world_3d); //use own world
	vc->add_child(viewport);
	viewport->set_disable_input(true);
	viewport->set_transparent_background(true);
	viewport->set_msaa(Viewport::MSAA_4X);

	camera = memnew(Camera3D);
	const real_t camera_distance = 55.0;
	camera->set_transform(Transform3D(Basis(), Vector3(0, 16, camera_distance)));
	camera->set_perspective(45, 0.1, 100);
	camera->make_current();
	viewport->add_child(camera);

	light1 = memnew(DirectionalLight3D);
	light1->set_rotation(Vector3(0, Math::deg2rad(-45.0), 0));
	viewport->add_child(light1);

	light2 = memnew(DirectionalLight3D);
	light2->set_rotation(Vector3(0, Math::deg2rad(45.0), 0));
	light2->set_color(Color(0.7, 0.7, 0.7));
	viewport->add_child(light2);

	mesh_instance = memnew(MeshInstance3D);
	viewport->add_child(mesh_instance);

	material.instantiate();
	Ref<Shader> shader;
	shader.instantiate();
	shader->set_code("shader_type spatial;\n"
		"void fragment() {\n"
		"	// same normal vector for every face\n"
		"	NORMAL = -normalize(cross(dFdx(VERTEX), dFdy(VERTEX)));\n"
		"	\n"
		"	vec3 world_normal = NORMAL * mat3(\n"
		"		VIEW_MATRIX[0].xyz,\n"
		"		VIEW_MATRIX[1].xyz,\n"
		"		VIEW_MATRIX[2].xyz);\n"
		"	\n"
		"	ALBEDO = vec3(0.85, 0.85, 0.7);\n"
		"	ROUGHNESS = 0.45;\n"
		"}\n");
	material->set_shader(shader);
	mesh_instance->set_material_override(material);

	set_custom_minimum_size(Size2(400 * EDSCALE, 150 * EDSCALE));

	env.instantiate();
	Ref<Sky> sky = memnew(Sky());
	env->set_sky(sky);
	env->set_background(Environment::BG_COLOR);
	env->set_ambient_source(Environment::AMBIENT_SOURCE_SKY);
	env->set_reflection_source(Environment::REFLECTION_SOURCE_SKY);
	camera->set_environment(env);

	// Threshold
	HBoxContainer *hb = memnew(HBoxContainer);
	add_child(hb);
	
	spin_box = memnew(SpinBox);
	spin_box->set_allow_greater(true);
	spin_box->set_allow_lesser(true);
	spin_box->set_min(-100);
	spin_box->set_max(100);
	spin_box->set_step(0.01);
	spin_box->set_exp_ratio(true);
	spin_box->connect("value_changed", callable_mp(this, &Noise3dPreview::threshold_changed));
	
	hb->add_child(memnew(Label("Threshold: ")));
	hb->add_child(spin_box);

	timer = memnew(Timer);
	timer->set_one_shot(true);
	timer->connect("timeout", callable_mp(this, &Noise3dPreview::update_mesh_preview));
	add_child(timer);

	vc->connect("gui_input", callable_mp(this, &Noise3dPreview::_gui_input));
}
