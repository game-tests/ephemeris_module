// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "noise_mixer.h"
#include "core/math/math_defs.h"
#include "core/string/print_string.h"


String NoiseMixer::get_caption() const {
	return "Graph";
}

int NoiseMixer::get_input_port_count() const {
	return 0;
}

VisualNoiseNode::PortType NoiseMixer::get_input_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String NoiseMixer::get_input_port_name(int p_port) const {
	return "";
}

int NoiseMixer::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType NoiseMixer::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String NoiseMixer::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> NoiseMixer::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

Ref<NoiseBuffer> NoiseMixer::generate_noise(const Rect2i &p_rect, const int p_height, const int p_step, const int p_seed) {
	Ref<NoiseBuffer> res = memnew(NoiseBuffer);
	res->resize_v(p_rect.size / p_step);

	const real_t max_v = 1.0;
	const real_t min_v = -1.0;
	const real_t value_range = (max_v - min_v); // TO DO
	// range between global threshold and max normalized value
	const real_t th_range = max_v - get_target_threshold(); // TO DO
	
	for (GraphInfo &gi : data) {
		// Normalization between min_v and max_v
		const real_t normalization_factor = value_range / (gi.noise->get_max_value() - gi.noise->get_min_value());
		
		Ref<NoiseBuffer> tmp = gi.noise->generate_noise(p_rect, p_height, p_step, p_seed);

		int i = 0;
		const Vector2i limit = p_rect.get_end();
		for (int r = p_rect.position.x; r < limit.x; r += p_step) {
		for (int c = p_rect.position.y; c < limit.y; c += p_step) {
			const float ecotope_infl = gi.influence->operator()(r, c);
			const float global_infl = gi.g_influence->operator()(r, c);
			float influence_factor = ecotope_infl * global_infl;

			// New float value. 3D polygonization doesnt't care about values over 1 so we normalize the
			// noise value.
			float v = min_v + (tmp->get_value_linear(i) - gi.noise->get_min_value()) * normalization_factor;
			// Apply height influence
			if (gi.infl_curve.is_valid()) {
				influence_factor *= gi.infl_curve->get_height_value(p_height);
			}
			// Apply terrain influence
			// an influence of 0 means the minimun noise value
			v -= (th_range - th_range * influence_factor);
			// Apply normalized threshold offset
			v -= gi.threshold;
			tmp->set_value_linear(i, v);
			i++;
		}}
		res->add(tmp);
	}
	return res;
}

void NoiseMixer::set_height_range(const int p_min, const int p_max) {
	min_height = p_min;
	max_height = p_max;
}

void NoiseMixer::add_graph(Ref<NoiseGraph> &p_noise, Ref<InfluenceCurve> &p_infl_curve, const Matrix<float> *p_ginfluence, const Matrix<float> *p_influence, const float p_threshold) {
	GraphInfo i;
	i.infl_curve = p_infl_curve;
	i.noise = p_noise;
	i.influence = p_influence;
	i.g_influence = p_ginfluence;
	i.threshold = p_threshold;

	data.push_back(i);
}

real_t NoiseMixer::get_target_threshold() const {
	return 0.5;
}

NoiseMixer::NoiseMixer() {

}
