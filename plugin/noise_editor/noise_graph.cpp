// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "noise_graph.h"

// VisualNoiseNode

void VisualNoiseNode::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_default_input_value", "port_idx", "value"), &VisualNoiseNode::set_default_input_value);
	ClassDB::bind_method(D_METHOD("get_default_input_value", "port_idx"), &VisualNoiseNode::get_default_input_value);
	ClassDB::bind_method(D_METHOD("_set_default_input_values", "values"), &VisualNoiseNode::_set_default_input_values);
	ClassDB::bind_method(D_METHOD("_get_default_input_values"), &VisualNoiseNode::_get_default_input_values);

	ADD_PROPERTY(PropertyInfo(Variant::ARRAY, "_default_input_values", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR | PROPERTY_USAGE_INTERNAL), "_set_default_input_values", "_get_default_input_values");
}

real_t VisualNoiseNode::parse_scalar(const Variant &p_v) const {
	real_t res = 0.0;
	if (p_v.get_type() == Variant::FLOAT || p_v.get_type() == Variant::INT) {
		res = p_v;
	} else if (p_v.get_type() == Variant::OBJECT) { // Get the noise buffer value.
		Ref<NoiseBuffer> noise = p_v;
		if (noise.is_null()) {
			// error
		} else {
			res = noise->get_value(0, 0);
		}
	} else { // Invalid type
		// error
	}
	return res;
}

Ref<NoiseBuffer> VisualNoiseNode::parse_noise(const Variant &p_v, const GraphData &p_graph_data, const bool p_copy) const {
	Ref<NoiseBuffer> res;
	if (p_v.get_type() == Variant::FLOAT ||  p_v.get_type() == Variant::INT) { // A number requires conversion.
		float value = p_v;
		res.instantiate();
		res->_resize_no_memset(p_graph_data.rect.size);
		res->scalar_assign(value);

	} else if (p_v.get_type() == Variant::OBJECT) { // Get the noise buffer.
		res = p_v;
		if (res.is_null()) {
			//r_error_str = "TODO";
		} else if (p_copy) {
			res = res->duplicate();
		}
	} else if (p_v.get_type() == Variant::NIL) {
		res.instantiate();
		res->resize_v(p_graph_data.rect.size);
	} else { // Invalid type
		//r_error_str = "TODO";
	}
	return res;
}

bool VisualNoiseNode::is_dirty() const {
	return dirty;
}
void VisualNoiseNode::set_dirty(const bool p_dirty) {
	dirty = p_dirty;
}

void VisualNoiseNode::_set_default_input_values(Array p_values) {
	if (p_values.size() % 2 == 0) {
		for (int i = 0; i < p_values.size(); i += 2) {
			default_input_values[p_values[i + 0]] = p_values[i + 1];
		}
	}
}

Array VisualNoiseNode::_get_default_input_values() const {
	Array ret;
	for (RBMap<int, Variant>::Element *E = default_input_values.front(); E; E = E->next()) {
		ret.push_back(E->key());
		ret.push_back(E->get());
	}
	return ret;
}

void VisualNoiseNode::set_output_value(const Variant &p_val) {
	output = p_val;
}

void VisualNoiseNode::set_default_input_value(int p_port, const Variant &p_value) {
	default_input_values[p_port] = p_value;
}

Variant VisualNoiseNode::get_default_input_value(int p_port) const {
	if (default_input_values.has(p_port)) {
		return default_input_values[p_port];
	}

	return Variant();
}

real_t VisualNoiseNode::get_min_value() const {
	return min_value;
}

real_t VisualNoiseNode::get_max_value() const {
	return max_value;
}

Variant VisualNoiseNode::get_output_value() const {
	return output;
}

bool VisualNoiseNode::show_output_preview() const {
	return true;
}

// NoiseGraph

String NoiseGraph::get_caption() const {
	return "Graph";
}

int NoiseGraph::get_input_port_count() const {
	return input_nodes.size();
}

VisualNoiseNode::PortType NoiseGraph::get_input_port_type(int p_port) const {
	return input_nodes[p_port]->get_output_port_type(1);
}

String NoiseGraph::get_input_port_name(int p_port) const {
	return input_nodes[p_port]->get_output_port_name(1);
}

int NoiseGraph::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType NoiseGraph::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String NoiseGraph::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> NoiseGraph::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

Ref<NoiseBuffer> NoiseGraph::generate_noise(const Rect2i &p_rect, const int p_height, const int p_step, const int p_seed) {
	const int node_index = 0;
	propagate_dirty_backwards(node_index);
	evaluate_nodes(p_rect, p_height, p_step, p_seed);
	Ref<NoiseBuffer> res = get_output_value();
	return res;
}

void NoiseGraph::evaluate_nodes(const Rect2i &p_rect, const int p_height, const int p_step, const int p_seed) {
	GraphData graph_data;
	graph_data.rect = p_rect;
	graph_data.seed = p_seed;
	graph_data.height = p_height;
	graph_data.step = p_step;

	if (cache_dirty) {
		connection_cache = build_backwards_port_map();
		cache_dirty = false;
	}

	// Evaluate every node marked as dirty.
	for (const RBMap<int, Node>::Element *E = graph.nodes.front(); E; E = E->next()) {
		if (E->value().node->is_dirty()) {
			_evaluate_nodes(E->key(), E->value().node, graph_data, connection_cache);
		}
	}
	set_output_value(get_node(0)->get_output_value());

	min_value = get_node(0)->get_min_value();
	max_value = get_node(0)->get_max_value();
}

void NoiseGraph::_evaluate_nodes(const int p_id, Ref<VisualNoiseNode> p_node, const GraphData &p_graph_data, const InputConnections &p_conn) {
	ConnectionKey conn_key;
	conn_key.node = p_id;
	
	Array inputs;
	inputs.resize(p_node->get_input_port_count());
	Vector<Ref<VisualNoiseNode>> nodes_list;
	nodes_list.resize(p_node->get_input_port_count());
	// Gather all the input values
	for (int i = 0; i < p_node->get_input_port_count(); i++) {
		conn_key.port = i;
		int index = p_conn.find(conn_key);
		// If the input i is not connected to other node take the default port value or null.
		if (index == -1) {
			inputs[i] = p_node->get_default_input_value(i);
		// If the port is connected evaluate that connected node.
		} else {
			const Connection &port_connection = p_conn.getv(index)->get();
			Ref<VisualNoiseNode> n = get_node(port_connection.from_node);
			if (n->is_dirty()) {
				_evaluate_nodes(port_connection.from_node, n, p_graph_data, p_conn);
			}
			inputs[i] = n->get_output_value();

			nodes_list.insert(i, n);
		}
	}
	p_node->update_minmax(inputs, nodes_list);
	p_node->evaluate(inputs, p_graph_data);
	p_node->set_dirty(false);
}

void NoiseGraph::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	// TODO inputs
	evaluate_nodes(p_graph_data.rect, p_graph_data.height, p_graph_data.step, p_graph_data.seed);
}

void NoiseGraph::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {

}

void NoiseGraph::_propagate_dirty_forward(const int p_id, const OutputConnections &p_conn) {
	Ref<VisualNoiseNode> n = get_node(p_id);
	n->set_dirty(true);
	
	ConnectionKey from_key;
	from_key.node = p_id;
	from_key.port = 0;

	int index = p_conn.find(from_key);
	if (index == -1) {
		return;
	}

	const List<const List<Connection>::Element *> l = p_conn.getv(index);
	for (const List<const List<Connection>::Element *>::Element *E = l.front(); E; E = E->next()) {
		const Connection &C = E->get()->get();
		_propagate_dirty_forward(C.to_node, p_conn);
	}
}

void NoiseGraph::_propagate_dirty_backwards(const int p_id, const InputConnections &p_conn) {
	Ref<VisualNoiseNode> n = get_node(p_id);
	n->set_dirty(true);
	for (int i = 0; i < n->get_input_port_count(); i++) {

		ConnectionKey from_key;
		from_key.node = p_id;
		from_key.port = i;
		
		int index = p_conn.find(from_key);
		if (index == -1) {
			continue;
		}

		const List<Connection>::Element *E = p_conn.getv(index);
		_propagate_dirty_backwards(E->get().from_node, p_conn);
	}
}

void NoiseGraph::propagate_dirty_forward(const int p_id) {
	OutputConnections c = build_forward_port_map();
	_propagate_dirty_forward(p_id, c);
}

void NoiseGraph::propagate_dirty_backwards(const int p_id) {
	if (cache_dirty) {
		connection_cache = build_backwards_port_map();
		cache_dirty = false;
	}
	_propagate_dirty_backwards(p_id, connection_cache);
}

NoiseGraph::OutputConnections NoiseGraph::build_forward_port_map() {
	OutputConnections res;

	for (const List<Connection>::Element *E = graph.connections.front(); E; E = E->next()) {
		ConnectionKey from_key;
		from_key.node = E->get().from_node;
		from_key.port = E->get().from_port;
		if (res.has(from_key)) {
			res[from_key].push_back(E);
		} else {
			List<const List<Connection>::Element *> l;
			l.push_back(E);
			res.insert(from_key, l);
		}
	}
	return res;
}

NoiseGraph::InputConnections NoiseGraph::build_backwards_port_map() {
	InputConnections res;

	for (const List<Connection>::Element *E = graph.connections.front(); E; E = E->next()) {
		ConnectionKey to_key;
		to_key.node = E->get().to_node;
		to_key.port = E->get().to_port;
		res.insert(to_key, E);
	}
	return res;
}

void NoiseGraph::set_editor_dirty(const bool p_dirty) {
	for (RBMap<int, Node>::Element *E = graph.nodes.front(); E; E = E->next()) {
		E->get().node->set_dirty(true);
	}
}

void NoiseGraph::set_dirty(const bool p_dirty) {
	propagate_dirty_backwards(0);
}

String NoiseGraph::get_graph_name() const {
	return name;
}

void NoiseGraph::set_graph_name(const String &p_name) {
	name = p_name;
}

void NoiseGraph::set_graph_offset(const Vector2 &p_offset) {
	graph_offset = p_offset;
}

Vector2 NoiseGraph::get_graph_offset() const {
	return graph_offset;
}

Vector2 NoiseGraph::get_node_position(int p_id) const {
	Vector2 res;
	const RBMap<int, Node>::Element *E = graph.nodes.find(p_id);
	if (E) {
		res = E->get().position;
	}
	return res;
}

void NoiseGraph::set_node_position(int p_id, const Vector2 &p_pos) {
	RBMap<int, Node>::Element *E = graph.nodes.find(p_id);
	if (E) {
		E->get().position = p_pos;
	}
}

void NoiseGraph::add_node(const Ref<VisualNoiseNode> &p_node, const Vector2 &p_position, int p_id) {
	// check if add to input nodes
	graph.nodes.insert(p_id, Node(p_node, p_position));
	std::vector<PortInfo> pi;
	pi.resize(p_node->get_input_port_count());
	graph.ports_info.insert(p_id, pi);
	cache_dirty = true;
}

void NoiseGraph::remove_node(int p_id) {
	graph.nodes.erase(p_id);
	graph.ports_info.erase(p_id);
	cache_dirty = true;
}

bool NoiseGraph::has_node(int p_id) const {
	return graph.nodes.has(p_id);
}

int NoiseGraph::get_valid_node_id() const {
	return graph.nodes.size() ? MAX(2, graph.nodes.back()->key() + 1) : 2;
}

Vector<int> NoiseGraph::get_node_list() const { // TODO do we need it?
	Vector<int> ret;
	
	for (RBMap<int, Node>::Element *E = graph.nodes.front(); E; E = E->next()) {
		ret.push_back(E->key());
	}

	return ret;
}

Ref<VisualNoiseNode> NoiseGraph::get_node(int p_id) const {
	Ref<VisualNoiseNode> res;
	const RBMap<int, Node>::Element *E = graph.nodes.find(p_id);
	if (E) {
		res = E->get().node;
	}
	return res;
}

bool NoiseGraph::is_nodes_connected_relatively(int p_node, int p_target) const {
	bool result = false;

	const Node &node = graph.nodes[p_node];

	for (const List<int>::Element *E = node.prev_connected_nodes.front(); E; E = E->next()) {
		if (E->get() == p_target) {
			return true;
		}

		result = is_nodes_connected_relatively(E->get(), p_target);
		if (result) {
			break;
		}
	}
	return result;
}

bool NoiseGraph::can_connect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port) const {

	if (!graph.nodes.has(p_from_node)) {
		return false;
	}

	if (p_from_node == p_to_node) {
		return false;
	}

	if (p_from_port < 0 || p_from_port >= graph.nodes[p_from_node].node->get_output_port_count()) {
		return false;
	}

	if (!graph.nodes.has(p_to_node)) {
		return false;
	}

	if (p_to_port < 0 || p_to_port >= graph.nodes[p_to_node].node->get_input_port_count()) {
		return false;
	}

	// input already used, covers already exist case.
	if (graph.ports_info[p_to_node][p_to_port].port_connected) {
		return false;
	}

	if (is_nodes_connected_relatively(p_from_node, p_to_node)) {
		return false;
	}

	return true;
}


Error NoiseGraph::connect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port) {
	ERR_FAIL_COND_V(!graph.nodes.has(p_from_node), ERR_INVALID_PARAMETER);
	ERR_FAIL_INDEX_V(p_from_port, graph.nodes[p_from_node].node->get_output_port_count(), ERR_INVALID_PARAMETER);
	ERR_FAIL_COND_V(!graph.nodes.has(p_to_node), ERR_INVALID_PARAMETER);
	ERR_FAIL_INDEX_V(p_to_port, graph.nodes[p_to_node].node->get_input_port_count(), ERR_INVALID_PARAMETER);

	for (List<Connection>::Element *E = graph.connections.front(); E; E = E->next()) {
		if (E->get().from_node == p_from_node && E->get().from_port == p_from_port && E->get().to_node == p_to_node && E->get().to_port == p_to_port) {
			ERR_FAIL_V(ERR_ALREADY_EXISTS);
		}
	}

	Connection c;
	c.from_node = p_from_node;
	c.from_port = p_from_port;
	c.to_node = p_to_node;
	c.to_port = p_to_port;
	graph.connections.push_back(c);
	graph.nodes[p_to_node].prev_connected_nodes.push_back(p_from_node);

	graph.ports_info[p_to_node][p_to_port].port_connected = true;
	cache_dirty = true;

	return Error::OK;
}

void NoiseGraph::disconnect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port) {
	for (List<Connection>::Element *E = graph.connections.front(); E; E = E->next()) {
		if (E->get().from_node == p_from_node && E->get().from_port == p_from_port && E->get().to_node == p_to_node && E->get().to_port == p_to_port) {
			graph.connections.erase(E);
			graph.nodes[p_to_node].prev_connected_nodes.erase(p_from_node);
			graph.ports_info[p_to_node][p_to_port].port_connected = false;
			break;
		}
	}
	cache_dirty = true;
}

void NoiseGraph::get_node_connections(List<Connection> *r_connections) const {
	for (const List<Connection>::Element *E = graph.connections.front(); E; E = E->next()) {
		r_connections->push_back(E->get());
	}
}

bool NoiseGraph::is_node_input_connected(const int p_node, const int p_port) const {
	bool res = false;
	auto *E = graph.ports_info.find(p_node);
	if (E) {
		res = E->get()[p_port].port_connected;
	}
	return res;
}

NoiseGraph::NoiseGraph() {
	Ref<VisualNoiseNodeOutput> output;
	output.instantiate();
	add_node(output, Vector2(400, 150), 0);
}

void NoiseGraph::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_graph_name", "name"), &NoiseGraph::set_graph_name);
	ClassDB::bind_method(D_METHOD("get_graph_name"), &NoiseGraph::get_graph_name);

	ClassDB::bind_method(D_METHOD("set_graph_offset", "offset"), &NoiseGraph::set_graph_offset);
	ClassDB::bind_method(D_METHOD("get_graph_offset"), &NoiseGraph::get_graph_offset);

	ClassDB::bind_method(D_METHOD("set_node_position", "pos"), &NoiseGraph::set_node_position);
	ClassDB::bind_method(D_METHOD("get_node_position"), &NoiseGraph::get_node_position);

	ClassDB::bind_method(D_METHOD("connect_nodes"), &NoiseGraph::connect_nodes);
	ClassDB::bind_method(D_METHOD("disconnect_nodes"), &NoiseGraph::disconnect_nodes);

	ClassDB::bind_method(D_METHOD("add_node", "node", "position", "id"), &NoiseGraph::add_node);
	ClassDB::bind_method(D_METHOD("remove_node", "id"), &NoiseGraph::remove_node);
	ClassDB::bind_method(D_METHOD("get_node", "id"), &NoiseGraph::get_node);

	//ClassDB::bind_method(D_METHOD("_notify_changed"), &VisualAccidentalNoiseNodeComponent::_notify_changed); // TODO

	ADD_PROPERTY(PropertyInfo(Variant::STRING, "name"), "set_graph_name", "get_graph_name");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2, "graph_offset", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR), "set_graph_offset", "get_graph_offset");
}

bool NoiseGraph::_set(const StringName &p_name, const Variant &p_value) {
	String name = p_name;
	if (name.begins_with("nodes/")) {

		String index = name.get_slicec('/', 1);
		if (index == "connections") {

			Vector<int> conns = p_value;
			if (conns.size() % 4 == 0) {
				for (int i = 0; i < conns.size(); i += 4) {
					connect_nodes(conns[i + 0], conns[i + 1], conns[i + 2], conns[i + 3]);
				}
			}
			return true;
		}

		int id = index.to_int();
		String what = name.get_slicec('/', 2);

		if (what == "node") {
			add_node(p_value, Vector2(), id);
			return true;
		} else if (what == "position") {
			set_node_position(id, p_value);
			return true;
		}
	}
	return false;
}

bool NoiseGraph::_get(const StringName &p_name, Variant &r_ret) const {
	String name = p_name;
	if (name.begins_with("nodes/")) {

		String index = name.get_slicec('/', 1);
		if (index == "connections") {

			Vector<int> conns;
			for (const List<Connection>::Element *E = graph.connections.front(); E; E = E->next()) {
				conns.push_back(E->get().from_node);
				conns.push_back(E->get().from_port);
				conns.push_back(E->get().to_node);
				conns.push_back(E->get().to_port);
			}

			r_ret = conns;
			return true;
		}

		int id = index.to_int();
		String what = name.get_slicec('/', 2);

		if (what == "node") {
			r_ret = get_node(id);
			return true;
		} else if (what == "position") {
			r_ret = get_node_position(id);
			return true;
		}
	}
	return false;
}

void NoiseGraph::_get_property_list(List<PropertyInfo> *p_list) const {
	for (RBMap<int, Node>::Element *E = graph.nodes.front(); E; E = E->next()) {

		String prop_name = "nodes/";
		prop_name += itos(E->key());

		if (E->key() != 0) {

			p_list->push_back(PropertyInfo(Variant::OBJECT, prop_name + "/node", PROPERTY_HINT_RESOURCE_TYPE, "VisualNoiseNode", PROPERTY_USAGE_NO_EDITOR | PROPERTY_USAGE_DO_NOT_SHARE_ON_DUPLICATE));
		}
		p_list->push_back(PropertyInfo(Variant::VECTOR2, prop_name + "/position", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR));
	}
	p_list->push_back(PropertyInfo(Variant::PACKED_INT32_ARRAY, "nodes/connections", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR));
}



////////////// Output

String VisualNoiseNodeOutput::get_caption() const {
	return "Output";
}

int VisualNoiseNodeOutput::get_input_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeOutput::get_input_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeOutput::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "output";
		default:
			return "";
	}
}

int VisualNoiseNodeOutput::get_output_port_count() const {
	return 0;
}

VisualNoiseNode::PortType VisualNoiseNodeOutput::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeOutput::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeOutput::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

void VisualNoiseNodeOutput::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {

	Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data, false);
	
	set_output_value(a);
}

void VisualNoiseNodeOutput::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	if (p_nodes[0].is_null()) {
		min_value = 0;
		max_value = 0;
	} else {
		min_value = p_nodes[0]->get_min_value();
		max_value = p_nodes[0]->get_max_value();
	}
}

bool VisualNoiseNodeOutput::show_output_preview() const {
	return false;
}


void VisualNoiseNodeOutput::_bind_methods() {
}

////////////// Input

String VisualNoiseNodeInput::get_caption() const {
	return "Input";
}

int VisualNoiseNodeInput::get_input_port_count() const {
	return 0;
}

VisualNoiseNode::PortType VisualNoiseNodeInput::get_input_port_type(int p_port) const {
	return port_type;
}

String VisualNoiseNodeInput::get_input_port_name(int p_port) const {
	return "";
}

int VisualNoiseNodeInput::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeInput::get_output_port_type(int p_port) const {
	return port_type;
}

String VisualNoiseNodeInput::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeInput::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

void VisualNoiseNodeInput::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	Variant output;
	set_output_value(output);
}

void VisualNoiseNodeInput::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	// TODO
}

bool VisualNoiseNodeInput::show_output_preview() const {
	return false;
}

void VisualNoiseNodeInput::_bind_methods() {
}
