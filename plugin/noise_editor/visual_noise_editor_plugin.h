// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "editor/editor_node.h"
#include "editor/editor_plugin.h"
#include "scene/gui/button.h"
#include "scene/gui/graph_edit.h"
#include <vector>

#include <noise_3d_preview.h>

class SubViewportContainer;

class NoiseGraph;
class VisualNoiseNode;

class VisualNoiseEditor : public VBoxContainer {

	GDCLASS(VisualNoiseEditor, VBoxContainer);

	friend class VisualNoiseEditorPlugin;

	Ref<NoiseGraph> noise_graph;

	UndoRedo *undo_redo;

	int to_node;
	int to_slot;
	int from_node;
	int from_slot;

	Point2 saved_node_pos;
	bool saved_node_pos_dirty;

	// Drag
	struct DragOp {
		int node;
		Vector2 from;
		Vector2 to;
	};
	List<DragOp> drag_buffer;
	bool drag_dirty = false;
	void _node_dragged(const Vector2 &p_from, const Vector2 &p_to, int p_node);
	void _nodes_dragged();

	Variant get_drag_data_fw(const Point2 &p_point, Control *p_from);
	bool can_drop_data_fw(const Point2 &p_point, const Variant &p_data, Control *p_from) const;
	void drop_data_fw(const Point2 &p_point, const Variant &p_data, Control *p_from);

	// Property editor
	CustomPropertyEditor *property_editor;
	int editing_node;
	int editing_port;

	struct PortInfo {
		Button *property_preview = nullptr;
	};
	RBMap<int, std::vector<PortInfo>> ports_info;

	void _edit_port_default_input(Object *p_button, int p_node, int p_port);
	void _port_edited();
	void set_default_input_value(const int p_node, const int p_port, const Variant &p_value);
	
	// Main container
	HSplitContainer *main_box;
	GraphEdit *graph;
	Button *add_node_button;
	Label *error_text;

	// 3D preview
	
	Button *preview_noise;
	Noise3dPreview *noise3d_preview;

	// Add node dialog
	ConfirmationDialog *members_dialog;
	Tree *members;
	LineEdit *node_filter;
	RichTextLabel *node_desc;

	struct NodeOption {
		String name;
		String type;
		String category;
		String description;
		int func;

		NodeOption(const String &p_name, const String &p_category, const String &p_type, const String &p_description, const int p_func = -1) {
			name = p_name;
			type = p_type;
			category = p_category;
			description = p_description;
			func = p_func;
		}
	};

	std::vector<NodeOption> node_options;

	// Context menu
	PopupMenu *popup_menu;
	enum NodeMenuOptions {
		ADD,
		COPY,
		PASTE,
		DELETE,
		DUPLICATE,
	};
	Vector2 menu_point;
	void _node_menu_id_pressed(int p_idx);
	MenuButton *tools;
	enum ToolsMenuOptions {
		EXPAND_ALL,
		COLLAPSE_ALL
	};
	void _tools_menu_option(int p_idx);

	void _member_filter_changed(const String &p_text);
	void _sbox_input(const Ref<InputEvent> &p_ie);

	void _member_selected();
	void _member_unselected();
	void _member_create();
	void _member_cancel();

	void _update_graph();
	void _update_options_menu();

	VisualNoiseNode *_add_node_request(int p_idx);

	static VisualNoiseEditor *singleton;

	void _connection_request(const String &p_from, int p_from_index, const String &p_to, int p_to_index);
	void _disconnection_request(const String &p_from, int p_from_index, const String &p_to, int p_to_index);
	
	void _scroll_changed(const Vector2 &p_scroll);
	void _node_selected(Object *p_node);

	void set_node_position(int p_id, const Vector2 &p_position);

	Vector2 selection_center;
	List<int> copy_nodes_buffer;

	void _dup_copy_nodes();
	void _dup_paste_nodes(const  Vector2 &p_offset);
	
	void _duplicate_nodes();
	void _clear_buffer();
	void _copy_nodes();
	void _paste_nodes(bool p_use_custom_position = false, const Vector2 &p_custom_position = Vector2());
	
	void _delete_nodes(const List<int> &p_nodes);
	void _delete_node_request(int p_node);
	void _delete_nodes_request();
	
	void _graph_gui_input(const Ref<InputEvent> &p_event);

	void _connection_to_empty(const String &p_from, int p_from_slot, const Vector2 &p_release_position);
	void _connection_from_empty(const String &p_to, int p_to_slot, const Vector2 &p_release_position);


	void _show_members_dialog(bool at_mouse_pos);
	void _show_3D_noise_preview();
	void _evaluate_nodes();
	void _update_3d_preview();

	Control *create_editor(Ref<VisualNoiseNode> p_node);
	void _property_changed(const String &p_property, const Variant &p_value, const String &p_field, bool p_changing, Ref<VisualNoiseNode> p_node, Object *p_prop);
	void _update_property(Object *p_prop);

protected:
	void _notification(int p_what);
	static void _bind_methods();

public:
	void edit(const Ref<NoiseGraph> &p_graph);

	void add_node(int p_id);
	void remove_node(int p_id);

	void connect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port);
	void disconnect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port);

	static VisualNoiseEditor *get_singleton() { return singleton; }

	VisualNoiseEditor();
};

class VisualNoiseEditorPlugin : public EditorPlugin {

	GDCLASS(VisualNoiseEditorPlugin, EditorPlugin);

	VisualNoiseEditor *noise_editor = nullptr;
	Button *button = nullptr;

protected:
	virtual String get_name() const override { return "VisualNoiseEditorPlugin"; }
	bool has_main_screen() const override { return false; }
	virtual void edit(Object *p_object) override;
	virtual bool handles(Object *p_object) const override;
	virtual void make_visible(bool p_visible) override;

public:
	VisualNoiseEditorPlugin();
	~VisualNoiseEditorPlugin();
};


class VisualNoiseNodePortPreview : public VBoxContainer {
	GDCLASS(VisualNoiseNodePortPreview, VBoxContainer);

	Ref<VisualNoiseNode> node;

	TextureRect *texture_rect = nullptr;
	Label *label = nullptr;

protected:
	void _notification(int p_what);
	static void _bind_methods();

public:
	void update_preview();
	void setup(Ref<VisualNoiseNode> p_node);
	VisualNoiseNodePortPreview();
};