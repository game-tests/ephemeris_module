// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/io/resource.h"
#include <noisebuffer.h>
#include <vector>

struct GraphData {
	Rect2i rect;
	int height = 0;
	int seed = 0;
	int step = 1;
};

// VisualNoiseNode

class VisualNoiseNode : public Resource {
    GDCLASS(VisualNoiseNode, Resource);

	RBMap<int, Variant> default_input_values;

	void _set_default_input_values(Array p_values);
	Array _get_default_input_values() const;


protected:
	static void _bind_methods();

	// Utility methods.
	real_t parse_scalar(const Variant &p_v) const;
	Ref<NoiseBuffer> parse_noise(const Variant &p_v, const GraphData &p_graph_data, const bool p_copy = true) const;

	void set_output_value(const Variant &p_val);

	Variant output;
	bool dirty = false;
	real_t min_value = 0;
	real_t max_value = 0;

public:
	enum PortType {
		PORT_TYPE_SCALAR,
		PORT_TYPE_NOISE,
	};

	// Title of the GraphNode.
	virtual String get_caption() const = 0;

	virtual int get_input_port_count() const = 0;
	virtual PortType get_input_port_type(int p_port) const = 0;
	virtual String get_input_port_name(int p_port) const = 0;

	virtual int get_output_port_count() const = 0;
	virtual PortType get_output_port_type(int p_port) const = 0;
	virtual String get_output_port_name(int p_port) const = 0;

	virtual Vector<StringName> get_editable_properties() const = 0;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) = 0;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) = 0;

	virtual bool is_dirty() const;
	virtual void set_dirty(const bool p_dirty);

	void set_default_input_value(int p_port, const Variant &p_value);
	Variant get_default_input_value(int p_port) const;

	real_t get_min_value() const;
	real_t get_max_value() const;

	Variant get_output_value() const;
	virtual bool show_output_preview() const;
};

VARIANT_ENUM_CAST(VisualNoiseNode::PortType);


// NoiseGraph

class NoiseGraph : public VisualNoiseNode {
	GDCLASS(NoiseGraph, VisualNoiseNode);
	OBJ_SAVE_TYPE(NoiseGraph);
    RES_BASE_EXTENSION("noise");


public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

    virtual Ref<NoiseBuffer> generate_noise(const Rect2i &p_rect, const int p_height, const int p_step, const int p_seed);
	void evaluate_nodes(const Rect2i &p_rect, const int p_height, const int p_step, const int p_seed);
	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	void propagate_dirty_forward(const int p_id);
	void propagate_dirty_backwards(const int p_id);
	void set_editor_dirty(const bool p_dirty);
	void set_dirty(const bool p_dirty) override;

	struct Connection {
		int from_node;
		int from_port;
		int to_node;
		int to_port;
	};

	String get_graph_name() const;
	void set_graph_name(const String &p_name);

	void set_graph_offset(const Vector2 &p_offset);
	Vector2 get_graph_offset() const;

	Vector2 get_node_position(int p_id) const;
	void set_node_position(int p_id, const Vector2 &p_pos);

	void add_node(const Ref<VisualNoiseNode> &p_node, const Vector2 &p_position, int p_id);
	void remove_node(int p_id);

	bool has_node(int p_id) const;
	int get_valid_node_id() const;

	Vector<int> get_node_list() const;

	Ref<VisualNoiseNode> get_node(int p_id) const;

	bool is_nodes_connected_relatively(int p_node, int p_target) const;
	bool can_connect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port) const;
	Error connect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port);
	void disconnect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port);

	void get_node_connections(List<Connection> *r_connections) const;

	bool is_node_input_connected(const int p_node, const int p_port) const;

	NoiseGraph();

protected:
	static void _bind_methods();

	bool _set(const StringName &p_name, const Variant &p_value);
	bool _get(const StringName &p_name, Variant &r_ret) const;
	void _get_property_list(List<PropertyInfo> *p_list) const;

private:
	String name;

	struct Node {
		Ref<VisualNoiseNode> node;
		Vector2 position;
		List<int> prev_connected_nodes;

		Node() {}
		Node(const Ref<VisualNoiseNode> &p_node, const Vector2 &p_position) : node(p_node), position(p_position) {}
	};

	struct PortInfo {
		bool port_connected = false;
	};

	struct Graph {
		RBMap<int, Node> nodes;
		List<Connection> connections;
		RBMap<int, std::vector<PortInfo>> ports_info;
	};

	Graph graph;

	union ConnectionKey {

		struct {
			uint64_t node : 32;
			uint64_t port : 32;
		};
		uint64_t key;
		bool operator<(const ConnectionKey &p_key) const {
			return key < p_key.key;
		}
	};

	using OutputConnections = VMap<ConnectionKey, List<const List<Connection>::Element *>>;
	using InputConnections = VMap<ConnectionKey, const List<Connection>::Element *>;

	OutputConnections build_forward_port_map();
	InputConnections build_backwards_port_map();
	void _propagate_dirty_forward(const int p_id, const OutputConnections &p_con);
	void _propagate_dirty_backwards(const int p_id, const InputConnections &p_con);
	void _evaluate_nodes(const int p_id, Ref<VisualNoiseNode> p_node, const GraphData &p_graph_data, const InputConnections &p_con);

	InputConnections connection_cache;
	bool cache_dirty = true;

	Vector2 graph_offset;

	// TODO register input nodes
	List<VisualNoiseNode*> input_nodes;

};


////////////// Output

class VisualNoiseNodeOutput : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeOutput, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	virtual bool show_output_preview() const override;

private:
	Ref<NoiseBuffer> output;
};


////////////// Input

class VisualNoiseNodeInput : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeInput, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	virtual bool show_output_preview() const override;

private:
	Variant input;
	PortType port_type = PORT_TYPE_SCALAR;
};