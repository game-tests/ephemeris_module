// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "noise_graph.h"
#include <fastnoisegenerator.h>

////////////// Noise Generator

class VisualNoiseNodeNoiseGenerator : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeNoiseGenerator, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	enum NoiseType {
		TYPE_SIMPLEX = FastNoiseLite::NoiseType_OpenSimplex2,
		TYPE_SIMPLEX_S = FastNoiseLite::NoiseType_OpenSimplex2S,
		TYPE_PERLIN = FastNoiseLite::NoiseType_Perlin,
		TYPE_CUBIC = FastNoiseLite::NoiseType_ValueCubic,
		TYPE_VALUE = FastNoiseLite::NoiseType_Value,
		TYPE_WHITE_NOISE = FastNoiseLite::NoiseType_White,
	};

	void set_noise_type(NoiseType p_type);
	NoiseType get_noise_type() const;

protected:
	NoiseType noise_type = TYPE_SIMPLEX;

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeNoiseGenerator();

private:
	Ref<FastNoiseGenerator> generator;
};

VARIANT_ENUM_CAST(VisualNoiseNodeNoiseGenerator::NoiseType);

////////////// Fractal Noise Generator

class VisualNoiseNodeFractalGenerator : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeFractalGenerator, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	enum NoiseType {
		TYPE_SIMPLEX = FastNoiseLite::NoiseType_OpenSimplex2,
		TYPE_SIMPLEX_S = FastNoiseLite::NoiseType_OpenSimplex2S,
		TYPE_PERLIN = FastNoiseLite::NoiseType_Perlin,
		TYPE_CUBIC = FastNoiseLite::NoiseType_ValueCubic,
		TYPE_VALUE = FastNoiseLite::NoiseType_Value,
	};

	void set_noise_type(NoiseType p_type);
	NoiseType get_noise_type() const;

	enum FractalType {
		FRACTAL_FBM = FastNoiseLite::FractalType_FBm,
		FRACTAL_RIDGED = FastNoiseLite::FractalType_Ridged,
		FRACTAL_PINGPONG = FastNoiseLite::FractalType_PingPong,
	};

	void set_fractal_type(FractalType p_type);
	FractalType get_fractal_type() const;

protected:
	NoiseType noise_type = TYPE_SIMPLEX;
	FractalType fractal_type = FRACTAL_FBM;

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeFractalGenerator();

private:
	Ref<FastNoiseGenerator> generator;
};

VARIANT_ENUM_CAST(VisualNoiseNodeFractalGenerator::NoiseType);
VARIANT_ENUM_CAST(VisualNoiseNodeFractalGenerator::FractalType);


////////////// Cell Noise Generator

class VisualNoiseNodeCellNoiseGenerator : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeCellNoiseGenerator, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	enum DistanceFunction {
		DISTANCE_EUCLIDEAN = FastNoiseLite::CellularDistanceFunction_Euclidean,
		DISTANCE_MANHATTAN = FastNoiseLite::CellularDistanceFunction_Manhattan,
		DISTANCE_NATURAL = FastNoiseLite::CellularDistanceFunction_EuclideanSq,
	};

	enum CellularReturnType {
		RETURN_CELL_VALUE = FastNoiseLite::CellularReturnType_CellValue,
		RETURN_DISTANCE = FastNoiseLite::CellularReturnType_Distance,
		RETURN_DISTANCE2 = FastNoiseLite::CellularReturnType_Distance2,
		RETURN_DISTANCE2_ADD = FastNoiseLite::CellularReturnType_Distance2Add,
		RETURN_DISTANCE2_SUB = FastNoiseLite::CellularReturnType_Distance2Sub,
		RETURN_DISTANCE2_MUL = FastNoiseLite::CellularReturnType_Distance2Mul,
		RETURN_DISTANCE2_DIV = FastNoiseLite::CellularReturnType_Distance2Div
	};

	enum FractalType {
		FRACTAL_FBM = FastNoiseLite::FractalType_FBm,
		FRACTAL_RIDGED = FastNoiseLite::FractalType_Ridged,
		FRACTAL_PINGPONG = FastNoiseLite::FractalType_PingPong,
	};

	void set_distance_function(DistanceFunction p_dist);
	DistanceFunction get_distance_function() const;

	void set_cellular_return_type(CellularReturnType p_ret);
	CellularReturnType get_cellular_return_type() const;

	void set_fractal_type(FractalType p_type);
	FractalType get_fractal_type() const;

	void set_fractal_enabled(const bool p_enable);
	bool get_fractal_enabled() const;

protected:
	DistanceFunction distance_function = DISTANCE_EUCLIDEAN;
	CellularReturnType return_type = RETURN_CELL_VALUE;

	bool enable_fractal = false;
	FractalType fractal_type;

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeCellNoiseGenerator();

private:
	Ref<FastNoiseGenerator> generator;
};

VARIANT_ENUM_CAST(VisualNoiseNodeCellNoiseGenerator::DistanceFunction);
VARIANT_ENUM_CAST(VisualNoiseNodeCellNoiseGenerator::CellularReturnType);
VARIANT_ENUM_CAST(VisualNoiseNodeCellNoiseGenerator::FractalType);

////////////// Noise Gradient

//class VisualNoiseNodeGradient : public VisualNoiseNode {
//	GDCLASS(VisualNoiseNodeGradient, VisualNoiseNode);
//
//public:
//	virtual String get_caption() const override;
//
//	virtual int get_input_port_count() const override;
//	virtual PortType get_input_port_type(int p_port) const override;
//	virtual String get_input_port_name(int p_port) const override;
//
//	virtual int get_output_port_count() const override;
//	virtual PortType get_output_port_type(int p_port) const override;
//	virtual String get_output_port_name(int p_port) const override;
//
//	virtual Vector<StringName> get_editable_properties() const override;
//};

////////////// Abs

class VisualNoiseNodeAbs : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeAbs, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeAbs();
};

////////////// Scalar Operator

class VisualNoiseNodeScalarOperator : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeScalarOperator, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	enum Operator {
		OP_ADD,
		OP_SUB,
		OP_MUL,
		OP_DIV,
		OP_MOD,
		OP_POW,
		OP_MAX,
		OP_MIN,
	};

	void set_operator(Operator p_op);
	Operator get_operator() const;

protected:
	Operator op;

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeScalarOperator();
};

VARIANT_ENUM_CAST(VisualNoiseNodeScalarOperator::Operator);

////////////// Operator

class VisualNoiseNodeOperator : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeOperator, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	enum Operator {
		OP_ADD,
		OP_SUB,
		OP_MUL,
		OP_POW,
		OP_MAX,
		OP_MIN,
	};

	void set_operator(Operator p_op);
	Operator get_operator() const;

protected:
	Operator op;

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeOperator();
};

VARIANT_ENUM_CAST(VisualNoiseNodeOperator::Operator);

////////////// Blend

class VisualNoiseNodeBlend : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeBlend, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeBlend();
};

////////////// Clamp

class VisualNoiseNodeClamp : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeClamp, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeClamp();
};

////////////// Invert

class VisualNoiseNodeInvert : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeInvert, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeInvert();
};

////////////// Scalar Constant

class VisualNoiseNodeScalarConstant : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeScalarConstant, VisualNoiseNode);

protected:
	static void _bind_methods();

	void set_constant(const real_t p_constant);
	real_t get_constant() const;

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	virtual bool show_output_preview() const override;

	VisualNoiseNodeScalarConstant();
};

////////////// Threshold

class VisualNoiseNodeThreshold : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeThreshold, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	enum ThresholdType {
		BINARY,
		BINARY_INVERTED,
		TRUNCATE,
		TO_ZERO,
		TO_ZERO_INVERTED,
	};

	void set_threshold_type(ThresholdType p_type);
	ThresholdType get_threshold_type() const;

protected:
	ThresholdType type;
public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeThreshold();
};

VARIANT_ENUM_CAST(VisualNoiseNodeThreshold::ThresholdType);

////////////// Normalize

class VisualNoiseNodeNormalize : public VisualNoiseNode {
	GDCLASS(VisualNoiseNodeNormalize, VisualNoiseNode);

protected:
	static void _bind_methods();

public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

	virtual void evaluate(const Array &p_inputs, const GraphData &p_graph_data) override;
	virtual void update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) override;

	VisualNoiseNodeNormalize();
};