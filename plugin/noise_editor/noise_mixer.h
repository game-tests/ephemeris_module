// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "noise_graph.h"
#include <matrix_2d.h>
#include <influence_curve.h>


// NoiseGraph

class NoiseMixer : public NoiseGraph {
	GDCLASS(NoiseMixer, NoiseGraph);
	OBJ_SAVE_TYPE(NoiseMixer);
    RES_BASE_EXTENSION("noise");


public:
	virtual String get_caption() const override;

	virtual int get_input_port_count() const override;
	virtual PortType get_input_port_type(int p_port) const override;
	virtual String get_input_port_name(int p_port) const override;

	virtual int get_output_port_count() const override;
	virtual PortType get_output_port_type(int p_port) const override;
	virtual String get_output_port_name(int p_port) const override;

	virtual Vector<StringName> get_editable_properties() const override;

    virtual Ref<NoiseBuffer> generate_noise(const Rect2i &p_rect, const int p_height, const int p_step, const int p_seed) override;

	void set_height_range(const int p_min, const int p_max);
	void add_graph(Ref<NoiseGraph> &p_noise, Ref<InfluenceCurve> &p_infl_curve, const Matrix<float> *p_ginfluence, const Matrix<float> *p_influence, const float p_threshold);

	real_t get_target_threshold() const;

	NoiseMixer();

	private:

	struct GraphInfo {
		Ref<InfluenceCurve> infl_curve;
		Ref<NoiseGraph> noise;
		const Matrix<float> *influence;
		const Matrix<float> *g_influence;
		float threshold;
	};

	std::vector<GraphInfo> data;
	int min_height = -64;
	int max_height = 64;

};