// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "noise_editor_nodes.h"


////////////// Noise Generator

void VisualNoiseNodeNoiseGenerator::set_noise_type(NoiseType p_type) {
	noise_type = p_type;
}

VisualNoiseNodeNoiseGenerator::NoiseType VisualNoiseNodeNoiseGenerator::get_noise_type() const {
	return noise_type;
}

String VisualNoiseNodeNoiseGenerator::get_caption() const {
	return "Noise Generator";
}

int VisualNoiseNodeNoiseGenerator::get_input_port_count() const {
	return 2;
}

VisualNoiseNode::PortType VisualNoiseNodeNoiseGenerator::get_input_port_type(int p_port) const {
	return PORT_TYPE_SCALAR;
}

String VisualNoiseNodeNoiseGenerator::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "seed offset";
		case 1:
			return "frequency";
		default:
			return "";
	}
}

int VisualNoiseNodeNoiseGenerator::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeNoiseGenerator::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeNoiseGenerator::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeNoiseGenerator::get_editable_properties() const {
	Vector<StringName> props;
	props.push_back("noise_type");
	return props;
}

void VisualNoiseNodeNoiseGenerator::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	real_t seed_offset = parse_scalar(p_inputs[0]);
	real_t frequency = parse_scalar(p_inputs[1]);

	generator->set_noise_type((FastNoiseGenerator::NoiseType)noise_type);
	generator->set_seed(p_graph_data.seed + seed_offset);
	generator->set_frequency(frequency);
	
	set_output_value(generator->get_noise_buffer(p_graph_data.rect, p_graph_data.height, p_graph_data.step));
}

void VisualNoiseNodeNoiseGenerator::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	min_value = -1.0;
	max_value = 1.0;
}

void VisualNoiseNodeNoiseGenerator::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_noise_type", "type"), &VisualNoiseNodeNoiseGenerator::set_noise_type);
	ClassDB::bind_method(D_METHOD("get_noise_type"), &VisualNoiseNodeNoiseGenerator::get_noise_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "noise_type", PROPERTY_HINT_ENUM, "SIMPLEX,SIMPLEX_S,PERLIN,CUBIC,VALUE,WHITE"), "set_noise_type", "get_noise_type");
}

VisualNoiseNodeNoiseGenerator::VisualNoiseNodeNoiseGenerator() {
	generator.instantiate();
	set_default_input_value(0, 0);
	set_default_input_value(1, generator->get_frequency());
}

////////////// Fractal Noise Generator

void VisualNoiseNodeFractalGenerator::set_noise_type(NoiseType p_type) {
	noise_type = p_type;
}

VisualNoiseNodeFractalGenerator::NoiseType VisualNoiseNodeFractalGenerator::get_noise_type() const {
	return noise_type;
}

void VisualNoiseNodeFractalGenerator::set_fractal_type(FractalType p_type) {
	fractal_type = p_type;
}

VisualNoiseNodeFractalGenerator::FractalType VisualNoiseNodeFractalGenerator::get_fractal_type() const {
	return fractal_type;
}

String VisualNoiseNodeFractalGenerator::get_caption() const {
	return "Fractal Generator";
}

int VisualNoiseNodeFractalGenerator::get_input_port_count() const {
	return 5;
}

VisualNoiseNode::PortType VisualNoiseNodeFractalGenerator::get_input_port_type(int p_port) const {
	return PORT_TYPE_SCALAR;
}

String VisualNoiseNodeFractalGenerator::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "seed offset";
		case 1:
			return "frequency";
		case 2:
			return "octaves";
		case 3:
			return "lacunarity";
		case 4:
			return "fractal gain";
		default:
			return "";
	}
}

int VisualNoiseNodeFractalGenerator::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeFractalGenerator::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeFractalGenerator::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeFractalGenerator::get_editable_properties() const {
	Vector<StringName> props;
	props.push_back("noise_type");
	props.push_back("fractal_type");
	return props;
}

void VisualNoiseNodeFractalGenerator::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	real_t seed_offset = parse_scalar(p_inputs[0]);
	real_t frequency = parse_scalar(p_inputs[1]);
	real_t octaves = parse_scalar(p_inputs[2]);
	real_t lacunarity = parse_scalar(p_inputs[3]);
	real_t fractal_gain = parse_scalar(p_inputs[4]);

	generator->set_fractal_type((FastNoiseGenerator::FractalType)fractal_type);
	generator->set_noise_type((FastNoiseGenerator::NoiseType)noise_type);
	generator->set_seed(p_graph_data.seed + seed_offset);
	generator->set_frequency(frequency);
	generator->set_fractal_octaves(octaves);
	generator->set_fractal_lacunarity(lacunarity);
	generator->set_fractal_gain(fractal_gain);
	
	set_output_value(generator->get_noise_buffer(p_graph_data.rect, p_graph_data.height, p_graph_data.step));
}

void VisualNoiseNodeFractalGenerator::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	min_value = -1.0;
	max_value = 1.0;
}

void VisualNoiseNodeFractalGenerator::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_noise_type", "type"), &VisualNoiseNodeFractalGenerator::set_noise_type);
	ClassDB::bind_method(D_METHOD("get_noise_type"), &VisualNoiseNodeFractalGenerator::get_noise_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "noise_type", PROPERTY_HINT_ENUM, "SIMPLEX,SIMPLEX_S,PERLIN,CUBIC,VALUE"), "set_noise_type", "get_noise_type");

	ClassDB::bind_method(D_METHOD("set_fractal_type", "type"), &VisualNoiseNodeFractalGenerator::set_fractal_type);
	ClassDB::bind_method(D_METHOD("get_fractal_type"), &VisualNoiseNodeFractalGenerator::get_fractal_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_type", PROPERTY_HINT_ENUM, "FBM, RIDGED, PINGPONG"), "set_fractal_type", "get_fractal_type");
}

VisualNoiseNodeFractalGenerator::VisualNoiseNodeFractalGenerator() {
	generator.instantiate();
	set_default_input_value(0, 0);
	set_default_input_value(1, generator->get_frequency());
	set_default_input_value(2, generator->get_fractal_octaves());
	set_default_input_value(3, generator->get_fractal_lacunarity());
	set_default_input_value(4, generator->get_fractal_gain());
}

////////////// Cell Generator

void VisualNoiseNodeCellNoiseGenerator::set_distance_function(DistanceFunction p_dist) {
	distance_function = p_dist;
}

VisualNoiseNodeCellNoiseGenerator::DistanceFunction VisualNoiseNodeCellNoiseGenerator::get_distance_function() const {
	return distance_function;
}

void VisualNoiseNodeCellNoiseGenerator::set_cellular_return_type(CellularReturnType p_ret) {
	return_type = p_ret;
}

VisualNoiseNodeCellNoiseGenerator::CellularReturnType VisualNoiseNodeCellNoiseGenerator::get_cellular_return_type() const {
	return return_type;
}

void VisualNoiseNodeCellNoiseGenerator::set_fractal_type(FractalType p_type) {
	fractal_type = p_type;
}

VisualNoiseNodeCellNoiseGenerator::FractalType VisualNoiseNodeCellNoiseGenerator::get_fractal_type() const {
	return fractal_type;
}

void VisualNoiseNodeCellNoiseGenerator::set_fractal_enabled(const bool p_enable) {
	enable_fractal = p_enable;
}

bool VisualNoiseNodeCellNoiseGenerator::get_fractal_enabled() const {
	return enable_fractal;
}

String VisualNoiseNodeCellNoiseGenerator::get_caption() const {
	return enable_fractal ? "Cell Fractal Generator" : "Cell Generator";
}

int VisualNoiseNodeCellNoiseGenerator::get_input_port_count() const {
	return enable_fractal ?  6 : 3;
}

VisualNoiseNode::PortType VisualNoiseNodeCellNoiseGenerator::get_input_port_type(int p_port) const {
	return PORT_TYPE_SCALAR;
}

String VisualNoiseNodeCellNoiseGenerator::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "seed offset";
		case 1:
			return "frequency";
		case 2:
			return "jitter";
		case 3:
			return "octaves";
		case 4:
			return "lacunarity";
		case 5:
			return "fractal gain";
		default:
			return "";
	}
}

int VisualNoiseNodeCellNoiseGenerator::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeCellNoiseGenerator::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeCellNoiseGenerator::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeCellNoiseGenerator::get_editable_properties() const {
	Vector<StringName> props;
	props.push_back("return_type");
	props.push_back("distance_function");
	if (enable_fractal) {
		props.push_back("fractal_type");
	}
	return props;
}

void VisualNoiseNodeCellNoiseGenerator::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	real_t seed_offset = parse_scalar(p_inputs[0]);
	real_t frequency = parse_scalar(p_inputs[1]);
	real_t jitter = parse_scalar(p_inputs[2]);
	if (enable_fractal) {
		real_t octaves = parse_scalar(p_inputs[2]);
		real_t lacunarity = parse_scalar(p_inputs[3]);
		real_t fractal_gain = parse_scalar(p_inputs[4]);

		generator->set_fractal_type((FastNoiseGenerator::FractalType)fractal_type);
		generator->set_fractal_octaves(octaves);
		generator->set_fractal_lacunarity(lacunarity);
		generator->set_fractal_gain(fractal_gain);
	}

	generator->set_noise_type(FastNoiseGenerator::TYPE_CELLULAR);
	generator->set_seed(p_graph_data.seed + seed_offset);
	generator->set_frequency(frequency);
	generator->set_cellular_jitter(jitter);
	generator->set_cellular_distance_function((FastNoiseGenerator::CellularDistanceFunction)distance_function);
	generator->set_cellular_return_type((FastNoiseGenerator::CellularReturnType)return_type);

	set_output_value(generator->get_noise_buffer(p_graph_data.rect, p_graph_data.height, p_graph_data.step));
}

void VisualNoiseNodeCellNoiseGenerator::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	min_value = -1.0;
	max_value = 1.0;
}

void VisualNoiseNodeCellNoiseGenerator::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_cellular_return_type", "ret"), &VisualNoiseNodeCellNoiseGenerator::set_cellular_return_type);
	ClassDB::bind_method(D_METHOD("get_cellular_return_type"), &VisualNoiseNodeCellNoiseGenerator::get_cellular_return_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "return_type", PROPERTY_HINT_ENUM, "CELL_VALUE, DISTANCE, DISTANCE2, DISTANCE2_ADD, DISTANCE2_SUB, DISTANCE2_MUL, DISTANCE2_DIV"), "set_cellular_return_type", "get_cellular_return_type");

	ClassDB::bind_method(D_METHOD("set_distance_function", "dist"), &VisualNoiseNodeCellNoiseGenerator::set_distance_function);
	ClassDB::bind_method(D_METHOD("get_distance_function"), &VisualNoiseNodeCellNoiseGenerator::get_distance_function);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "distance_function", PROPERTY_HINT_ENUM, "DISTANCE_EUCLIDEAN, DISTANCE_MANHATTAN, DISTANCE_NATURAL"), "set_distance_function", "get_distance_function");

	ClassDB::bind_method(D_METHOD("set_fractal_type", "type"), &VisualNoiseNodeCellNoiseGenerator::set_fractal_type);
	ClassDB::bind_method(D_METHOD("get_fractal_type"), &VisualNoiseNodeCellNoiseGenerator::get_fractal_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_type", PROPERTY_HINT_ENUM, "FBM, RIDGED, PINGPONG"), "set_fractal_type", "get_fractal_type");

	ClassDB::bind_method(D_METHOD("set_fractal_enabled", "enabled"), &VisualNoiseNodeCellNoiseGenerator::set_fractal_enabled);
	ClassDB::bind_method(D_METHOD("get_fractal_enabled"), &VisualNoiseNodeCellNoiseGenerator::get_fractal_enabled);
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "fractal_enabled"), "set_fractal_enabled", "get_fractal_enabled");
}

VisualNoiseNodeCellNoiseGenerator::VisualNoiseNodeCellNoiseGenerator() {
	generator.instantiate();
	set_default_input_value(0, 0);
	set_default_input_value(1, generator->get_frequency());
	set_default_input_value(2, generator->get_cellular_jitter());
	set_default_input_value(3, generator->get_fractal_octaves());
	set_default_input_value(4, generator->get_fractal_lacunarity());
	set_default_input_value(5, generator->get_fractal_gain());
}


////////////// Abs

String VisualNoiseNodeAbs::get_caption() const {
	return "Abs";
}

int VisualNoiseNodeAbs::get_input_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeAbs::get_input_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeAbs::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "input";
		default:
			return "";
	}
}

int VisualNoiseNodeAbs::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeAbs::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeAbs::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeAbs::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

void VisualNoiseNodeAbs::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data);
	a->abs();
	set_output_value(a);
}

void VisualNoiseNodeAbs::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	if (p_nodes[0].is_null()) {
		min_value = 0;
		max_value = 0;
	} else {
		real_t val_a = ABS(p_nodes[0]->get_min_value());
		real_t val_b = ABS(p_nodes[0]->get_max_value());
		if (val_a < val_b) {
			min_value = val_a;
			max_value = val_b;
		} else {
			min_value = val_b;
			max_value = val_a;
		}
	}
}

void VisualNoiseNodeAbs::_bind_methods() {
}

VisualNoiseNodeAbs::VisualNoiseNodeAbs() {
}


////////////// Scalar Operator

void VisualNoiseNodeScalarOperator::set_operator(Operator p_op) {
	op = p_op;
}

VisualNoiseNodeScalarOperator::Operator VisualNoiseNodeScalarOperator::get_operator() const {
	return op;
}

String VisualNoiseNodeScalarOperator::get_caption() const {
	return "Scalar Operator";
}

int VisualNoiseNodeScalarOperator::get_input_port_count() const {
	return 2;
}

VisualNoiseNode::PortType VisualNoiseNodeScalarOperator::get_input_port_type(int p_port) const {
	return PORT_TYPE_SCALAR;
}

String VisualNoiseNodeScalarOperator::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "a";
		case 1:
			return "b";
		default:
			return "";
	}
}

int VisualNoiseNodeScalarOperator::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeScalarOperator::get_output_port_type(int p_port) const {
	return PORT_TYPE_SCALAR;
}

String VisualNoiseNodeScalarOperator::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeScalarOperator::get_editable_properties() const {
	Vector<StringName> props;
	props.push_back("operator");
	return props;
}

void VisualNoiseNodeScalarOperator::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	real_t a = parse_scalar(p_inputs[0]);
	real_t b = parse_scalar(p_inputs[1]);

	Variant out;

	switch (op) {
	case OP_ADD: {
		out = a + b;
	} break;
	case OP_SUB: {
		out = a - b;
	} break;
	case OP_MUL: {
		out = a * b;
	} break;
	case OP_DIV: {
		out = a / b;
	} break;
	case OP_MOD: {
		out = (int)a % (int)b;
	} break;
	case OP_POW: {
		out = Math::pow(a, b);
	} break;
	case OP_MAX: {
		out = MAX(a, b);
	} break;
	case OP_MIN: {
		out = MIN(a, b);
	} break;
	default:
		break;
	}

	set_output_value(out);
}

void VisualNoiseNodeScalarOperator::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {

	real_t a = parse_scalar(p_inputs[0]);
	real_t b = parse_scalar(p_inputs[1]);

	switch (op) {
	case OP_ADD: {
		min_value = a + b;
	} break;
	case OP_SUB: {
		min_value = a - b;
	} break;
	case OP_MUL: {
		min_value = a * b;
	} break;
	case OP_DIV: {
		min_value = a / b;
	} break;
	case OP_MOD: {
		min_value = (int)a % (int)b;
	} break;
	case OP_POW: {
		min_value = Math::pow(a, b);
	} break;
	case OP_MAX: {
		min_value = MAX(a, b);
	} break;
	case OP_MIN: {
		min_value = MIN(a, b);
	} break;
	default:
		break;
	}
	max_value = min_value;
}

void VisualNoiseNodeScalarOperator::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_operator", "op"), &VisualNoiseNodeScalarOperator::set_operator);
	ClassDB::bind_method(D_METHOD("get_operator"), &VisualNoiseNodeScalarOperator::get_operator);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "operator", PROPERTY_HINT_ENUM, "ADD, SUB, MUL, DIV, MOD, POW, MAX, MIN"), "set_operator", "get_operator");
}

VisualNoiseNodeScalarOperator::VisualNoiseNodeScalarOperator() {
	set_default_input_value(0, 0);
	set_default_input_value(1, 0);
}


////////////// Operator

void VisualNoiseNodeOperator::set_operator(Operator p_op) {
	op = p_op;
}

VisualNoiseNodeOperator::Operator VisualNoiseNodeOperator::get_operator() const {
	return op;
}

String VisualNoiseNodeOperator::get_caption() const {
	return "Operator";
}

int VisualNoiseNodeOperator::get_input_port_count() const {
	return 2;
}

VisualNoiseNode::PortType VisualNoiseNodeOperator::get_input_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeOperator::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "a";
		case 1:
			return "b";
		default:
			return "";
	}
}

int VisualNoiseNodeOperator::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeOperator::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeOperator::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeOperator::get_editable_properties() const {
	Vector<StringName> props;
	props.push_back("operator");
	return props;
}

void VisualNoiseNodeOperator::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {

	const bool a_is_noise = p_inputs[0].get_type() == Variant::OBJECT;
	const bool b_is_noise = p_inputs[1].get_type() == Variant::OBJECT;

	if (a_is_noise && b_is_noise) {
		Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data);
		Ref<NoiseBuffer> b = parse_noise(p_inputs[1], p_graph_data, false);

		switch (op) {
			case OP_ADD: {
				a->add(b);
			} break;
			case OP_SUB: {
				a->sub(b);
			} break;
			case OP_MUL: {
				a->multiply(b);
			} break;
			case OP_POW: {
				a->pow(b);
			} break;
			case OP_MAX: {
				a->max(b);
			} break;
			case OP_MIN: {
				a->min(b);
			} break;
			default:
				break;
		}
		set_output_value(a);
	} else if (!a_is_noise && !b_is_noise) {
		real_t a = parse_scalar(p_inputs[0]);
		real_t b = parse_scalar(p_inputs[1]);

		real_t out;

		switch (op) {
		case OP_ADD: {
			out = a + b;
		} break;
		case OP_SUB: {
			out = a - b;
		} break;
		case OP_MUL: {
			out = a * b;
		} break;
		case OP_POW: {
			out = Math::pow(a, b);
		} break;
		case OP_MAX: {
			out = MAX(a, b);
		} break;
		case OP_MIN: {
			out = MIN(a, b);
		} break;
		default:
			break;
		}
		Ref<NoiseBuffer> res;
		res.instantiate();
		res->_resize_no_memset(p_graph_data.rect.size);
		res->scalar_assign(out);

		set_output_value(res);
	} else {
		Ref<NoiseBuffer> a;
		real_t b;

		if (a_is_noise) {
			a = parse_noise(p_inputs[0], p_graph_data);
			b = parse_scalar(p_inputs[1]);

		} else {
			a = parse_noise(p_inputs[1], p_graph_data);
			b = parse_scalar(p_inputs[0]);
		}

		switch (op) {
			case OP_ADD: {
				a->scalar_add(b);
			} break;
			case OP_SUB: {
				a->scalar_sub(b);
			} break;
			case OP_MUL: {
				a->scalar_multiply(b);
			} break;
			case OP_POW: {
				a->scalar_pow(b);
			} break;
			case OP_MAX: {
				a->scalar_max(b);
			} break;
			case OP_MIN: {
				a->scalar_min(b);
			} break;
			default:
				break;
		}
		set_output_value(a);
	}
}

void VisualNoiseNodeOperator::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	real_t min_a;
	real_t max_a;
	real_t min_b;
	real_t max_b;
	
	if (p_nodes[0].is_null()) {
		min_a = 0;
		max_a = 0;
	} else {
		min_a = p_nodes[0]->get_min_value();
		max_a = p_nodes[0]->get_max_value();
	}

	if (p_nodes[1].is_null()) {
		min_b = 0;
		max_b = 0;
	} else {
		min_b = p_nodes[1]->get_min_value();
		max_b = p_nodes[1]->get_max_value();
	}

	switch (op) {
	case OP_ADD: {
		min_value = min_a + min_b;
		max_value = max_a + max_b;
	} break;
	case OP_SUB: {
		min_value = min_a - max_b;
		max_value = max_a - min_b;
	} break;
	case OP_MUL: {
		const real_t v1 = min_a * min_b;
		const real_t v2 = min_a * max_b;
		const real_t v3 = max_a * min_b;
		const real_t v4 = max_a * max_b;
		min_value = MIN(MIN(MIN(v1, v2), v3), v4);
		max_value = MAX(MAX(MAX(v1, v2), v3), v4);
	} break;
	case OP_POW: {
		min_value = Math::pow(min_a, min_b);
		max_value = Math::pow(max_a, max_b);
	} break;
	case OP_MAX: {
		min_value = MAX(min_a, min_b);
		max_value = MAX(max_a, max_b);
	} break;
	case OP_MIN: {
		min_value = MIN(min_a, min_b);
		max_value = MIN(max_a, max_b);
	} break;
	default:
		break;
	}
}

void VisualNoiseNodeOperator::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_operator", "op"), &VisualNoiseNodeOperator::set_operator);
	ClassDB::bind_method(D_METHOD("get_operator"), &VisualNoiseNodeOperator::get_operator);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "operator", PROPERTY_HINT_ENUM, "ADD, SUB, MUL, POW, MAX, MIN"), "set_operator", "get_operator");
}

VisualNoiseNodeOperator::VisualNoiseNodeOperator() {
}


////////////// Blend

String VisualNoiseNodeBlend::get_caption() const {
	return "Blend";
}

int VisualNoiseNodeBlend::get_input_port_count() const {
	return 3;
}

VisualNoiseNode::PortType VisualNoiseNodeBlend::get_input_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeBlend::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "a";
		case 1:
			return "b";
		case 2:
			return "control";
		default:
			return "";
	}
}

int VisualNoiseNodeBlend::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeBlend::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeBlend::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeBlend::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

void VisualNoiseNodeBlend::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data);
	Ref<NoiseBuffer> b = parse_noise(p_inputs[1], p_graph_data, false);
	Ref<NoiseBuffer> control = parse_noise(p_inputs[2], p_graph_data, false);

	a->blend(b, control);

	set_output_value(a);
}

void VisualNoiseNodeBlend::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	real_t min_a;
	real_t max_a;
	real_t min_b;
	real_t max_b;
	
	if (p_nodes[0].is_null()) {
		min_a = 0;
		max_a = 0;
	} else {
		min_a = p_nodes[0]->get_min_value();
		max_a = p_nodes[0]->get_max_value();
	}

	if (p_nodes[1].is_null()) {
		min_b = 0;
		max_b = 0;
	} else {
		min_b = p_nodes[1]->get_min_value();
		max_b = p_nodes[1]->get_max_value();
	}
	
	min_value = MIN(min_a, min_b);
	max_value = MAX(max_a, max_b);
}

void VisualNoiseNodeBlend::_bind_methods() {
}

VisualNoiseNodeBlend::VisualNoiseNodeBlend() {
}


////////////// Clamp

String VisualNoiseNodeClamp::get_caption() const {
	return "Clamp";
}

int VisualNoiseNodeClamp::get_input_port_count() const {
	return 3;
}

VisualNoiseNode::PortType VisualNoiseNodeClamp::get_input_port_type(int p_port) const {
	return p_port == 0 ? PORT_TYPE_NOISE : PORT_TYPE_SCALAR;
}

String VisualNoiseNodeClamp::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "input";
		case 1:
			return "min";
		case 2:
			return "max";
		default:
			return "";
	}
}

int VisualNoiseNodeClamp::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeClamp::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeClamp::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeClamp::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

void VisualNoiseNodeClamp::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data);
	real_t min = parse_scalar(p_inputs[1]);
	real_t max = parse_scalar(p_inputs[2]);

	a->clamp(min, max);

	set_output_value(a);
}

void VisualNoiseNodeClamp::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	real_t min_a;
	real_t max_a;
	
	if (p_nodes[0].is_null()) {
		min_a = 0;
		max_a = 0;
	} else {
		min_a = p_nodes[0]->get_min_value();
		max_a = p_nodes[0]->get_max_value();
	}

	real_t min = parse_scalar(p_inputs[1]);
	real_t max = parse_scalar(p_inputs[2]);

	min_value = MAX(min_a, min);
	max_value = MIN(max_a, max);
}

void VisualNoiseNodeClamp::_bind_methods() {
}

VisualNoiseNodeClamp::VisualNoiseNodeClamp() {
	set_default_input_value(1, 0);
	set_default_input_value(2, 1);
}


////////////// Invert

String VisualNoiseNodeInvert::get_caption() const {
	return "Invert";
}

int VisualNoiseNodeInvert::get_input_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeInvert::get_input_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeInvert::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "input";
		default:
			return "";
	}
}

int VisualNoiseNodeInvert::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeInvert::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeInvert::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeInvert::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

void VisualNoiseNodeInvert::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data);

	a->invert();

	set_output_value(a);
}

void VisualNoiseNodeInvert::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	real_t min_a;
	real_t max_a;
	
	if (p_nodes[0].is_null()) {
		min_a = 0;
		max_a = 0;
	} else {
		min_a = p_nodes[0]->get_min_value();
		max_a = p_nodes[0]->get_max_value();
	}

	min_value = -max_a;
	max_value = -min_a;
}

void VisualNoiseNodeInvert::_bind_methods() {
}

VisualNoiseNodeInvert::VisualNoiseNodeInvert() {
}


////////////// Scalar Constant

void VisualNoiseNodeScalarConstant::set_constant(const real_t p_constant) {
	set_output_value(p_constant);
}

real_t VisualNoiseNodeScalarConstant::get_constant() const {
	return get_output_value();
}

String VisualNoiseNodeScalarConstant::get_caption() const {
	return "Scalar Constant";
}

int VisualNoiseNodeScalarConstant::get_input_port_count() const {
	return 0;
}

VisualNoiseNode::PortType VisualNoiseNodeScalarConstant::get_input_port_type(int p_port) const {
	return PORT_TYPE_SCALAR;
}

String VisualNoiseNodeScalarConstant::get_input_port_name(int p_port) const {
	return "";
}

int VisualNoiseNodeScalarConstant::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeScalarConstant::get_output_port_type(int p_port) const {
	return PORT_TYPE_SCALAR;
}

String VisualNoiseNodeScalarConstant::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeScalarConstant::get_editable_properties() const {
	Vector<StringName> props;
	props.push_back("constant");
	return props;
}

void VisualNoiseNodeScalarConstant::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
}

void VisualNoiseNodeScalarConstant::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	min_value = get_constant();
	max_value = min_value;
}

bool VisualNoiseNodeScalarConstant::show_output_preview() const {
	return false;
}

void VisualNoiseNodeScalarConstant::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_constant", "constant"), &VisualNoiseNodeScalarConstant::set_constant);
	ClassDB::bind_method(D_METHOD("get_constant"), &VisualNoiseNodeScalarConstant::get_constant);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "constant"), "set_constant", "get_constant");
}

VisualNoiseNodeScalarConstant::VisualNoiseNodeScalarConstant() {
}


////////////// Threshold

void VisualNoiseNodeThreshold::set_threshold_type(ThresholdType p_type) {
	type = p_type;
}

VisualNoiseNodeThreshold::ThresholdType VisualNoiseNodeThreshold::get_threshold_type() const {
	return type;
}

String VisualNoiseNodeThreshold::get_caption() const {
	return "Threshold";
}

int VisualNoiseNodeThreshold::get_input_port_count() const {
	return 2;
}

VisualNoiseNode::PortType VisualNoiseNodeThreshold::get_input_port_type(int p_port) const {
	return p_port == 0? PORT_TYPE_NOISE : PORT_TYPE_SCALAR;
}

String VisualNoiseNodeThreshold::get_input_port_name(int p_port) const {
	switch (p_port) {
		case 0:
			return "input";
		case 1:
			return "threshold";
		default:
			return "";
	}
}

int VisualNoiseNodeThreshold::get_output_port_count() const {
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeThreshold::get_output_port_type(int p_port) const {
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeThreshold::get_output_port_name(int p_port) const {
	return "";
}

Vector<StringName> VisualNoiseNodeThreshold::get_editable_properties() const {
	Vector<StringName> props;
	props.push_back("threshold_type");
	return props;
}

void VisualNoiseNodeThreshold::evaluate(const Array &p_inputs, const GraphData &p_graph_data) {
	Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data);
	real_t threshold_val = parse_scalar(p_inputs[1]);

	a->threshold(threshold_val, (NoiseBuffer::ThresholdType)type);

	set_output_value(a);
}

void VisualNoiseNodeThreshold::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	real_t min_a;
	real_t max_a;
	
	if (p_nodes[0].is_null()) {
		min_a = 0;
		max_a = 0;
	} else {
		min_a = p_nodes[0]->get_min_value();
		max_a = p_nodes[0]->get_max_value();
	}

	real_t threshold_val = parse_scalar(p_inputs[1]);

	switch ((NoiseBuffer::ThresholdType)type) {
		case NoiseBuffer::THRESHOLD_TYPE_BINARY: {
			min_value = 0;
			max_value = 1;
		} break;
		case NoiseBuffer::THRESHOLD_TYPE_BINARY_INV: {
			min_value = 0;
			max_value = 1;
		} break;
		case NoiseBuffer::THRESHOLD_TYPE_TRUNCATE: {
			min_value = MIN(min_a, threshold_val);
			max_value = MIN(max_a, threshold_val);
		} break;
		case NoiseBuffer::THRESHOLD_TYPE_TO_ZERO: {
			real_t v1 = min_a < threshold_val ? 0 : min_a;
			real_t v2 = max_a < threshold_val ? 0 : max_a;
			min_value = MIN(v1, v2);
			max_value = MAX(v1, v2);
		} break;
		case NoiseBuffer::THRESHOLD_TYPE_TO_ZERO_INV: {
			real_t v1 = min_a > threshold_val ? 0 : min_a;
			real_t v2 = max_a > threshold_val ? 0 : max_a;
			min_value = MIN(v1, v2);
			max_value = MAX(v1, v2);
		} break;

		default:
			break;
	}
}

void VisualNoiseNodeThreshold::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_threshold_type", "type"), &VisualNoiseNodeThreshold::set_threshold_type);
	ClassDB::bind_method(D_METHOD("get_threshold_type"), &VisualNoiseNodeThreshold::get_threshold_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "threshold_type", PROPERTY_HINT_ENUM, "BINARY, BINARY_INVERTED, TRUNCATE, TO_ZERO, TO_ZERO_INVERTED"), "set_threshold_type", "get_threshold_type");
}

VisualNoiseNodeThreshold::VisualNoiseNodeThreshold() {
	set_default_input_value(1, 0.5);
}


////////////// Normalize

String VisualNoiseNodeNormalize::get_caption() const{
	return "Normalize";
}

int VisualNoiseNodeNormalize::get_input_port_count() const{
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeNormalize::get_input_port_type(int p_port) const{
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeNormalize::get_input_port_name(int p_port) const{
	switch (p_port) {
		case 0:
			return "input";
		default:
			return "";
	}
}

int VisualNoiseNodeNormalize::get_output_port_count() const{
	return 1;
}

VisualNoiseNode::PortType VisualNoiseNodeNormalize::get_output_port_type(int p_port) const{
	return PORT_TYPE_NOISE;
}

String VisualNoiseNodeNormalize::get_output_port_name(int p_port) const{
	return "";
}

Vector<StringName> VisualNoiseNodeNormalize::get_editable_properties() const {
	Vector<StringName> props;
	return props;
}

void VisualNoiseNodeNormalize::evaluate(const Array &p_inputs, const GraphData &p_graph_data){
	Ref<NoiseBuffer> a = parse_noise(p_inputs[0], p_graph_data);

	// (a + -min_val) * (1/(max_value-min_value))
	// TODO
	//a->scalar_add(-a->get_min());
	//a->scalar_multiply(1/(max_value-min_value));

	set_output_value(a);
}

void VisualNoiseNodeNormalize::update_minmax(const Array &p_inputs, const Vector<Ref<VisualNoiseNode>> &p_nodes) {
	min_value = 0;
	max_value = 1;
}

void VisualNoiseNodeNormalize::_bind_methods() {
}

VisualNoiseNodeNormalize::VisualNoiseNodeNormalize() {
}
