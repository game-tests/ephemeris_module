// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "visual_noise_editor_plugin.h"
#include "core/string/print_string.h"
#include "editor/editor_scale.h"
#include "noise_graph.h"
#include "noise_editor_nodes.h"
#include "scene/gui/subviewport_container.h"

// VisualNoiseEditor

VisualNoiseEditor *VisualNoiseEditor::singleton = nullptr;

void VisualNoiseEditor::_node_dragged(const Vector2 &p_from, const Vector2 &p_to, int p_node) {
	drag_buffer.push_back({ p_node, p_from, p_to });
	if (!drag_dirty) {
		call_deferred(SNAME("_nodes_dragged"));
	}
	drag_dirty = true;
}

void VisualNoiseEditor::_nodes_dragged() {
	drag_dirty = false;
	undo_redo->create_action(TTR("Node(s) Moved"));

	for (const DragOp &op : drag_buffer) {
		undo_redo->add_do_method(noise_graph.ptr(), "set_node_position", op.node, op.to);
		undo_redo->add_undo_method(noise_graph.ptr(), "set_node_position", op.node, op.from);
		undo_redo->add_do_method(this, "set_node_position", op.node, op.to);
		undo_redo->add_undo_method(this, "set_node_position", op.node, op.from);
	}

	drag_buffer.clear();
	undo_redo->commit_action();
}

Variant VisualNoiseEditor::get_drag_data_fw(const Point2 &p_point, Control *p_from) {
	if (p_from == members) {
		TreeItem *it = members->get_item_at_position(p_point);
		if (!it) {
			return Variant();
		}
		if (!it->has_meta("id")) {
			return Variant();
		}

		int id = it->get_meta("id");

		Dictionary d;
		d["id"] = id;

		Label *label = memnew(Label);
		label->set_text(it->get_text(0));
		set_drag_preview(label);
		return d;
	}
	return Variant();
}

bool VisualNoiseEditor::can_drop_data_fw(const Point2 &p_point, const Variant &p_data, Control *p_from) const {
	if (p_from == graph) {
		Dictionary d = p_data;

		if (d.has("id")) {
			return true;
		}
	}

	return false;
}

void VisualNoiseEditor::drop_data_fw(const Point2 &p_point, const Variant &p_data, Control *p_from) {
	if (p_from == graph) {
		Dictionary d = p_data;

		if (d.has("id")) {
			int idx = d["id"];
			saved_node_pos = p_point;
			saved_node_pos_dirty = true;
			_add_node_request(idx);
		}
	}
}

void VisualNoiseEditor::_node_menu_id_pressed(int p_idx) {
	switch (p_idx) {
		case NodeMenuOptions::ADD:
			_show_members_dialog(true);
			break;
		case NodeMenuOptions::COPY:
			_copy_nodes();
			break;
		case NodeMenuOptions::PASTE:
			_paste_nodes(true, menu_point);
			break;
		case NodeMenuOptions::DELETE:
			_delete_nodes_request();
			break;
		case NodeMenuOptions::DUPLICATE:
			_duplicate_nodes();
			break;
	}
}

void VisualNoiseEditor::_tools_menu_option(int p_idx) {

}

void VisualNoiseEditor::_member_filter_changed(const String &p_text) {
	_update_options_menu();
}

void VisualNoiseEditor::_sbox_input(const Ref<InputEvent> &p_ie) {

}

void VisualNoiseEditor::_member_selected() {
	TreeItem *item = members->get_selected();

	if (item != nullptr && item->has_meta("id")) {
		members_dialog->get_ok_button()->set_disabled(false);
		node_desc->set_text(node_options[item->get_meta("id")].description);
	} else {
		members_dialog->get_ok_button()->set_disabled(true);
		node_desc->set_text("");
	}
}

void VisualNoiseEditor::_member_unselected() {
}

void VisualNoiseEditor::_member_create() {
	TreeItem *item = members->get_selected();
	if (item != nullptr && item->has_meta("id")) {
		int idx = members->get_selected()->get_meta("id");
		_add_node_request(idx);
		members_dialog->hide();
	}
}

void VisualNoiseEditor::_member_cancel() {
	to_node = -1;
	to_slot = -1;
	from_node = -1;
	from_slot = -1;
}

void VisualNoiseEditor::_update_graph() {
	graph->set_scroll_ofs(noise_graph->get_graph_offset() * EDSCALE);

	graph->clear_connections();
	//erase all nodes
	for (int i = 0; i < graph->get_child_count(); i++) {
		if (Object::cast_to<GraphNode>(graph->get_child(i))) {
			Node *node = graph->get_child(i);
			graph->remove_child(node);
			memdelete(node);
			i--;
		}
	}

	Vector<int> nodes = noise_graph->get_node_list();

	for (int n_i = 0; n_i < nodes.size(); n_i++) {
		add_node(nodes[n_i]);
	}

	List<NoiseGraph::Connection> conns;
	noise_graph->get_node_connections(&conns);

	for (const NoiseGraph::Connection &c : conns) {
		const int from = c.from_node;
		const int from_idx = c.from_port;
		const int to = c.to_node;
		const int to_idx = c.to_port;

		graph->connect_node(itos(from), from_idx, itos(to), to_idx);
	}
	call_deferred(SNAME("_evaluate_nodes"));
}

void VisualNoiseEditor::_update_options_menu() {
	node_desc->set_text("");
	members_dialog->get_ok_button()->set_disabled(true);

	members->clear();
	TreeItem *root = members->create_item();

	String filter = node_filter->get_text().strip_edges();
	bool use_filter = !filter.is_empty();

	bool is_first_item = true;

	HashMap<String, TreeItem *> folders;

	for (int i = 0; i < node_options.size(); i++) {
		if (use_filter && node_options[i].name.findn(filter) == -1 && node_options[i].description.findn(filter) == -1 && node_options[i].category.findn(filter) == -1) {
			continue;
		}
		String path = node_options[i].category;
		TreeItem *category = nullptr;

		if (!folders.has(path)) {
			category = members->create_item(root);
			category->set_selectable(0, false);
			category->set_collapsed(!use_filter);
			category->set_text(0, path);
			folders.insert(path, category);

		} else {
			category = folders[path];
		}

		TreeItem *item = members->create_item(category);
		item->set_text(0, node_options[i].name);
		if (is_first_item && use_filter) {
			item->select(0);
			node_desc->set_text(node_options[i].description);
			is_first_item = false;
		}
		item->set_meta("id", i);
	}
}

VisualNoiseNode *VisualNoiseEditor::_add_node_request(int p_idx) {
	ERR_FAIL_INDEX_V(p_idx, node_options.size(), nullptr);

	Ref<VisualNoiseNode> vsnode;

	VisualNoiseNode *vnn = Object::cast_to<VisualNoiseNode>(ClassDB::instantiate(node_options[p_idx].type));
	ERR_FAIL_COND_V(!vnn, nullptr);

	int func_type = node_options[p_idx].func;
	if (func_type != -1) {

		VisualNoiseNodeNoiseGenerator *gen = Object::cast_to<VisualNoiseNodeNoiseGenerator>(vnn);

		if (gen) {
			gen->set_noise_type((VisualNoiseNodeNoiseGenerator::NoiseType)node_options[p_idx].func);
		}

		VisualNoiseNodeFractalGenerator *fract = Object::cast_to<VisualNoiseNodeFractalGenerator>(vnn);

		if (fract) {
			fract->set_noise_type((VisualNoiseNodeFractalGenerator::NoiseType)node_options[p_idx].func);
		}

		VisualNoiseNodeCellNoiseGenerator *cell = Object::cast_to<VisualNoiseNodeCellNoiseGenerator>(vnn);

		if (cell) {
			cell->set_fractal_enabled((bool)node_options[p_idx].func);
		}

		VisualNoiseNodeScalarOperator *escalar_op = Object::cast_to<VisualNoiseNodeScalarOperator>(vnn);

		if (escalar_op) {
			escalar_op->set_operator((VisualNoiseNodeScalarOperator::Operator)node_options[p_idx].func);
		}

		VisualNoiseNodeOperator *op = Object::cast_to<VisualNoiseNodeOperator>(vnn);

		if (op) {
			op->set_operator((VisualNoiseNodeOperator::Operator)node_options[p_idx].func);
		}
	}

	vsnode = Ref<VisualNoiseNode>(vnn);

	int id_to_use = noise_graph->get_valid_node_id();

	Point2 position = graph->get_scroll_ofs();

	if (saved_node_pos_dirty) {
		position += saved_node_pos;
	} else {
		position += graph->get_size() * 0.5;
		position /= EDSCALE;
	}

	undo_redo->create_action(TTR("Add Node to Visual Shader"));
	undo_redo->add_do_method(noise_graph.ptr(), "add_node", vsnode, position, id_to_use);
	undo_redo->add_do_method(this, "add_node", id_to_use);
	undo_redo->add_undo_method(this, "remove_node", id_to_use);
	undo_redo->add_undo_method(noise_graph.ptr(), "remove_node", id_to_use);
	
	if (to_node != -1 && to_slot != -1) {
		if (vsnode->get_output_port_count() > 0) {
			int _from_node = id_to_use;
			int _from_slot = 0;
			// TODO
			//if (noise_graph->is_port_types_compatible(vsnode->get_output_port_type(_from_slot), noise_graph->get_node(to_node)->get_input_port_type(to_slot))) {
				undo_redo->add_do_method(noise_graph.ptr(), "connect_nodes", _from_node, _from_slot, to_node, to_slot);
				undo_redo->add_undo_method(noise_graph.ptr(), "disconnect_nodes", _from_node, _from_slot, to_node, to_slot);
				undo_redo->add_do_method(this, "connect_nodes", _from_node, _from_slot, to_node, to_slot);
				undo_redo->add_undo_method(this, "disconnect_nodes", _from_node, _from_slot, to_node, to_slot);
			//}
		}
	} else if (from_node != -1 && from_slot != -1) {
		if (vsnode->get_input_port_count() > 0) {
			int _to_node = id_to_use;
			int _to_slot = 0;

			//if (noise_graph->is_port_types_compatible(visual_shader->get_node(from_node)->get_output_port_type(from_slot), vsnode->get_input_port_type(_to_slot))) {
				undo_redo->add_do_method(noise_graph.ptr(), "connect_nodes", from_node, from_slot, _to_node, _to_slot);
				undo_redo->add_undo_method(noise_graph.ptr(), "disconnect_nodes", from_node, from_slot, _to_node, _to_slot);
				undo_redo->add_do_method(this, "connect_nodes", from_node, from_slot, _to_node, _to_slot);
				undo_redo->add_undo_method(this, "disconnect_nodes", from_node, from_slot, _to_node, _to_slot);
			//}
		}
	}
	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();

	_member_cancel(); // TODO remove when fixed

	return vsnode.ptr();

}

void VisualNoiseEditor::_connection_request(const String &p_from, int p_from_index, const String &p_to, int p_to_index) {
	int from = p_from.to_int();
	int to = p_to.to_int();

	if (!noise_graph->can_connect_nodes(from, p_from_index, to, p_to_index)) {
		return;
	}
	undo_redo->create_action(TTR("Nodes Connected"));

	undo_redo->add_do_method(noise_graph.ptr(), "connect_nodes", from, p_from_index, to, p_to_index);
	undo_redo->add_undo_method(noise_graph.ptr(), "disconnect_nodes", from, p_from_index, to, p_to_index);
	undo_redo->add_do_method(this, "connect_nodes", from, p_from_index, to, p_to_index);
	undo_redo->add_undo_method(this, "disconnect_nodes", from, p_from_index, to, p_to_index);

	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();
}

void VisualNoiseEditor::_disconnection_request(const String &p_from, int p_from_index, const String &p_to, int p_to_index) {
	int from = p_from.to_int();
	int to = p_to.to_int();

	undo_redo->create_action(TTR("Nodes Disconnected"));

	undo_redo->add_do_method(noise_graph.ptr(), "disconnect_nodes", from, p_from_index, to, p_to_index);
	undo_redo->add_undo_method(noise_graph.ptr(), "connect_nodes", from, p_from_index, to, p_to_index);
	undo_redo->add_do_method(this, "disconnect_nodes", from, p_from_index, to, p_to_index);
	undo_redo->add_undo_method(this, "connect_nodes", from, p_from_index, to, p_to_index);

	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();
}

void VisualNoiseEditor::_scroll_changed(const Vector2 &p_scroll) {
	noise_graph->set_graph_offset(p_scroll / EDSCALE);
}

void VisualNoiseEditor::_node_selected(Object *p_node) {
}

void VisualNoiseEditor::set_node_position(int p_id, const Vector2 &p_position) {
	GraphNode *n = Object::cast_to<GraphNode>(graph->get_node(itos(p_id)));
	if (n) {
		n->set_position_offset(p_position);
	}
}

void VisualNoiseEditor::_dup_copy_nodes() {
	_clear_buffer();

	for (int i = 0; i < graph->get_child_count(); i++) {
		GraphNode *gn = Object::cast_to<GraphNode>(graph->get_child(i));
		if (gn) {
			int id = String(gn->get_name()).to_int();
			Ref<VisualNoiseNode> node = noise_graph->get_node(id);
			Ref<VisualNoiseNodeOutput> output = node;
			if (output.is_valid()) { // can't duplicate output
				continue;
			}
			if (node.is_valid() && gn->is_selected()) {
				Vector2 pos = noise_graph->get_node_position(id);
				selection_center += pos;
				copy_nodes_buffer.push_back(id);
			}
		}
	}

	selection_center /= (float)copy_nodes_buffer.size();
}

void VisualNoiseEditor::_dup_paste_nodes(const  Vector2 &p_offset) {
	int base_id = noise_graph->get_valid_node_id();
	int id_from = base_id;
	HashMap<int, int> connection_remap;

	for (int n : copy_nodes_buffer) {
		connection_remap[n] = id_from;
		Ref<VisualNoiseNode> node = noise_graph->get_node(n);

		Ref<VisualNoiseNode> dupli = node->duplicate();

		undo_redo->add_do_method(noise_graph.ptr(), "add_node", dupli, noise_graph->get_node_position(n) + p_offset, id_from);
		undo_redo->add_do_method(this, "add_node", id_from);

		id_from++;
	}

	List<NoiseGraph::Connection> conns;
	noise_graph->get_node_connections(&conns);

	for (const NoiseGraph::Connection &c : conns) {
		if (connection_remap.has(c.from_node) && connection_remap.has(c.to_node)) {
			undo_redo->add_do_method(noise_graph.ptr(), "connect_nodes", connection_remap[c.from_node], c.from_port, connection_remap[c.to_node], c.to_port);
			undo_redo->add_do_method(this, "connect_nodes", connection_remap[c.from_node], c.from_port, connection_remap[c.to_node], c.to_port);
		}
	}

	id_from = base_id;
	for (int i = 0; i < copy_nodes_buffer.size(); i++) {
		undo_redo->add_undo_method(this, "remove_node", id_from);
		undo_redo->add_undo_method(noise_graph.ptr(), "remove_node", id_from);

		id_from++;
	}

	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();

	for (int i = 0; i < graph->get_child_count(); i++) {
		GraphNode *gn = Object::cast_to<GraphNode>(graph->get_child(i));
		if (gn) {
			int id = String(gn->get_name()).to_int();
			if (id >= base_id) {
				gn->set_selected(true);
			} else {
				gn->set_selected(false);
			}
		}
	}
}

void VisualNoiseEditor::_duplicate_nodes() {
	undo_redo->create_action(TTR("Duplicate VisualShader Node(s)"));
	_dup_copy_nodes();
	_dup_paste_nodes(Vector2(10, 10) * EDSCALE);
}

void VisualNoiseEditor::_clear_buffer() {
	copy_nodes_buffer.clear();
	selection_center.x = 0.0f;
	selection_center.y = 0.0f;
}

void VisualNoiseEditor::_copy_nodes() {
	_dup_copy_nodes();
}

void VisualNoiseEditor::_paste_nodes(bool p_use_custom_position, const Vector2 &p_custom_position) {
	if (copy_nodes_buffer.is_empty()) {
		return;
	}

	float scale = graph->get_zoom();

	Vector2 mpos;
	if (p_use_custom_position) {
		mpos = p_custom_position;
	} else {
		mpos = graph->get_local_mouse_position();
	}

	undo_redo->create_action(TTR("Paste VisualShader Node(s)"));
	_dup_paste_nodes(graph->get_scroll_ofs() / scale + mpos / scale - selection_center);
}

void VisualNoiseEditor::_delete_nodes(const List<int> &p_nodes) {
	List<NoiseGraph::Connection> conns;
	noise_graph->get_node_connections(&conns);

	for (const int n : p_nodes) {
		for (List<NoiseGraph::Connection>::Element *E = conns.front(); E; E = E->next()) {
			if (E->get().from_node == n || E->get().to_node == n) {
				undo_redo->add_do_method(this, "disconnect_nodes", E->get().from_node, E->get().from_port, E->get().to_node, E->get().to_port);
				undo_redo->add_do_method(noise_graph.ptr(), "disconnect_nodes", E->get().from_node, E->get().from_port, E->get().to_node, E->get().to_port);
			}
		}
	}

	for (const int n : p_nodes) {
		Ref<VisualNoiseNode> node = noise_graph->get_node(n);

		undo_redo->add_do_method(noise_graph.ptr(), "remove_node", n);
		undo_redo->add_undo_method(noise_graph.ptr(), "add_node", node, noise_graph->get_node_position(n), n);
		undo_redo->add_undo_method(this, "add_node", n);
	}

	List<NoiseGraph::Connection> used_conns;
	for (const int n : p_nodes) {
		for (const NoiseGraph::Connection &connection : conns) {
			if (connection.from_node == n || connection.to_node == n) {
				bool cancel = false;
				for (const NoiseGraph::Connection &used_c : used_conns) {
					if (used_c.from_node == connection.from_node && used_c.from_port == connection.from_port && used_c.to_node == connection.to_node && used_c.to_port == connection.to_port) {
						cancel = true; // to avoid ERR_ALREADY_EXISTS warning
						break;
					}
				}
				if (!cancel) {
					undo_redo->add_undo_method(noise_graph.ptr(), "connect_nodes", connection.from_node, connection.from_port, connection.to_node, connection.to_port);
					undo_redo->add_undo_method(this, "connect_nodes", connection.from_node, connection.from_port, connection.to_node, connection.to_port);
					used_conns.push_back(connection);
				}
			}
		}
	}

	// delete nodes from the graph
	for (const List<int>::Element *F = p_nodes.front(); F; F = F->next()) {
		undo_redo->add_do_method(this, "remove_node", F->get());
	}


	_clear_buffer();
}

void VisualNoiseEditor::_delete_node_request(int p_node) {
	List<int> to_erase;
	to_erase.push_back(p_node);

	undo_redo->create_action(TTR("Delete VisualNoise Node"));
	_delete_nodes(to_erase);
	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();
}

void VisualNoiseEditor::_delete_nodes_request() {
	List<int> to_erase;

	for (int i = 0; i < graph->get_child_count(); i++) {
		GraphNode *gn = Object::cast_to<GraphNode>(graph->get_child(i));
		if (gn) {
			if (gn->is_selected() && gn->is_close_button_visible()) {
				to_erase.push_back(gn->get_name().operator String().to_int());
			}
		}
	}

	if (to_erase.is_empty()) {
		return;
	}

	undo_redo->create_action(TTR("Delete VisualNoise Node(s)"));
	_delete_nodes(to_erase);
	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();
}

void VisualNoiseEditor::_graph_gui_input(const Ref<InputEvent> &p_event) {
	Ref<InputEventMouseButton> mb = p_event;

	if (mb.is_valid() && mb->is_pressed() && mb->get_button_index() == MouseButton::RIGHT) {
		List<int> to_change;
		for (int i = 0; i < graph->get_child_count(); i++) {
			GraphNode *gn = Object::cast_to<GraphNode>(graph->get_child(i));
			if (gn) {
				if (gn->is_selected() && gn->is_close_button_visible()) {
					to_change.push_back(gn->get_name().operator String().to_int());
				}
			}
		}
		if (to_change.is_empty() && copy_nodes_buffer.is_empty()) {
			_show_members_dialog(true);
		} else {
			popup_menu->set_item_disabled(NodeMenuOptions::COPY, to_change.is_empty());
			popup_menu->set_item_disabled(NodeMenuOptions::PASTE, copy_nodes_buffer.is_empty());
			popup_menu->set_item_disabled(NodeMenuOptions::DELETE, to_change.is_empty());
			popup_menu->set_item_disabled(NodeMenuOptions::DUPLICATE, to_change.is_empty());
			menu_point = graph->get_local_mouse_position();
			Point2 gpos = Input::get_singleton()->get_mouse_position();
			popup_menu->set_position(gpos);
			popup_menu->popup();
		}
	}
}

void VisualNoiseEditor::_connection_to_empty(const String &p_from, int p_from_slot, const Vector2 &p_release_position) {
	from_node = p_from.to_int();
	from_slot = p_from_slot;
	_show_members_dialog(true);
}

void VisualNoiseEditor::_connection_from_empty(const String &p_to, int p_to_slot, const Vector2 &p_release_position) {
	to_node = p_to.to_int();
	to_slot = p_to_slot;
	_show_members_dialog(true);
}

void VisualNoiseEditor::_show_members_dialog(bool at_mouse_pos) {
	if (at_mouse_pos) {
		saved_node_pos_dirty = true;
		saved_node_pos = graph->get_local_mouse_position();

		Point2 gpos = Input::get_singleton()->get_mouse_position();
		members_dialog->popup();
		members_dialog->set_position(gpos);
	} else {
		members_dialog->popup();
		saved_node_pos_dirty = false;
		members_dialog->set_position(graph->get_global_position() + Point2(5 * EDSCALE, 65 * EDSCALE));
	}

	// keep dialog within window bounds
	Size2 window_size = DisplayServer::get_singleton()->window_get_size();
	Rect2 dialog_rect = Rect2(members_dialog->get_position(), members_dialog->get_size());
	if (dialog_rect.position.y + dialog_rect.size.y > window_size.y) {
		int difference = dialog_rect.position.y + dialog_rect.size.y - window_size.y;
		members_dialog->set_position(members_dialog->get_position() - Point2(0, difference));
	}
	if (dialog_rect.position.x + dialog_rect.size.x > window_size.x) {
		int difference = dialog_rect.position.x + dialog_rect.size.x - window_size.x;
		members_dialog->set_position(members_dialog->get_position() - Point2(difference, 0));
	}

	node_filter->call_deferred(SNAME("grab_focus")); // still not visible
	node_filter->select_all();
}

void VisualNoiseEditor::_show_3D_noise_preview() {
	noise3d_preview->set_visible(preview_noise->is_pressed());
}

void VisualNoiseEditor::_evaluate_nodes() {
	noise_graph->set_editor_dirty(true);
	noise_graph->evaluate_nodes(Rect2i(0, 0, 100, 100), 0, 1, 234234);
	emit_signal(SNAME("update_previews"));
	_update_3d_preview();
}

void VisualNoiseEditor::_update_3d_preview() {
	noise3d_preview->set_noise(noise_graph);
}


void VisualNoiseEditor::_property_changed(const String &p_property, const Variant &p_value, const String &p_field, bool p_changing, Ref<VisualNoiseNode> p_node, Object *p_prop) {
	if (p_changing) {
		return;
	}
	UndoRedo *undo_redo = EditorNode::get_singleton()->get_undo_redo();
	undo_redo->create_action(TTR("Edit Visual Property") + ": " + p_property, UndoRedo::MERGE_ENDS);
	undo_redo->add_undo_method(p_node.ptr(), "set", p_property, p_node->get(p_property));
	undo_redo->add_do_method(p_node.ptr(), "set", p_property, p_value);
	undo_redo->add_undo_method(this, "_update_property", p_prop);
	undo_redo->add_do_method(this, "_update_property", p_prop);
	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();
}

void VisualNoiseEditor::_update_property(Object *p_prop) {
	Object::cast_to<EditorProperty>(p_prop)->update_property();
}

void VisualNoiseEditor::_notification(int p_what) {

	if (p_what == NOTIFICATION_ENTER_TREE) {
		node_filter->set_clear_button_enabled(true);

		// collapse tree by default

		TreeItem *category = members->get_root()->get_first_child();
		while (category) {
			category->set_collapsed(true);
			TreeItem *sub_category = category->get_first_child();
			while (sub_category) {
				sub_category->set_collapsed(true);
				sub_category = sub_category->get_next();
			}
			category = category->get_next();
		}
	}

	if (p_what == NOTIFICATION_DRAG_BEGIN) {
		Dictionary dd = get_viewport()->gui_get_drag_data();
		if (members->is_visible_in_tree() && dd.has("id")) {
			members->set_drop_mode_flags(Tree::DROP_MODE_ON_ITEM);
		}
	} else if (p_what == NOTIFICATION_DRAG_END) {
		members->set_drop_mode_flags(0);
	}

	if (p_what == NOTIFICATION_ENTER_TREE || p_what == NOTIFICATION_THEME_CHANGED) {

		node_filter->set_right_icon(Control::get_theme_icon("Search", "EditorIcons"));

		// TODO
		//error_text->add_theme_font_override("font", get_theme_font("status_source", "EditorFonts"));
		//error_text->add_theme_color_override("font_color", get_theme_color("error_color", "Editor"));

		tools->set_icon(EditorNode::get_singleton()->get_gui_base()->get_theme_icon("Tools", "EditorIcons"));

		if (p_what == NOTIFICATION_THEME_CHANGED && is_visible_in_tree()) {
			_update_graph();
		}
	}
}

void VisualNoiseEditor::_bind_methods() {
	ClassDB::bind_method("_update_graph", &VisualNoiseEditor::_update_graph);
	ClassDB::bind_method("_update_options_menu", &VisualNoiseEditor::_update_options_menu);
	ClassDB::bind_method("add_node", &VisualNoiseEditor::add_node);
	ClassDB::bind_method("remove_node", &VisualNoiseEditor::remove_node);
	ClassDB::bind_method("_clear_buffer", &VisualNoiseEditor::_clear_buffer);
	ClassDB::bind_method("_nodes_dragged", &VisualNoiseEditor::_nodes_dragged);
	ClassDB::bind_method("set_node_position", &VisualNoiseEditor::set_node_position);
	ClassDB::bind_method("connect_nodes", &VisualNoiseEditor::connect_nodes);
	ClassDB::bind_method("disconnect_nodes", &VisualNoiseEditor::disconnect_nodes);
	ClassDB::bind_method("_evaluate_nodes", &VisualNoiseEditor::_evaluate_nodes);
	ClassDB::bind_method("_update_property", &VisualNoiseEditor::_update_property);
	ClassDB::bind_method(D_METHOD("set_default_input_value"), &VisualNoiseEditor::set_default_input_value);

	ClassDB::bind_method(D_METHOD("get_drag_data_fw"), &VisualNoiseEditor::get_drag_data_fw);
	ClassDB::bind_method(D_METHOD("can_drop_data_fw"), &VisualNoiseEditor::can_drop_data_fw);
	ClassDB::bind_method(D_METHOD("drop_data_fw"), &VisualNoiseEditor::drop_data_fw);

	ADD_SIGNAL(MethodInfo("update_previews"));
}


void VisualNoiseEditor::edit(const Ref<NoiseGraph> &p_graph) {
    if (p_graph.is_null()) {
		return;
	}

	if (noise_graph == p_graph) {
		return;
	}

	noise_graph = p_graph;

	noise_graph->set_graph_offset(graph->get_scroll_ofs() / EDSCALE);

	_update_graph();
}

Ref<StyleBoxEmpty> make_empty_stylebox(float p_margin_left = -1, float p_margin_top = -1, float p_margin_right = -1, float p_margin_bottom = -1) {
	Ref<StyleBoxEmpty> style(memnew(StyleBoxEmpty));
	style->set_default_margin(SIDE_LEFT, p_margin_left * EDSCALE);
	style->set_default_margin(SIDE_RIGHT, p_margin_right * EDSCALE);
	style->set_default_margin(SIDE_BOTTOM, p_margin_bottom * EDSCALE);
	style->set_default_margin(SIDE_TOP, p_margin_top * EDSCALE);
	return style;
}

Control *VisualNoiseEditor::create_editor(Ref<VisualNoiseNode> p_node) {
	Vector<StringName> properties = p_node->get_editable_properties();
	if (properties.size() == 0) {
		return nullptr;
	}

	List<PropertyInfo> props;
	p_node->get_property_list(&props);

	Vector<PropertyInfo> pinfo;

	for (List<PropertyInfo>::Element *E = props.front(); E; E = E->next()) {
		for (int i = 0; i < properties.size(); i++) {
			if (E->get().name == String(properties[i])) {
				pinfo.push_back(E->get());
			}
		}
	}

	if (pinfo.size() == 0) {
		return nullptr;
	}

	VBoxContainer *editor = memnew(VBoxContainer);

	for (int i = 0; i < pinfo.size(); i++) {
		EditorProperty *prop = EditorInspector::instantiate_property_editor(p_node.ptr(), pinfo[i].type, pinfo[i].name, pinfo[i].hint, pinfo[i].hint_string, pinfo[i].usage);
		ERR_FAIL_NULL_V(prop, nullptr);

		prop->connect("property_changed", callable_mp(this, &VisualNoiseEditor::_property_changed), varray(p_node, prop));

		Label *prop_name = memnew(Label);
		String prop_name_str = pinfo[i].name;
		prop_name_str = prop_name_str.capitalize() + ":";
		prop_name->set_text(prop_name_str);
		editor->add_child(prop_name);

		prop->set_h_size_flags(Control::SIZE_EXPAND_FILL);
		editor->add_child(prop);

		prop->set_object_and_property(p_node.ptr(), pinfo[i].name);
		prop->update_property();
		prop->set_name_split_ratio(0);
		

	}
	return editor;
}

void VisualNoiseEditor::_edit_port_default_input(Object *p_button, int p_node, int p_port) {
	Ref<VisualNoiseNode> vsn = noise_graph->get_node(p_node);

	Button *button = Object::cast_to<Button>(p_button);
	ERR_FAIL_COND(!button);
	Variant value = vsn->get_default_input_value(p_port);
	property_editor->set_position(button->get_screen_position() + Vector2(0, button->get_size().height));
	property_editor->edit(nullptr, "", value.get_type(), value, 0, "");
	property_editor->popup();
	editing_node = p_node;
	editing_port = p_port;
}

void VisualNoiseEditor::_port_edited() {
	Variant value = property_editor->get_variant();
	Ref<VisualNoiseNode> vsn = noise_graph->get_node(editing_node);
	ERR_FAIL_COND(!vsn.is_valid());

	undo_redo->create_action(TTR("Set Input Default Port"));

	undo_redo->add_do_method(vsn.ptr(), "set_default_input_value", editing_port, value);
	undo_redo->add_undo_method(vsn.ptr(), "set_default_input_value", editing_port, vsn->get_default_input_value(editing_port));
	undo_redo->add_do_method(this, "set_default_input_value", editing_node, editing_port, value);
	undo_redo->add_undo_method(this, "set_default_input_value", editing_node, editing_port, vsn->get_default_input_value(editing_port));

	undo_redo->add_undo_method(this, "_evaluate_nodes");
	undo_redo->add_do_method(this, "_evaluate_nodes");
	undo_redo->commit_action();

	property_editor->hide();
}

void VisualNoiseEditor::set_default_input_value(const int p_node, const int p_port, const Variant &p_value) {
	ports_info[p_node][p_port].property_preview->set_text(String::num(p_value, 4));
}

void VisualNoiseEditor::add_node(int p_id) {

	static const Color type_color[2] = {
		Color(0.38, 0.85, 0.96), // scalar
		Color(0.84, 0.49, 0.93), // noise
	};

	static Ref<StyleBoxEmpty> label_style = make_empty_stylebox(2, 1, 2, 1);

	Ref<VisualNoiseNode> vnnode = noise_graph->get_node(p_id);

	GraphNode *node = memnew(GraphNode);

	node->set_position_offset(noise_graph->get_node_position(p_id));
	node->set_title(vnnode->get_caption());
	node->set_name(itos(p_id));
	if (p_id >= 2) {
		node->set_show_close_button(true);
		node->connect("close_request", callable_mp(this, &VisualNoiseEditor::_delete_node_request), varray(p_id), CONNECT_DEFERRED);
	}
	node->connect("dragged", callable_mp(this, &VisualNoiseEditor::_node_dragged), varray(p_id));

	int port_offset = 0;
	
	// Add property editor
	Control *prop_editor = create_editor(vnnode);
	if (prop_editor) {
		node->add_child(prop_editor);
		port_offset++;

		if (vnnode->get_input_port_count() || vnnode->get_output_port_count()) {
			Control *offset = memnew(Control);
			offset->set_custom_minimum_size(Size2(0, 5 * EDSCALE));
			node->add_child(offset);
			port_offset++;
		}
	}

	std::vector<PortInfo> pi;
	pi.resize(vnnode->get_input_port_count());

	for (int i = 0; i < MAX(vnnode->get_input_port_count(), vnnode->get_output_port_count()); i++) {

		HBoxContainer *hb = memnew(HBoxContainer);
		hb->set_name("container");
		hb->add_theme_constant_override("separation", 7 * EDSCALE);

		// LEFT
		bool valid_left = i < vnnode->get_input_port_count();
		VisualNoiseNode::PortType port_left = vnnode->get_input_port_type(i);

		if (valid_left) {
			String port_name = vnnode->get_input_port_name(i);
			
			Label *label = memnew(Label);
			label->set_text(port_name);
			label->add_theme_style_override("normal", label_style);

			VisualNoiseNode::PortType port_type = vnnode->get_input_port_type(i);
			if (port_type == VisualNoiseNode::PORT_TYPE_SCALAR) {
				Button *prop_button = memnew(Button);

				prop_button->set_text(String::num(vnnode->get_default_input_value(i), 4));
				prop_button->connect("pressed", callable_mp(this, &VisualNoiseEditor::_edit_port_default_input), varray(prop_button, p_id, i));
				pi[i].property_preview = prop_button;
				hb->add_child(prop_button);
			}
			hb->add_child(label);
		}

		hb->add_spacer();
	
		//RIGHT
		bool valid_right = i < vnnode->get_output_port_count();
		VisualNoiseNode::PortType port_right = vnnode->get_output_port_type(i);
		
		if (valid_right) {
			String port_name = vnnode->get_output_port_name(i);

			Label *label = memnew(Label);
			label->set_text(port_name);
			label->add_theme_style_override("normal", label_style);
			hb->add_child(label);
		}

		node->add_child(hb);

		node->set_slot(i + port_offset, valid_left, port_left, type_color[port_left], valid_right, port_right, type_color[port_right]);
	}

	Control *offset = memnew(Control);
	offset->set_custom_minimum_size(Size2(0, 5 * EDSCALE));
	node->add_child(offset);

	graph->add_child(node);

	if (vnnode->show_output_preview() && vnnode->get_output_port_count()) {
		VisualNoiseNodePortPreview *preview = memnew(VisualNoiseNodePortPreview);
		preview->setup(vnnode);
		connect("update_previews", callable_mp(preview, &VisualNoiseNodePortPreview::update_preview));
		node->add_child(preview);
	}

	ports_info.insert(p_id, pi);
}

void VisualNoiseEditor::remove_node(int p_id) {
	Node *n = graph->get_node(itos(p_id));
	if (n) {
		graph->remove_child(n);
		memdelete(n);
	}
	ports_info.erase(p_id);
}

void VisualNoiseEditor::connect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port) {
	graph->connect_node(itos(p_from_node), p_from_port, itos(p_to_node), p_to_port);
	Button *b = ports_info[p_to_node][p_to_port].property_preview;
	if (b) {
		b->hide();
	}
}

void VisualNoiseEditor::disconnect_nodes(int p_from_node, int p_from_port, int p_to_node, int p_to_port) {
	graph->disconnect_node(itos(p_from_node), p_from_port, itos(p_to_node), p_to_port);
	Button *b = ports_info[p_to_node][p_to_port].property_preview;
	if (b) {
		b->show();
	}
}


// VisualNoiseNode /////////////////////////////////////////////////////////////////////////////////
#include "editor/plugins/material_editor_plugin.h"
VisualNoiseEditor::VisualNoiseEditor() {

	singleton = this;

	saved_node_pos_dirty = false;
	saved_node_pos = Point2(0, 0);

	to_node = -1;
	to_slot = -1;
	from_node = -1;
	from_slot = -1;

	undo_redo = EditorNode::get_singleton()->get_undo_redo();

	main_box = memnew(HSplitContainer);
	main_box->set_v_size_flags(SIZE_EXPAND_FILL);
	main_box->set_h_size_flags(SIZE_EXPAND_FILL);
	add_child(main_box);

	graph = memnew(GraphEdit);
	graph->get_zoom_hbox()->set_h_size_flags(SIZE_EXPAND_FILL);
	graph->set_v_size_flags(SIZE_EXPAND_FILL);
	graph->set_h_size_flags(SIZE_EXPAND_FILL);
	main_box->add_child(graph);
	graph->set_drag_forwarding(this);
	graph->set_minimap_enabled(false);

	graph->add_valid_right_disconnect_type(VisualNoiseNode::PORT_TYPE_SCALAR);
	graph->add_valid_right_disconnect_type(VisualNoiseNode::PORT_TYPE_NOISE);

	graph->set_v_size_flags(SIZE_EXPAND_FILL);
	graph->connect("connection_request", callable_mp(this, &VisualNoiseEditor::_connection_request), varray(), CONNECT_DEFERRED);
	graph->connect("disconnection_request", callable_mp(this, &VisualNoiseEditor::_disconnection_request), varray(), CONNECT_DEFERRED);
	graph->connect("node_selected", callable_mp(this, &VisualNoiseEditor::_node_selected));
	graph->connect("scroll_offset_changed", callable_mp(this, &VisualNoiseEditor::_scroll_changed));
	graph->connect("duplicate_nodes_request", callable_mp(this, &VisualNoiseEditor::_duplicate_nodes));
	graph->connect("copy_nodes_request", callable_mp(this, &VisualNoiseEditor::_copy_nodes));
	graph->connect("paste_nodes_request", callable_mp(this, &VisualNoiseEditor::_paste_nodes), varray(false, Point2()));
	graph->connect("delete_nodes_request", callable_mp(this, &VisualNoiseEditor::_delete_nodes_request));
	graph->connect("gui_input", callable_mp(this, &VisualNoiseEditor::_graph_gui_input));
	graph->connect("connection_to_empty", callable_mp(this, &VisualNoiseEditor::_connection_to_empty));
	graph->connect("connection_from_empty", callable_mp(this, &VisualNoiseEditor::_connection_from_empty));

	graph->add_valid_connection_type(VisualNoiseNode::PORT_TYPE_SCALAR, VisualNoiseNode::PORT_TYPE_SCALAR);
	graph->add_valid_connection_type(VisualNoiseNode::PORT_TYPE_SCALAR, VisualNoiseNode::PORT_TYPE_NOISE);
	graph->add_valid_connection_type(VisualNoiseNode::PORT_TYPE_NOISE, VisualNoiseNode::PORT_TYPE_SCALAR);
	graph->add_valid_connection_type(VisualNoiseNode::PORT_TYPE_NOISE, VisualNoiseNode::PORT_TYPE_NOISE);
	
	VSeparator *vs = memnew(VSeparator);
	graph->get_zoom_hbox()->add_child(vs);
	graph->get_zoom_hbox()->move_child(vs, 0);

	add_node_button = memnew(Button);
	add_node_button->set_flat(true);
	graph->get_zoom_hbox()->add_child(add_node_button);
	add_node_button->set_text(TTR("Add Node..."));
	graph->get_zoom_hbox()->move_child(add_node_button, 0);
	add_node_button->connect("pressed", callable_mp(this, &VisualNoiseEditor::_show_members_dialog), varray(false));


	property_editor = memnew(CustomPropertyEditor);
	add_child(property_editor);

	property_editor->connect("variant_changed", callable_mp(this, &VisualNoiseEditor::_port_edited));

	///////////////////////////////////////
	// 3D PREVIEW PANEL
	///////////////////////////////////////
	preview_noise = memnew(Button);
	preview_noise->set_flat(true);
	preview_noise->set_toggle_mode(true);
	preview_noise->set_tooltip(TTR("Show resulted 3D Noise."));
	preview_noise->set_text(TTR("View 3D"));
	graph->get_zoom_hbox()->add_child(preview_noise);
	preview_noise->connect("pressed", callable_mp(this, &VisualNoiseEditor::_show_3D_noise_preview));

	noise3d_preview = memnew(Noise3dPreview);
	main_box->add_child(noise3d_preview);
	
	noise3d_preview->set_visible(false);


	///////////////////////////////////////
	// POPUP MENU
	///////////////////////////////////////

	popup_menu = memnew(PopupMenu);
	add_child(popup_menu);
	popup_menu->add_item("Add Node", NodeMenuOptions::ADD);
	popup_menu->add_separator();
	popup_menu->add_item("Copy", NodeMenuOptions::COPY);
	popup_menu->add_item("Paste", NodeMenuOptions::PASTE);
	popup_menu->add_item("Delete", NodeMenuOptions::DELETE);
	popup_menu->add_item("Duplicate", NodeMenuOptions::DUPLICATE);
	popup_menu->connect("id_pressed", callable_mp(this, &VisualNoiseEditor::_node_menu_id_pressed));

	///////////////////////////////////////
	// NOISE NODES TREE
	///////////////////////////////////////

	VBoxContainer *members_vb = memnew(VBoxContainer);
	members_vb->set_v_size_flags(SIZE_EXPAND_FILL);

	HBoxContainer *filter_hb = memnew(HBoxContainer);
	members_vb->add_child(filter_hb);

	node_filter = memnew(LineEdit);
	filter_hb->add_child(node_filter);
	node_filter->connect("text_changed", callable_mp(this, &VisualNoiseEditor::_member_filter_changed));
	node_filter->connect("gui_input", callable_mp(this, &VisualNoiseEditor::_sbox_input));
	node_filter->set_h_size_flags(SIZE_EXPAND_FILL);
	node_filter->set_placeholder(TTR("Search"));

	tools = memnew(MenuButton);
	filter_hb->add_child(tools);
	tools->set_tooltip(TTR("Options"));
	tools->get_popup()->connect("id_pressed", callable_mp(this, &VisualNoiseEditor::_tools_menu_option));
	tools->get_popup()->add_item(TTR("Expand All"), EXPAND_ALL);
	tools->get_popup()->add_item(TTR("Collapse All"), COLLAPSE_ALL);

	members = memnew(Tree);
	members_vb->add_child(members);
	members->set_drag_forwarding(this);
	members->set_h_size_flags(SIZE_EXPAND_FILL);
	members->set_v_size_flags(SIZE_EXPAND_FILL);
	members->set_hide_root(true);
	members->set_allow_reselect(true);
	members->set_hide_folding(false);
	members->set_custom_minimum_size(Size2(180 * EDSCALE, 200 * EDSCALE));
	members->connect("item_activated", callable_mp(this, &VisualNoiseEditor::_member_create));
	members->connect("item_selected", callable_mp(this, &VisualNoiseEditor::_member_selected));
	members->connect("nothing_selected", callable_mp(this, &VisualNoiseEditor::_member_unselected));

	HBoxContainer *desc_hbox = memnew(HBoxContainer);
	members_vb->add_child(desc_hbox);

	Label *desc_label = memnew(Label);
	desc_hbox->add_child(desc_label);
	desc_label->set_text(TTR("Description:"));

	desc_hbox->add_spacer();

	node_desc = memnew(RichTextLabel);
	members_vb->add_child(node_desc);
	node_desc->set_h_size_flags(SIZE_EXPAND_FILL);
	node_desc->set_v_size_flags(SIZE_FILL);
	node_desc->set_custom_minimum_size(Size2(0, 70 * EDSCALE));

	members_dialog = memnew(ConfirmationDialog);
	members_dialog->set_title(TTR("Create Node"));
	members_dialog->set_exclusive(false);
	members_dialog->add_child(members_vb);
	members_dialog->get_ok_button()->set_text(TTR("Create"));
	members_dialog->get_ok_button()->connect("pressed", callable_mp(this, &VisualNoiseEditor::_member_create));
	members_dialog->get_ok_button()->set_disabled(true);
	members_dialog->connect("cancelled", callable_mp(this, &VisualNoiseEditor::_member_cancel));
	add_child(members_dialog);
	
	///////////////////////////////////////
	// NOISE NODES TREE OPTIONS
	///////////////////////////////////////

	node_options.reserve(32);

	node_options.push_back(NodeOption("Input", "Common", "VisualNoiseNodeInput", "Create arguments to pass to the graph."));
	node_options.push_back(NodeOption("Scalar Constant", "Common", "VisualNoiseNodeScalarConstant", "Fixed scalar value."));

	node_options.push_back(NodeOption("Value Noise", "Noise", "VisualNoiseNodeNoiseGenerator", "Value noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_VALUE));
	node_options.push_back(NodeOption("Perlin Noise", "Noise", "VisualNoiseNodeNoiseGenerator", " noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_PERLIN));
	node_options.push_back(NodeOption("Simplex Noise", "Noise", "VisualNoiseNodeNoiseGenerator", " noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_SIMPLEX));
	node_options.push_back(NodeOption("Cubic Noise", "Noise", "VisualNoiseNodeNoiseGenerator", " noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_CUBIC));
	node_options.push_back(NodeOption("White Noise", "Noise", "VisualNoiseNodeNoiseGenerator", " noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_WHITE_NOISE));

	node_options.push_back(NodeOption("Fractal Value Noise", "Noise", "VisualNoiseNodeFractalGenerator", "Fractal value noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_VALUE));
	node_options.push_back(NodeOption("Fractal Perlin Noise", "Noise", "VisualNoiseNodeFractalGenerator", "Fractal perlin noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_PERLIN));
	node_options.push_back(NodeOption("Fractal Simplex Noise", "Noise", "VisualNoiseNodeFractalGenerator", "Fractal simplex noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_SIMPLEX));
	node_options.push_back(NodeOption("Fractal Cubic Noise", "Noise", "VisualNoiseNodeFractalGenerator", "Fractal cubic noise generator.", VisualNoiseNodeNoiseGenerator::TYPE_CUBIC));

	const bool enable_fractal = true;
	node_options.push_back(NodeOption("Cell Noise", "Noise", "VisualNoiseNodeCellNoiseGenerator", "Cellular noise generator.", !enable_fractal));
	node_options.push_back(NodeOption("Cell Fractal Noise", "Noise", "VisualNoiseNodeCellNoiseGenerator", "Cellular Fractal noise generator.", enable_fractal));

	node_options.push_back(NodeOption("Abs", "Function", "VisualNoiseNodeAbs", "Absolute value function for noise."));
	node_options.push_back(NodeOption("Blend", "Function", "VisualNoiseNodeBlend", "Blend function between 2 noise sorces."));
	node_options.push_back(NodeOption("Clamp", "Function", "VisualNoiseNodeClamp", "Clamp function for noise."));
	node_options.push_back(NodeOption("Invert", "Function", "VisualNoiseNodeInvert", "Absolute value function for noise."));
	node_options.push_back(NodeOption("Threshold", "Function", "VisualNoiseNodeThreshold", "Absolute value function for noise."));
	node_options.push_back(NodeOption("Normalize", "Function", "VisualNoiseNodeNormalize", "Make the noise be between 0 and 1."));

	node_options.push_back(NodeOption("Scalar Add", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar add operator.", VisualNoiseNodeScalarOperator::OP_ADD));
	node_options.push_back(NodeOption("Scalar Sub", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar subtraction operator.", VisualNoiseNodeScalarOperator::OP_SUB));
	node_options.push_back(NodeOption("Scalar Mul", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar multiply operator.", VisualNoiseNodeScalarOperator::OP_MUL));
	node_options.push_back(NodeOption("Scalar Div", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar division operator.", VisualNoiseNodeScalarOperator::OP_DIV));
	node_options.push_back(NodeOption("Scalar Mod", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar module operator.", VisualNoiseNodeScalarOperator::OP_MOD));
	node_options.push_back(NodeOption("Scalar Pow", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar pow operator.", VisualNoiseNodeScalarOperator::OP_POW));
	node_options.push_back(NodeOption("Scalar Max", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar max operator.", VisualNoiseNodeScalarOperator::OP_MAX));
	node_options.push_back(NodeOption("Scalar Min", "Scalar Operator", "VisualNoiseNodeScalarOperator", "Scalar min operator.", VisualNoiseNodeScalarOperator::OP_MIN));

	node_options.push_back(NodeOption("Add", "Noise Operator", "VisualNoiseNodeOperator", "Add 2 noise sources.", VisualNoiseNodeOperator::OP_ADD));
	node_options.push_back(NodeOption("Sub", "Noise Operator", "VisualNoiseNodeOperator", "Subtact 2 noise sources.", VisualNoiseNodeOperator::OP_SUB));
	node_options.push_back(NodeOption("Mul", "Noise Operator", "VisualNoiseNodeOperator", "Multiply 2 noise sources.", VisualNoiseNodeOperator::OP_MUL));
	node_options.push_back(NodeOption("Pow", "Noise Operator", "VisualNoiseNodeOperator", "Raise a noise source to the power of other noise source.", VisualNoiseNodeOperator::OP_POW));
	node_options.push_back(NodeOption("Max", "Noise Operator", "VisualNoiseNodeOperator", "Max between 2 noise sources.", VisualNoiseNodeOperator::OP_MAX));
	node_options.push_back(NodeOption("Min", "Noise Operator", "VisualNoiseNodeOperator", "Min between 2 noise sources.", VisualNoiseNodeOperator::OP_MIN));

	_update_options_menu();

}

// VisualNoiseEditorPlugin

void VisualNoiseEditorPlugin::edit(Object *p_object) {
    NoiseGraph *g = Object::cast_to<NoiseGraph>(p_object);
	noise_editor->edit(g);
}

bool VisualNoiseEditorPlugin::handles(Object *p_object) const {
	return p_object->is_class("NoiseGraph");
}

void VisualNoiseEditorPlugin::make_visible(bool p_visible) {
    if (p_visible) {
        button->show();
		EditorNode::get_singleton()->make_bottom_panel_item_visible(noise_editor);
		noise_editor->set_process_input(true);

	} else {
		if (noise_editor->is_visible_in_tree()) {
			EditorNode::get_singleton()->hide_bottom_panel();
		}
        button->hide();
		noise_editor->set_process_input(false);
	}
}

VisualNoiseEditorPlugin::VisualNoiseEditorPlugin() {
    noise_editor = memnew(VisualNoiseEditor);
    noise_editor->set_custom_minimum_size(Size2(0, 300) * EDSCALE);
    button = EditorNode::get_singleton()->add_bottom_panel_item(TTR("Noise"), noise_editor);
	button->hide();
}

VisualNoiseEditorPlugin::~VisualNoiseEditorPlugin() {

}


void VisualNoiseNodePortPreview::update_preview() {
	if (!is_visible_in_tree()) {
		return;
	}
	if (texture_rect) {
		Ref<NoiseBuffer> value = node->get_output_value();
		ERR_FAIL_NULL(value);
		texture_rect->set_texture(value->get_texture());
	} else {
		real_t value = node->get_output_value();
		label->set_text("output: " + String::num(value, 4));
	}
}

void VisualNoiseNodePortPreview::_notification(int p_what) {
}

void VisualNoiseNodePortPreview::_bind_methods() {
}

void VisualNoiseNodePortPreview::setup(Ref<VisualNoiseNode> p_node) {
	node = p_node;

	VisualNoiseNode::PortType pt = node->get_output_port_type(0);
	if (pt == VisualNoiseNode::PortType::PORT_TYPE_NOISE) {
		texture_rect = memnew(TextureRect);
		add_child(texture_rect);
	} else {
		label = memnew(Label);
		label->set_horizontal_alignment(HORIZONTAL_ALIGNMENT_LEFT);
		add_child(label);
	}
}

VisualNoiseNodePortPreview::VisualNoiseNodePortPreview() {
	
}
