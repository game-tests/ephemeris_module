// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "editor/editor_node.h"
#include "editor/editor_plugin.h"
#include <worldcompositor.h>

class GenerationEditor : public HBoxContainer {

	GDCLASS(GenerationEditor, HBoxContainer);

	friend class GenerationPlugin;

	Button *generate_button;
	Button *rebuild_button;
	Button *configure_button;
	Button *clear_button;
	Button *validate_button;

public:
	GenerationEditor();
};

class GenerationPlugin : public EditorPlugin {

	GDCLASS(GenerationPlugin, EditorPlugin);

	GenerationEditor *gen_editor = nullptr;

	Node *edited_node = nullptr;

	Ref<WorldCompositor> compositor;

	bool is_generating = false;

protected:
	virtual String get_name() const override { return "GenerationPlugin"; }
	bool has_main_screen() const override { return false; }
	virtual void edit(Object *p_object) override;
	virtual bool handles(Object *p_object) const override;
	virtual void make_visible(bool p_visible) override;
	virtual EditorPlugin::AfterGUIInput forward_spatial_gui_input(Camera3D *p_camera, const Ref<InputEvent> &p_event) override;


	static void _bind_methods();

public:
	void generate();
	void rebuild();
	void on_generation_ended(Node *p_node);
	void clear_generation();
	void configure_generation();
	void validate_biome();

	static Camera3D *get_active_camera();

	GenerationPlugin();
	~GenerationPlugin();
};