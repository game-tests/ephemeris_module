// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "generationplugin.h"
#include "editor/plugins/node_3d_editor_plugin.h"


GenerationEditor::GenerationEditor() {
	add_child(memnew(VSeparator));

	generate_button = memnew(Button);
	generate_button->set_flat(true);
	generate_button->set_text(TTR("Generate"));
	generate_button->set_icon(EditorNode::get_singleton()->get_gui_base()->get_theme_icon("Shortcut", "EditorIcons"));
	add_child(generate_button);

	rebuild_button = memnew(Button);
	rebuild_button->set_flat(true);
	rebuild_button->set_text(TTR("Rebuild"));
	rebuild_button->set_icon(EditorNode::get_singleton()->get_gui_base()->get_theme_icon("Shortcut", "EditorIcons"));
	add_child(rebuild_button);

	clear_button = memnew(Button);
	clear_button->set_flat(true);
	clear_button->set_text(TTR("Clear"));
	clear_button->set_icon(EditorNode::get_singleton()->get_gui_base()->get_theme_icon("Clear", "EditorIcons"));
	add_child(clear_button);

	add_child(memnew(VSeparator));

	configure_button = memnew(Button);
	configure_button->set_flat(true);
	configure_button->set_text(TTR("Configure"));
	configure_button->set_icon(EditorNode::get_singleton()->get_gui_base()->get_theme_icon("Tools", "EditorIcons"));
	add_child(configure_button);

	validate_button = memnew(Button);
	validate_button->set_flat(true);
	validate_button->set_text(TTR("Validate"));
	validate_button->set_icon(EditorNode::get_singleton()->get_gui_base()->get_theme_icon("Tools", "EditorIcons"));
	add_child(validate_button);
}

void GenerationPlugin::generate() {
	Ref<CompositionData> data = compositor->get_composition_data();
	data->set_seed(data->get_seed() + 1);
	rebuild();
}

void GenerationPlugin::rebuild() {
	
	if (is_generating) {
		print_line("A world generation is already running");
		return;
	}
	is_generating = true;

	compositor->generate_biome(edited_node);
}

void GenerationPlugin::on_generation_ended(Node *p_node) {
	if (!p_node) {
		is_generating = false;
		return;
	}
	clear_generation();
	p_node->set_name("__procgen__");
	edited_node->add_child(p_node);
	is_generating = false;
}

void GenerationPlugin::clear_generation() {
	
	Node *root = get_editor_interface()->get_edited_scene_root();
	Node *procgen_node = root->find_child("__procgen__", true, false);
	if (!procgen_node) {
		return;
	}
	if (!procgen_node->is_inside_tree()) {
		return;
	}
	procgen_node->get_parent()->remove_child(procgen_node);
	// procgen_node->queue_delete(); // TODO
	memdelete(procgen_node);
}


void GenerationPlugin::configure_generation() {
	InspectorDock::get_inspector_singleton()->edit(compositor->get_composition_data().ptr());
}

void GenerationPlugin::validate_biome() {
	bool ok = compositor->validate_biome(edited_node);
	if (ok) {
		print_line("Validation success!");
	}
}

void GenerationPlugin::edit(Object *p_object) {
	edited_node = EditorNode::get_singleton()->get_edited_scene();
}

bool GenerationPlugin::handles(Object *p_object) const {
	Node *root = EditorNode::get_singleton()->get_edited_scene();
	return root && root->is_class("Biome");
}

void GenerationPlugin::make_visible(bool p_visible) {

	if (p_visible) {
		gen_editor->show();
	} else {
		gen_editor->hide();
	}
}

static Camera3D *active_camera = nullptr;

EditorPlugin::AfterGUIInput GenerationPlugin::forward_spatial_gui_input(Camera3D *p_camera, const Ref<InputEvent> &p_event) {
	active_camera = p_camera;
	return EditorPlugin::AFTER_GUI_INPUT_PASS;
}

Camera3D *GenerationPlugin::get_active_camera() {
	return active_camera;
}

void GenerationPlugin::_bind_methods() {
	ClassDB::bind_method(D_METHOD("generate"), &GenerationPlugin::generate);
	ClassDB::bind_method(D_METHOD("rebuild"), &GenerationPlugin::rebuild);
	ClassDB::bind_method(D_METHOD("on_generation_ended", "node"), &GenerationPlugin::on_generation_ended);
	ClassDB::bind_method(D_METHOD("clear"), &GenerationPlugin::clear);
	ClassDB::bind_method(D_METHOD("configure"), &GenerationPlugin::configure_generation);
	ClassDB::bind_method(D_METHOD("validate_biome"), &GenerationPlugin::validate_biome);
}

GenerationPlugin::GenerationPlugin() {
	compositor.instantiate();
	compositor->connect("generation_ended", callable_mp(this, &GenerationPlugin::on_generation_ended));

	gen_editor = memnew(GenerationEditor);
	Node3DEditor::get_singleton()->add_control_to_menu_panel(gen_editor);
	gen_editor->hide();

	gen_editor->generate_button->connect("pressed", callable_mp(this, &GenerationPlugin::generate));
	gen_editor->rebuild_button->connect("pressed", callable_mp(this, &GenerationPlugin::rebuild));
	gen_editor->clear_button->connect("pressed", callable_mp(this, &GenerationPlugin::clear_generation));
	gen_editor->configure_button->connect("pressed", callable_mp(this, &GenerationPlugin::configure_generation));
	gen_editor->validate_button->connect("pressed", callable_mp(this, &GenerationPlugin::validate_biome));
}

GenerationPlugin::~GenerationPlugin() {}
