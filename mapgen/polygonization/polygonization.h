// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <rawmesh.h>
#include <matrix_2d.h>
#include <vector>
#include <FastNoiseLite.h>
#include <noise_graph.h>

class  Polygonizer {
public:
	void set_chunk_size(const int p_size);
	int get_chunk_size() const;
	void set_vertex_distance(const int p_distance);
	int get_vertex_distance() const;

	void set_lod_settings(const int p_lod);

	void set_offset_gain(const real_t p_gain);
	bool get_offset_gain() const;
	// Mesh
	std::vector<RawMesh> polygonize_heightmap(Matrix<float> &p_mat) const;
	RawMesh polygonize_flat_chunk(const float p_height, const Vector2 &p_position) const;
	RawMesh marching_cubes(Ref<NoiseGraph> &p_noise, const Point2i &p_begin, const int p_height, const int p_seed, const real_t p_threshold = 0.0);

	// Collision
	// TODO walls

	static std::vector<Rect2i> subdivide_rect(const Rect2i &p_rect, const int p_count);

	Polygonizer();
	~Polygonizer();

private:
	int chunk_size = 16;
	int vertex_distance = 2;
	real_t offset_gain = 0;
	mutable FastNoiseLite offset_generator;

	_FORCE_INLINE_ real_t get_offset(const real_t x, const real_t y) const {
		return offset_generator.GetNoise<real_t>(x, y) * offset_gain;
	}

	RawMesh polygonize_matrix_region(const Matrix<float> &p_mat, const Rect2i &p_rect, const int p_separation) const;

	Vector3 vertex_interp(const float isolevel, const Vector3 &p1, const Vector3 &p2, const float valp1, const float valp2);
};