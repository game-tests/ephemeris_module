// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "polygonization.h"
#include "mapgenconstants.h"

void Polygonizer::set_chunk_size(const int p_size) {
	chunk_size = p_size;
}

int Polygonizer::get_chunk_size() const {
	return chunk_size;
}

void Polygonizer::set_vertex_distance(const int p_distance) {
	vertex_distance = p_distance;
}

int Polygonizer::get_vertex_distance() const {
	return vertex_distance;
}

void Polygonizer::set_lod_settings(const int p_lod) {
	chunk_size = Lod::get_lod_size(p_lod);
	vertex_distance = Lod::get_vertex_separation(p_lod);
	offset_gain = p_lod == 0 ? 1 : 0;
}

void Polygonizer::set_offset_gain(const real_t p_gain) {
	offset_gain = p_gain;
}

bool Polygonizer::get_offset_gain() const {
	return offset_gain;
}

std::vector<RawMesh> Polygonizer::polygonize_heightmap(Matrix<float> &p_mat) const {
	// Copy for safer multithreading
	const int _chunk_size = chunk_size;
	const int _vertex_distance = vertex_distance;

	std::vector<RawMesh> res;
	const int row_num = p_mat.rows() / _chunk_size;
	const int col_num = p_mat.columns() / _chunk_size;
	res.reserve(row_num * col_num);

	Rect2i rect;
	rect.size = Size2i(_chunk_size, _chunk_size);

	for (int i = 0; i < row_num; i ++) {
		rect.position.x = i * _chunk_size;
		for (int j = 0; j < col_num; j ++) {
			rect.position.y = j * _chunk_size;
			res.push_back(polygonize_matrix_region(p_mat, rect, _vertex_distance));
		}
	}

	return res;
}

RawMesh Polygonizer::polygonize_flat_chunk(const float p_height, const Vector2 &p_position) const {
	// Copy for safer multithreading
	int _chunk_size = chunk_size;
	int _vertex_distance = vertex_distance;

	const Matrix<float> m(_chunk_size + 1, _chunk_size + 1, p_height);
	Rect2i rect(0, 0, _chunk_size, _chunk_size);
	RawMesh res = polygonize_matrix_region(m, rect, _vertex_distance);
	// Aplly offset
	for (size_t i = 0; i < res.vertices.size(); i++) {
		res.vertices[i].x += p_position.x;
		res.vertices[i].z += p_position.y;
	}
	return res;
}


#include "mclookuptables.h"

//      4-------------5
//     /|            /|
//    / |           / |
//   /  |          /  |
//  7---+---------6   |
//  |   0---------+---1
//  |  /          |  /
//  | /           | /
//  |/            |/
//  3-------------2
//
// Z: Front - X: Right - Y: Up


Vector3 Polygonizer::vertex_interp(const float isolevel, const Vector3 &p1, const Vector3 &p2, const float valp1, const float valp2) {
	Vector3 res;
	if (Math::absf(isolevel - valp1) < 0.00001f) {
		res = p1;
	} else if (Math::absf(isolevel - valp2) < 0.00001f) {
		res = p2;
	} else if (Math::absf(valp1 - valp2) < 0.00001f) {
		res = p1;
	} else {
		const real_t t = CLAMP((isolevel - valp1) / (valp2 - valp1), 0.0, 1.0);
		res = p1.lerp(p2, t);
	}
	return res;
}

RawMesh Polygonizer::marching_cubes(Ref<NoiseGraph> &p_noise, const Point2i &p_begin, const int p_height, const int p_seed, const real_t p_threshold) {
	RawMesh ret;
	std::vector<Vector3> svertices;

	Ref<NoiseBuffer> iso_cache_a;
	Ref<NoiseBuffer> iso_cache_b;

	const int cache_row_size = static_cast<int>(chunk_size / vertex_distance);

	const Rect2i noise_rect = Rect2i(p_begin, Size2i(chunk_size + vertex_distance, chunk_size + vertex_distance));
	iso_cache_b = p_noise->generate_noise(noise_rect, p_height, vertex_distance, p_seed);

	// TODO check y p_begin in octree
	for (int y = p_height; y < p_height + chunk_size; y += vertex_distance) {

		iso_cache_a = iso_cache_b;
		iso_cache_b = p_noise->generate_noise(noise_rect, y + vertex_distance, vertex_distance, p_seed);

		int pos_a = 0;
		int pos_b = cache_row_size + 1;
		for (int x = p_begin.x; x < p_begin.x + chunk_size; x += vertex_distance) {
		for (int z = p_begin.y; z < p_begin.y + chunk_size; z += vertex_distance) {

			Vector3 p0(x,                   y,                   z                  );
			Vector3 p1(x + vertex_distance, y,                   z                  );
			Vector3 p2(x + vertex_distance, y,                   z + vertex_distance);
			Vector3 p3(x,                   y,                   z + vertex_distance);
			Vector3 p4(x,                   y + vertex_distance, z                  );
			Vector3 p5(x + vertex_distance, y + vertex_distance, z                  );
			Vector3 p6(x + vertex_distance, y + vertex_distance, z + vertex_distance);
			Vector3 p7(x,                   y + vertex_distance, z + vertex_distance);

			float v0 = iso_cache_a->get_value_linear(pos_a);
			float v1 = iso_cache_a->get_value_linear(pos_b);
			float v2 = iso_cache_a->get_value_linear(pos_b + 1);
			float v3 = iso_cache_a->get_value_linear(pos_a + 1);
			float v4 = iso_cache_b->get_value_linear(pos_a);
			float v5 = iso_cache_b->get_value_linear(pos_b);
			float v6 = iso_cache_b->get_value_linear(pos_b + 1);
			float v7 = iso_cache_b->get_value_linear(pos_a + 1);

			pos_a++;
			pos_b++;

			int vertex_flags = 0;
			vertex_flags |= (v0 < p_threshold);
			vertex_flags |= (v1 < p_threshold) << 1;
			vertex_flags |= (v2 < p_threshold) << 2;
			vertex_flags |= (v3 < p_threshold) << 3;
			vertex_flags |= (v4 < p_threshold) << 4;
			vertex_flags |= (v5 < p_threshold) << 5;
			vertex_flags |= (v6 < p_threshold) << 6;
			vertex_flags |= (v7 < p_threshold) << 7;

			int edge_flags = edge_table[vertex_flags];

			if (!edge_flags) {
				continue;
			}

			Vector3 lerp_values[12];
			if (edge_flags & 1) {
				lerp_values[0] = vertex_interp(p_threshold, p0, p1, v0, v1);
			}
			if (edge_flags & 2) {
				lerp_values[1] = vertex_interp(p_threshold, p1, p2, v1, v2);
			}
			if (edge_flags & 4) {
				lerp_values[2] = vertex_interp(p_threshold, p2, p3, v2, v3);
			}
			if (edge_flags & 8) {
				lerp_values[3] = vertex_interp(p_threshold, p3, p0, v3, v0);
			}
			if (edge_flags & 16) {
				lerp_values[4] = vertex_interp(p_threshold, p4, p5, v4, v5);
			}
			if (edge_flags & 32) {
				lerp_values[5] = vertex_interp(p_threshold, p5, p6, v5, v6);
			}
			if (edge_flags & 64) {
				lerp_values[6] = vertex_interp(p_threshold, p6, p7, v6, v7);
			}
			if (edge_flags & 128) {
				lerp_values[7] = vertex_interp(p_threshold, p7, p4, v7, v4);
			}
			if (edge_flags & 256) {
				lerp_values[8] = vertex_interp(p_threshold, p0, p4, v0, v4);
			}
			if (edge_flags & 512) {
				lerp_values[9] = vertex_interp(p_threshold, p1, p5, v1, v5);
			}
			if (edge_flags & 1024) {
				lerp_values[10] = vertex_interp(p_threshold, p2, p6, v2, v6);
			}
			if (edge_flags & 2048) {
				lerp_values[11] = vertex_interp(p_threshold, p3, p7, v3, v7);
			}

			int (&points)[16] = tri_table[vertex_flags];

			for (int point_index = 0; points[point_index] != -1; point_index += 3) {
				int index1 = points[point_index];
				int index2 = points[point_index + 1];
				int index3 = points[point_index + 2];

				Vector3 vert_1 = lerp_values[index1];
				Vector3 vert_2 = lerp_values[index2];
				Vector3 vert_3 = lerp_values[index3];

				svertices.push_back(vert_1);
				svertices.push_back(vert_2);
				svertices.push_back(vert_3);
			}
		}
		pos_a++;
		pos_b++;
		}
	}

	ret.vertices = svertices;
	return ret;
}

void split_rect(const Rect2i &p_rect, const bool p_horizontal, const int p_distance, Rect2i &r_slice, Rect2i &r_remainder) {
	r_slice = p_rect;
	r_remainder = p_rect;
	if (p_horizontal) {
		r_slice.size.x = p_distance;
		r_remainder.size.x -= p_distance;
		r_remainder.position.x = r_slice.position.x + p_distance;
	} else {
		r_slice.size.y = p_distance;
		r_remainder.size.y -= p_distance;
		r_remainder.position.y = r_slice.position.y + p_distance;
	}
}

std::vector<Rect2i> Polygonizer::subdivide_rect(const Rect2i &p_rect, const int p_count) {
	std::vector<Rect2i> res;
	res.reserve(p_count);

	res.push_back(p_rect);

	Rect2i a, b;
	while (res.size() < (size_t)p_count) {
		// Find the biggest rect
		int index = 0;
		for (size_t i = 0; i < res.size(); i++) {
			if (res[index].get_area() < res[i].get_area()) {
				index = i;
			}
		}
		// Split the rect.
		Size2i size = res[index].get_size();
		if (size.x > size.y) {
			split_rect(res[index], true, (size.x / 2), a, b);
		} else {
			split_rect(res[index], false, (size.y / 2), a, b);
		}

		// Replace rect with slices.
		res[index] = res.back();
		res.back() = a;
		res.push_back(b);
	}
	return res;
}

RawMesh Polygonizer::polygonize_matrix_region(const Matrix<float> &p_mat, const Rect2i &p_rect, const int p_separation) const
{
	RawMesh res;

	const int _chunk_size = p_rect.size.x;
	// Cache next position
	Point2i npos;
	// limits of the chunks being generated
	const Point2i chunk_end = p_rect.get_end();

	const int row_size = (_chunk_size / p_separation) + 1;

	res.vertices.reserve(pow(row_size, 2));
	res.indices.reserve(6 * pow(_chunk_size / p_separation, 2));

	// Vertex offset
	float offset = 0;

	//	  a + b		 next_a + b
	//		  *----------*
	//		  |       /  |
	//		  |     /    |
	//		  |   /	     |
	//		  | /        |
	//		  *----------*
	//  a + next_b		next_a + next_b

	// a and b are the offsets asociated to x and y
	// next_a and next_b are the offsets asociated to x+1 and y+1
	int a = -row_size;
	int b, next_a, next_b = 0;
	for (int x = p_rect.position.x; x < chunk_end.x; x = npos.x) {
		npos.x = x + p_separation;

		a += row_size;
		b = -1;
		for (int y = p_rect.position.y; y < chunk_end.y; y = npos.y) {
			npos.y = y + p_separation;
			b++;

			offset = get_offset(x, y);
			res.vertices.push_back(Vector3(x + offset, p_mat(x, y), y - offset));

			next_a = a + row_size;
			next_b = b + 1;

			// frist triangle
			res.indices.push_back(a + b);
			res.indices.push_back(next_a + b);
			res.indices.push_back(a + next_b);

			// second triangle
			res.indices.push_back(next_a + b);
			res.indices.push_back(next_a + next_b);
			res.indices.push_back(a + next_b);
		}

		offset = get_offset(x, npos.y);
		res.vertices.push_back(Vector3(x + offset, p_mat(x, npos.y), npos.y - offset));
	}

	b = 0;
	for (int y = p_rect.position.y; y <= chunk_end.y; y += p_separation) {
		offset = get_offset(npos.x, y);
		res.vertices.push_back(Vector3(npos.x + offset, p_mat(npos.x, y), y - offset));
	}

	return res;
}

Polygonizer::Polygonizer() {
	offset_generator.SetFrequency(0.1);
	offset_generator.SetFractalOctaves(1);
}

Polygonizer::~Polygonizer() {}

