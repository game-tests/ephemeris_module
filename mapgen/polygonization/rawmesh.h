// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include "scene/resources/mesh.h"
#include "scene/resources/convex_polygon_shape_3d.h"
#include "scene/resources/concave_polygon_shape_3d.h"

struct RawMesh {
	std::vector<Vector3> vertices;
	std::vector<int> indices;
	std::vector<Color> colors;

	Array get_mesh_array() const;

	Vector<Vector3> get_collision_vertices() const;
	Vector<Face3> get_faces() const;

	Ref<ConcavePolygonShape3D> get_concave_shape() const;
	
	void to_indexed();
	void to_indexed_rounded();
	void decimate(int target_count, double agressiveness=7);
	
	_FORCE_INLINE_ bool is_valid_unindexed() const {
		return (vertices.size() % 3) == 0 && vertices.size() > 2;
	}

	_FORCE_INLINE_ bool is_valid_indexed() const {
		return (indices.size() % 3) == 0 && vertices.size() > 2;
	}

	_FORCE_INLINE_ bool has_data() const {
		return vertices.size();
	}

	_FORCE_INLINE_ bool is_empty() const {
		return vertices.size() == 0;
	}

	RawMesh();
};