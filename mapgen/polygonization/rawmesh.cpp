// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "rawmesh.h"
#include <Simplify.h>

_FORCE_INLINE_ Vector3 compute_normal(const Vector3 &p_point1, const Vector3 &p_point2, const Vector3 &p_point3) {
	Vector3 normal = (p_point1 - p_point3).cross(p_point1 - p_point2);
	normal.normalize();
	return normal;
}

Array RawMesh::get_mesh_array() const {
	Array a;
	a.resize(Mesh::ARRAY_MAX);

	// Vertices
	Vector<Vector3> v;
	v.resize(vertices.size());

	// Normals
	Vector<Vector3> normals;
	normals.resize(vertices.size());

	// Index
	Vector<int> in;
	in.resize(indices.size());

	Vector3 *wv = v.ptrw();
	int *widx = in.ptrw();
	Vector3 *wn = normals.ptrw();
	
	for (size_t i = 0; i < vertices.size(); i++) {
		wv[i] = vertices[i];
	}

	for (size_t i = 0; i < indices.size(); i += 3) {
		widx[i] = indices[i];
		widx[i+1] = indices[i+1];
		widx[i+2] = indices[i+2];

		const int ia = indices[i];
		const int ib = indices[i+1];
		const int ic = indices[i+2];

		const Vector3 normal = compute_normal(vertices[ia], vertices[ib], vertices[ic]);

		// Normals
		wn[ia] += normal;
		wn[ib] += normal;
		wn[ic] += normal;
	}

	for (int i = 0; i < normals.size(); i++) {
		wn[i].normalize();
	}

	a[Mesh::ARRAY_VERTEX] = v;
	a[Mesh::ARRAY_NORMAL] = normals;
	a[Mesh::ARRAY_INDEX] = in;

	// Color
	if (colors.size()) {
		Vector<Color> c;
		c.resize(colors.size()); {
			Color *w = c.ptrw();
			for (size_t i = 0; i < colors.size(); i++) {
				w[i] = colors[i];
			}
		}
		a[Mesh::ARRAY_COLOR] = c;
	}

	return a;
}

Vector<Vector3> RawMesh::get_collision_vertices() const {
	Vector<Vector3> res;

	if (indices.empty()) {
		res.resize(vertices.size());
		Vector3 *w = res.ptrw();

		for (size_t i = 0; i < vertices.size(); i++) {
			w[i] = vertices[i];
		}
	} else {
		res.resize(indices.size());
		Vector3 *w = res.ptrw();

		for (size_t i = 0; i < indices.size(); i++) {
			w[i] = vertices[indices[i]];
		}
	}
	return res;
}

Vector<Face3> RawMesh::get_faces() const {
	const int num_faces = indices.size() / 3;
	Vector<Face3> res;
	res.resize(num_faces);
	Face3 *w = res.ptrw();
	
	int j = 0;
	for (int i = 0; i < num_faces; i++) {
		w[i].vertex[0] = vertices[indices[j++]];
		w[i].vertex[1] = vertices[indices[j++]];
		w[i].vertex[2] = vertices[indices[j++]];
	}
	return res;
}

Ref<ConcavePolygonShape3D> RawMesh::get_concave_shape() const {
	Ref<ConcavePolygonShape3D> shape;
	shape.instantiate();

	shape->set_faces(get_collision_vertices());

	return shape;
}

// TODO test Map and HashMap
void RawMesh::to_indexed() {

	std::vector<Vector3> vertex_list;
	vertices.swap(vertex_list);

	HashMap<Vector3, int> m;
	int index_counter = 0;

	for (size_t i = 0; i < vertex_list.size(); i++) {
		const Vector3 v = vertex_list[i];

		const int *index = m.getptr(v);
		if (index) {
			indices.push_back(*index);
		} else {
			m[v] = index_counter;
			vertices.push_back(v);
			indices.push_back(index_counter);
			index_counter++;
		}
	}
}

_FORCE_INLINE_ double round_to_4_decimals(const double p_n) {
	return Math::floor(p_n * 10000) / 10000;
}

void RawMesh::to_indexed_rounded() {

	if (is_empty() || indices.size()) {
		return;
	}

	std::vector<Vector3> vertex_list;
	vertices.swap(vertex_list);

	Vector3 v;
	HashMap<Vector3, int> m;
	int index_counter = 0;

	for (size_t i = 0; i < vertex_list.size(); i++) {
		const Vector3 vi = vertex_list[i];
		v.x = round_to_4_decimals(vi.x);
		v.y = round_to_4_decimals(vi.y);
		v.z = round_to_4_decimals(vi.z);

		const int *index = m.getptr(v);
		if (index) {
			indices.push_back(*index);
		} else {
			m[v] = index_counter;
			vertices.push_back(v);
			indices.push_back(index_counter);
			index_counter++;
		}
	}
}

void RawMesh::decimate(const int target_count, const double agressiveness) {
	ERR_FAIL_COND_MSG(indices.empty(), "You can't decimante a non indexed mesh");

	Simplify::Simplifier simplifier;
	simplifier.set_data(vertices, indices);
	simplifier.simplify_mesh(target_count, agressiveness);
	vertices = simplifier.get_vertices();
	indices = simplifier.get_indices();
}

RawMesh::RawMesh() {}