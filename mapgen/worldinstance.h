// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "scene/3d/node_3d.h"
#include "scene/3d/navigation_region_3d.h"
#include "scene/3d/navigation_region_3d.h"
#include "scene/3d/physics_body_3d.h"
#include "scene/3d/gpu_particles_3d.h"
#include "scene/audio/audio_stream_player.h"
#include "scene/animation/tween.h"
#include <vector>
#include <matrix_2d.h>
#include <mapgenconstants.h>

// TODO play with this to evaluate optimal value
const int PHYSICS_CHUNK_SIZE = Lod::CHUNK_SIDE_SIZE;


struct RegionData {
	Rect2i region; // TODO assign before increase in size
	std::vector<GPUParticles3D*> particles;

	std::vector<AudioStreamPlayer*> audios;
};


class WorldInstance : public Node3D {
	GDCLASS(WorldInstance, Node3D);

	friend class BiomeContext;

protected:
	static void _bind_methods();
	void _notification(int p_what);

private:
	std::vector<Ref<Resource>> resource_references;

	std::vector<RID> mesh_rids;
	std::vector<RID> visual_instances;

	std::vector<RID> multimesh_rids;
	std::vector<RID> multimesh_instances;

	std::vector<RegionData> regions;
	RegionData *active_region = nullptr;

	Node3D *physics_node = nullptr;
	Matrix<NavigationRegion3D*> chunk_map;
	Matrix<StaticBody3D*> floor_bodies;

	_FORCE_INLINE_ Vector2i chunk_index_from_pos(const Vector3 &p_pos) {
		Vector2i index = Vector2i(
		p_pos.x / PHYSICS_CHUNK_SIZE,
		p_pos.z / PHYSICS_CHUNK_SIZE);
		index.x = CLAMP(index.x, 0, chunk_map.rows() -1);
		index.y = CLAMP(index.y, 0, chunk_map.columns() -1);
		return index;
	}

	void _disable_content();
	void _enable_content();
	void _update_visibility();

public:
// Lower level methods for generation only
	void _set_active_world_dimensions(const Vector2 &p_dimensions);
	void _add_region(const Rect2i &p_region, const std::vector<GPUParticles3D*> &p_particles,
		const std::vector<AudioStreamPlayer*> &p_audios);
	void _save_resource(const Ref<Resource> &resource);
	void _set_custom_space(RID p_local_space);
	void _reparent_physics(Node *p_parent);
	void _register_mesh(RID p_mesh, RID p_instance);
	void _register_multimesh(RID p_multimesh, RID p_instance);
	void _clear_multimeshes();
	void _bake_navigation(Ref<NavigationMesh> &nav_mesh);
	NavigationRegion3D *_get_chunk_node_from_pos(const Vector3 &p_pos);
	StaticBody3D *_get_floor_body_from_pos(const Vector3 &p_pos);

// public methods
	void add_world_child(Node *p_node, const bool bake_navmesh);

	WorldInstance();
	~WorldInstance();
};