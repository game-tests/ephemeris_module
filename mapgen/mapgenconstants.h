// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/math/math_funcs.h"
#include "core/string/ustring.h"
#include "core/math/vector2i.h"
#include <bitset>

_FORCE_INLINE_ real_t vector2i_distance(const Vector2i &p_a, const Vector2i &p_b) {
	return Math::sqrt((real_t)(p_a.x - p_b.x) * (p_a.x - p_b.x) + (p_a.y - p_b.y) * (p_a.y - p_b.y));
}

using distribution_layer_t = std::bitset<4>;

namespace Lod {
	extern const int BASE_MESH_SIZE;
	extern const int BASE_VERTEX_DISTANCE;
	extern const int NUM_LODS;
	extern const int LAST_LOD;
	extern const int LAST_LOD_SIDE_SIZE;
	extern const int CHUNK_SIDE_SIZE;
	extern const int OCTREE_SIDE_SIZE;

	extern const double UPDATE_FREQ;

	_FORCE_INLINE_ int get_vertex_separation(const int p_lod) {
		return BASE_VERTEX_DISTANCE * Math::pow(2.0, static_cast<double>(p_lod));
	}

	_FORCE_INLINE_ int get_lod_size(const int p_lod) {
		return BASE_MESH_SIZE * Math::pow(2.0, static_cast<double>(p_lod));
	}

	_FORCE_INLINE_ int get_lod_item_count(const int p_lod) {
		return Math::pow(8.0, static_cast<double>(NUM_LODS - p_lod));
	}

	_FORCE_INLINE_ int get_lod_vertex_count(const int p_lod) {
		return 200 + 64 * p_lod; // TODO adjust
	}
}

#include "core/templates/list.h"

class ErrorLogger {
	String prefix = "Generator";

	struct ContextualErr {
		String context;
		List<String> errors;
	};

	// Contextual error saved before close_context
	List<String> tmp_errors;

	List<ContextualErr> context_list;

public:
	explicit operator bool() const {
		return context_list.size();
	}
	// Text displayed before every error.
	void set_prefix(const String &p_prefix);
	// Error without context.
	void push_error(const String &p_err);
	// This error will be grouped in a context after calling close_context.
	void push_contex_error(const String &p_err);

	void close_context(const String &p_context);

	void print() const;
};
