// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "compositortypes.h"
#include "core/math/math_funcs.h"


// DISTRIBUTION CONTEXT ///////////////////////////////////////////////////////

bool DistributionContext::has_data() const {
	return feature_v.size() != 0;
}

void DistributionContext::update_probabilities() {
	probabilities.resize(feature_v.size());

	float sum = 0.0;
	for (size_t i = 0; i < feature_v.size(); i++) {
		DistributionFeature *f = feature_v[i];
		probabilities[i] = f->get_spawn_chance();
		sum += f->get_spawn_chance();
	}
	// TODO
	// prevent division by 0
	if (sum == 0.0) {
		return;
	}
	// normalize
	for (size_t i = 0; i < feature_v.size(); i++) {
		probabilities[i] /= sum;
	}
}

void DistributionContext::export_resources(std::vector<Ref<Resource>> &p_res_vector) {
	for (size_t i = 0; i < feature_v.size(); i++) {
		DistributionFeature *r = feature_v[i];

		if (r->get_shape().is_valid()) {
			p_res_vector.push_back(r->get_shape());
		}
		if (r->get_mesh().is_valid()) {
			p_res_vector.push_back(r->get_mesh());
		}
	}
}



// ECOTOPE CONTEXT ////////////////////////////////////////////////////////////

void EcotopeContext::add_area_of_interest(AreaOfInterest *p_aoi) {
	AoiContext aoi(p_aoi);
	aoi_list.push_front(aoi);
}

void EcotopeContext::add_distribution_feature(DistributionFeature *p_feature) {
	const int index = static_cast<int>(p_feature->get_group_type());
	DistributionContext &distrib_context = distribution_context_v[index];
	distrib_context.feature_v.push_back(p_feature);
}

void EcotopeContext::configure_distribution_features() {
	for (int i = 0; i < 4; i++) {
		if (distribution_context_v[i].has_data()) {
			distribution_context_v[i].update_probabilities();
		}
	}
}

void EcotopeContext::export_resources(std::vector<Ref<Resource>> &p_res_vector) {
	for (DistributionContext &dc : distribution_context_v) {
		dc.export_resources(p_res_vector);
	}
}

void EcotopeContext::init(const Ref<CompositionData> &p_composition_data) {
	for (size_t i = 0; i < distribution_context_v.size(); i++) {
		FastNoiseLite &n = distribution_context_v[i].vegetation_noise;
		n.SetSeed(p_composition_data->get_seed());
		n.SetFractalGain(0.5);
		n.SetFractalOctaves(2);
		n.SetFractalLacunarity(1.6);
		n.SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2);

		FastNoiseLite &s = distribution_context_v[i].selector_noise;
		s.SetSeed(p_composition_data->get_seed());
		s.SetFractalGain(0.5);
		s.SetFractalOctaves(2);
		s.SetFractalLacunarity(1.6);
	}
}

EcotopeContext::EcotopeContext(Ecotope *p_ecotope) : ecotope(p_ecotope) {
	distribution_context_v.resize(4);

	distribution_context_v[0].group_type = DistributionFeature::GROUP_GRASS;
	distribution_context_v[0].generator.set_distribution(
		DistributionGenerator::GRASS,
		ecotope->get_grass_min_distance());
	distribution_context_v[0].mask = (ecotope->get_grass_mask() * 2) - 1;
	distribution_context_v[0].vegetation_noise.SetFrequency(ecotope->get_grass_noise_freq());
	distribution_context_v[0].selector_noise.SetFrequency(ecotope->get_grass_group_freq());

	distribution_context_v[1].group_type = DistributionFeature::GROUP_BUSH;
	distribution_context_v[1].generator.set_distribution(
		DistributionGenerator::GENERAL,
		ecotope->get_bush_min_distance());
	distribution_context_v[1].mask = (ecotope->get_bush_mask() * 2) - 1;
	distribution_context_v[1].vegetation_noise.SetFrequency(ecotope->get_bush_noise_freq());
	distribution_context_v[1].selector_noise.SetFrequency(ecotope->get_bush_group_freq());

	distribution_context_v[2].group_type = DistributionFeature::GROUP_TREE;
	distribution_context_v[2].generator.set_distribution(
		DistributionGenerator::GENERAL,
		ecotope->get_tree_min_distance());
	distribution_context_v[2].mask = (ecotope->get_tree_mask() * 2) - 1;
	distribution_context_v[2].vegetation_noise.SetFrequency(ecotope->get_tree_noise_freq());
	distribution_context_v[2].selector_noise.SetFrequency(ecotope->get_tree_group_freq());

	distribution_context_v[3].group_type = DistributionFeature::GROUP_MISC;
	distribution_context_v[3].generator.set_distribution(
		DistributionGenerator::GENERAL,
		ecotope->get_misc_min_distance());
	distribution_context_v[3].mask = (ecotope->get_misc_mask() * 2) - 1;
	distribution_context_v[3].vegetation_noise.SetFrequency(ecotope->get_misc_noise_freq());
	distribution_context_v[3].selector_noise.SetFrequency(ecotope->get_misc_group_freq());
}



// BIOME CONTEXT //////////////////////////////////////////////////////////////

// Height

void BiomeContext::add_height(const int p_x, const int p_y, const float p_val) {
	heightmap(p_x, p_y) += p_val * influence_matrix(p_x, p_y);
}

void BiomeContext::set_height(const int p_x, const int p_y, const float p_val) {
	heightmap(p_x, p_y) = p_val;
}

// Influence / vegetation

void BiomeContext::set_influence(const int p_x, const int p_y, const float p_val) {
	if (influence_matrix(p_x, p_y) > p_val) {
		influence_matrix(p_x, p_y) = p_val;
	}
}

// transition length starting from the limits of a reserved area
real_t compute_transition_length(const float p_radius) {
	return CLAMP(p_radius / 2, 20.0, 40.0);
}

// TODO
void BiomeContext::reserve_area_influence(const Vector2i &p_center, const float p_radius, const distribution_layer_t p_layers) {

	const real_t transition_length = compute_transition_length(p_radius);
	const real_t extended_radius = p_radius + transition_length;
	const Vector2i side_vect(extended_radius, extended_radius);
	
	// Get clipped area.
	const Rect2i world_rect = Rect2i(Point2i(0, 0), influence_matrix.size_vector());
	Rect2i area;
	area.position = p_center - side_vect;
	area.size = side_vect * 2;
	area = world_rect.intersection(area);

	const Vector2i limits = area.get_end();
	Vector2i pos;

	for (pos.x = area.position.x; pos.x < limits.x; pos.x++) {
		for (pos.y = area.position.y; pos.y < limits.y; pos.y++) {
			const real_t distance = vector2i_distance(p_center, pos);
			if (distance <= p_radius) {
				set_influence(pos.x, pos.y, 0.01);
				set_vegetation_influence(pos.x, pos.y, p_layers);
			} else if (distance <= extended_radius) {
				set_influence(pos.x, pos.y, (distance - p_radius) / transition_length);
			} else {
				set_influence(pos.x, pos.y, 1);
			}
		}
	}
}

void BiomeContext::reserve_path_influence(std::vector<Vector2> &p_path, const float p_width, const distribution_layer_t p_layers) {
	Vector2i pos;
	for (const Vector2 &point : p_path) {
		Vector2i limits;
		limits.x = point.x + p_width;

		for (pos.x = point.x - p_width; pos.x < limits.x; pos.x++) {
			limits.y = point.y + p_width;
			for (pos.y = point.y - p_width; pos.y < limits.y; pos.y++) {
				pos.x = CLAMP(pos.x, 0, influence_matrix.rows());
				pos.y = CLAMP(pos.y, 0, influence_matrix.columns());

				influence_matrix(pos) = 0.0;
				vegetation_influence_matrix(pos) = p_layers;
			}
		}
	}
}


// Influence / vegetation

void BiomeContext::set_vegetation_influence(const int p_x, const int p_y, const distribution_layer_t p_val) {
	vegetation_influence_matrix(p_x, p_y) &= p_val;
}

bool BiomeContext::is_vegetation_enabled(const Vector2i &p_pos, const DistributionFeature::GroupType p_group) const {
	return vegetation_influence_matrix(p_pos)[p_group];
}


// Flatten

void BiomeContext::flatten_area(const Vector2i &p_center, const float p_radius, const float p_height) {
	const real_t transition_length = compute_transition_length(p_radius);
	const real_t extended_radius = p_radius + transition_length;
	const Vector2i side_vect(extended_radius, extended_radius);
	
	// Get clipped area.
	const Rect2i world_rect = Rect2i(Point2i(0, 0), influence_matrix.size_vector());
	Rect2i area;
	area.position = p_center - side_vect;
	area.size = side_vect * 2;
	area = world_rect.intersection(area);

	const Vector2i limits = area.get_end();
	Vector2i pos;

	for (pos.x = area.position.x; pos.x < limits.x; pos.x++) {
		for (pos.y = area.position.y; pos.y < limits.y; pos.y++) {
			const real_t distance = vector2i_distance(p_center, pos);
			if (distance <= p_radius) {
				heightmap(pos.x, pos.y) = p_height;
			} else if (distance <= extended_radius) {
				heightmap(pos.x, pos.y) = Math::lerp(p_height, heightmap(pos.x, pos.y), (distance - p_radius) / transition_length);
			} else {
				// Do nothing, ourside of the area
			}
		}
	}
}

void BiomeContext::flatten_path(const std::vector<Vector2i> &p_path, const float p_width) {
	Vector2i pos;

	for (int i = 0; i < p_path.size(); i++) {
		pos = p_path[i];
		const float height = heightmap(pos.x, pos.y);

		const Vector2i limits(pos.x + (int)p_width, pos.y + (int)p_width);

		for (pos.x = pos.x - (int)p_width; pos.x < limits.x; pos.x++) {
			for (pos.y = pos.x - (int)p_width; pos.y < limits.y; pos.y++) {
				heightmap(pos.x, pos.y) = height;
			}
		}
	}
}


// State

void BiomeContext::apply_terrain_vertex_color(std::vector<RawMesh> &p_meshes) {
	// TODO cache
	Matrix<Color> color_matrix;
	color_matrix.resize(get_biome_size());

	// Define the color of each heightmap coordinate with ecotope blending.
	for (int x = 0; x < color_matrix.rows(); x++) {
		for (int y = 0; y < color_matrix.columns(); y++) {
			Color pos_color(0.0, 0.0, 0.0);
			for (int i = 0; i < ecotope_refs.size(); i++) {
				const float influence = ecotope_influences[i](x, y);
				const Color c = ecotope_refs[i]->ecotope->get_palette()->get_terrain_color();

				pos_color += (c * influence);
			}
			color_matrix(x, y) = pos_color;
		}
	}
	for (RawMesh &raw_mesh : p_meshes) {
		raw_mesh.colors.resize(raw_mesh.vertices.size());
		for (int i = 0; i < raw_mesh.colors.size(); i++) {
			Point2i p;
			p.x = CLAMP(raw_mesh.vertices[i].x, 0, color_matrix.rows() - 1);
			p.y = CLAMP(raw_mesh.vertices[i].z, 0, color_matrix.columns() - 1);

			raw_mesh.colors[i] = color_matrix(p);
		}
	}
}

void BiomeContext::apply_water_vertex_color(std::vector<RawMesh> &p_meshes) {
	const Color c = biome->get_palette()->get_sea_color();

	for (RawMesh &raw_mesh : p_meshes) {
		raw_mesh.colors.resize(raw_mesh.vertices.size());
		for (int i = 0; i < raw_mesh.colors.size(); i++) {
			raw_mesh.colors[i] = c;
		}
	}

}

void BiomeContext::add_path_point(const Vector2 &p_path_point) {
	path_points.push_back(p_path_point);
}

#include "core/math/delaunay_2d.h"

struct VecIndexCompare {
	_FORCE_INLINE_ bool operator()(const Vector2i &l, const Vector2i &r) const {
		return vertices[l.x].distance_to(vertices[l.y]) < vertices[r.x].distance_to(vertices[r.y]);
	}

	static Vector2 const * vertices;
};

Vector2 const *VecIndexCompare::vertices = nullptr;

_FORCE_INLINE_ Vector2i get_triangle_edge(const int p_edge, const Delaunay2D::Triangle &p_tri) {
	Vector2i res;
	if (p_tri.points[p_edge] < p_tri.points[p_edge + 1]) {
		res.x = p_tri.points[p_edge];
		res.y = p_tri.points[p_edge + 1];
	} else {
		res.x = p_tri.points[p_edge + 1];
		res.y = p_tri.points[p_edge];
	}
	return res;
}

void BiomeContext::generate_paths() {
	const Vector<Delaunay2D::Triangle> triangles = Delaunay2D::triangulate(path_points);

	// make edges unique.
	RBSet<Vector2i> edge_set;
	for (int i = 0; i < triangles.size(); i++) {
		const Delaunay2D::Triangle &tri = triangles[i];
		edge_set.insert(get_triangle_edge(0, tri));
		edge_set.insert(get_triangle_edge(1, tri));
		edge_set.insert(get_triangle_edge(2, tri));
	}

	Vector<Vector2i> edges;
	edges.resize(edge_set.size());
	Vector2i *edges_w = edges.ptrw();
	for (RBSet<Vector2i>::Element *E = edge_set.front(); E; E = E->next()) {
		*edges_w = E->get();
		edges_w++;
	}
	
	// sort the edges by distance
	VecIndexCompare::vertices = path_points.ptr();
	edges.sort_custom<VecIndexCompare>();

	List<Vector2i> final_edges;

	for (int i = 0; i < edges.size(); i++) {

		const Vector2i &edge = edges[i];

		// TODO check loop
		bool has_loop = false;

		if (has_loop) {
			continue;
		}

		final_edges.push_back(edge);

		if (final_edges.size() == path_points.size() - 1) {
			break;
		}
	}
	
	// TODO process edges
	// TODO move to function upper part
}

RandomPCG BiomeContext::get_new_pcg() const {
	RandomPCG pcg;
	pcg.seed(compositor_data->get_seed());
	return pcg;
}

FastNoiseLite BiomeContext::get_new_noise() const {
	FastNoiseLite noise;
	noise.SetSeed(compositor_data->get_seed());
	return noise;
}

void BiomeContext::init(const Ref<CompositionData> &p_composition_data) {
	compositor_data = p_composition_data->duplicate(); // TODO check

	for (size_t i = 0; i < ecotope_context_v.size(); i++) {
		ecotope_context_v[i].init(compositor_data);
	}

	white_noise.SetSeed(compositor_data->get_seed());
	white_noise.SetNoiseType(FastNoiseLite::NoiseType_White);

	// Ensure polygonization padding
	Vector2i world_size = compositor_data->get_world_size() + Vector2i(1, 1);
	heightmap.resize(world_size, 0.0);
	influence_matrix.resize(world_size, 1.0);
	vegetation_influence_matrix.resize(world_size, ~0);

	world->_set_active_world_dimensions(world_size);

	if (world_environment && !Engine::get_singleton()->is_editor_hint()) {
		world->add_child(world_environment->duplicate());
	}
}

BiomeContext::BiomeContext(Biome *p_biome) : biome(p_biome) {
	world = memnew(WorldInstance);
	world->_save_resource(biome->get_terrain_material_or_default());
	world->_save_resource(biome->get_water_material_or_default());

	//ShaderMaterial *sm = Object::cast_to<ShaderMaterial>(biome->get_water_material_or_default().ptr());
	//if (!sm) {
	//	sm->set_shader_param("water_color", biome->get_palette()->get_water_color());
	//}
}

BiomeContext::~BiomeContext() {
}