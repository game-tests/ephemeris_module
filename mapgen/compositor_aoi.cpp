// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "compositortypes.h"
#include <incremental_halton.h>

// TODO review values

// Number of tries before the aoi is determined to be unplaceable.
#define PLACEMENT_TRIES 12
// maximun influence for a placing area.
#define INFLUENCE_THRESHOLD 0.002

void BiomeContext::add_area_of_interest(AreaOfInterest *p_aoi) {
	AoiContext aoi(p_aoi);
	aoi_list.push_front(aoi);
}

struct AoiComparator {
	bool operator() (const AoiContext &p_a, const AoiContext &p_b) const { return p_a.instances > p_b.instances; }
};

void BiomeContext::update_aoi_instances() {

	RandomPCG pcg = get_new_pcg();
	// Assign ecotope instance value.
	for (size_t i = 0; i < ecotope_refs.size(); i++) {
		EcotopeContext *ecotope_context = ecotope_refs[i];
		Vector2i &size = ecotope_rects[i].size;
		// TODO document
		// size_ratio represents the amount of ecotope for each 1000m^2
		float size_ratio = Math::sqrt((float)size.x * size.y) / 1000;
		
		for (AoiContext &aoi : ecotope_context->aoi_list) {

			aoi.instances = pcg.random(aoi.data->get_min_instances(), aoi.data->get_max_instances());

			if (aoi.data->get_scale_with_world()) {
				aoi.instances = Math::ceil(aoi.instances * size_ratio);
			}
		}
	}
	// Assign global instance value.
	Vector2i w_size = get_biome_size();
	float worldsize_ratio = Math::sqrt((float)w_size.x * w_size.y) / 1000;
	for (AoiContext &aoi : aoi_list) {

		aoi.instances = pcg.random(aoi.data->get_min_instances(), aoi.data->get_max_instances());

		if (aoi.data->get_scale_with_world()) {
			aoi.instances = Math::ceil(aoi.instances * worldsize_ratio);
		}
	}
	assign_biome_aoi();
	// We need the aoi with higher reserved area to be placed first
	for (size_t i = 0; i < ecotope_refs.size(); i++) {
		EcotopeContext *ecotope_context = ecotope_refs[i];
		ecotope_context->aoi_list.sort_custom<AoiComparator>();
	}
}

void BiomeContext::assign_biome_aoi() {
	// Every AOI assigned to the biome
	for (const AoiContext &aoi : aoi_list) {
		// Add the aoi we are distributing to every ecotope.
		for (size_t i = 0; i < ecotope_refs.size(); i++) {
			EcotopeContext *ecotope_context = ecotope_refs[i];
			AoiContext ecotope_aoi(aoi.data);
			ecotope_context->aoi_list.push_front(ecotope_aoi);
		}
		// Distribute the instances over every ecotope.
		for (int i = 0; i < aoi.instances; i++) {
			EcotopeContext *ecotope_context = ecotope_refs[i % ecotope_refs.size()];
			ecotope_context->aoi_list[0].instances++;
		}
		// Remove the aoi where no instances were distributed.
		for (size_t i = 0; i < ecotope_refs.size(); i++) {
			EcotopeContext *ecotope_context = ecotope_refs[i];
			if (ecotope_context->aoi_list[0].instances == 0) {
				ecotope_context->aoi_list.pop_front();
			}
		}
	}
}

// Get the surface rect based on a center + radius
Rect2i area_to_rect(const Vector2i &p_center, const float p_radius) {
	Vector2 radius_vector(p_radius, p_radius);
	Rect2i area;
	area.position = p_center - radius_vector;
	area.size = radius_vector * 2;
	return area;
}

bool BiomeContext::is_region_available(const Vector2i &p_center, const float p_radius, float &r_lowest_point) const {

	Rect2i area = area_to_rect(p_center, p_radius);
	const Rect2i world_rect = Rect2i(Point2i(0, 0), influence_matrix.size_vector());
	if (!world_rect.encloses(area)) {
		return false;
	}

	const Vector2i limits = area.get_end();
	Vector2i pos = area.position;

	r_lowest_point = heightmap(pos);

	for (; pos.x < limits.x; pos.x++) {
		for (; pos.y < limits.y; pos.y++) {
			r_lowest_point = MIN(r_lowest_point, heightmap(pos));
			const real_t distance = vector2i_distance(p_center, pos);
			// We cant place in a reserved area.
			if (distance < p_radius && influence_matrix(pos) <= INFLUENCE_THRESHOLD) {
				return false;
			}
		}
	}
	return true;
}

void BiomeContext::instance_areas_of_interest(ErrorLogger &r_err) {

	RandomPCG pcg = get_new_pcg();

	for (size_t i = 0; i < ecotope_refs.size(); i++) {
		HaltonSequence seq;
		EcotopeContext *ecotope_context = ecotope_refs[i];
		Rect2i rect = ecotope_rects[i];

		for (AoiContext &aoi : ecotope_context->aoi_list) {
			const real_t radius = aoi.data->get_reserve_radius();

			for (int inst = 0; inst < aoi.instances; inst++) {
				int tries = 0;
				while (tries < PLACEMENT_TRIES) {

					seq.step();
					Vector2 center;
					center.x = rect.position.x + seq.get_x() * rect.size.x;
					center.y = rect.position.y + seq.get_y() * rect.size.y;

					real_t r_placement_heigth = 0;
					const bool can_instantiate = is_region_available(center, radius, r_placement_heigth);

					if (can_instantiate) {
						Node3D *instance = Object::cast_to<Node3D>(aoi.data->get_feature_scene()->instantiate());
						if (!instance) {
							r_err.push_error("The scene \"" + aoi.data->get_feature_scene()->get_path() +
								"\" from the AreaOfInterest \"" + aoi.data->get_name() + "\" should have a Node3D as root");
							// Don't keep trying to instance the other of the same AOI.
							inst = aoi.instances; // end for loop
							break; // out of while
						}

						// instance
						world->add_world_child(instance, false);
						Transform3D t;
						
						const float rotation = pcg.random(aoi.data->get_min_rotation().y, aoi.data->get_max_rotation().y);
						t.rotate(Vector3(0, 1, 0), rotation);
						
						Vector3 final_pos(center.x, r_placement_heigth, center.y);
						const float y_offset = pcg.random(aoi.data->get_min_y_offset(), aoi.data->get_max_y_offset());
						final_pos.y += y_offset;
						t.set_origin(final_pos);

						instance->set_transform(t);
						
						if (radius > 0.001) {
							flatten_area(center, radius, r_placement_heigth);
							reserve_area_influence(center, radius, aoi.data->get_distribution_layers());
						}
						break; // out of while
					}
					tries++;
				}
			}
		}
	}

}
