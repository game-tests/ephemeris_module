// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "worldcompositornode.h"
#include <cstddef>

#define TOP_CAST_HEIGHT 100
#define BOTTOM_CAST_HEIGHT -100

///////////////////////////////////////////////////////////////////////////////
// FORWARD DECLARATION
///////////////////////////////////////////////////////////////////////////////

void process_vegetation_chunks(BiomeContext *p_biome_context);


///////////////////////////////////////////////////////////////////////////////
// WORLDCOMPOSITORNODE METHODS
///////////////////////////////////////////////////////////////////////////////

void WorldCompositorNode::_notification(int p_what) {
	
	switch (p_what) {

		case NOTIFICATION_INTERNAL_PHYSICS_PROCESS: {

			if (!vegetation_needs_generation) {
				return;
			}
			process_vegetation();
			vegetation_needs_generation = false;
			cond.notify_one();

		} break;
		case NOTIFICATION_INTERNAL_PROCESS: {

			if (mesh_tasks.is_empty()) {
				return;
			}
			process_meshes(mesh_tasks.front()->get());
			mesh_tasks.pop_front();
			cond.notify_one();

		} break;
	}
}

void WorldCompositorNode::queue_vegetation_generation() {
	vegetation_needs_generation = true;
}

void WorldCompositorNode::queue_mesh_task(std::vector<RawMesh> p_meshes, Ref<Material> p_material) {
	MeshTask t;
	t.material = p_material;
	t.meshes = p_meshes;
	mesh_tasks.push_back(t);
}

void WorldCompositorNode::process_vegetation() {
	// Local physics
	RID local_space = PhysicsServer3D::get_singleton()->space_create();

	PhysicsServer3D::get_singleton()->space_set_active(local_space, true);

	// Add collisions to the SceneTree within ha separate space.
	biome_context->world->_reparent_physics(this);
	biome_context->world->_set_custom_space(local_space);

	process_vegetation_chunks(biome_context);

	biome_context->world->_reparent_physics(nullptr);

	PhysicsServer3D::get_singleton()->free(local_space);
}

void WorldCompositorNode::process_meshes(const MeshTask &p_task) {
	RID terrain_material_rid = p_task.material->get_rid();

	for (size_t i = 0; i < p_task.meshes.size(); i++) {
		const RawMesh &raw_mesh = p_task.meshes[i];

		if (raw_mesh.is_empty()) {
			continue;
		}

		// Create the mesh
		RID mesh_rid = RS::get_singleton()->mesh_create();

		RS::get_singleton()->mesh_add_surface_from_arrays(
			mesh_rid, RS::PRIMITIVE_TRIANGLES, raw_mesh.get_mesh_array());
		RS::get_singleton()->mesh_surface_set_material(mesh_rid, 0, terrain_material_rid);

		RID instance_rid = RS::get_singleton()->instance_create();
		RS::get_singleton()->instance_set_base(instance_rid, mesh_rid);

		biome_context->world->_register_mesh(mesh_rid, instance_rid);
	}
}

void WorldCompositorNode::set_processing_enabled(const bool p_enabled) {
	set_physics_process_internal(p_enabled);
	set_process_internal(p_enabled);
}

WorldCompositorNode::WorldCompositorNode() {
}

RID process_multimesh(std::vector<Transform3D> &transform_list, const Ref<Mesh> &mesh) {
	RID multimesh_rid = RS::get_singleton()->multimesh_create();
	RS::get_singleton()->multimesh_allocate_data(multimesh_rid, transform_list.size(), RS::MULTIMESH_TRANSFORM_3D);
	RS::get_singleton()->multimesh_set_mesh(multimesh_rid, mesh->get_rid());

	for (size_t i = 0; i < transform_list.size(); i++) {
		const Transform3D &t = transform_list[i];
		RS::get_singleton()->multimesh_instance_set_transform(multimesh_rid, i, t);
	}
	return multimesh_rid;
}

void process_feature_group(BiomeContext *p_biome_context, const EcotopeContext *p_ecotope_context, const DistributionContext &p_context, const Rect2i &p_rect, RandomPCG &p_pcg) {

	if (p_context.feature_v.empty()) {
		return;
	}
	std::vector<Vector2> points = p_context.generator.get_rect_points(p_rect);

	std::vector<std::vector<Transform3D>> distrib_elems(p_context.feature_v.size());

	// Reserve internal vectors to prevent reallocs.
	for (size_t i = 0; i < p_context.probabilities.size(); i++) {
		distrib_elems[i].reserve(p_context.probabilities[i] * points.size() * 1.1);
	}

	PhysicsDirectSpaceState3D::RayParameters ray_params;
	ray_params.from = Vector3(0, TOP_CAST_HEIGHT, 0);
	ray_params.to = Vector3(0, BOTTOM_CAST_HEIGHT, 0);

	const Point2i center_pos = p_rect.get_center();
	StaticBody3D *static_body = p_biome_context->world->_get_floor_body_from_pos(Vector3(center_pos.x, 0, center_pos.y));
	RID local_space = PhysicsServer3D::get_singleton()->body_get_space(static_body->get_rid());

	PhysicsDirectSpaceState3D *space_state = PhysicsServer3D::get_singleton()->space_get_direct_state(local_space);
 
	// Validate points
	for (const Vector2 &point : points) {

		if (!p_biome_context->is_vegetation_enabled(point, p_context.group_type)) {
			continue;
		}

		const float ecotope_influence = p_biome_context->get_ecotope_influence(p_ecotope_context, point);
		const float vegetation_value = p_context.get_vegetation_value(point);// * ecotope_influence;
		// mask + offset * number_to_make_the_mask_min_value
		// offset: when ecotope_influence == 0 -> 1; when ecotope_influence == 1 -> 0
		const float mask = p_context.mask + (1.0 - ecotope_influence) * (-1.0 - p_context.mask);
		if (vegetation_value > mask) {
			continue;
		}

		// Random normalized value for point.
		float threshold_value = (p_context.get_selector_value(point) + 1) / 2;

		// Find the feature matching the threshold_value.
		int elem_index = -1;
		for (int i = 0; i < (int)p_context.probabilities.size(); i++) {
			threshold_value -= p_context.probabilities[i];
			if (threshold_value <= 0) {
				elem_index = i;
				break;
			}
		}
		DistributionFeature *feature = p_context.feature_v[elem_index];

		// Start validation from raycast.

		// update ray check position.
		ray_params.from.x = point.x;
		ray_params.from.z = point.y;
		ray_params.to.x = point.x;
		ray_params.to.z = point.y;

		PhysicsDirectSpaceState3D::RayResult ray_result;
		bool intersected = space_state->intersect_ray(ray_params, ray_result);

		if (!intersected) {
			continue;
		}

		// Check slope.
		const float slope = Math::acos(ray_result.normal.y);
		if (slope < feature->get_min_slope() || slope > feature->get_max_slope()) {
			continue;
		}

		// Check height value.
		if (ray_result.position.y < feature->get_min_height() || ray_result.position.y > feature->get_max_height()) {
			continue;
		}

		// The position is valid!
		Transform3D t;

		// Apply transform modifications

		// Rotation.
		// https://www.reddit.com/r/godot/comments/a8q93e/orient_node_to_normal_of_ground/
		if (feature->get_align_with_floor()) {

			const Vector3 target = ray_result.position.cross(ray_result.normal);
			const Vector3 up = Vector3(0, 1, 0);
			t.set_look_at(t.origin, target, up);
			
			const float rotation = p_pcg.random(feature->get_min_rotation().y, feature->get_max_rotation().y);
			t.rotate(ray_result.normal, rotation);
		} else {
			const Vector3 min_rot = feature->get_min_rotation();
			const Vector3 max_rot = feature->get_max_rotation();

			const Vector3 rotation(
				p_pcg.random(min_rot.x, max_rot.x),
				p_pcg.random(min_rot.y, max_rot.y),
				p_pcg.random(min_rot.z, max_rot.z)
			);
			t.rotate(Vector3(1, 0, 0), rotation.x);
			t.rotate(Vector3(0, 1, 0), rotation.y);
			t.rotate(Vector3(0, 0, 1), rotation.z);
		}

		// Scale.
		const Vector3 min_scale = feature->get_min_scale();
		const Vector3 max_scale = feature->get_max_scale();

		const Vector3 scale(
			p_pcg.random(min_scale.x, max_scale.x),
			p_pcg.random(min_scale.y, max_scale.y),
			p_pcg.random(min_scale.z, max_scale.z)
		);
		t.scale(scale);

		// Position (must be the last).
		Vector3 final_pos = ray_result.position;
		const float y_offset = p_pcg.random(feature->get_min_y_offset(), feature->get_max_y_offset());
		final_pos.y += y_offset;
		t.set_origin(final_pos);

		distrib_elems[elem_index].push_back(t);
	}

	// Generate multimeshes/collisions.

	for (size_t i = 0; i < p_context.feature_v.size(); i++) {

		std::vector<Transform3D> &transform_list = distrib_elems[i];
		DistributionFeature *distrib_feature = p_context.feature_v[i];

		if (transform_list.empty()) {
			continue;
		}

		// Multimesh
		if (distrib_feature->get_mesh().is_valid()) {
			RID mm = process_multimesh(transform_list, distrib_feature->get_mesh());
			
			RID instance_rid = RS::get_singleton()->instance_create();
			RS::get_singleton()->instance_geometry_set_cast_shadows_setting(instance_rid, (RS::ShadowCastingSetting)distrib_feature->get_cast_shadow());
			if (distrib_feature->get_material_override().is_valid()) {
				RS::get_singleton()->instance_geometry_set_material_override(instance_rid, distrib_feature->get_material_override()->get_rid());
			}
			// TODO
			//RS::get_singleton()->instance_geometry_set_lod_bias(instance_rid, 0.01);
			RS::get_singleton()->instance_set_base(instance_rid, mm);
			p_biome_context->world->_register_multimesh(mm, instance_rid);
		}
		// Collision
		if (distrib_feature->get_shape().is_valid()) {

			for (size_t j = 0; j < transform_list.size(); j++) {
				uint32_t shape_owner = static_body->create_shape_owner(nullptr);
				static_body->shape_owner_add_shape(shape_owner, distrib_feature->get_shape());
				static_body->shape_owner_set_transform(shape_owner, transform_list[j]);
			}
			
		}
	}
}

void process_chunk_distribution(BiomeContext *p_biome_context, const EcotopeContext *p_context, const Rect2i &p_rect, RandomPCG &p_pcg) {

	for (size_t i = 0; i < p_context->distribution_context_v.size(); i++) {
		process_feature_group(p_biome_context, p_context, p_context->distribution_context_v[i], p_rect, p_pcg);
	}
}

void process_vegetation_chunks(BiomeContext *p_biome_context) {
	Rect2i chunk_rect;
	chunk_rect.size = Vector2i(Lod::CHUNK_SIDE_SIZE, Lod::CHUNK_SIDE_SIZE);

	RandomPCG pcg = p_biome_context->get_new_pcg();
	
	const Vector2i chunk_size = p_biome_context->get_chunk_size();
	for (int x = 0; x < chunk_size.x; x++) {
		chunk_rect.position.x = x * Lod::CHUNK_SIDE_SIZE;
		for (int y = 0; y < chunk_size.y; y++) {
			chunk_rect.position.y = y * Lod::CHUNK_SIDE_SIZE;

			std::vector<EcotopeContext*> ecotopes = p_biome_context->get_ecotopes_in_area(chunk_rect);
			
			for (int i = 0; i < ecotopes.size(); i++) {
				process_chunk_distribution(p_biome_context, ecotopes[i], chunk_rect, pcg);
			}
			
		}
	}	
}