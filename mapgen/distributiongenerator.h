// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include "core/math/vector3.h"
#include "core/math/vector2.h"
//include "core/math/transform.h"
#include "core/math/rect2.h"


class DistributionGenerator {
public:

	enum DistributionType {
		GRASS,
		GENERAL,
	};

	void set_distribution(const DistributionType p_type, const real_t p_distance);

	std::vector<Vector2> get_rect_points(const Rect2 &p_rect) const;

private:
	std::vector<Vector2> tile_distance_points;
	real_t tile_height = 0;
	real_t tile_width = 0;

	DistributionType distribution_type = DistributionType::GRASS;

public:
	DistributionGenerator();
};