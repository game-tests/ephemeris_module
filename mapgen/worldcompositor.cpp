// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "worldcompositor.h"
#include "core/string/print_string.h"
#include "ecotope.h"
#include "influence_curve.h"
#include "servers/physics_server_3d.h"
#include "scene/3d/physics_body_3d.h"
#include "scene/main/scene_tree.h"
#include <benchmark.h>
#include <thread>
#include <polygonization.h>
#include <compositortypes.h>
#include <worldcompositornode.h>
#include "scene/main/window.h"


///////////////////////////////////////////////////////////////////////////////
// WORLDCOMPOSITOR METHODS
///////////////////////////////////////////////////////////////////////////////

void WorldCompositor::generate_biome(Node *p_biome) {

	std::thread([this](Node *biome) {

		WorldInstance *world_instance = this->_generate_biome(biome);
		this->emit_signal(SNAME("generation_ended"), world_instance);

	}, p_biome).detach();

}

// TODO
#include <noise_mixer.h>

WorldInstance *WorldCompositor::_generate_biome(Node *p_biome_node) {

	ErrorLogger err;
	BiomeContext *biome_context = nullptr;

	if (!composition_data->is_valid(err)) {
		goto handle_error;
	}

	// Context creation
	biome_context = generate_biome_context(p_biome_node, err, composition_data->get_report_disabled_nodes());
	compositor_node->biome_context = biome_context;

	if (err) {
		goto handle_error;
	}

	print_line("Generation data ---------------");
	composition_data->print_data();

	biome_context->init(composition_data);

	compositor_node->set_processing_enabled(true);

	Benchmark::reset_global_accumulator();
	print_line("Generation times --------------");

	BENCHMARK_MILLIS("Process biome features") {
		process_sea(biome_context, err);
		process_biome_features(biome_context, err);

	} // BENCHMARK

	process_regions(biome_context, err);

	if (err) {
		goto handle_error;
	}
	
	BENCHMARK_MILLIS("Process heightmap") {
		process_heightmap(biome_context, err);
	} // BENCHMARK

	if (err) {
		goto handle_error;
	}

	BENCHMARK_MILLIS("Process 3D Noise") {
		process_3d(biome_context, err);
	} // BENCHMARK

	if (err) {
		goto handle_error;
	}

	BENCHMARK_MILLIS("Process Areas of Interest") {
		process_area_of_interest(biome_context, err);
	} // BENCHMARK

	if (err) {
		goto handle_error;
	}

	BENCHMARK_MILLIS("|-- Process heightmap meshes") {
		std::vector<RawMesh> heightmap_meshes;
		heightmap_meshes = polygonize_heightmap(biome_context);

		#pragma omp parallel for
		for (size_t i = 0; i < heightmap_meshes.size(); i++) {
			RawMesh &raw_mesh = heightmap_meshes[i];
			raw_mesh.decimate(Lod::get_lod_vertex_count(0));
		}

		biome_context->apply_terrain_vertex_color(heightmap_meshes);
		
		// Request the main thread helper for faster processing.
		compositor_node->queue_mesh_task(heightmap_meshes, biome_context->biome->get_terrain_material_or_default());
		wait_for_compositor_node();

		BENCHMARK_MILLIS("/--- Generate collisions") {
			for (size_t i = 0; i < heightmap_meshes.size(); i++) {
				RawMesh &raw_mesh = heightmap_meshes[i];

				const Vector3 &central_vector = raw_mesh.vertices[raw_mesh.vertices.size() >> 1];
				StaticBody3D *body = biome_context->world->_get_floor_body_from_pos(central_vector);

				Ref<Shape3D> shape = raw_mesh.get_concave_shape();
				uint32_t shape_owner = body->create_shape_owner(nullptr);
				body->shape_owner_add_shape(shape_owner, shape);
			}

		} // BENCHMARK
	} // BENCHMARK


	if (composition_data->get_navigation_mesh().is_valid() && !Engine::get_singleton()->is_editor_hint()) {

		Ref<NavigationMesh> navmesh = composition_data->get_navigation_mesh();

		BENCHMARK_MILLIS("Bake navmesh") {
			biome_context->world->_bake_navigation(navmesh);
		} // BENCHMARK
	}
	BENCHMARK_MILLIS("Generate vegetation") {
		
		// Request the main thread helper for physics processing.
		compositor_node->queue_vegetation_generation();
		wait_for_compositor_node();
	} // BENCHMARK

	Benchmark::print_global_accumulator();

handle_error:
	err.print();
	WorldInstance *res = nullptr;
	if (biome_context) {
		if (err) {
			memdelete(biome_context->world);
			biome_context->world = nullptr;
		}
		res = biome_context->world;
		memdelete(biome_context);
	}
	compositor_node->set_processing_enabled(false);
	return res;
}

bool WorldCompositor::validate_biome(Node *p_biome_node) {

	ErrorLogger err;

	if (!composition_data->is_valid(err)) {
		err.print();
		return false;
	}

	// Context creation
	BiomeContext *biome_context = generate_biome_context(p_biome_node, err, composition_data->get_report_disabled_nodes());
	memfree(biome_context);
	if (err) {
		err.print();
	}
	return !err;
}

Ref<CompositionData> WorldCompositor::get_composition_data() {
	return composition_data;
}

void WorldCompositor::set_composition_data(const Ref<CompositionData> &p_composition_data) {
	composition_data = p_composition_data;
}

void WorldCompositor::wait_for_compositor_node() const {
	std::unique_lock<std::mutex> lk(compositor_node->mutex);
	compositor_node->cond.wait(lk);
}

void WorldCompositor::_bind_methods() {
	ClassDB::bind_method(D_METHOD("generate_biome", "biome"), &WorldCompositor::generate_biome);

	ADD_SIGNAL(MethodInfo("generation_ended", PropertyInfo(Variant::OBJECT, "node", PROPERTY_HINT_RESOURCE_TYPE, "Node3D")));
}

WorldCompositor::WorldCompositor() {
	composition_data.instantiate();
	compositor_node = memnew(WorldCompositorNode);
	SceneTree *st = SceneTree::get_singleton();
	st->get_root()->add_child(compositor_node);
}

WorldCompositor::~WorldCompositor() {
	compositor_node->queue_delete();
}


bool check_component_enabled(BiomeComponent *p_component, const bool report_disabled) {
	bool enabled = p_component->get_component_enabled();
	if (report_disabled && !enabled) {
		print_line("The node " + p_component->get_name() + " is disabled!");
	}
	return enabled;
}

BiomeContext *WorldCompositor::generate_biome_context(Node *p_biome_node, ErrorLogger &r_err, const bool report_disabled) {

	Biome *biome = 	Object::cast_to<Biome>(p_biome_node);

	if (!biome) {
		r_err.push_error("the Biome node is invalid.");
		return nullptr;
	}

	if (!biome->is_valid(r_err)) {
		return nullptr;
	}

	BiomeContext *biome_context = memnew(BiomeContext(biome));

	for (int i = 0; i < biome->get_child_count(); i++) {
		Node *node = biome->get_child(i);
		{ // ENVIRONMENT
			WorldEnvironment* environment = Object::cast_to<WorldEnvironment>(node);
			if (environment) {
				biome_context->world_environment = environment;
				continue;
			}
		}
		{ // BIOME FEATURE
			BiomeFeature* biome_feature = Object::cast_to<BiomeFeature>(node);
			if (biome_feature && check_component_enabled(biome_feature, report_disabled)) {
				biome_context->biome_feature_v.push_back(biome_feature);
				continue;
			}
		}
		{ // AOI
			AreaOfInterest* area_of_interest = Object::cast_to<AreaOfInterest>(node);
			if (area_of_interest && check_component_enabled(area_of_interest, report_disabled)) {
				if (!area_of_interest->is_valid(r_err)) {
					continue;
				}
				biome_context->add_area_of_interest(area_of_interest);
				continue;
			}
		}
		Ecotope* ecotope = Object::cast_to<Ecotope>(node);
		if (ecotope && check_component_enabled(ecotope, report_disabled)) {

			if (!ecotope->is_valid(r_err)) {
				continue;
			}

			EcotopeContext ecotope_context(ecotope);

			for (int j = 0; j < node->get_child_count(); j++) {
				Node *child_node = node->get_child(j);
				{ // DISTRIBUTION
					DistributionFeature* distribution_feature = Object::cast_to<DistributionFeature>(child_node);
					if (distribution_feature && check_component_enabled(distribution_feature, report_disabled)) {
						if (!distribution_feature->is_valid(r_err)) {
							continue;
						}
						biome_context->world->_save_resource(distribution_feature->get_mesh());
						if (distribution_feature->get_material_override().is_valid()) {
							biome_context->world->_save_resource(distribution_feature->get_material_override());
						}
						ecotope_context.add_distribution_feature(distribution_feature);
						continue;
					}
				}
				{ // AOI
					AreaOfInterest* area_of_interest = Object::cast_to<AreaOfInterest>(child_node);
					if (area_of_interest && check_component_enabled(area_of_interest, report_disabled)) {
						if (!area_of_interest->is_valid(r_err)) {
							continue;
						}
						ecotope_context.add_area_of_interest(area_of_interest);
						continue;
					}
				}
				{ // PARTICLES
					GPUParticles3D* particles = Object::cast_to<GPUParticles3D>(child_node);
					if (particles) {
						ecotope_context.particles.push_back(particles);
						continue;
					}
				}
				{ // AUDIO
					AudioStreamPlayer* audio = Object::cast_to<AudioStreamPlayer>(child_node);
					if (audio) {
						ecotope_context.sounds.push_back(audio);
						continue;
					}
				}
			}

			ecotope_context.configure_distribution_features();
			biome_context->ecotope_context_v.push_back(ecotope_context);

			continue;
		}
		
	}

	// TODO adapt for no ecotope?
	if (!biome_context->has_ecotopes()) {
		r_err.push_error("The Biome \"" + biome->get_name() + "\" doesn't have valid ecotopes.");
	}

	return biome_context;
}


void WorldCompositor::process_biome_features(BiomeContext *p_biome_context, ErrorLogger &r_err) const {
	for (BiomeFeature *bf : p_biome_context->biome_feature_v) {
		// TODO
		bf->process_biome(p_biome_context->heightmap, p_biome_context->compositor_data->get_seed());
	}
}


void WorldCompositor::process_regions(BiomeContext *p_biome_context, ErrorLogger &r_err) const {
	// Choose which ecotopes use in the biome.
	p_biome_context->choose_ecotopes();
	// Choose the surface ocupied by each ecotope.
	p_biome_context->update_ecotope_rects();
	p_biome_context->update_ecotope_influences();
	p_biome_context->save_ecotope_resources();
	print_line("The biome will generate " + String::num_int64(p_biome_context->ecotope_rects.size()) + " ecotope(s).");
}


void WorldCompositor::process_area_of_interest(BiomeContext *p_biome_context, ErrorLogger &r_err) const {
	// Assign AreaOfInterest from biome to ecotopes
	p_biome_context->update_aoi_instances();

	p_biome_context->instance_areas_of_interest(r_err);
}


std::vector<RawMesh> WorldCompositor::polygonize_heightmap(BiomeContext *p_biome_context) const {
	Polygonizer polygonizer;
	polygonizer.set_lod_settings(0);
	return polygonizer.polygonize_heightmap(p_biome_context->heightmap);
}


void WorldCompositor::process_heightmap(BiomeContext *p_biome_context, ErrorLogger &r_err) const {

	Rect2i limits(Vector2i(0, 0), p_biome_context->get_biome_size());
	std::vector<Rect2i> rects(p_biome_context->ecotope_rects.size());

	for (size_t i = 0; i < rects.size(); i++) {
		Rect2i rect = p_biome_context->ecotope_rects[i];
		rect.size += Vector2i(1, 1);

		// Expand for ecotope height overlap.
		rects[i] = rect.grow(p_biome_context->biome->get_ecotope_blend_length() / 2).intersection(limits);
	}

	for (size_t i = 0; i < rects.size(); i++) {
		Rect2i rect = rects[i];

		Ecotope *ecotope = p_biome_context->ecotope_refs[i]->ecotope;
		Ref<NoiseBuffer> buffer = ecotope->generate_noise_region(
			rect, p_biome_context->compositor_data->get_seed());
		
		if (buffer.is_null()) {
			r_err.push_error("The Ecotope \"" + ecotope->get_name() + "\" doesn't return a valid noise region.");
			return;
		}
		if (buffer->get_size() != rect.size) {
			r_err.push_error("The Ecotope \"" + ecotope->get_name() + "\" returns a noise region of size " +
				(String)buffer->get_size() + " when size " + (String)rect.size + " is expected.");
			return;
		}
		// Add buffer to the heightmap.
		const Vector2i end = rect.size;
		Vector2i heightmap_pos; 
		for (int x = 0; x < end.x; x++) {
			heightmap_pos.x = rect.position.x + x;
			for (int y = 0; y < end.y; y++) {
				heightmap_pos.y = rect.position.y + y;
				const float v = buffer->get_value(x, y) * p_biome_context->ecotope_influences[i](heightmap_pos);
				p_biome_context->add_height(heightmap_pos.x, heightmap_pos.y, v);
			}
		}
	}
}


void WorldCompositor::process_3d(BiomeContext *p_biome_context, ErrorLogger &r_err) const {
	// Multiplier for chunk size
	const int size_multiplier = 1;
	const int side_size = Lod::CHUNK_SIDE_SIZE * size_multiplier;

	const int base_height = -10;
	const int height_chunks = 4 / size_multiplier;
	const int vertex_distance = 8 * size_multiplier;
	const int limit_height = base_height + (side_size * height_chunks);

	const Vector2i chunk_size = p_biome_context->get_chunk_size() /  size_multiplier;

	std::vector<RawMesh> heightmap_meshes;
	heightmap_meshes.resize(chunk_size.x * chunk_size.y * height_chunks);

	// TODO change
	// The first use of this class is in a parallel for so it wont initialize correctly.
	NoiseMixer::initialize_class();

	// Initialize the curve cache.
	for (int i = 0; i < p_biome_context->ecotope_refs.size(); i++) {
		Ref<InfluenceCurve> ic = p_biome_context->ecotope_refs[i]->ecotope->get_height_curve();
		if (ic.is_valid()) {
			ic->set_height_range(base_height, limit_height);
		}
	}

#pragma omp parallel
{
	Rect2i chunk_rect;
	chunk_rect.size = Vector2i(side_size, side_size);
	
	Polygonizer polygonizer;
	polygonizer.set_vertex_distance(vertex_distance);
	polygonizer.set_chunk_size(side_size);

	// Copy the ecotope refs in a Vector so we can use find() and obtain the index.
	Vector<EcotopeContext*> ref_data;
	ref_data.resize(p_biome_context->ecotope_refs.size());
	for (int i = 0; i < p_biome_context->ecotope_refs.size(); i++) {
		ref_data.set(i, p_biome_context->ecotope_refs[i]);
	}
	// Copy every ecotope NoiseGraph in each thread.
	std::vector<Ref<NoiseGraph>> graph_copies;
	graph_copies.reserve(p_biome_context->ecotope_refs.size());
	for (int i = 0; i < p_biome_context->ecotope_refs.size(); i++) {
		const Ecotope *e = p_biome_context->ecotope_refs[i]->ecotope;
		Ref<NoiseGraph> copy = e->get_noise_graph()->duplicate(true);
		graph_copies.push_back(copy);
	}

	#pragma omp for
	for (int x = 0; x < chunk_size.x; x++) {
		chunk_rect.position.x = x * side_size;
		for (int y = 0; y < chunk_size.y; y++) {
			int idx = height_chunks * x * chunk_size.x + y * height_chunks;
			chunk_rect.position.y = y * side_size;

			std::vector<EcotopeContext*> ecotopes = p_biome_context->get_ecotopes_in_area(chunk_rect);
			bool is_3d_enabled = false;

			NoiseMixer *mixer_ptr = memnew(NoiseMixer);
			mixer_ptr->set_height_range(base_height, limit_height);
			const real_t target_threshold = mixer_ptr->get_target_threshold();

			for (int i = 0; i < ecotopes.size(); i++) {
				Ecotope *ec = ecotopes[i]->ecotope;

				Ref<InfluenceCurve> curve = ec->get_height_curve();
				Ref<NoiseGraph> noise3d = graph_copies[ref_data.find(ecotopes[i])];

				if (noise3d.is_null() || curve.is_null()) {
					continue;
				}

				is_3d_enabled = true;

				Matrix<float>* ec_influence = &p_biome_context->ecotope_influences[p_biome_context->get_ecotope_index(ecotopes[i])];
				Matrix<float>* global_influence = &p_biome_context->influence_matrix;
				mixer_ptr->add_graph(noise3d, curve, ec_influence, global_influence, ec->get_3d_threshold());
			}
			Ref<NoiseGraph> noise_mixer = Object::cast_to<NoiseGraph>(mixer_ptr);

			if (!is_3d_enabled) {
				continue;
			}


			for (int height = base_height; height < limit_height; height += side_size) {
				RawMesh rm = polygonizer.marching_cubes(noise_mixer, chunk_rect.position, height,
					p_biome_context->compositor_data->get_seed(), target_threshold);

				if (!rm.has_data()) {
					idx++;
					continue;
				}

				rm.to_indexed_rounded();
				heightmap_meshes[idx] = rm;
				idx++;
			}
		}
	}
} // Parallel region

	if (!heightmap_meshes.empty()) {
		// TODO check
		p_biome_context->apply_terrain_vertex_color(heightmap_meshes);
		compositor_node->queue_mesh_task(heightmap_meshes, p_biome_context->biome->get_terrain_material_or_default());
		wait_for_compositor_node();
	}
}

void WorldCompositor::process_sea(BiomeContext *p_biome_context, ErrorLogger &r_err) const {
	if (!p_biome_context->biome->get_sea_enabled()) {
		return;
	}
	const real_t sea_level = p_biome_context->biome->get_sea_level();

	int sea_chunk_size = 64;
	int water_tri_size = 8;
	int extra_border_chunks = 10;

	const Vector2i sea_chunks = p_biome_context->get_biome_size() / sea_chunk_size;
	std::vector<RawMesh> sea_meshes;
	sea_meshes.reserve((extra_border_chunks * sea_chunks.x) * (extra_border_chunks * sea_chunks.y));

	Polygonizer polygonizer;
	polygonizer.set_vertex_distance(water_tri_size);
	polygonizer.set_chunk_size(sea_chunk_size);
	for (int x = -extra_border_chunks; x < sea_chunks.x + extra_border_chunks; x++) {
		for (int y = -extra_border_chunks; y < sea_chunks.y + extra_border_chunks; y++) {
			RawMesh m = polygonizer.polygonize_flat_chunk(sea_level, Vector2(x, y) * sea_chunk_size);
			sea_meshes.push_back(m);
		}
	}

	compositor_node->queue_mesh_task(sea_meshes, p_biome_context->biome->get_water_material_or_default());
	wait_for_compositor_node();
}