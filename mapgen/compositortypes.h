// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "scene/3d/gpu_particles_3d.h"
#include "scene/audio/audio_stream_player.h"
#include "scene/3d/physics_body_3d.h"
#include "scene/3d/world_environment.h"
#include <vector>
#include <FastNoiseLite.h>
#include <matrix_2d.h>
#include <distributiongenerator.h>
#include <distributionfeature.h>
#include <ecotope.h>
#include <aoi.h>
#include <biome.h>
#include <biomefeature.h>
#include <worldinstance.h>
#include <compositiondata.h>
#include <rawmesh.h>


// AOI CONTEXT ///////////////////////////////////////////////////////
struct AoiContext {
	AreaOfInterest *data = nullptr;
	int instances = 0;

	AoiContext(AreaOfInterest *p_aoi) : data(p_aoi) {
	}

	AoiContext() {}
};

// DISTRIBUTION CONTEXT ///////////////////////////////////////////////////////

// It represents all the DistributionFeature of the same group for an Ecotope.
struct DistributionContext {
	DistributionGenerator generator;

	DistributionFeature::GroupType group_type;

	// One probability for every DistributionFeature 
	std::vector<DistributionFeature*> feature_v;
	std::vector<float> probabilities;

	// Mask to threshold the distribution surface of the group.  
	float mask = 0;

	bool has_data() const;

	// The sum of all 
	void update_probabilities();

	void export_resources(std::vector<Ref<Resource>> &p_res_vector);

	// Vegetation distribution function
	mutable FastNoiseLite vegetation_noise;
	mutable FastNoiseLite selector_noise;
	
	_FORCE_INLINE_ float get_vegetation_value(const Point2i &p_point) const {
		return vegetation_noise.GetNoise<real_t>(p_point.x, p_point.y);
	}

	_FORCE_INLINE_ float get_selector_value(const Point2i &p_point) const {
		return selector_noise.GetNoise<real_t>(p_point.x, p_point.y);
	}
};



// ECOTOPE CONTEXT ////////////////////////////////////////////////////////////

struct EcotopeContext {
	Ecotope *ecotope;

	std::vector<DistributionContext> distribution_context_v;

	List<AoiContext> aoi_list;

	std::vector<GPUParticles3D*> particles;

	std::vector<AudioStreamPlayer*> sounds;


	void add_area_of_interest(AreaOfInterest *p_aoi);

	void add_distribution_feature(DistributionFeature *p_feature);

	void configure_distribution_features();

	void export_resources(std::vector<Ref<Resource>> &p_res_vector);

	void init(const Ref<CompositionData> &p_composition_data);

	EcotopeContext(Ecotope *p_ecotope);
};


// BIOME CONTEXT //////////////////////////////////////////////////////////////

class BiomeContext {
public:
	Biome *biome = nullptr;
	Ref<CompositionData> compositor_data;

	// Environment
	WorldEnvironment *world_environment = nullptr;
	
	// Features
	std::vector<BiomeFeature*> biome_feature_v;

	// Height
	Matrix<float> heightmap;
	
	void add_height(const int p_x, const int p_y, const float p_val);
	void set_height(const int p_x, const int p_y, const float p_val);
	
	// Influence / vegetation
	Matrix<float> influence_matrix;

	void set_influence(const int p_x, const int p_y, const float p_val);

	void reserve_area_influence(const Vector2i &p_center, const float p_radius, const distribution_layer_t p_layers);
	void reserve_path_influence(std::vector<Vector2> &p_path, const float p_width, const distribution_layer_t p_layers);

	bool is_region_available(const Vector2i &p_center, const float p_radius, float &r_lowest_point) const;

	Matrix<distribution_layer_t> vegetation_influence_matrix;

	void set_vegetation_influence(const int p_x, const int p_y, const distribution_layer_t p_val);
	bool is_vegetation_enabled(const Vector2i &p_pos, const DistributionFeature::GroupType p_group) const;

	// Flatten
	void flatten_area(const Vector2i &p_center, const float p_radius, const float p_height);
	void flatten_path(const std::vector<Vector2i> &p_path, const float p_width);

	// Ecotope
	std::vector<EcotopeContext> ecotope_context_v;

	bool has_ecotopes() const;


	// list of ecotope regions instanced in the world (chunk based size).
	std::vector<Rect2i> ecotope_rects;
	// references to ecotope_context_v, each index correlates with ecotope_rects.
	std::vector<EcotopeContext*> ecotope_refs;
	// influence matrix for each ecotope in the whole world.
	std::vector<Matrix<float>> ecotope_influences;

	Matrix<Color> terrain_vertex_color;
	Matrix<Color> water_vertex_color;

	// Gen utils
	void choose_ecotopes();
	void update_ecotope_rects();
	void save_ecotope_resources();
	void update_ecotope_influences();
	void apply_ecotope_blending(Matrix<float> &p_influence, const Rect2i &p_rect);
	std::vector<EcotopeContext*> get_ecotopes_in_area(const Rect2i &p_rect) const;
	Ecotope *get_ecotope_from_pos(const Vector3 &p_pos);
	int get_ecotope_index(EcotopeContext *p_ecotope) const;
	float get_ecotope_influence(const EcotopeContext* p_ecotope, const Point2 &p_point) const;
	void apply_terrain_vertex_color(std::vector<RawMesh> &p_meshes);
	void apply_water_vertex_color(std::vector<RawMesh> &p_meshes);

	Vector2i get_chunk_size() const {
		return heightmap.size_vector() / Lod::CHUNK_SIDE_SIZE;
	}

	Vector2i get_biome_size() const {
		return heightmap.size_vector();
	}

	// State
	PackedVector2Array path_points;

	void add_path_point(const Vector2 &p_path_point);
	void generate_paths();

	// AOI
	List<AoiContext> aoi_list;

	void add_area_of_interest(AreaOfInterest *p_aoi);

	// Decide how many instances will be placed.
	void update_aoi_instances();
	// Distribute biome areas of interes into the available ecotopes. (called by update_aoi_instances)
	void assign_biome_aoi();
	// 
	void instance_areas_of_interest(ErrorLogger &r_err);

	// Number generation
	mutable FastNoiseLite white_noise;
	
	RandomPCG get_new_pcg() const;
	FastNoiseLite get_new_noise() const;

	_FORCE_INLINE_ float get_rand_value(const Point2i &p_point) const {
		return white_noise.GetNoise<real_t>(p_point.x, p_point.y);
	}

	WorldInstance *world = nullptr;

	void init(const Ref<CompositionData> &p_composition_data);

	BiomeContext(Biome *p_biome);
	~BiomeContext();
};
