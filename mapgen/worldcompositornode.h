// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once


#include "scene/main/node.h"
#include <compositortypes.h>
#include <condition_variable>

class WorldCompositor;

struct MeshTask {
	std::vector<RawMesh> meshes;
	Ref<Material> material;
};

class WorldCompositorNode : public Node {

	GDCLASS(WorldCompositorNode, Node);

	friend class WorldCompositor;

protected:
	//static void _bind_methods();
	void _notification(int p_what);

public:

	void set_processing_enabled(const bool p_enabled);

	WorldCompositorNode();

private:
	void process_vegetation();
	void process_meshes(const MeshTask &p_task);
	
	void queue_vegetation_generation();
	void queue_mesh_task(std::vector<RawMesh> p_meshes, Ref<Material> p_material);

	std::condition_variable cond;
	std::mutex mutex;


	BiomeContext *biome_context = nullptr;
	Node *collisions = nullptr;

	List<MeshTask> mesh_tasks;
	bool vegetation_needs_generation = false;
};
