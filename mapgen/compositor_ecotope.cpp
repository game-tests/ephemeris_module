// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "compositortypes.h"
#include "core/math/rect2i.h"
#include "core/math/vector2i.h"
#include "core/string/print_string.h"
#include <polygonization.h>

bool BiomeContext::has_ecotopes() const {
	return ecotope_context_v.size() != 0;
}


template<class T>
void shuffle(std::vector<T> &p_v, const int seed) {

	RandomPCG rng(seed);

	const int n = p_v.size();
	if (n < 2) {
		return;
	}
	for (int i = n - 1; i >= 1; i--) {
		const int j = rng.rand() % (i + 1);
		const T tmp = p_v[j];
		p_v[j] = p_v[i];
		p_v[i] = tmp;
	}
}

void BiomeContext::choose_ecotopes() {

	std::vector<EcotopeContext*> forced_ecotopes;
	std::vector<EcotopeContext*> optional_ecotopes;

	for (EcotopeContext &ec : ecotope_context_v) {
		if (ec.ecotope->get_force_generation()) {
			forced_ecotopes.push_back(&ec);
		} else {
			optional_ecotopes.push_back(&ec);
		}
	}

	shuffle(forced_ecotopes, compositor_data->get_seed());
	shuffle(optional_ecotopes, compositor_data->get_seed());

	size_t prefered_count = Math::floor(heightmap.size() / Math::pow((double)biome->get_ecotope_surface(), 2.0));
	prefered_count = MIN(MAX(prefered_count, 1), ecotope_context_v.size());

	ecotope_refs.clear();
	ecotope_refs.reserve(prefered_count);

	for (size_t i = 0; i < forced_ecotopes.size() && i < prefered_count; i++) {
		ecotope_refs.push_back(forced_ecotopes[i]);
	}

	const int optional_needed = prefered_count - ecotope_refs.size();
	for (int i = 0; i < optional_needed; i++) {
		ecotope_refs.push_back(optional_ecotopes[i]);
	}

	ecotope_influences.resize(ecotope_refs.size());
	for (Matrix<float> &influence : ecotope_influences) {
		influence.resize(heightmap.size_vector());
	}
}

void BiomeContext::update_ecotope_rects() {
	const Vector2i chunkmap_size = compositor_data->get_world_size() / Lod::CHUNK_SIDE_SIZE;
	// We generate the rects at chunk scale to ensure regions cover whole mesh
	ecotope_rects = Polygonizer::subdivide_rect(Rect2i(Vector2i(0, 0), chunkmap_size), ecotope_refs.size());

	for (Rect2i &r : ecotope_rects) {
		r.position = r.position * Lod::CHUNK_SIDE_SIZE;
		r.size = r.size * Lod::CHUNK_SIDE_SIZE;
		r.size += Vector2i(1, 1);
	}
}

void BiomeContext::save_ecotope_resources() {
	for (EcotopeContext *ec : ecotope_refs) {
		ec->export_resources(world->resource_references);
	}
}


void BiomeContext::update_ecotope_influences() {
	for (int i = 0; i < ecotope_rects.size(); i++) {
		const Rect2i &r = ecotope_rects[i];
		Matrix<float> &influence_m = ecotope_influences[i];
		Vector2i pos;
		// Set the inner of the region of each ecotope to max influence.
		for (int x = 0; x < r.size.x; x++) {
			pos.x = r.position.x + x;
			for (int y = 0; y < r.size.y; y++) {
				pos.y = r.position.y + y;

				influence_m(pos) = 1.0;
			}
		}
		apply_ecotope_blending(influence_m, r);

	}
}


void BiomeContext::apply_ecotope_blending(Matrix<float> &p_influence, const Rect2i &p_rect) {
	const int transition_length = biome->get_ecotope_blend_length();

	Rect2i limits(Vector2i(0, 0), get_biome_size());
	Rect2i rect = p_rect.grow(transition_length / 2);

	if (rect.size.x < 2 * transition_length || rect.size.y < 2 * transition_length) {
		print_error("One regions is too small, this shouldn't happen");
		return;
	}

	const real_t step = 1.0 / transition_length;

	const int offset = transition_length;
	const int offset_end_x = rect.size.x - transition_length; // TODO X Y
	const int offset_end_y = rect.size.y - transition_length;
	const int end_x = rect.size.x;
	const int end_y = rect.size.y;

//                    offset |             | offset_end_y
//                           v     TOP     v
//                     0 0 0 | 0 0 0 0 0 0 | 0 0 0
//         TOPLEFT ->  0 1 1 | 1 1 1 1 1 1 | 1 1 0 <- TOPRIGHT
//                     0 1 2 | 2 2 2 2 2 2 | 2 1 0
//          offset ->  ————— + ——————————— + —————
//                     0 1 2 |             | 2 1 0
//                     0 1 2 |             | 2 1 0
//            LEFT ->  0 1 2 |             | 2 1 0 <- RIGHT
//                     0 1 2 |             | 2 1 0
//                     0 1 2 |             | 2 1 0
//    offset_end_x ->  ————— + ——————————— + —————
//                     0 1 2 | 2 2 2 2 2 2 | 2 1 0
//      BOTTOMLEFT ->  0 1 1 | 1 1 1 1 1 1 | 1 1 0 <- BOTTOMRIGHT
//                     0 0 0 | 0 0 0 0 0 0 | 0 0 0
//                               BOTTOM

	real_t factor = 0.0;
	// TOP
	factor = 0.0;
	for (int x = 0; x < offset; x++) {
		for (int y = offset; y < offset_end_y; y++) {
			const Vector2i p = rect.position + Vector2i(x, y);
			if (limits.has_point(p)) {
				p_influence(p) = factor;
			}
		}
		factor += step;
	}
	// BOTTOM
	factor = 1.0;
	for (int x = offset_end_x; x < end_x; x++) {
		for (int y = offset; y < offset_end_y; y++) {
			const Vector2i p = rect.position + Vector2i(x, y);
			if (limits.has_point(p)) {
				p_influence(p) = factor;
			}
		}
		factor -= step;
	}
	// LEFT
	for (int x = offset; x < offset_end_x; x++) {
		factor = 0.0;
		for (int y = 0; y < offset; y++) {
			const Vector2i p = rect.position + Vector2i(x, y);
			if (limits.has_point(p)) {
				p_influence(p) = factor;
			}
			factor += step;
		}
	}
	// RIGHT
	for (int x = offset; x < offset_end_x; x++) {
		factor = 1.0;
		for (int y = offset_end_y; y < end_y; y++) {
			const Vector2i p = rect.position + Vector2i(x, y);
			if (limits.has_point(p)) {
				p_influence(p) = factor;
			}
			factor -= step;
		}
	}

	Vector2 pos;
	Vector2 target_pos;

	// TOPLEFT
	target_pos = Vector2(offset, offset);
	for (pos.x = 0; pos.x < offset; pos.x++) {
		for (pos.y = 0; pos.y < offset; pos.y++) {
			const Vector2i p = rect.position + pos;
			if (!limits.has_point(p)) {
				continue;
			}
			factor = offset - target_pos.distance_to(pos);
			factor = factor > 0 ? (factor / offset) : 0;
			p_influence(p) = factor;
		}
	}
	// TOPRIGHT
	target_pos = Vector2(offset, offset_end_y);
	for (pos.x = 0; pos.x < offset; pos.x++) {
		for (pos.y = offset_end_y; pos.y < end_y; pos.y++) {
			const Vector2i p = rect.position + pos;
			if (!limits.has_point(p)) {
				continue;
			}
			factor = offset - target_pos.distance_to(pos);
			factor = factor > 0 ? (factor / offset) : 0;
			p_influence(p) = factor;
		}
	}
	//BOTTOMLEFT
	target_pos = Vector2(offset_end_x, offset);
	for (pos.x = offset_end_x; pos.x < end_x; pos.x++) {
		for (pos.y = 0; pos.y < offset; pos.y++) {
			const Vector2i p = rect.position + pos;
			if (!limits.has_point(p)) {
				continue;
			}
			factor = offset - target_pos.distance_to(pos);
			factor = factor > 0 ? (factor / offset) : 0;
			p_influence(p) = factor;
		}
	}
	//BOTTOMRIGHT
	target_pos = Vector2(offset_end_x, offset_end_y);
	for (pos.x = offset_end_x; pos.x < end_x; pos.x++) {
		for (pos.y = offset_end_y; pos.y < end_y; pos.y++) {
			const Vector2i p = rect.position + pos;
			if (!limits.has_point(p)) {
				continue;
			}
			factor = offset - target_pos.distance_to(pos);
			factor = factor > 0 ? (factor / offset) : 0;
			p_influence(p) = factor;
		}
	}
}


std::vector<EcotopeContext*> BiomeContext::get_ecotopes_in_area(const Rect2i &p_rect) const {
	std::vector<EcotopeContext*> res;
	// TODO
	//const Rect2i intersection = p_rect.intersection(Rect2i(Vector2i(0,0), heightmap.size_vector()));
	Point2i a = p_rect.position;
	Point2i b = p_rect.position + Point2i(p_rect.size.x, 0);
	Point2i c = p_rect.position + Point2i(0, p_rect.size.y);
	Point2i d = p_rect.get_end();
	for (int i = 0; i < ecotope_refs.size(); i++) {
		if (ecotope_influences[i](a) > 0.0 || ecotope_influences[i](b) > 0.0 ||
			ecotope_influences[i](c) > 0.0 || ecotope_influences[i](d) > 0.0) {
				res.push_back(ecotope_refs[i]);
		}
	}
	return res;
}

Ecotope *BiomeContext::get_ecotope_from_pos(const Vector3 &p_pos) {
	Vector2i index = Vector2i(p_pos.x, p_pos.z);
	index.x = CLAMP(index.x, 0, heightmap.rows() -1);
	index.y = CLAMP(index.y, 0, heightmap.columns() -1);

	float max_v = -1.0;
	Ecotope *res = nullptr;
	for (int i = 0; i < ecotope_refs.size(); i++) {
		if (ecotope_influences[i](index) > max_v) {
			max_v = ecotope_influences[i](index);
			res = ecotope_refs[i]->ecotope;
		}
	}

	return res;
}

int BiomeContext::get_ecotope_index(EcotopeContext *p_ecotope) const {
	int res = 0;
	for (int i = 0; i < ecotope_refs.size(); i++) {
		if (ecotope_refs[i] == p_ecotope) {
			res = i;
			break;
		}
	}
	return res;
}


float BiomeContext::get_ecotope_influence(const EcotopeContext* p_ecotope, const Point2 &p_point) const {
	float res = 0.0;
	for (int i = 0; i < ecotope_refs.size(); i++) {
		if (ecotope_refs[i] == p_ecotope) {
			res = ecotope_influences[i](p_point);
			break;
		}
	}
	return res; 
}