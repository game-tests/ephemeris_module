// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "compositiondata.h"

bool CompositionData::is_valid(ErrorLogger &r_err) const {
	if (world_size.x <= 0 && world_size.y <= 0) {
		r_err.push_contex_error("World size can't be 0 or less");
	}
	r_err.close_context("invalid generator configuration");
	return !r_err;
}

void CompositionData::print_data() const {
	print_line("Seed: " + String::num_int64(seed));
	print_line("World size: " + (String)world_size);
}

Vector2i CompositionData::get_world_size() const {
	return world_size;
}

void CompositionData::set_world_size(const Vector2i &p_world_size) {
	if (p_world_size.x < Lod::CHUNK_SIDE_SIZE || p_world_size.y < Lod::CHUNK_SIDE_SIZE) {
		return;
	}
	// Ensure multiple of chunk size.
	if (world_size.x > p_world_size.x) {
		world_size.x = Math::floor((double)p_world_size.x / Lod::CHUNK_SIDE_SIZE);
		world_size.x *= Lod::CHUNK_SIDE_SIZE;
	} else if (world_size.x < p_world_size.x) {
		world_size.x = Math::ceil((double)p_world_size.x / Lod::CHUNK_SIDE_SIZE);
		world_size.x *= Lod::CHUNK_SIDE_SIZE;
	}
	if (world_size.y > p_world_size.y) {
		world_size.y = Math::floor((double)p_world_size.y / Lod::CHUNK_SIDE_SIZE);
		world_size.y *= Lod::CHUNK_SIDE_SIZE;
	} else if (world_size.y < p_world_size.y) {
		world_size.y = Math::ceil((double)p_world_size.y / Lod::CHUNK_SIDE_SIZE);
		world_size.y *= Lod::CHUNK_SIDE_SIZE;
	}
}

int CompositionData::get_seed() const {
	return seed;
}

void CompositionData::set_seed(const int p_seed) {
	if (p_seed < 0) {
		return;
	}
	seed = p_seed;
}

void CompositionData::set_report_disabled_nodes(const bool p_report) {
	report_disabled_nodes = p_report;
}

bool CompositionData::get_report_disabled_nodes() const {
	return report_disabled_nodes;
}

void CompositionData::set_navigation_mesh(const Ref<NavigationMesh> &p_navmesh) {
	nav_mesh = p_navmesh;
}

Ref<NavigationMesh> CompositionData::get_navigation_mesh() const {
	return nav_mesh;
}

void CompositionData::_bind_methods() {
	ClassDB::bind_method(D_METHOD("get_world_size"), &CompositionData::get_world_size);
	ClassDB::bind_method(D_METHOD("set_world_size", "world_size"), &CompositionData::set_world_size);
	ClassDB::bind_method(D_METHOD("get_seed"), &CompositionData::get_seed);
	ClassDB::bind_method(D_METHOD("set_seed", "seed"), &CompositionData::set_seed);
	ClassDB::bind_method(D_METHOD("get_report_disabled_nodes"), &CompositionData::get_report_disabled_nodes);
	ClassDB::bind_method(D_METHOD("set_report_disabled_nodes", "report"), &CompositionData::set_report_disabled_nodes);
	ClassDB::bind_method(D_METHOD("set_navigation_mesh", "navmesh"), &CompositionData::set_navigation_mesh);
	ClassDB::bind_method(D_METHOD("get_navigation_mesh"), &CompositionData::get_navigation_mesh);

	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2I, "world_size"), "set_world_size", "get_world_size");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "seed"), "set_seed", "get_seed");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "report_disabled_nodes"), "set_report_disabled_nodes", "get_report_disabled_nodes");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "navmesh", PROPERTY_HINT_RESOURCE_TYPE, "NavigationMesh"), "set_navigation_mesh", "get_navigation_mesh");

}

CompositionData::CompositionData() {}