// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "worldinstance.h"
#include "servers/rendering_server.h"
#include "scene/main/viewport.h"
#include "scene/3d/camera_3d.h"
#ifdef TOOLS_ENABLED
#include <generationplugin.h>
#endif

// TODO
//const real_t TRANSITION_TIME = 3.0;
const real_t REGION_HEIGHT_TOP = 100.0;
const real_t REGION_HEIGHT_BOTTOM = -50.0;


void WorldInstance::_bind_methods() {
	ClassDB::bind_method(D_METHOD("add_world_child", "node", "bake_navmesh"), &WorldInstance::add_world_child, DEFVAL(true));
}

void WorldInstance::_notification(int p_what) {
	
	switch (p_what) {

		case NOTIFICATION_INTERNAL_PROCESS: {

			Camera3D *camera = nullptr;
#ifdef TOOLS_ENABLED
			if (Engine::get_singleton()->is_editor_hint()) {
				camera = GenerationPlugin::get_active_camera();
			} else {
				camera = get_viewport()->get_camera_3d();
			}
#else
			Camera3D *camera = get_viewport()->get_camera_3d();
#endif

			if (!camera) {
				return;
			}

			Vector3 camera_pos = to_local(camera->get_global_transform().origin);

			if (active_region) {
				// Update particle transforms
			}

			// TODO update every n frames
			Point2 camera_pos_2d = Point2(camera_pos.x, camera_pos.z);

			RegionData *detected_region = nullptr;
			if (camera_pos.y < REGION_HEIGHT_TOP && camera_pos.y > REGION_HEIGHT_BOTTOM) {
				for (RegionData &r : regions) {
					if (r.region.has_point(camera_pos_2d) && &r != active_region) {
						detected_region = &r;
						// TODO begin transition
					}
				}
			}

			if (detected_region != active_region) {
				if (active_region) { // Leaving
					for (GPUParticles3D *particle : active_region->particles) {
						particle->set_emitting(false);
					}
					// TODO
					//for (AudioStreamPlayer *audio : active_region->audios) {
					//	//tween->interpolate_property(audio, NodePath("volume_db"), audio->get_volume_db(), -70, TRANSITION_TIME);
					//}
				}

				if (detected_region) { // Entering
					for (GPUParticles3D *particle : detected_region->particles) {
						particle->set_emitting(true);
					}
					// TODO
					//for (AudioStreamPlayer *audio : detected_region->audios) {
					//	//tween->interpolate_property(audio, NodePath("volume_db"), audio->get_volume_db(), 0, TRANSITION_TIME);
					//}
				}
				active_region = detected_region;

				// TODO remove/add from tree

				//tween->start();
			}

			// TODO Update LOD

		} break;
		case NOTIFICATION_ENTER_WORLD: {

			_enable_content();
			_update_visibility();

		} break;
		case NOTIFICATION_TRANSFORM_CHANGED: {

			// TODO check if working
			// also check if we need to modify the position in the lod
			// update so we work with local coords
			Transform3D gt = get_global_transform();
			for (size_t i = 0; i < visual_instances.size(); i++) {
				RS::get_singleton()->instance_set_transform(visual_instances[i], gt);
			}
			for (size_t i = 0; i < multimesh_instances.size(); i++) {
				RS::get_singleton()->instance_set_transform(multimesh_instances[i], gt);
			}

		} break;
		case NOTIFICATION_EXIT_WORLD: {

			_disable_content();

		} break;
		case NOTIFICATION_VISIBILITY_CHANGED: {

			_update_visibility();

		} break;
	}
}

void WorldInstance::add_world_child(Node *p_node, const bool bake_navmesh) {
	Node3D *spatial = Object::cast_to<Node3D>(p_node);
	ERR_FAIL_COND_MSG(!spatial, "Can't add child, the node must be a Node3D");
	
	Vector3 pos;
	if (spatial->is_inside_tree()) {
		pos = to_local(spatial->get_global_transform().origin);
		p_node->get_parent()->remove_child(p_node);
	} else {
		pos = spatial->get_transform().origin;
	}
	NavigationRegion3D *r = _get_chunk_node_from_pos(pos);
	r->add_child(p_node);

	if (bake_navmesh) {
		// TODO
		//r->bake_navigation_mesh();
	}
}

void WorldInstance::_set_active_world_dimensions(const Vector2 &p_dimensions) {
	if (physics_node) {
		memdelete(physics_node);
	}
	
	Vector2i size = Vector2i(
		Math::ceil(p_dimensions.x / PHYSICS_CHUNK_SIZE),
		Math::ceil(p_dimensions.y / PHYSICS_CHUNK_SIZE));
	
	chunk_map.resize(size);
	floor_bodies.resize(size);

	physics_node = memnew(Node3D);

	for (int i = 0; i < chunk_map.size(); i++) {
		NavigationRegion3D *region = memnew(NavigationRegion3D);
		StaticBody3D *body = memnew(StaticBody3D);
		body->create_shape_owner(body);
		region->add_child(body);
		
		chunk_map(i) = region;
		floor_bodies(i) = body;
		physics_node->add_child(region);
	}

	add_child(physics_node);

	// TODO setup LOD
}

WorldInstance::WorldInstance() {
	//set_physics_process_internal(true);
	set_process_internal(true);
}

WorldInstance::~WorldInstance() {

	for (size_t i = 0; i < visual_instances.size(); i++) {
		RS::get_singleton()->free(visual_instances[i]);
	}
	visual_instances.clear();

	for (size_t i = 0; i < mesh_rids.size(); i++) {
		RS::get_singleton()->free(mesh_rids[i]);
	}
	mesh_rids.clear();

	_clear_multimeshes();

	resource_references.clear();
}






// UTILITY

const int REGION_MARGIN = 10;

void WorldInstance::_add_region(const Rect2i &p_region, const std::vector<GPUParticles3D*> &p_particles,
		const std::vector<AudioStreamPlayer*> &p_audios)
{
	RegionData data;
	// TODO review
	// we make the region smaller to prevent fast changes in the limits of the regions.
	data.region = p_region.grow(-REGION_MARGIN);
	data.particles = p_particles;
	data.audios = p_audios;
	for (GPUParticles3D *particle : data.particles) {
		particle->set_emitting(false);
	}
	for (AudioStreamPlayer *audio : data.audios) {
		audio->set_volume_db(-70);
	}
	regions.push_back(data);
}

void WorldInstance::_save_resource(const Ref<Resource> &p_resource) {
	resource_references.push_back(p_resource);
}

void WorldInstance::_set_custom_space(RID p_space) {

	for (NavigationRegion3D* region : chunk_map) {
		StaticBody3D *body = (StaticBody3D*)region->get_child(0);
		PhysicsServer3D::get_singleton()->body_set_space(body->get_rid(), p_space);
	}
}

void WorldInstance::_reparent_physics(Node *p_parent) {
	if (!chunk_map.size()) {
		return;
	}

	Node *nav_parent = physics_node->get_parent();

	if (nav_parent == p_parent) {
		return;
	}

	if (nav_parent) {
		nav_parent->remove_child(physics_node);
	}
	if (p_parent) {
		p_parent->add_child(physics_node);
	} else { // we restore default state
		add_child(physics_node);
	}
}

void WorldInstance::_register_mesh(RID p_mesh, RID p_instance) {
	mesh_rids.push_back(p_mesh);
	visual_instances.push_back(p_instance);
}

void WorldInstance::_register_multimesh(RID p_multimesh, RID p_instance) {
	multimesh_rids.push_back(p_multimesh);
	multimesh_instances.push_back(p_instance);
}

void WorldInstance::_clear_multimeshes() {
	for (size_t i = 0; i < multimesh_instances.size(); i++) {
		RS::get_singleton()->free(multimesh_instances[i]);
	}
	multimesh_instances.clear();

	for (size_t i = 0; i < multimesh_rids.size(); i++) {
		RS::get_singleton()->free(multimesh_rids[i]);
	}
	multimesh_rids.clear();
}

void WorldInstance::_bake_navigation(Ref<NavigationMesh> &nav_mesh) {
	for (NavigationRegion3D* &region : chunk_map) {
		//region->bake_navigation_mesh();
		region->set_navigation_mesh(nav_mesh->duplicate());
	}
}

NavigationRegion3D *WorldInstance::_get_chunk_node_from_pos(const Vector3 &p_pos) {
	if (chunk_map.empty()) {
		return nullptr;
	}

	return chunk_map(chunk_index_from_pos(p_pos));
}

StaticBody3D *WorldInstance::_get_floor_body_from_pos(const Vector3 &p_pos) {
		if (chunk_map.empty()) {
		return nullptr;
	}

	return floor_bodies(chunk_index_from_pos(p_pos));
}

void WorldInstance::_disable_content() {
	const RID null_rid;

	for (size_t i = 0; i < visual_instances.size(); i++) {
		RS::get_singleton()->instance_set_scenario(visual_instances[i], null_rid);
	}

	for (size_t i = 0; i < multimesh_instances.size(); i++) {
		RS::get_singleton()->instance_set_scenario(multimesh_instances[i], null_rid);
	}
}

void WorldInstance::_enable_content() {
	for (size_t i = 0; i < visual_instances.size(); i++) {
		RS::get_singleton()->instance_set_scenario(
			visual_instances[i], get_world_3d()->get_scenario());
	}

	for (size_t i = 0; i < multimesh_instances.size(); i++) {
		RS::get_singleton()->instance_set_scenario(
			multimesh_instances[i], get_world_3d()->get_scenario());
	}
}

void WorldInstance::_update_visibility() {
	if (!is_inside_tree()) {
		return;
	}

	bool visibility = is_visible_in_tree();

	for (size_t i = 0; i < visual_instances.size(); i++) {
		RS::get_singleton()->instance_set_visible(
			visual_instances[i], visibility);
	}

	for (size_t i = 0; i < multimesh_instances.size(); i++) {
		RS::get_singleton()->instance_set_visible(
			multimesh_instances[i], visibility);
	}
}