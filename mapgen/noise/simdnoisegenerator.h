// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

// Based on https://github.com/tinmanjuggernaut/godot_fastnoise_simd

#pragma once

#include "core/object/ref_counted.h"
#include "noisegenerator.h"
#include <FastNoiseSIMD.h>

class SIMDNoiseGenerator : public NoiseGenerator {
    GDCLASS(SIMDNoiseGenerator, NoiseGenerator);

public:
	enum NoiseType {
		TYPE_VALUE = FastNoiseSIMD::Value,
		TYPE_VALUE_FRACTAL = FastNoiseSIMD::ValueFractal,
		TYPE_PERLIN = FastNoiseSIMD::Perlin,
		TYPE_PERLIN_FRACTAL = FastNoiseSIMD::PerlinFractal,
		TYPE_SIMPLEX = FastNoiseSIMD::Simplex,
		TYPE_SIMPLEX_FRACTAL = FastNoiseSIMD::SimplexFractal,
		TYPE_WHITE_NOISE = FastNoiseSIMD::WhiteNoise,
		TYPE_CELLULAR = FastNoiseSIMD::Cellular,
		TYPE_CUBIC = FastNoiseSIMD::Cubic,
		TYPE_CUBIC_FRACTAL = FastNoiseSIMD::CubicFractal
	};

	enum FractalType {
		FRACTAL_FBM = FastNoiseSIMD::FBM,
		FRACTAL_BILLOW = FastNoiseSIMD::Billow,
		FRACTAL_RIDGED_MULTI = FastNoiseSIMD::RigidMulti
	};

	enum PerturbType {
		PERTURB_NONE = FastNoiseSIMD::None,
		PERTURB_GRADIENT = FastNoiseSIMD::Gradient,
		PERTURB_GRADIENT_FRACTAL = FastNoiseSIMD::GradientFractal,
		PERTURB_NORMALIZE = FastNoiseSIMD::Normalise,
		PERTURB_GRADIENT_NORMALIZE = FastNoiseSIMD::Gradient_Normalise,
		PERTURB_GRADIENT_FRACTAL_NORMALIZE = FastNoiseSIMD::GradientFractal_Normalise
	};

	enum CellularDistanceFunction {
		DISTANCE_EUCLIDEAN = FastNoiseSIMD::Euclidean,
		DISTANCE_MANHATTAN = FastNoiseSIMD::Manhattan,
		DISTANCE_NATURAL = FastNoiseSIMD::Natural
	};

	enum CellularReturnType {
		RETURN_CELL_VALUE = FastNoiseSIMD::CellValue,
		RETURN_DISTANCE = FastNoiseSIMD::Distance,
		RETURN_DISTANCE2 = FastNoiseSIMD::Distance2,
		RETURN_DISTANCE2_ADD = FastNoiseSIMD::Distance2Add,
		RETURN_DISTANCE2_SUB = FastNoiseSIMD::Distance2Sub,
		RETURN_DISTANCE2_MUL = FastNoiseSIMD::Distance2Mul,
		RETURN_DISTANCE2_DIV = FastNoiseSIMD::Distance2Div,
		RETURN_DISTANCE2_CAVE = FastNoiseSIMD::Distance2Cave,
		RETURN_NOISE_LOOKUP = FastNoiseSIMD::NoiseLookup

	};

    // Noise generation
    Ref<NoiseBuffer> get_noise_buffer(const Rect2i &p_rect, const int p_height = 0, const int p_step = 1) const override;
    void fill_noise_buffer(const Ref<NoiseBuffer> &p_buffer, const Vector2i &p_pos, const int p_height = 0, const int p_step = 1) const override;

    // General noise settings
    void set_noise_type(NoiseType p_noise_type);
	NoiseType get_noise_type() const;

	void set_seed(int p_seed) override;
	int get_seed() const override;

	void set_frequency(float p_freq);
	float get_frequency() const;

	void set_axis_scales(Vector3 p_scale);
	Vector3 get_axis_scales() const;

	void set_scale(float p_scale);
	float get_scale() const;

	int get_simd_level() const;

    // Perturb texture coordinates within the noise functions
	void set_perturb_type(PerturbType p_type);
	PerturbType get_perturb_type() const;

	void set_perturb_amplitude(float p_amp);
	float get_perturb_amplitude() const;

	void set_perturb_frequency(float p_freq);
	float get_perturb_frequency() const;

	void set_perturb_fractal_octaves(int p_octaves);
	int get_perturb_fractal_octaves();

	void set_perturb_fractal_lacunarity(float p_lacunarity);
	float get_perturb_fractal_lacunarity();

	void set_perturb_fractal_gain(float p_gain);
	float get_perturb_fractal_gain();

	void set_perturb_normalize_length(float p_length);
	float get_perturb_normalize_length();

	// Fractal specific
	void set_fractal_type(FractalType p_type);
	FractalType get_fractal_type() const;

	void set_fractal_octaves(int p_octaves);
	int get_fractal_octaves() const;

	void set_fractal_lacunarity(float p_lacunarity);
	float get_fractal_lacunarity() const;

	void set_fractal_gain(float p_gain);
	float get_fractal_gain() const;

	// Cellular specific
	void set_cellular_distance_function(CellularDistanceFunction p_func);
	CellularDistanceFunction get_cellular_distance_function() const;

	void set_cellular_return_type(CellularReturnType p_type);
	CellularReturnType get_cellular_return_type() const;

	void set_cellular_distance2_indices(int p_index0, int p_index1);
	PackedInt32Array get_cellular_distance2_indices() const;

	void set_cellular_distance2_index0(int p_index0); // Editor helpers
	int get_cellular_distance2_index0() const;
	void set_cellular_distance2_index1(int p_index1);
	int get_cellular_distance2_index1() const;

	void set_cellular_jitter(float p_jitter);
	float get_cellular_jitter() const;

	void set_cellular_noise_lookup_type(NoiseType p_type);
	NoiseType get_cellular_noise_lookup_type() const;

	void set_cellular_noise_lookup_frequency(float p_freq);
	float get_cellular_noise_lookup_frequency() const;

    SIMDNoiseGenerator();
    ~SIMDNoiseGenerator();

protected:
	static void _bind_methods();

private:
	FastNoiseSIMD *_noise;

	float _scale = 1.0;

	// Store these locally as using directly from the library is awkward for various reasons
	float _perturb_amp;
	Vector3 _axis_scales;

	int _cell_dist_index0;
	int _cell_dist_index1;
};

VARIANT_ENUM_CAST(SIMDNoiseGenerator::NoiseType);
VARIANT_ENUM_CAST(SIMDNoiseGenerator::FractalType);
VARIANT_ENUM_CAST(SIMDNoiseGenerator::PerturbType);
VARIANT_ENUM_CAST(SIMDNoiseGenerator::CellularDistanceFunction);
VARIANT_ENUM_CAST(SIMDNoiseGenerator::CellularReturnType);