// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/object/ref_counted.h"
#include <noisebuffer.h>

class NoiseGenerator : public RefCounted {

    GDCLASS(NoiseGenerator, RefCounted);

protected:
	//static void _bind_methods();

public:
	virtual void set_seed(int p_seed) = 0;
	virtual int get_seed() const = 0;

    // Noise generation
    virtual Ref<NoiseBuffer> get_noise_buffer(const Rect2i &p_rect, const int p_height = 0, const int p_step = 1) const = 0;
    virtual void fill_noise_buffer(const Ref<NoiseBuffer> &p_buffer, const Vector2i &p_pos, const int p_height = 0, const int p_step = 1) const = 0;

    NoiseGenerator() = default;
    ~NoiseGenerator() = default;
};