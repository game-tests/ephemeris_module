/*************************************************************************/
/*  influence_curve.cpp                                                  */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           GODOT ENGINE                                */
/*                      https://godotengine.org                          */
/*************************************************************************/
/* Copyright (c) 2007-2022 Juan Linietsky, Ariel Manzur.                 */
/* Copyright (c) 2014-2022 Godot Engine contributors (cf. AUTHORS.md).   */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "influence_curve.h"
#include "core/math/math_defs.h"
#include "core/string/print_string.h"
#include "core/typedefs.h"
#include "scene/resources/curve.h"


void InfluenceCurve::set_height_range(const int p_min, const int p_max) {
	min_height = p_min;
	max_height = p_max;
	_height_values.resize(p_max - p_min + 1);

	real_t factor = 1.0 / _height_values.size();
	for (int i = 0; i < _height_values.size(); i++) {
		_height_values[i] = curve->interpolate(static_cast<real_t>(i) * factor);
	}
}

real_t InfluenceCurve::get_height_value(const int p_height) {
	if (curve->get_point_count() == 0 || _height_values.empty()) {
		return 1.0;
	}
	const int index = CLAMP(p_height - min_height, 0, _height_values.size());
	return _height_values[index];
}

Ref<Curve> InfluenceCurve::get_curve() const {
	return curve;
}

void InfluenceCurve::set_curve(Ref<Curve> p_curve) {
	curve = p_curve;
}

InfluenceCurve::InfluenceCurve() {
}

void InfluenceCurve::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_curve", "curve"), &InfluenceCurve::set_curve);
	ClassDB::bind_method(D_METHOD("get_curve"), &InfluenceCurve::get_curve);

	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "curve", PROPERTY_HINT_RESOURCE_TYPE, "Curve", PROPERTY_USAGE_NO_EDITOR |PROPERTY_USAGE_STORAGE | PROPERTY_USAGE_NETWORK | PROPERTY_USAGE_EDITOR_INSTANTIATE_OBJECT), "set_curve", "get_curve");
}