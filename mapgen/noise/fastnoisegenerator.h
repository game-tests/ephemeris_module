// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/object/ref_counted.h"
#include "noisegenerator.h"
#include <FastNoiseLite.h>

class FastNoiseGenerator : public NoiseGenerator {

    GDCLASS(FastNoiseGenerator, NoiseGenerator);

public:
	enum NoiseType {
		TYPE_SIMPLEX = FastNoiseLite::NoiseType_OpenSimplex2,
		TYPE_SIMPLEX_S = FastNoiseLite::NoiseType_OpenSimplex2S,
		TYPE_PERLIN = FastNoiseLite::NoiseType_Perlin,
		TYPE_CUBIC = FastNoiseLite::NoiseType_ValueCubic,
		TYPE_VALUE = FastNoiseLite::NoiseType_Value,
		TYPE_WHITE_NOISE = FastNoiseLite::NoiseType_White,
		TYPE_CELLULAR = FastNoiseLite::NoiseType_Cellular,
	};

	enum FractalType {
		FRACTAL_NONE = FastNoiseLite::FractalType_None,
		FRACTAL_FBM = FastNoiseLite::FractalType_FBm,
		FRACTAL_RIDGED = FastNoiseLite::FractalType_Ridged,
		FRACTAL_PINGPONG = FastNoiseLite::FractalType_PingPong,
		FRACTAL_WARP_PROGRRESSIVE = FastNoiseLite::FractalType_DomainWarpProgressive,
		FRACTAL_WARP_INDEPENDENT = FastNoiseLite::FractalType_DomainWarpIndependent
	};

	enum CellularDistanceFunction {
		DISTANCE_EUCLIDEAN = FastNoiseLite::CellularDistanceFunction_Euclidean,
		DISTANCE_MANHATTAN = FastNoiseLite::CellularDistanceFunction_Manhattan,
		DISTANCE_NATURAL = FastNoiseLite::CellularDistanceFunction_EuclideanSq,
		HYBRID = FastNoiseLite::CellularDistanceFunction_Hybrid
	};

	enum CellularReturnType {
		RETURN_CELL_VALUE = FastNoiseLite::CellularReturnType_CellValue,
		RETURN_DISTANCE = FastNoiseLite::CellularReturnType_Distance,
		RETURN_DISTANCE2 = FastNoiseLite::CellularReturnType_Distance2,
		RETURN_DISTANCE2_ADD = FastNoiseLite::CellularReturnType_Distance2Add,
		RETURN_DISTANCE2_SUB = FastNoiseLite::CellularReturnType_Distance2Sub,
		RETURN_DISTANCE2_MUL = FastNoiseLite::CellularReturnType_Distance2Mul,
		RETURN_DISTANCE2_DIV = FastNoiseLite::CellularReturnType_Distance2Div
	};

	enum WarpType {
		WARP_OPENSIMPLEX = FastNoiseLite::DomainWarpType_OpenSimplex2,
		WARP_OPENSIMPLEX_REDUCED = FastNoiseLite::DomainWarpType_OpenSimplex2Reduced,
		WARP_GRID = FastNoiseLite::DomainWarpType_BasicGrid
	};

	FastNoiseGenerator();
    ~FastNoiseGenerator();

	// Noise generation
    Ref<NoiseBuffer> get_noise_buffer(const Rect2i &p_rect, const int p_height = 0, const int p_step = 1) const override;
    void fill_noise_buffer(const Ref<NoiseBuffer> &p_buffer, const Vector2i &p_pos, const int p_height = 0, const int p_step = 1) const override;

	// Noise Warped
	Ref<NoiseBuffer> get_warped(const Rect2i &p_rect);
	void fill_warped(const Ref<NoiseBuffer> &p_buffer, const Vector2i &p_pos);

	// General noise settings
	void set_noise_type(const NoiseType p_noise_type);
	NoiseType get_noise_type() const;

	void set_seed(int p_seed) override;
	int get_seed() const override;

	void set_frequency(real_t p_freq);
	real_t get_frequency() const;

	// Fractal specific
	void set_fractal_type(FractalType p_type);
	FractalType get_fractal_type() const;

	void set_fractal_octaves(int p_octaves);
	int get_fractal_octaves() const;

	void set_fractal_lacunarity(real_t p_lacunarity);
	real_t get_fractal_lacunarity() const;

	void set_fractal_gain(real_t p_gain);
	real_t get_fractal_gain() const;

	void set_fractal_weighted_strength(real_t p_strength);//
	real_t get_fractal_weighted_strength() const;

	void set_fractal_pingpong_strength(real_t p_strength);//
	real_t get_fractal_pingpong_strength() const;

	// Cellular specific
	void set_cellular_distance_function(CellularDistanceFunction p_func);
	CellularDistanceFunction get_cellular_distance_function() const;

	void set_cellular_jitter(real_t p_jitter);
	real_t get_cellular_jitter() const;

	void set_cellular_return_type(CellularReturnType p_ret);
	CellularReturnType get_cellular_return_type() const;

	// Domain Warp
	void set_noise_warp_type(const WarpType p_warp_type);
	WarpType get_noise_warp_type() const;
	

protected:
	static void _bind_methods();

private:
	mutable FastNoiseLite _noise; // TODO
};

VARIANT_ENUM_CAST(FastNoiseGenerator::NoiseType);
VARIANT_ENUM_CAST(FastNoiseGenerator::FractalType);
VARIANT_ENUM_CAST(FastNoiseGenerator::CellularDistanceFunction);
VARIANT_ENUM_CAST(FastNoiseGenerator::CellularReturnType);
VARIANT_ENUM_CAST(FastNoiseGenerator::WarpType);