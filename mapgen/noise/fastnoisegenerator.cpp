// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "fastnoisegenerator.h"

// Noise generation

Ref<NoiseBuffer> FastNoiseGenerator::get_noise_buffer(const Rect2i &p_rect, const int p_height, const int p_step) const {
    Ref<NoiseBuffer> res = memnew(NoiseBuffer);
	res->_resize_no_memset(p_rect.size / p_step);
    fill_noise_buffer(res, p_rect.position, p_height, p_step);
    return res;
}

void FastNoiseGenerator::fill_noise_buffer(const Ref<NoiseBuffer> &p_buffer, const Vector2i &p_pos, const int p_height, const int p_step) const {
    const Vector2i start(p_pos.x, p_pos.y);
	const Vector2i end(p_pos.x + p_buffer->rows * p_step, p_pos.y + p_buffer->columns  * p_step);

    #pragma omp parallel for
	for (int x = start.x; x < end.x; x += p_step) {
		int idx = ((x - p_pos.x) / p_step) * p_buffer->columns;
		for (int y = start.y; y < end.y; y += p_step) {
			p_buffer->buffer[idx] = _noise.GetNoise<real_t>(x, p_height, y);
			idx++;
		}
	}
}

Ref<NoiseBuffer> FastNoiseGenerator::get_warped(const Rect2i &p_rect) {
	Ref<NoiseBuffer> res = memnew(NoiseBuffer);
	res->_resize_no_memset(p_rect.size);
	fill_warped(res, p_rect.position);
	return res;
}

void FastNoiseGenerator::fill_warped(const Ref<NoiseBuffer> &p_buffer, const Vector2i &p_pos) {
	const Vector2i start(p_pos.x, p_pos.y);
	const Vector2i end(p_pos.x + p_buffer->rows, p_pos.y + p_buffer->columns);

    #pragma omp parallel for
	for (int x = start.x; x < end.x; x++) {
		int idx = (x - p_pos.x) * p_buffer->columns;
		for (int y = start.y; y < end.y; y++) {
			//_noise.DomainWarp<real_t>(x, y); // TODO
			p_buffer->buffer[idx] = _noise.GetNoise<real_t>(x, y);
			idx++;
		}
	}
}

FastNoiseGenerator::FastNoiseGenerator() {
}

FastNoiseGenerator::~FastNoiseGenerator() {
}

// General settings

void FastNoiseGenerator::set_noise_type(const NoiseType p_noise_type) {
	_noise.SetNoiseType((FastNoiseLite::NoiseType)p_noise_type);
}

FastNoiseGenerator::NoiseType FastNoiseGenerator::get_noise_type() const {
	return (NoiseType)_noise.GetNoiseType();
}

void FastNoiseGenerator::set_seed(int p_seed) {
	_noise.SetSeed(p_seed);
}

int FastNoiseGenerator::get_seed() const {
	return _noise.GetSeed();
}

void FastNoiseGenerator::set_frequency(real_t p_freq) {
	_noise.SetFrequency(p_freq);
}

real_t FastNoiseGenerator::get_frequency() const {
	return _noise.GetFrequency();
}

// Fractal

void FastNoiseGenerator::set_fractal_type(FractalType p_type) {
	_noise.SetFractalType((FastNoiseLite::FractalType)p_type);
}

FastNoiseGenerator::FractalType FastNoiseGenerator::get_fractal_type() const {
	return (FractalType)_noise.GetFractalType();
}

void FastNoiseGenerator::set_fractal_octaves(int p_octaves) {
	_noise.SetFractalOctaves(p_octaves);
}

int FastNoiseGenerator::get_fractal_octaves() const {
	return _noise.GetFractalOctaves();
}

void FastNoiseGenerator::set_fractal_lacunarity(real_t p_lacunarity) {
	_noise.SetFractalLacunarity(p_lacunarity);
}

real_t FastNoiseGenerator::get_fractal_lacunarity() const {
	return _noise.GetFractalLacunarity();
}

void FastNoiseGenerator::set_fractal_gain(real_t p_gain) {
	_noise.SetFractalGain(p_gain);
}

real_t FastNoiseGenerator::get_fractal_gain() const {
	return _noise.GetFractalGain();
}

// Cellular

void FastNoiseGenerator::set_cellular_distance_function(CellularDistanceFunction p_func) {
	_noise.SetCellularDistanceFunction((FastNoiseLite::CellularDistanceFunction)p_func);
}

FastNoiseGenerator::CellularDistanceFunction FastNoiseGenerator::get_cellular_distance_function() const {
	return (CellularDistanceFunction)_noise.GetCellularDistanceFunction();
}

void FastNoiseGenerator::set_cellular_jitter(real_t p_jitter) {
	_noise.SetCellularJitter(p_jitter);
}

real_t FastNoiseGenerator::get_cellular_jitter() const {
	return _noise.GetCellularJitter();
}

void FastNoiseGenerator::set_cellular_return_type(CellularReturnType p_ret) {
	_noise.SetCellularReturnType((FastNoiseLite::CellularReturnType)p_ret);
}

FastNoiseGenerator::CellularReturnType FastNoiseGenerator::get_cellular_return_type() const {
	return (CellularReturnType)_noise.GetCellularReturnType();
}

// Warp
void FastNoiseGenerator::set_noise_warp_type(const WarpType p_warp_type) {
	_noise.SetDomainWarpType((FastNoiseLite::DomainWarpType)p_warp_type);
}

FastNoiseGenerator::WarpType FastNoiseGenerator::get_noise_warp_type() const {
	return (WarpType)_noise.GetDomainWarpType();
}

void FastNoiseGenerator::_bind_methods() {

    // Generation
    ClassDB::bind_method(D_METHOD("get_noise_buffer", "rect", "height", "step"), &FastNoiseGenerator::get_noise_buffer, DEFVAL(0), DEFVAL(1));
    ClassDB::bind_method(D_METHOD("fill_noise_buffer", "buffer", "pos", "height", "step"), &FastNoiseGenerator::fill_noise_buffer, DEFVAL(0), DEFVAL(1));
    ClassDB::bind_method(D_METHOD("get_warped", "rect"), &FastNoiseGenerator::get_warped);
    ClassDB::bind_method(D_METHOD("fill_warped", "buffer", "pos"), &FastNoiseGenerator::fill_warped);

	// General settings

	ClassDB::bind_method(D_METHOD("set_noise_type", "type"), &FastNoiseGenerator::set_noise_type);
	ClassDB::bind_method(D_METHOD("get_noise_type"), &FastNoiseGenerator::get_noise_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "noise_type", PROPERTY_HINT_ENUM, "Value,Perlin,Simplex,Cellular,WhiteNoise,Cubic"), "set_noise_type", "get_noise_type");

	ClassDB::bind_method(D_METHOD("set_seed", "seed"), &FastNoiseGenerator::set_seed);
	ClassDB::bind_method(D_METHOD("get_seed"), &FastNoiseGenerator::get_seed);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "seed"), "set_seed", "get_seed");

	ClassDB::bind_method(D_METHOD("set_frequency", "freq"), &FastNoiseGenerator::set_frequency);
	ClassDB::bind_method(D_METHOD("get_frequency"), &FastNoiseGenerator::get_frequency);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "frequency", PROPERTY_HINT_RANGE, ".001,1"), "set_frequency", "get_frequency");

	// Fractal

	ADD_GROUP("Fractal", "fractal_");
	ClassDB::bind_method(D_METHOD("set_fractal_type", "type"), &FastNoiseGenerator::set_fractal_type);
	ClassDB::bind_method(D_METHOD("get_fractal_type"), &FastNoiseGenerator::get_fractal_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_type", PROPERTY_HINT_ENUM, "None,FBM,Billow,Ridged,PingPong"), "set_fractal_type", "get_fractal_type");

	ClassDB::bind_method(D_METHOD("set_fractal_octaves", "octave_count"), &FastNoiseGenerator::set_fractal_octaves);
	ClassDB::bind_method(D_METHOD("get_fractal_octaves"), &FastNoiseGenerator::get_fractal_octaves);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_octaves", PROPERTY_HINT_RANGE, "1,10,1"), "set_fractal_octaves", "get_fractal_octaves");

	ClassDB::bind_method(D_METHOD("set_fractal_lacunarity", "lacunarity"), &FastNoiseGenerator::set_fractal_lacunarity);
	ClassDB::bind_method(D_METHOD("get_fractal_lacunarity"), &FastNoiseGenerator::get_fractal_lacunarity);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "fractal_lacunarity"), "set_fractal_lacunarity", "get_fractal_lacunarity");

	ClassDB::bind_method(D_METHOD("set_fractal_gain", "gain"), &FastNoiseGenerator::set_fractal_gain);
	ClassDB::bind_method(D_METHOD("get_fractal_gain"), &FastNoiseGenerator::get_fractal_gain);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "fractal_gain"), "set_fractal_gain", "get_fractal_gain");

	// Cellular

	ADD_GROUP("Cellular", "cellular_");
	ClassDB::bind_method(D_METHOD("set_cellular_distance_function", "func"), &FastNoiseGenerator::set_cellular_distance_function);
	ClassDB::bind_method(D_METHOD("get_cellular_distance_function"), &FastNoiseGenerator::get_cellular_distance_function);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_dist_func", PROPERTY_HINT_ENUM, "Euclidean,Manhattan,Natural,Hibrid"), "set_cellular_distance_function", "get_cellular_distance_function");

	ClassDB::bind_method(D_METHOD("set_cellular_jitter", "jitter"), &FastNoiseGenerator::set_cellular_jitter);
	ClassDB::bind_method(D_METHOD("get_cellular_jitter"), &FastNoiseGenerator::get_cellular_jitter);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "cellular_jitter"), "set_cellular_jitter", "get_cellular_jitter");

	ClassDB::bind_method(D_METHOD("set_cellular_return_type", "ret"), &FastNoiseGenerator::set_cellular_return_type);
	ClassDB::bind_method(D_METHOD("get_cellular_return_type"), &FastNoiseGenerator::get_cellular_return_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_return_type", PROPERTY_HINT_ENUM, "CellValue,Distance,Distance2,Distance2Add,Distance2Sub,Distance2Mul,Distance2Div"), "set_cellular_return_type", "get_cellular_return_type");

	// Warp



	BIND_ENUM_CONSTANT(TYPE_VALUE);
	BIND_ENUM_CONSTANT(TYPE_PERLIN);
	BIND_ENUM_CONSTANT(TYPE_SIMPLEX);
	BIND_ENUM_CONSTANT(TYPE_CELLULAR);
	BIND_ENUM_CONSTANT(TYPE_WHITE_NOISE);
	BIND_ENUM_CONSTANT(TYPE_CUBIC);

	BIND_ENUM_CONSTANT(FRACTAL_NONE);
	BIND_ENUM_CONSTANT(FRACTAL_FBM);
	BIND_ENUM_CONSTANT(FRACTAL_RIDGED);
	BIND_ENUM_CONSTANT(FRACTAL_PINGPONG);
	BIND_ENUM_CONSTANT(FRACTAL_WARP_PROGRRESSIVE);
	BIND_ENUM_CONSTANT(FRACTAL_WARP_INDEPENDENT);

	BIND_ENUM_CONSTANT(DISTANCE_EUCLIDEAN);
	BIND_ENUM_CONSTANT(DISTANCE_MANHATTAN);
	BIND_ENUM_CONSTANT(DISTANCE_NATURAL);
	BIND_ENUM_CONSTANT(HYBRID);

	BIND_ENUM_CONSTANT(RETURN_CELL_VALUE);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_ADD);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_SUB);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_MUL);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_DIV);

	BIND_ENUM_CONSTANT(WARP_OPENSIMPLEX);
	BIND_ENUM_CONSTANT(WARP_OPENSIMPLEX_REDUCED);
	BIND_ENUM_CONSTANT(WARP_GRID);
}