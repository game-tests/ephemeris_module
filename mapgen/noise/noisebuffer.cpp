// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "noisebuffer.h"
#include <FastNoiseSIMD.h>

void NoiseBuffer::_bind_methods() {
	ClassDB::bind_method(D_METHOD("get_columns"), &NoiseBuffer::get_columns);
	ClassDB::bind_method(D_METHOD("get_rows"), &NoiseBuffer::get_rows);
	ClassDB::bind_method(D_METHOD("get_buffer_size"), &NoiseBuffer::get_buffer_size);
	ClassDB::bind_method(D_METHOD("get_size"), &NoiseBuffer::get_size);
	ClassDB::bind_method(D_METHOD("get_value", "row", "column"), &NoiseBuffer::get_value);
	ClassDB::bind_method(D_METHOD("set_value", "value", "row", "column"), &NoiseBuffer::set_value);
	ClassDB::bind_method(D_METHOD("is_compatible", "buffer"), &NoiseBuffer::is_compatible);
	ClassDB::bind_method(D_METHOD("duplicate"), &NoiseBuffer::duplicate);
	ClassDB::bind_method(D_METHOD("resize"), &NoiseBuffer::resize);
	ClassDB::bind_method(D_METHOD("abs"), &NoiseBuffer::abs);
	ClassDB::bind_method(D_METHOD("scalar_assign", "value"), &NoiseBuffer::scalar_assign);
	ClassDB::bind_method(D_METHOD("add", "buffer"), &NoiseBuffer::add);
	ClassDB::bind_method(D_METHOD("scalar_add", "value"), &NoiseBuffer::scalar_add);
	ClassDB::bind_method(D_METHOD("sub", "buffer"), &NoiseBuffer::add);
	ClassDB::bind_method(D_METHOD("scalar_sub", "value"), &NoiseBuffer::scalar_add);
	ClassDB::bind_method(D_METHOD("multiply", "buffer"), &NoiseBuffer::multiply);
	ClassDB::bind_method(D_METHOD("scalar_multiply", "value"), &NoiseBuffer::scalar_multiply);
	ClassDB::bind_method(D_METHOD("pow", "buffer"), &NoiseBuffer::pow);
	ClassDB::bind_method(D_METHOD("scalar_pow", "value"), &NoiseBuffer::scalar_pow);
	ClassDB::bind_method(D_METHOD("blend", "buffer", "control"), &NoiseBuffer::blend);
	ClassDB::bind_method(D_METHOD("clamp", "min", "max"), &NoiseBuffer::clamp);
	ClassDB::bind_method(D_METHOD("invert"), &NoiseBuffer::invert);
	ClassDB::bind_method(D_METHOD("max", "buffer"), &NoiseBuffer::max);
	ClassDB::bind_method(D_METHOD("min", "buffer"), &NoiseBuffer::min);
	ClassDB::bind_method(D_METHOD("threshold", "threshold_val", "type"), &NoiseBuffer::threshold);
	ClassDB::bind_method(D_METHOD("get_image"), &NoiseBuffer::get_image);

	BIND_ENUM_CONSTANT(THRESHOLD_TYPE_BINARY);
	BIND_ENUM_CONSTANT(THRESHOLD_TYPE_BINARY_INV);
	BIND_ENUM_CONSTANT(THRESHOLD_TYPE_TO_ZERO);
	BIND_ENUM_CONSTANT(THRESHOLD_TYPE_TO_ZERO_INV);
	BIND_ENUM_CONSTANT(THRESHOLD_TYPE_TRUNCATE);
}

void NoiseBuffer::_resize_no_memset(const int p_r, const int p_c) {
	if (p_r == rows && p_c == columns) {
		return;
	}
	rows = p_r;
	columns = p_c;

	if (buffer) {
		FastNoiseSIMD::FreeNoiseSet(buffer);
	}
	buffer = FastNoiseSIMD::GetEmptySet(this->get_buffer_size());
}

Ref<NoiseBuffer> NoiseBuffer::duplicate() const {
	Ref<NoiseBuffer> res = memnew(NoiseBuffer);
	res->columns = columns;
	res->rows = rows;
	res->buffer = FastNoiseSIMD::GetEmptySet(this->get_buffer_size());
	for (int i = 0; i < this->get_buffer_size(); i++) {
		res->buffer[i] = buffer[i];
	}
	
	return res;
}

void NoiseBuffer::abs() {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = Math::absf(buffer[i]);
	}
}

void NoiseBuffer::scalar_assign(const float p_f) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = p_f;
	}
}

void NoiseBuffer::add(const Ref<NoiseBuffer> &p_nb) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] += p_nb->buffer[i];
	}
}

void NoiseBuffer::scalar_add(const float p_f) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] += p_f;
	}
}

void NoiseBuffer::sub(const Ref<NoiseBuffer> &p_nb) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] -= p_nb->buffer[i];
	}
}

void NoiseBuffer::scalar_sub(const float p_f) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] -= p_f;
	}
}

void NoiseBuffer::multiply(const Ref<NoiseBuffer> &p_nb) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] *= p_nb->buffer[i];
	}
}

void NoiseBuffer::scalar_multiply(const float p_f) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] *= p_f;
	}
}

void NoiseBuffer::pow(const Ref<NoiseBuffer> &p_nb) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = Math::pow(buffer[i], p_nb->buffer[i]);
	}
}

void NoiseBuffer::scalar_pow(const float p_f) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = Math::pow(buffer[i], p_f);
	}
}

void NoiseBuffer::blend(const Ref<NoiseBuffer> &p_nb, const Ref<NoiseBuffer> &p_control) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] += Math::lerp(buffer[i], p_nb->buffer[i], p_control->buffer[i]);
	}
}

void NoiseBuffer::clamp(const float p_min, const float p_max) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = CLAMP(buffer[i], p_min, p_max);
	}
}

void NoiseBuffer::invert() {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = -buffer[i];
	}
}

void NoiseBuffer::max(const Ref<NoiseBuffer> &p_nb) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = MAX(buffer[i], p_nb->buffer[i]);
	}
}

void NoiseBuffer::scalar_max(const float p_f) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = MAX(buffer[i], p_f);
	}
}

void NoiseBuffer::min(const Ref<NoiseBuffer> &p_nb) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = MIN(buffer[i], p_nb->buffer[i]);
	}
}

void NoiseBuffer::scalar_min(const float p_f) {
	for (int i = 0; i < get_buffer_size(); i++) {
		buffer[i] = MIN(buffer[i], p_f);
	}
}

void NoiseBuffer::threshold(const float p_threshold, const ThresholdType p_type) {

	switch (p_type) {
		case THRESHOLD_TYPE_BINARY: {
			
			for (int i = 0; i < get_buffer_size(); i++) {
				buffer[i] = buffer[i] < p_threshold ? 0 : 1;
			}
		} break;
		case THRESHOLD_TYPE_BINARY_INV: {
			
			for (int i = 0; i < get_buffer_size(); i++) {
				buffer[i] = buffer[i] > p_threshold ? 0 : 1;
			}
		} break;
		case THRESHOLD_TYPE_TRUNCATE: {

			for (int i = 0; i < get_buffer_size(); i++) {
				buffer[i] = MIN(buffer[i], p_threshold);
			}
		} break;
		case THRESHOLD_TYPE_TO_ZERO: {
			
			for (int i = 0; i < get_buffer_size(); i++) {
				buffer[i] = buffer[i] < p_threshold ? 0 : buffer[i];
			}
		} break;
		case THRESHOLD_TYPE_TO_ZERO_INV: {
			
			for (int i = 0; i < get_buffer_size(); i++) {
				buffer[i] = buffer[i] > p_threshold ? 0 : buffer[i];
			}
		} break;
		
		default:
			break;
	}
}

Ref<Image> NoiseBuffer::get_image() {

	Vector<uint8_t> data;
	data.resize(rows * columns * 4);

	uint8_t *wd8 = data.ptrw();

	for (int x = 0; x < rows; x++) {
		for (int y = 0; y < columns; y++) {

			float v = get_value(x, y);

			v = v * 0.5 + 0.5; // Normalize [0..1]
			uint8_t value = uint8_t(CLAMP(v * 255.0, 0, 255));
			wd8[(x * rows + y) * 4 + 0] = value;
			wd8[(x * rows + y) * 4 + 1] = value;
			wd8[(x * rows + y) * 4 + 2] = value;
			wd8[(x * rows + y) * 4 + 3] = 255;
		}
	}

	Ref<Image> image = memnew(Image(rows, columns, false, Image::FORMAT_RGBA8, data));
	return image;
}

Ref<Texture> NoiseBuffer::get_texture() {
	const Ref<Image> &image = get_image();

	Ref<ImageTexture> texture = memnew(ImageTexture);
	texture->create_from_image(image);

	return texture;
}

NoiseBuffer::operator String() const {
	String res;
	for (int x = 0; x < rows; x++) {
		res += "[ ";
		for (int y = 0; y < columns; y++) {
			float v = get_value(x, y);
			res += String::num(v, 2);
			res += " ";
		}
		res += "]\n";
	}
	res += "\n";
	return res;
}

NoiseBuffer::NoiseBuffer() {}

NoiseBuffer::~NoiseBuffer() {
	if (buffer) {
		FastNoiseSIMD::FreeNoiseSet(buffer);
	}
}