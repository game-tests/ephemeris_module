// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/math/math_funcs.h"
#include "core/typedefs.h"
#include "core/object/ref_counted.h"
#include "core/io/image.h"
#include "scene/resources/texture.h"

class NoiseBuffer : public RefCounted {

	GDCLASS(NoiseBuffer, RefCounted);

protected:
	static void _bind_methods();

public:
	float *buffer = nullptr;
	int columns = 0;
	int rows = 0;

	// Used for convenience inside the generator
	_FORCE_INLINE_ float& get_value_ref(const int p_r, const int p_c) const {
		return buffer[p_r * columns + p_c];
	}

	// resize without setting default value
	void _resize_no_memset(const int p_r, const int p_c);

	// resized without setting default value
	_FORCE_INLINE_ void _resize_no_memset(const Vector2i &p_vector) {
		_resize_no_memset(p_vector.x, p_vector.y);
	}

	// General
	_FORCE_INLINE_ int get_columns() const {
		return columns;
	}

	_FORCE_INLINE_ int get_rows() const {
		return rows;
	}

	_FORCE_INLINE_ int get_buffer_size() const {
		return rows * columns;
	}

	_FORCE_INLINE_ Vector2i get_size() const {
		return Vector2i(rows, columns);
	}

	_FORCE_INLINE_ float get_value(const int p_r, const int p_c) const {
		return buffer[p_r * columns + p_c];
	}

	_FORCE_INLINE_ float get_value_linear(const int p_index) const {
		return buffer[p_index];
	}

	_FORCE_INLINE_ void set_value(const int p_r, const int p_c, const float p_val) {
		buffer[p_r * columns + p_c] = p_val;
	}

	_FORCE_INLINE_ void set_value_linear(const int p_index, const float p_val) {
		buffer[p_index] = p_val;
	}

	_FORCE_INLINE_ bool is_compatible(const Ref<NoiseBuffer> &p_nb) {
		return columns == p_nb->columns && rows == p_nb->rows;
	}

	Ref<NoiseBuffer> duplicate() const;
	
	// sets all to 0
	_FORCE_INLINE_ void resize_v(const Vector2i &p_vector) {
		resize(p_vector.x, p_vector.y);
	}

	_FORCE_INLINE_ void resize(const int p_r, const int p_c) {
		_resize_no_memset(p_r, p_c);

		for (int i = 0; i < this->get_buffer_size(); i++) {
			buffer[i] = 0;
		}
	}

	// OPERATORS
	void abs();

	void scalar_assign(const float p_f);

	void add(const Ref<NoiseBuffer> &p_nb);

	void scalar_add(const float p_f);

	void sub(const Ref<NoiseBuffer> &p_nb);

	void scalar_sub(const float p_f);

	void multiply(const Ref<NoiseBuffer> &p_nb);

	void scalar_multiply(const float p_f);

	void pow(const Ref<NoiseBuffer> &p_nb);

	void scalar_pow(const float p_f);

	void blend(const Ref<NoiseBuffer> &p_nb, const Ref<NoiseBuffer> &p_control);

	void clamp(const float p_min, const float p_max);

	void invert();

	void max(const Ref<NoiseBuffer> &p_nb);

	void scalar_max(const float p_f);

	void min(const Ref<NoiseBuffer> &p_nb);

	void scalar_min(const float p_f);

	enum ThresholdType {
		THRESHOLD_TYPE_BINARY,
		THRESHOLD_TYPE_BINARY_INV,
		THRESHOLD_TYPE_TRUNCATE,
		THRESHOLD_TYPE_TO_ZERO,
		THRESHOLD_TYPE_TO_ZERO_INV,
	};

	void threshold(const float p_threshold, const ThresholdType p_type);

	Ref<Image> get_image();
	Ref<Texture> get_texture();

	operator String() const;

	NoiseBuffer();
	~NoiseBuffer();
};

VARIANT_ENUM_CAST(NoiseBuffer::ThresholdType);