// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

// Based on https://github.com/tinmanjuggernaut/godot_fastnoise_simd

#include "simdnoisegenerator.h"

// Noise generation

Ref<NoiseBuffer> SIMDNoiseGenerator::get_noise_buffer(const Rect2i &p_rect, const int p_height, const int p_step) const {
    Ref<NoiseBuffer> res = memnew(NoiseBuffer);
	res->buffer = _noise->GetNoiseSet(p_rect.position.x, 0, p_rect.position.y,
        p_rect.size.x, 1, p_rect.size.y, _scale);
    res->rows = p_rect.size.x;
    res->columns = p_rect.size.y;

    return res;
}

void SIMDNoiseGenerator::fill_noise_buffer(const Ref<NoiseBuffer> &p_buffer, const Vector2i &p_pos, const int p_height, const int p_step) const {
    _noise->FillNoiseSet(p_buffer->buffer, p_pos.x, 0, p_pos.y, p_buffer->get_rows(), 1, p_buffer->get_columns(), _scale);
}

// General settings

void SIMDNoiseGenerator::set_noise_type(NoiseType p_noise_type) {
	_noise->SetNoiseType((FastNoiseSIMD::NoiseType)p_noise_type);
}

SIMDNoiseGenerator::NoiseType SIMDNoiseGenerator::get_noise_type() const {
	return (NoiseType)_noise->GetNoiseType();
}

void SIMDNoiseGenerator::set_seed(int p_seed) {
	_noise->SetSeed(p_seed);
}

int SIMDNoiseGenerator::get_seed() const {
	return _noise->GetSeed();
}

void SIMDNoiseGenerator::set_frequency(float p_freq) {
	_noise->SetFrequency(p_freq);
	
}

float SIMDNoiseGenerator::get_frequency() const {
	return _noise->GetFrequency();
}

void SIMDNoiseGenerator::set_axis_scales(Vector3 p_scale) {
	_axis_scales = p_scale;
	_noise->SetAxisScales(p_scale.x, p_scale.y, p_scale.z);
}

Vector3 SIMDNoiseGenerator::get_axis_scales() const {
	return _axis_scales;
}

void SIMDNoiseGenerator::set_scale(float p_scale) {
	_scale = p_scale;
}

float SIMDNoiseGenerator::get_scale() const {
	return _scale;
}

int SIMDNoiseGenerator::get_simd_level() const {
	return _noise->GetSIMDLevel();
}

// Perturb

void SIMDNoiseGenerator::set_perturb_type(PerturbType p_type) {
	_noise->SetPerturbType((FastNoiseSIMD::PerturbType)p_type);
	
}

SIMDNoiseGenerator::PerturbType SIMDNoiseGenerator::get_perturb_type() const {
	return (PerturbType)_noise->GetPerturbType();
}

void SIMDNoiseGenerator::set_perturb_amplitude(float p_amp) {
	_perturb_amp = p_amp;
	_noise->SetPerturbAmp(_perturb_amp);
	
}

float SIMDNoiseGenerator::get_perturb_amplitude() const {
	return _perturb_amp;
}

void SIMDNoiseGenerator::set_perturb_frequency(float p_freq) {
	_noise->SetPerturbFrequency(p_freq);
	
}

float SIMDNoiseGenerator::get_perturb_frequency() const {
	return _noise->GetPerturbFrequency();
}

void SIMDNoiseGenerator::set_perturb_fractal_octaves(int p_octaves) {
	_noise->SetPerturbFractalOctaves(p_octaves);
	
}

int SIMDNoiseGenerator::get_perturb_fractal_octaves() {
	return _noise->GetPerturbFractalOctaves();
}

void SIMDNoiseGenerator::set_perturb_fractal_lacunarity(float p_lacunarity) {
	_noise->SetPerturbFractalLacunarity(p_lacunarity);
	
}

float SIMDNoiseGenerator::get_perturb_fractal_lacunarity() {
	return _noise->GetPerturbFractalLacunarity();
}

void SIMDNoiseGenerator::set_perturb_fractal_gain(float p_gain) {
	_noise->SetPerturbFractalGain(p_gain);
	
}

float SIMDNoiseGenerator::get_perturb_fractal_gain() {
	return _noise->GetPerturbFractalGain();
}

void SIMDNoiseGenerator::set_perturb_normalize_length(float p_length) {
	_noise->SetPerturbNormaliseLength(p_length);
	
}

float SIMDNoiseGenerator::get_perturb_normalize_length() {
	return _noise->GetPerturbNormaliseLength();
}

// Fractal

void SIMDNoiseGenerator::set_fractal_type(FractalType p_type) {
	_noise->SetFractalType((FastNoiseSIMD::FractalType)p_type);
	
}

SIMDNoiseGenerator::FractalType SIMDNoiseGenerator::get_fractal_type() const {
	return (FractalType)_noise->GetFractalType();
}

void SIMDNoiseGenerator::set_fractal_octaves(int p_octaves) {
	_noise->SetFractalOctaves(p_octaves);
	
}

int SIMDNoiseGenerator::get_fractal_octaves() const {
	return _noise->GetFractalOctaves();
}

void SIMDNoiseGenerator::set_fractal_lacunarity(float p_lacunarity) {
	_noise->SetFractalLacunarity(p_lacunarity);
	
}

float SIMDNoiseGenerator::get_fractal_lacunarity() const {
	return _noise->GetFractalLacunarity();
}

void SIMDNoiseGenerator::set_fractal_gain(float p_gain) {
	_noise->SetFractalGain(p_gain);
	
}

float SIMDNoiseGenerator::get_fractal_gain() const {
	return _noise->GetFractalGain();
}

// Cellular

void SIMDNoiseGenerator::set_cellular_distance_function(CellularDistanceFunction p_func) {
	_noise->SetCellularDistanceFunction((FastNoiseSIMD::CellularDistanceFunction)p_func);
	
}

SIMDNoiseGenerator::CellularDistanceFunction SIMDNoiseGenerator::get_cellular_distance_function() const {
	return (CellularDistanceFunction)_noise->GetCellularDistanceFunction();
}

void SIMDNoiseGenerator::set_cellular_return_type(CellularReturnType p_type) {
	_noise->SetCellularReturnType((FastNoiseSIMD::CellularReturnType)p_type);
	
}

SIMDNoiseGenerator::CellularReturnType SIMDNoiseGenerator::get_cellular_return_type() const {
	return (CellularReturnType)_noise->GetCellularReturnType();
}

void SIMDNoiseGenerator::set_cellular_distance2_indices(int p_index0, int p_index1) {
	// Valid range for index1: 1-3
	if (p_index1 > 3) {
		_cell_dist_index1 = 3;
	} else if (p_index1 < 1) {
		_cell_dist_index1 = 1;
	} else {
		_cell_dist_index1 = p_index1;
	}

	// Valid range for index0: 0-2 and < index1
	_cell_dist_index0 = p_index0;
	if (_cell_dist_index0 >= _cell_dist_index1) {
		_cell_dist_index0 = _cell_dist_index1 - 1;
	}

	if (_cell_dist_index0 < 0) {
		_cell_dist_index0 = 0;
	}

	_noise->SetCellularDistance2Indices(_cell_dist_index0, _cell_dist_index1);
	
}

PackedInt32Array SIMDNoiseGenerator::get_cellular_distance2_indices() const {
	PackedInt32Array a;
	a.append(_cell_dist_index0);
	a.append(_cell_dist_index1);
	return a;
}

void SIMDNoiseGenerator::set_cellular_distance2_index0(int p_index0) {
	set_cellular_distance2_indices(p_index0, _cell_dist_index1);
	
}

int SIMDNoiseGenerator::get_cellular_distance2_index0() const {
	return _cell_dist_index0;
}

void SIMDNoiseGenerator::set_cellular_distance2_index1(int p_index1) {
	set_cellular_distance2_indices(_cell_dist_index0, p_index1);
	
}

int SIMDNoiseGenerator::get_cellular_distance2_index1() const {
	return _cell_dist_index1;
}

void SIMDNoiseGenerator::set_cellular_jitter(float p_jitter) {
	_noise->SetCellularJitter(p_jitter);
	
}

float SIMDNoiseGenerator::get_cellular_jitter() const {
	return _noise->GetCellularJitter();
}

void SIMDNoiseGenerator::set_cellular_noise_lookup_type(NoiseType p_type) {
	_noise->SetCellularNoiseLookupType((FastNoiseSIMD::NoiseType)p_type);
	
}

SIMDNoiseGenerator::NoiseType SIMDNoiseGenerator::get_cellular_noise_lookup_type() const {
	return (NoiseType)_noise->GetCellularNoiseLookupType();
}

void SIMDNoiseGenerator::set_cellular_noise_lookup_frequency(float p_freq) {
	_noise->SetCellularNoiseLookupFrequency(p_freq);
	
}

float SIMDNoiseGenerator::get_cellular_noise_lookup_frequency() const {
	return _noise->GetCellularNoiseLookupFrequency();
}

void SIMDNoiseGenerator::_bind_methods() {

	// General settings
	ClassDB::bind_method(D_METHOD("set_noise_type", "type"), &SIMDNoiseGenerator::set_noise_type);
	ClassDB::bind_method(D_METHOD("get_noise_type"), &SIMDNoiseGenerator::get_noise_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "noise_type", PROPERTY_HINT_ENUM,
						 "Value,ValueFractal,Perlin,PerlinFractal,Simplex,SimplexFractal,WhiteNoise,Cellular,Cubic,CubicFractal"),
			"set_noise_type", "get_noise_type");

	ClassDB::bind_method(D_METHOD("set_seed", "seed"), &SIMDNoiseGenerator::set_seed);
	ClassDB::bind_method(D_METHOD("get_seed"), &SIMDNoiseGenerator::get_seed);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "seed"), "set_seed", "get_seed");

	ClassDB::bind_method(D_METHOD("set_frequency", "freq"), &SIMDNoiseGenerator::set_frequency);
	ClassDB::bind_method(D_METHOD("get_frequency"), &SIMDNoiseGenerator::get_frequency);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "frequency", PROPERTY_HINT_RANGE, "0.001,1"), "set_frequency", "get_frequency");

	ClassDB::bind_method(D_METHOD("set_axis_scales", "axis_scales"), &SIMDNoiseGenerator::set_axis_scales);
	ClassDB::bind_method(D_METHOD("get_axis_scales"), &SIMDNoiseGenerator::get_axis_scales);
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "axis_scales"), "set_axis_scales", "get_axis_scales");

	ClassDB::bind_method(D_METHOD("set_scale", "scale"), &SIMDNoiseGenerator::set_scale);
	ClassDB::bind_method(D_METHOD("get_scale"), &SIMDNoiseGenerator::get_scale);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "scale"), "set_scale", "get_scale");

	ClassDB::bind_method(D_METHOD("get_simd_level"), &SIMDNoiseGenerator::get_simd_level);

    // Generation
    ClassDB::bind_method(D_METHOD("get_noise_buffer", "rect", "height"), &SIMDNoiseGenerator::get_noise_buffer);
    ClassDB::bind_method(D_METHOD("fill_noise_buffer", "buffer", "pos", "height"), &SIMDNoiseGenerator::fill_noise_buffer);

	// Perturb

	ClassDB::bind_method(D_METHOD("set_perturb_type", "type"), &SIMDNoiseGenerator::set_perturb_type);
	ClassDB::bind_method(D_METHOD("get_perturb_type"), &SIMDNoiseGenerator::get_perturb_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "perturb_type", PROPERTY_HINT_ENUM, "None,Gradient,Gradient_Fractal,Normalize,Gradient_Normalize,Gradient_Fractal_Normalize"), "set_perturb_type", "get_perturb_type");

	ClassDB::bind_method(D_METHOD("set_perturb_amplitude", "amp"), &SIMDNoiseGenerator::set_perturb_amplitude);
	ClassDB::bind_method(D_METHOD("get_perturb_amplitude"), &SIMDNoiseGenerator::get_perturb_amplitude);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "perturb_amplitude"), "set_perturb_amplitude", "get_perturb_amplitude");

	ClassDB::bind_method(D_METHOD("set_perturb_frequency", "freq"), &SIMDNoiseGenerator::set_perturb_frequency);
	ClassDB::bind_method(D_METHOD("get_perturb_frequency"), &SIMDNoiseGenerator::get_perturb_frequency);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "perturb_frequency"), "set_perturb_frequency", "get_perturb_frequency");

	ClassDB::bind_method(D_METHOD("set_perturb_fractal_octaves", "octaves"), &SIMDNoiseGenerator::set_perturb_fractal_octaves);
	ClassDB::bind_method(D_METHOD("get_perturb_fractal_octaves"), &SIMDNoiseGenerator::get_perturb_fractal_octaves);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "perturb_fractal_octaves", PROPERTY_HINT_RANGE, "1,10,1"), "set_perturb_fractal_octaves", "get_perturb_fractal_octaves");

	ClassDB::bind_method(D_METHOD("set_perturb_fractal_lacunarity", "lacunarity"), &SIMDNoiseGenerator::set_perturb_fractal_lacunarity);
	ClassDB::bind_method(D_METHOD("get_perturb_fractal_lacunarity"), &SIMDNoiseGenerator::get_perturb_fractal_lacunarity);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "perturb_fractal_lacunarity"), "set_perturb_fractal_lacunarity", "get_perturb_fractal_lacunarity");

	ClassDB::bind_method(D_METHOD("set_perturb_fractal_gain", "gain"), &SIMDNoiseGenerator::set_perturb_fractal_gain);
	ClassDB::bind_method(D_METHOD("get_perturb_fractal_gain"), &SIMDNoiseGenerator::get_perturb_fractal_gain);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "perturb_fractal_gain"), "set_perturb_fractal_gain", "get_perturb_fractal_gain");

	ClassDB::bind_method(D_METHOD("set_perturb_normalize_length", "length"), &SIMDNoiseGenerator::set_perturb_normalize_length);
	ClassDB::bind_method(D_METHOD("get_perturb_normalize_length"), &SIMDNoiseGenerator::get_perturb_normalize_length);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "perturb_normalize_length"), "set_perturb_normalize_length", "get_perturb_normalize_length");

	// Fractal

	ClassDB::bind_method(D_METHOD("set_fractal_type", "type"), &SIMDNoiseGenerator::set_fractal_type);
	ClassDB::bind_method(D_METHOD("get_fractal_type"), &SIMDNoiseGenerator::get_fractal_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_type", PROPERTY_HINT_ENUM, "FBM,Billow,RidgedMulti"), "set_fractal_type", "get_fractal_type");

	ClassDB::bind_method(D_METHOD("set_fractal_octaves", "octaves"), &SIMDNoiseGenerator::set_fractal_octaves);
	ClassDB::bind_method(D_METHOD("get_fractal_octaves"), &SIMDNoiseGenerator::get_fractal_octaves);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_octaves", PROPERTY_HINT_RANGE, "1,10,1"), "set_fractal_octaves", "get_fractal_octaves");

	ClassDB::bind_method(D_METHOD("set_fractal_lacunarity", "lacunarity"), &SIMDNoiseGenerator::set_fractal_lacunarity);
	ClassDB::bind_method(D_METHOD("get_fractal_lacunarity"), &SIMDNoiseGenerator::get_fractal_lacunarity);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "fractal_lacunarity"), "set_fractal_lacunarity", "get_fractal_lacunarity");

	ClassDB::bind_method(D_METHOD("set_fractal_gain", "gain"), &SIMDNoiseGenerator::set_fractal_gain);
	ClassDB::bind_method(D_METHOD("get_fractal_gain"), &SIMDNoiseGenerator::get_fractal_gain);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "fractal_gain"), "set_fractal_gain", "get_fractal_gain");

	// Cellular

	ClassDB::bind_method(D_METHOD("set_cellular_distance_function", "func"), &SIMDNoiseGenerator::set_cellular_distance_function);
	ClassDB::bind_method(D_METHOD("get_cellular_distance_function"), &SIMDNoiseGenerator::get_cellular_distance_function);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_dist_func", PROPERTY_HINT_ENUM, "Euclidean,Manhattan,Natural"), "set_cellular_distance_function", "get_cellular_distance_function");

	ClassDB::bind_method(D_METHOD("set_cellular_jitter", "jitter"), &SIMDNoiseGenerator::set_cellular_jitter);
	ClassDB::bind_method(D_METHOD("get_cellular_jitter"), &SIMDNoiseGenerator::get_cellular_jitter);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "cellular_jitter"), "set_cellular_jitter", "get_cellular_jitter");

	ClassDB::bind_method(D_METHOD("set_cellular_return_type", "type"), &SIMDNoiseGenerator::set_cellular_return_type);
	ClassDB::bind_method(D_METHOD("get_cellular_return_type"), &SIMDNoiseGenerator::get_cellular_return_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_return_type", PROPERTY_HINT_ENUM, "CellValue,Distance,Distance2,Distance2Add,Distance2Sub,Distance2Mul,Distance2Div,Distance2Cave,NoiseLookup"), "set_cellular_return_type", "get_cellular_return_type");

	ClassDB::bind_method(D_METHOD("set_cellular_distance2_indices", "index0", "index1"), &SIMDNoiseGenerator::set_cellular_distance2_indices);
	ClassDB::bind_method(D_METHOD("get_cellular_distance2_indices"), &SIMDNoiseGenerator::get_cellular_distance2_indices);
	ClassDB::bind_method(D_METHOD("set_cellular_distance2_index0", "index0"), &SIMDNoiseGenerator::set_cellular_distance2_index0);
	ClassDB::bind_method(D_METHOD("get_cellular_distance2_index0"), &SIMDNoiseGenerator::get_cellular_distance2_index0);
	ClassDB::bind_method(D_METHOD("set_cellular_distance2_index1", "index1"), &SIMDNoiseGenerator::set_cellular_distance2_index1);
	ClassDB::bind_method(D_METHOD("get_cellular_distance2_index1"), &SIMDNoiseGenerator::get_cellular_distance2_index1);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_distance2_index0", PROPERTY_HINT_RANGE, "0,2,1"), "set_cellular_distance2_index0", "get_cellular_distance2_index0");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_distance2_index1", PROPERTY_HINT_RANGE, "1,3,1"), "set_cellular_distance2_index1", "get_cellular_distance2_index1");

	ClassDB::bind_method(D_METHOD("set_cellular_noise_lookup_type", "type"), &SIMDNoiseGenerator::set_cellular_noise_lookup_type);
	ClassDB::bind_method(D_METHOD("get_cellular_noise_lookup_type"), &SIMDNoiseGenerator::get_cellular_noise_lookup_type);
#ifdef SIMPLEX_ENABLED
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_noise_lookup_type", PROPERTY_HINT_ENUM,
						 "Value,ValueFractal,Perlin,PerlinFractal,Simplex,SimplexFractal,WhiteNoise,Cellular,Cubic,CubicFractal"),
			"set_cellular_noise_lookup_type", "get_cellular_noise_lookup_type");
#else
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cellular_noise_lookup_type", PROPERTY_HINT_ENUM,
						 "Value,ValueFractal,Perlin,PerlinFractal,WhiteNoise,Cellular,Cubic,CubicFractal"),
			"set_cellular_noise_lookup_type", "get_cellular_noise_lookup_type");
#endif

	ClassDB::bind_method(D_METHOD("set_cellular_noise_lookup_frequency", "freq"), &SIMDNoiseGenerator::set_cellular_noise_lookup_frequency);
	ClassDB::bind_method(D_METHOD("get_cellular_noise_lookup_frequency"), &SIMDNoiseGenerator::get_cellular_noise_lookup_frequency);
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "cellular_noise_lookup_frequency"), "set_cellular_noise_lookup_frequency", "get_cellular_noise_lookup_frequency");

	BIND_ENUM_CONSTANT(TYPE_VALUE);
	BIND_ENUM_CONSTANT(TYPE_VALUE_FRACTAL);
	BIND_ENUM_CONSTANT(TYPE_PERLIN);
	BIND_ENUM_CONSTANT(TYPE_PERLIN_FRACTAL);
#ifdef SIMPLEX_ENABLED
	BIND_ENUM_CONSTANT(TYPE_SIMPLEX);
	BIND_ENUM_CONSTANT(TYPE_SIMPLEX_FRACTAL);
#endif
	BIND_ENUM_CONSTANT(TYPE_WHITE_NOISE);
	BIND_ENUM_CONSTANT(TYPE_CELLULAR);
	BIND_ENUM_CONSTANT(TYPE_CUBIC);
	BIND_ENUM_CONSTANT(TYPE_CUBIC_FRACTAL);

	BIND_ENUM_CONSTANT(FRACTAL_FBM);
	BIND_ENUM_CONSTANT(FRACTAL_BILLOW);
	BIND_ENUM_CONSTANT(FRACTAL_RIDGED_MULTI);

	BIND_ENUM_CONSTANT(PERTURB_NONE);
	BIND_ENUM_CONSTANT(PERTURB_GRADIENT);
	BIND_ENUM_CONSTANT(PERTURB_GRADIENT_FRACTAL);
	BIND_ENUM_CONSTANT(PERTURB_NORMALIZE);
	BIND_ENUM_CONSTANT(PERTURB_GRADIENT_NORMALIZE);
	BIND_ENUM_CONSTANT(PERTURB_GRADIENT_FRACTAL_NORMALIZE);

	BIND_ENUM_CONSTANT(DISTANCE_EUCLIDEAN);
	BIND_ENUM_CONSTANT(DISTANCE_MANHATTAN);
	BIND_ENUM_CONSTANT(DISTANCE_NATURAL);

	BIND_ENUM_CONSTANT(RETURN_CELL_VALUE);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_ADD);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_SUB);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_MUL);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_DIV);
	BIND_ENUM_CONSTANT(RETURN_DISTANCE2_CAVE);
	BIND_ENUM_CONSTANT(RETURN_NOISE_LOOKUP);
}


SIMDNoiseGenerator::SIMDNoiseGenerator() {
    // Create a new FastNoiseSIMD for the highest supported instuction set of the CPU
	_noise = FastNoiseSIMD::NewFastNoiseSIMD();

	// Most defaults copied from the library
	set_noise_type(TYPE_VALUE);
	set_seed(0);
	set_frequency(0.01);
	set_axis_scales(Vector3(1.0, 1.0, 1.0));
	set_scale(1.0);

	set_fractal_type(FRACTAL_FBM);
	set_fractal_octaves(3);
	set_fractal_lacunarity(2.0);
	set_fractal_gain(0.5);

	set_cellular_distance_function(DISTANCE_EUCLIDEAN);
	set_cellular_return_type(RETURN_CELL_VALUE);
	set_cellular_distance2_indices(0, 1);
	set_cellular_jitter(0.45);

	set_perturb_type(PERTURB_NONE);
	set_perturb_amplitude(1);
	set_perturb_frequency(0.5);
	set_perturb_fractal_octaves(3);
	set_perturb_fractal_lacunarity(2.0);
	set_perturb_fractal_gain(0.5);
	set_perturb_normalize_length(1.0);
}

SIMDNoiseGenerator::~SIMDNoiseGenerator() {
    delete _noise;
}