// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once


#include "core/object/ref_counted.h"
#include <worldinstance.h>
#include <compositiondata.h>
#include <rawmesh.h>

class BiomeContext;
class WorldCompositorNode;

class WorldCompositor : public RefCounted {

	GDCLASS(WorldCompositor, RefCounted);

protected:
	static void _bind_methods();

public:
	void generate_biome(Node *p_biome_node);
	bool validate_biome(Node *p_biome_node);

	Ref<CompositionData> get_composition_data();
	void set_composition_data(const Ref<CompositionData> &p_composition_data);


	WorldCompositor();
	~WorldCompositor();
private:
	Ref<CompositionData> composition_data;

	WorldCompositorNode *compositor_node = nullptr;
	void wait_for_compositor_node() const;

	WorldInstance *_generate_biome(Node *p_biome);

	// Aux
	BiomeContext *generate_biome_context(Node *p_biome_node, ErrorLogger &r_err, const bool report_disabled);

	void process_biome_features(BiomeContext *p_biome_context, ErrorLogger &r_err) const;
	void process_regions(BiomeContext *p_biome_context, ErrorLogger &r_err) const;
	std::vector<RawMesh> polygonize_heightmap(BiomeContext *p_biome_context) const;
	void process_heightmap(BiomeContext *p_biome_context, ErrorLogger &r_err) const;
	void process_area_of_interest(BiomeContext *p_biome_context, ErrorLogger &r_err) const;
	void process_3d(BiomeContext *p_biome_context, ErrorLogger &r_err) const;
	void process_sea(BiomeContext *p_biome_context, ErrorLogger &r_err) const;
};
