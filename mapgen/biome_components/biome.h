// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include "scene/main/node.h"
#include <mapgenconstants.h>
#include <mappalette.h>

class Biome : public Node {

	GDCLASS(Biome, Node);

private:
	Ref<BiomePalette> palette;
	Ref<Material> water_material;
	Ref<Material> terrain_material;

	// TODO
	mutable Ref<Material> default_water_material;
	mutable Ref<Material> default_terrain_material;

	real_t ecotope_surface = 800.0;
	int ecotope_blend_length = 140;
	
	// Sea
	// Enable sea.
	bool sea_enabled = false;
	// level of the sea water.
	real_t sea_level = 2.0;
	// length of the beach transition.
	real_t border_length = 250.0;

protected:
	static void _bind_methods();

public:
	void set_palette(const Ref<BiomePalette> &p_palette);
	Ref<BiomePalette> get_palette() const;
	void set_water_material(const Ref<Material> &p_material);
	Ref<Material> get_water_material_or_default() const;
	Ref<Material> get_water_material() const;
	void set_terrain_material(const Ref<Material> &p_material);
	Ref<Material> get_terrain_material_or_default() const;
	Ref<Material> get_terrain_material() const;

	void set_ecotope_surface(const real_t p_surface);
	real_t get_ecotope_surface() const;
	void set_ecotope_blend_length(const int p_length);
	int get_ecotope_blend_length() const;

	void set_sea_enabled(const bool p_sea_enabled);
	bool get_sea_enabled() const;
	void set_sea_level(const real_t p_sea_level);
	real_t get_sea_level() const;
	void set_border_length(const real_t p_length);
	real_t get_border_length() const;


	bool is_valid(ErrorLogger &r_err) const;

	Biome();
	virtual ~Biome();
};
