// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "biomecomponent.h"

bool BiomeComponent::get_component_enabled() const {
    return component_enabled;
}

void BiomeComponent::set_component_enabled(const bool p_enabled) {
    component_enabled = p_enabled;
}

bool BiomeComponent::is_valid(ErrorLogger &r_err) const {
    return !r_err;
}

void BiomeComponent::_bind_methods() {
    ClassDB::bind_method(D_METHOD("set_component_enabled", "enabled"), &BiomeComponent::set_component_enabled);
	ClassDB::bind_method(D_METHOD("get_component_enabled"), &BiomeComponent::get_component_enabled);
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "enabled"), "set_component_enabled", "get_component_enabled");
}

BiomeComponent::BiomeComponent() {}

BiomeComponent::~BiomeComponent() {}
