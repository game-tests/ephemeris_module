// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/math/vector3.h"
#include <biomecomponent.h>
#include "scene/resources/material.h"
#include "servers/rendering_server.h"
#include "scene/resources/shape_3d.h"
#include "scene/resources/packed_scene.h"

class DistributionFeature : public BiomeComponent {

	GDCLASS(DistributionFeature, BiomeComponent);

public:
	enum GroupType {
		GROUP_GRASS,
		GROUP_BUSH,
		GROUP_TREE,
		GROUP_MISC,
	};

private:
	GroupType group_type = GroupType::GROUP_TREE;

	////////////////////////// RESOURCES
	// Scene of the distributed element.
	Ref<PackedScene> feature_scene;
	// Path to the Shape resource, leave empty to skip the collision
	// generation step.
	Ref<Shape3D> shape;

	////////////////////////// PLACEMENT
	// Probability of the mesh to be placed.
	real_t spawn_chance = 0.5;
	// Limit the slope where the object can be placed. (in rad)
	real_t min_slope = 0.0;
	real_t max_slope = 1.15;
	// Limit the height the object can be placed.
	real_t min_height = -100.0;
	real_t max_height = 100.0;
	// Offset of the y axis from the floor's y value.
	real_t min_y_offset = 0.0;
	real_t max_y_offset = 0.0;
	// Scaling factor between min_scale and max_scale. randomized for every mesh.
	Vector3 min_scale = Vector3(1.0, 1.0, 1.0);
	Vector3 max_scale = Vector3(1.0, 1.0, 1.0);
	// Rotation factor between min_scale and max_scale. randomized for every mesh.
	// A min_rotation of (0, 0, 0) and max_rotation of (0, 1, 0) means the 
	// mesh will rotate along the Y axis between 0 and 2*PI radians.
	Vector3 min_rotation = Vector3(0, 0, 0);
	Vector3 max_rotation = Vector3(0, Math_TAU, 0);
	// Align with the floor (ignores X and Z rotation).
	bool align_with_floor = true;
	bool cast_shadow = true;
	// Generates 3 random values for the shader to process.
	//bool use_custom_data;

	// Cached value of the mesh in the scene.
	mutable Ref<Mesh> mesh_cache;
	mutable Ref<Material> material_override_cache;

	// Returns true when the scene contains a mesh.
	 bool _update_mesh_cache() const;

protected:
	static void _bind_methods();

public:
	void set_group_type(const GroupType p_group_type);
	void set_feature_scene(const Ref<PackedScene> &p_feature_scene);
	void set_shape(const Ref<Shape3D> &p_shape);
	void set_spawn_chance(const real_t p_spawn_chance);
	void set_min_slope(const real_t p_min_slope);
	void set_max_slope(const real_t p_max_slope);
	void set_min_slope_degrees(const real_t p_min_slope_degrees);
	void set_max_slope_degrees(const real_t p_max_slope_degrees);
	void set_min_height(const real_t p_min_height);
	void set_max_height(const real_t p_max_height);
	void set_min_y_offset(const real_t p_min_y_offset);
	void set_max_y_offset(const real_t p_max_y_offset);
	void set_min_scale(const Vector3 &p_min_scale);
	void set_max_scale(const Vector3 &p_max_scale);
	void set_min_rotation(const Vector3 &p_min_rotation);
	void set_max_rotation(const Vector3 &p_max_rotation);
	void set_min_rotation_degrees(const Vector3 &p_min_rotation_degrees);
	void set_max_rotation_degrees(const Vector3 &p_max_rotation_degrees);
	void set_align_with_floor(const bool p_align_with_floor);
	void set_cast_shadow(const bool p_cast_shadow);

	GroupType get_group_type() const;
	Ref<PackedScene> get_feature_scene() const;
	Ref<Mesh> get_mesh() const;
	Ref<Material> get_material_override() const;
	Ref<Shape3D> get_shape() const;
	real_t get_spawn_chance() const;
	real_t get_min_slope() const;
	real_t get_max_slope() const;
	real_t get_min_slope_degrees() const;
	real_t get_max_slope_degrees() const;
	real_t get_min_height() const;
	real_t get_max_height() const;
	real_t get_min_y_offset() const;
	real_t get_max_y_offset() const;
	Vector3 get_min_scale() const;
	Vector3 get_max_scale() const;
	Vector3 get_min_rotation() const;
	Vector3 get_max_rotation() const;
	Vector3 get_min_rotation_degrees() const;
	Vector3 get_max_rotation_degrees() const;
	bool get_align_with_floor() const;
	bool get_cast_shadow() const;

	bool is_valid(ErrorLogger &r_err) const override;

	DistributionFeature();
};

VARIANT_ENUM_CAST(DistributionFeature::GroupType);