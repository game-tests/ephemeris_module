// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "distributionfeature.h"
#include "scene/3d/mesh_instance_3d.h"

void DistributionFeature::set_group_type(const GroupType p_group_type) {
	group_type = p_group_type;
}

void DistributionFeature::set_feature_scene(const Ref<PackedScene> &p_feature_scene) {
	feature_scene = p_feature_scene;
}

void DistributionFeature::set_shape(const Ref<Shape3D> &p_shape) {
	shape = p_shape;
}

void DistributionFeature::set_spawn_chance(const real_t p_spawn_chance) {
	spawn_chance = p_spawn_chance;
}

void DistributionFeature::set_min_slope(const real_t p_min_slope) {
	min_slope = MIN(p_min_slope, max_slope);
}

void DistributionFeature::set_max_slope(const real_t p_max_slope) {
	max_slope = MAX(p_max_slope, min_slope);
}

void DistributionFeature::set_min_slope_degrees(const real_t p_min_slope_degrees) {
	set_min_slope(Math::deg2rad(p_min_slope_degrees));
}

void DistributionFeature::set_max_slope_degrees(const real_t p_max_slope_degrees) {
	set_max_slope(Math::deg2rad(p_max_slope_degrees));
}

void DistributionFeature::set_min_height(const real_t p_min_height) {
	min_height = MIN(p_min_height, max_height);
}

void DistributionFeature::set_max_height(const real_t p_max_height) {
	max_height = MAX(p_max_height, min_height);
}

void DistributionFeature::set_min_y_offset(const real_t p_min_y_offset) {
	min_y_offset = MIN(p_min_y_offset, max_y_offset);
}

void DistributionFeature::set_max_y_offset(const real_t p_max_y_offset) {
	max_y_offset = MAX(p_max_y_offset, min_y_offset);
}

void DistributionFeature::set_min_scale(const Vector3 &p_min_scale) {
	min_scale.x = MIN(p_min_scale.x, max_scale.x);
	min_scale.y = MIN(p_min_scale.y, max_scale.y);
	min_scale.z = MIN(p_min_scale.z, max_scale.z);
}

void DistributionFeature::set_max_scale(const Vector3 &p_max_scale) {
	max_scale.x = MAX(p_max_scale.x, min_scale.x);
	max_scale.y = MAX(p_max_scale.y, min_scale.y);
	max_scale.z = MAX(p_max_scale.z, min_scale.z);
}

void DistributionFeature::set_min_rotation(const Vector3 &p_min_rotation) {
	min_rotation.x = MIN(p_min_rotation.x, max_rotation.x);
	min_rotation.y = MIN(p_min_rotation.y, max_rotation.y);
	min_rotation.z = MIN(p_min_rotation.z, max_rotation.z);
}

void DistributionFeature::set_max_rotation(const Vector3 &p_max_rotation) {
	max_rotation.x = MAX(p_max_rotation.x, min_rotation.x);
	max_rotation.y = MAX(p_max_rotation.y, min_rotation.y);
	max_rotation.z = MAX(p_max_rotation.z, min_rotation.z);
}

void DistributionFeature::set_min_rotation_degrees(const Vector3 &p_min_rotation_degrees) {
	set_min_rotation(Vector3(
		Math::deg2rad(p_min_rotation_degrees.x),
		Math::deg2rad(p_min_rotation_degrees.y),
		Math::deg2rad(p_min_rotation_degrees.z)
	));
}

void DistributionFeature::set_max_rotation_degrees(const Vector3 &p_max_rotation_degrees) {
	set_max_rotation(Vector3(
		Math::deg2rad(p_max_rotation_degrees.x),
		Math::deg2rad(p_max_rotation_degrees.y),
		Math::deg2rad(p_max_rotation_degrees.z)
	));
}

void DistributionFeature::set_align_with_floor(const bool p_align_with_floor) {
	align_with_floor = p_align_with_floor;
}

void DistributionFeature::set_cast_shadow(const bool p_cast_shadow) {
	cast_shadow = p_cast_shadow;
}

DistributionFeature::GroupType DistributionFeature::get_group_type() const {
	return group_type;
}

Ref<PackedScene> DistributionFeature::get_feature_scene() const {
	return feature_scene;
}

Ref<Mesh> DistributionFeature::get_mesh() const {
	return mesh_cache;
}

Ref<Material> DistributionFeature::get_material_override() const {
	return material_override_cache;
}

Ref<Shape3D> DistributionFeature::get_shape() const {
	return shape;
}

real_t DistributionFeature::get_spawn_chance() const {
	return spawn_chance;
}

real_t DistributionFeature::get_min_slope() const {
	return min_slope;
}

real_t DistributionFeature::get_max_slope() const {
	return max_slope;
}

real_t DistributionFeature::get_min_slope_degrees() const {
	return Math::rad2deg(get_min_slope());
}

real_t DistributionFeature::get_max_slope_degrees() const {
	return Math::rad2deg(get_max_slope());
}

real_t DistributionFeature::get_min_height() const {
	return min_height;
}

real_t DistributionFeature::get_max_height() const {
	return max_height;
}

real_t DistributionFeature::get_min_y_offset() const {
	return min_y_offset;
}

real_t DistributionFeature::get_max_y_offset() const {
	return max_y_offset;
}

Vector3 DistributionFeature::get_min_scale() const {
	return min_scale;
}

Vector3 DistributionFeature::get_max_scale() const {
	return max_scale;
}

Vector3 DistributionFeature::get_min_rotation() const {
	return min_rotation;
}

Vector3 DistributionFeature::get_max_rotation() const {
	return max_rotation;
}

Vector3 DistributionFeature::get_min_rotation_degrees() const {
	return Vector3(
		Math::rad2deg(min_rotation.x),
		Math::rad2deg(min_rotation.y),
		Math::rad2deg(min_rotation.z)
	);
}

Vector3 DistributionFeature::get_max_rotation_degrees() const {
	return Vector3(
		Math::rad2deg(max_rotation.x),
		Math::rad2deg(max_rotation.y),
		Math::rad2deg(max_rotation.z)
	);
}

bool DistributionFeature::get_align_with_floor() const {
	return align_with_floor;
}

bool DistributionFeature::get_cast_shadow() const {
	return cast_shadow;
}


Node *find_mesh_instance_in_node_tree(Node *p_node) {
	Node *res = nullptr;
	if (p_node->is_class("MeshInstance3D")) {
		res = p_node;
	} else {
		for (int i = 0; i < p_node->get_child_count(); i++) {
			res = find_mesh_instance_in_node_tree(p_node->get_child(i));
			if (res) {
				break;
			}
		}
	}
	return res;
}

bool DistributionFeature::_update_mesh_cache() const {
	bool res = false;
	if (feature_scene.is_valid()) {
		Node *scene_node = feature_scene->instantiate();
		Node *n = find_mesh_instance_in_node_tree(scene_node);
		if (n) {
			MeshInstance3D *mesh_node = Object::cast_to<MeshInstance3D>(n);
			if (mesh_node && mesh_node->get_mesh().is_valid()) {
				mesh_cache = mesh_node->get_mesh();
				material_override_cache = mesh_node->get_material_override();
				res = true;
			}
		}
		memdelete(scene_node);
	}
	return res;
}

void DistributionFeature::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_group_type", "group_type"), &DistributionFeature::set_group_type);
	ClassDB::bind_method(D_METHOD("set_feature_scene", "feature_scene"), &DistributionFeature::set_feature_scene);
	ClassDB::bind_method(D_METHOD("set_shape", "shape"), &DistributionFeature::set_shape);
	ClassDB::bind_method(D_METHOD("set_spawn_chance", "spawn_chance"), &DistributionFeature::set_spawn_chance);
	ClassDB::bind_method(D_METHOD("set_min_slope", "min_slope"), &DistributionFeature::set_min_slope);
	ClassDB::bind_method(D_METHOD("set_max_slope", "max_slope"), &DistributionFeature::set_max_slope);
	ClassDB::bind_method(D_METHOD("set_min_slope_degrees", "min_slope_degrees"), &DistributionFeature::set_min_slope_degrees);
	ClassDB::bind_method(D_METHOD("set_max_slope_degrees", "max_slope_degrees"), &DistributionFeature::set_max_slope_degrees);
	ClassDB::bind_method(D_METHOD("set_min_height", "min_height"), &DistributionFeature::set_min_height);
	ClassDB::bind_method(D_METHOD("set_max_height", "max_height"), &DistributionFeature::set_max_height);
	ClassDB::bind_method(D_METHOD("set_min_y_offset", "min_y_offset"), &DistributionFeature::set_min_y_offset);
	ClassDB::bind_method(D_METHOD("set_max_y_offset", "max_y_offset"), &DistributionFeature::set_max_y_offset);
	ClassDB::bind_method(D_METHOD("set_min_scale", "min_scale"), &DistributionFeature::set_min_scale);
	ClassDB::bind_method(D_METHOD("set_max_scale", "max_scale"), &DistributionFeature::set_max_scale);
	ClassDB::bind_method(D_METHOD("set_min_rotation", "min_rotation_radians"), &DistributionFeature::set_min_rotation);
	ClassDB::bind_method(D_METHOD("set_max_rotation", "max_rotation_radians"), &DistributionFeature::set_max_rotation);
	ClassDB::bind_method(D_METHOD("set_min_rotation_degrees", "min_rotation_degrees"), &DistributionFeature::set_min_rotation_degrees);
	ClassDB::bind_method(D_METHOD("set_max_rotation_degrees", "max_rotation_degrees"), &DistributionFeature::set_max_rotation_degrees);
	ClassDB::bind_method(D_METHOD("set_align_with_floor", "align_with_floor"), &DistributionFeature::set_align_with_floor);
	ClassDB::bind_method(D_METHOD("set_cast_shadow", "cast_shadow"), &DistributionFeature::set_cast_shadow);

	ClassDB::bind_method(D_METHOD("get_group_type"), &DistributionFeature::get_group_type);
	ClassDB::bind_method(D_METHOD("get_feature_scene"), &DistributionFeature::get_feature_scene);
	ClassDB::bind_method(D_METHOD("get_shape"), &DistributionFeature::get_shape);
	ClassDB::bind_method(D_METHOD("get_spawn_chance"), &DistributionFeature::get_spawn_chance);
	ClassDB::bind_method(D_METHOD("get_min_slope"), &DistributionFeature::get_min_slope);
	ClassDB::bind_method(D_METHOD("get_max_slope"), &DistributionFeature::get_max_slope);
	ClassDB::bind_method(D_METHOD("get_min_slope_degrees"), &DistributionFeature::get_min_slope_degrees);
	ClassDB::bind_method(D_METHOD("get_max_slope_degrees"), &DistributionFeature::get_max_slope_degrees);
	ClassDB::bind_method(D_METHOD("get_min_height"), &DistributionFeature::get_min_height);
	ClassDB::bind_method(D_METHOD("get_max_height"), &DistributionFeature::get_max_height);
	ClassDB::bind_method(D_METHOD("get_min_y_offset"), &DistributionFeature::get_min_y_offset);
	ClassDB::bind_method(D_METHOD("get_max_y_offset"), &DistributionFeature::get_max_y_offset);
	ClassDB::bind_method(D_METHOD("get_min_scale"), &DistributionFeature::get_min_scale);
	ClassDB::bind_method(D_METHOD("get_max_scale"), &DistributionFeature::get_max_scale);
	ClassDB::bind_method(D_METHOD("get_min_rotation"), &DistributionFeature::get_min_rotation);
	ClassDB::bind_method(D_METHOD("get_max_rotation"), &DistributionFeature::get_max_rotation);
	ClassDB::bind_method(D_METHOD("get_min_rotation_degrees"), &DistributionFeature::get_min_rotation_degrees);
	ClassDB::bind_method(D_METHOD("get_max_rotation_degrees"), &DistributionFeature::get_max_rotation_degrees);
	ClassDB::bind_method(D_METHOD("get_align_with_floor"), &DistributionFeature::get_align_with_floor);
	ClassDB::bind_method(D_METHOD("get_cast_shadow"), &DistributionFeature::get_cast_shadow);

	ADD_PROPERTY(PropertyInfo(Variant::INT, "group_type", PROPERTY_HINT_ENUM, "Grass,Bush,Tree,Misc"), "set_group_type", "get_group_type");

	ADD_GROUP("Resources", "");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "feature_scene", PROPERTY_HINT_RESOURCE_TYPE, "PackedScene"), "set_feature_scene", "get_feature_scene");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "shape", PROPERTY_HINT_RESOURCE_TYPE, "Shape3D"), "set_shape", "get_shape");

	ADD_GROUP("Placement", "");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "spawn_chance", PROPERTY_HINT_RANGE, "0.01,1,0.01"), "set_spawn_chance", "get_spawn_chance");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "min_slope", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR), "set_min_slope", "get_min_slope");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "max_slope", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR), "set_max_slope", "get_max_slope");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "min_slope_degrees", PROPERTY_HINT_RANGE, "-90,90,0.1"), "set_min_slope_degrees", "get_min_slope_degrees");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "max_slope_degrees", PROPERTY_HINT_RANGE, "-90,90,0.1"), "set_max_slope_degrees", "get_max_slope_degrees");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "min_height", PROPERTY_HINT_RANGE, "-100,100,0.1,or_lesser,or_greater"), "set_min_height", "get_min_height");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "max_height", PROPERTY_HINT_RANGE, "-100,100,0.1,or_lesser,or_greater"), "set_max_height", "get_max_height");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "min_y_offset", PROPERTY_HINT_RANGE, "-100,100,0.1,or_lesser,or_greater"), "set_min_y_offset", "get_min_y_offset");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "max_y_offset", PROPERTY_HINT_RANGE, "-100,100,0.1,or_lesser,or_greater"), "set_max_y_offset", "get_max_y_offset");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "min_scale"), "set_min_scale", "get_min_scale");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "max_scale"), "set_max_scale", "get_max_scale");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "min_rotation", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR), "set_min_rotation", "get_min_rotation");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "max_rotation", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_NO_EDITOR), "set_max_rotation", "get_max_rotation");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "min_rotation_degrees", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR), "set_min_rotation_degrees", "get_min_rotation_degrees");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "max_rotation_degrees", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR), "set_max_rotation_degrees", "get_max_rotation_degrees");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "align_with_floor"), "set_align_with_floor", "get_align_with_floor");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "cast_shadow"), "set_cast_shadow", "get_cast_shadow");

	BIND_ENUM_CONSTANT(GROUP_GRASS);
	BIND_ENUM_CONSTANT(GROUP_BUSH);
	BIND_ENUM_CONSTANT(GROUP_TREE);
	BIND_ENUM_CONSTANT(GROUP_MISC);
}

bool DistributionFeature::is_valid(ErrorLogger &r_err) const {
	if (feature_scene.is_null()) {
		r_err.push_contex_error("Empty feature scene");
	}
	if (!_update_mesh_cache()) {
		r_err.push_contex_error("The feature scene does not contain a Mesh");
	}
	if (spawn_chance <= 0.0) {
		r_err.push_contex_error("Spawn chance cannot be 0 or negative");
	}
	r_err.close_context("The " + get_class() + " node \"" + get_name() + "\" has an invalid configuration");
	return !r_err;
}

DistributionFeature::DistributionFeature() {}