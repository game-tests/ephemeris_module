// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "mappalette.h"

//EcotopePalette
Color EcotopePalette::get_terrain_color() const {
	return terrain_color;
}

void EcotopePalette::set_terrain_color(const Color &p_color) {
	terrain_color = p_color;
}

Color EcotopePalette::get_vegetation_color_1() const {
	return vegetation_color_1;
}

void EcotopePalette::set_vegetation_color_1(const Color &p_color) {
	vegetation_color_1 = p_color;
}

Color EcotopePalette::get_vegetation_color_2() const {
	return vegetation_color_2;
}

void EcotopePalette::set_vegetation_color_2(const Color &p_color) {
	vegetation_color_2 = p_color;
}

void EcotopePalette::_bind_methods() {
	ClassDB::bind_method(D_METHOD("get_terrain_color"), &EcotopePalette::get_terrain_color);
	ClassDB::bind_method(D_METHOD("set_terrain_color"), &EcotopePalette::set_terrain_color);
	ClassDB::bind_method(D_METHOD("get_vegetation_color_1"), &EcotopePalette::get_vegetation_color_1);
	ClassDB::bind_method(D_METHOD("set_vegetation_color_1"), &EcotopePalette::set_vegetation_color_1);
	ClassDB::bind_method(D_METHOD("get_vegetation_color_2"), &EcotopePalette::get_vegetation_color_2);
	ClassDB::bind_method(D_METHOD("set_vegetation_color_2"), &EcotopePalette::set_vegetation_color_2);

	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "terrain_color", PROPERTY_HINT_COLOR_NO_ALPHA), "set_terrain_color", "get_terrain_color");
	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "vegetation_color_1", PROPERTY_HINT_COLOR_NO_ALPHA), "set_vegetation_color_1", "get_vegetation_color_1");
	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "vegetation_color_2", PROPERTY_HINT_COLOR_NO_ALPHA), "set_vegetation_color_2", "get_vegetation_color_2");
}

EcotopePalette::EcotopePalette() {
	terrain_color = Color(0.93, 0.58, 0.12);
	water_color = Color(0.00, 0.65, 1.00);
	vegetation_color_1 = Color(0.03, 0.65, 0.03);
	vegetation_color_2 = Color(0.03, 0.65, 0.03);
}

EcotopePalette::~EcotopePalette() {

}


// BiomePalette
Color BiomePalette::get_sea_color() const {
	return sea_color;
}

void BiomePalette::set_sea_color(const Color &p_color) {
	sea_color = p_color;
}

Color BiomePalette::get_rock_color() const {
	return rock_color;
}

void BiomePalette::set_rock_color(const Color &p_color) {
	rock_color = p_color;
}

Color BiomePalette::get_sand_color() const {
	return sand_color;
}

void BiomePalette::set_sand_color(const Color &p_color) {
	sand_color = p_color;
}

void BiomePalette::_bind_methods() {
	ClassDB::bind_method(D_METHOD("get_sea_color"), &BiomePalette::get_sea_color);
	ClassDB::bind_method(D_METHOD("set_sea_color"), &BiomePalette::set_sea_color);
	ClassDB::bind_method(D_METHOD("get_rock_color"), &BiomePalette::get_rock_color);
	ClassDB::bind_method(D_METHOD("set_rock_color"), &BiomePalette::set_rock_color);
	ClassDB::bind_method(D_METHOD("get_sand_color"), &BiomePalette::get_sand_color);
	ClassDB::bind_method(D_METHOD("set_sand_color"), &BiomePalette::set_sand_color);

	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "sea_color", PROPERTY_HINT_COLOR_NO_ALPHA), "set_sea_color", "get_sea_color");
	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "rock_color", PROPERTY_HINT_COLOR_NO_ALPHA), "set_rock_color", "get_rock_color");
	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "sand_color", PROPERTY_HINT_COLOR_NO_ALPHA), "set_sand_color", "get_sand_color");
}

BiomePalette::BiomePalette() {
	sea_color = Color(0.00, 0.65, 1.00);
	rock_color = Color(0.25, 0.14, 0.02);
	sand_color = Color(0.95, 0.80, 0.31);

}

BiomePalette::~BiomePalette() {}