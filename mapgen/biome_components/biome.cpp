// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "biome.h"
#include "servers/audio_server.h"

Ref<Material> get_default_terrain_material();
Ref<Material> get_default_water_material();


void Biome::set_palette(const Ref<BiomePalette> &p_palette) {
	palette = p_palette;
}

Ref<BiomePalette> Biome::get_palette() const {
	return palette;
}

void Biome::set_water_material(const Ref<Material> &p_material) {
	water_material = p_material;
}

Ref<Material> Biome::get_water_material() const {
	return water_material;
}

Ref<Material> Biome::get_water_material_or_default() const {
	if (water_material.is_null()) {
		if (default_water_material.is_null()) {
			default_water_material = get_default_water_material();
		}
		return default_water_material;
	} else {
		return water_material;
	}
}

void Biome::set_terrain_material(const Ref<Material> &p_material) {
	terrain_material = p_material;
}

Ref<Material> Biome::get_terrain_material() const {
	return terrain_material;
}

Ref<Material> Biome::get_terrain_material_or_default() const {
	if (terrain_material.is_null()) {
		if (default_terrain_material.is_null()) {
			default_terrain_material = get_default_terrain_material();
		}
		return default_terrain_material;
	} else {
		return terrain_material;
	}
}

void Biome::set_ecotope_surface(const real_t p_surface) {
	ecotope_surface = p_surface;
}

real_t Biome::get_ecotope_surface() const {
	return ecotope_surface;
}

void Biome::set_ecotope_blend_length(const int p_length) {
	ecotope_blend_length = p_length;
}

int Biome::get_ecotope_blend_length() const {
	return ecotope_blend_length;
}

void Biome::set_sea_enabled(const bool p_sea_enabled) {
	sea_enabled = p_sea_enabled;
}

bool Biome::get_sea_enabled() const {
	return sea_enabled;
}

void Biome::set_sea_level(const real_t p_sea_level) {
	sea_level = p_sea_level;
}

real_t Biome::get_sea_level() const {
	return sea_level;
}

void Biome::set_border_length(const real_t p_length) {
	border_length = p_length;
}

real_t Biome::get_border_length() const {
	return border_length;
}

bool Biome::is_valid(ErrorLogger &r_err) const {
	if (ecotope_surface <= 0) {
		r_err.push_contex_error("Ecotope surface can't be 0 or less");
	}
	if (ecotope_blend_length < 0) {
		r_err.push_contex_error("Ecotope blend length can't be less than 0");
	}
	r_err.close_context("invalid Biome configuration");
	return !r_err;
}

void Biome::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_palette", "palette"), &Biome::set_palette);
	ClassDB::bind_method(D_METHOD("get_palette"), &Biome::get_palette);
	ClassDB::bind_method(D_METHOD("set_water_material", "material"), &Biome::set_water_material);
	ClassDB::bind_method(D_METHOD("get_water_material"), &Biome::get_water_material);
	ClassDB::bind_method(D_METHOD("set_terrain_material", "material"), &Biome::set_terrain_material);
	ClassDB::bind_method(D_METHOD("get_terrain_material"), &Biome::get_terrain_material);
	ClassDB::bind_method(D_METHOD("set_ecotope_surface", "surface"), &Biome::set_ecotope_surface);
	ClassDB::bind_method(D_METHOD("get_ecotope_surface"), &Biome::get_ecotope_surface);

	ClassDB::bind_method(D_METHOD("set_ecotope_blend_length", "length"), &Biome::set_ecotope_blend_length);
	ClassDB::bind_method(D_METHOD("get_ecotope_blend_length"), &Biome::get_ecotope_blend_length);

	ClassDB::bind_method(D_METHOD("set_sea_enabled", "sea_enabled"), &Biome::set_sea_enabled);
	ClassDB::bind_method(D_METHOD("get_sea_enabled"), &Biome::get_sea_enabled);
	ClassDB::bind_method(D_METHOD("set_sea_level", "sea_level"), &Biome::set_sea_level);
	ClassDB::bind_method(D_METHOD("get_sea_level"), &Biome::get_sea_level);
	ClassDB::bind_method(D_METHOD("set_border_length", "sea_length"), &Biome::set_border_length);
	ClassDB::bind_method(D_METHOD("get_border_length"), &Biome::get_border_length);

	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "palette", PROPERTY_HINT_RESOURCE_TYPE, "BiomePalette"), "set_palette", "get_palette");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "water_material", PROPERTY_HINT_RESOURCE_TYPE, "ShaderMaterial,StandardMaterial3D"), "set_water_material", "get_water_material");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "terrain_material", PROPERTY_HINT_RESOURCE_TYPE, "ShaderMaterial,StandardMaterial3D"), "set_terrain_material", "get_terrain_material");
	
	ADD_GROUP("Ecotope", "");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "ecotope_surface", PROPERTY_HINT_RANGE, "1,1500,1,or_greater"), "set_ecotope_surface", "get_ecotope_surface");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "ecotope_blend_length"), "set_ecotope_blend_length", "get_ecotope_blend_length");
	
	ADD_GROUP("Sea", "");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "sea_enabled"), "set_sea_enabled", "get_sea_enabled");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "sea_level", PROPERTY_HINT_RANGE, "-30,10,1"), "set_sea_level", "get_sea_level");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "border_length", PROPERTY_HINT_RANGE, "1,1500,1,or_greater"), "set_border_length", "get_border_length");
}

Biome::Biome() {
}

Biome::~Biome() {}



Ref<Material> get_default_terrain_material() {
	Ref<Shader> terrain_shader = memnew(Shader);

	terrain_shader->set_code(R"(
// Low poly terrain shader.

shader_type spatial;

uniform vec4 wall_color : source_color;
varying flat vec3 v_color;

void vertex() {
	v_color = COLOR.rgb;
}

void fragment() {
	// same normal vector for every face
	NORMAL = -normalize(cross(dFdx(VERTEX), dFdy(VERTEX)));
	
	vec3 world_normal = NORMAL * mat3(
		VIEW_MATRIX[0].xyz,
		VIEW_MATRIX[1].xyz,
		VIEW_MATRIX[2].xyz);
	
	// if (world_normal.y < 0.5) wall_color else v_color
	//ALBEDO = mix(wall_color.rgb, v_color.rgb, step(0.5, world_normal.y));
	ALBEDO = mix(wall_color.rgb, v_color.rgb, 1);
	ROUGHNESS = 0.45;
	METALLIC = 0.0;
}
)");

	Ref<ShaderMaterial> m = memnew(ShaderMaterial);
	m->set_shader(terrain_shader);
	return m;
}


Ref<Material> get_default_water_material() {
	Ref<Shader> water_shader = memnew(Shader);

	water_shader->set_code(R"(
// Low poly water shader.

shader_type spatial;

render_mode blend_mix, depth_draw_always;

uniform float wave_length = 4.0;
uniform float wave_amplitude = 0.2;
uniform float wave_speed = 0.15;
uniform float wave_height = 1.5;
uniform float opacity = 1.0;
uniform vec4 albedo : source_color = vec4(0.0624, 0.3485, 0.3554, 1.0);
uniform vec4 shoreline_color : source_color = vec4(1.0, 1.0, 1.0, 1.0);
uniform vec4 absorption_color : source_color = vec4(0.0195, 1.0, 0.5174, 1.0);
uniform float near_plane = 0.1;
uniform float far_plane = 400.0;

float generate_offset(vec2 xz, float factor1, float factor2, float time) {
	float pi = 3.141592;
	float x = xz[0];
	float z = xz[1];
	float xval = ((mod(x + z * x * factor1, wave_length) / wave_length) + (time * wave_speed) * mod(x * 0.8 + z, 1.5)) * 2.0 * pi;
	float zval = ((mod(factor2 * (z * x + x * z), wave_length) / wave_length) + (time * wave_speed) * 2.0 * mod(x, 2.0)) * 2.0 * pi;
	return wave_amplitude * 0.5 * (sin(zval) + cos(xval));
}

vec3 apply_distortion(vec3 point, float time) {
	float x_dist = generate_offset(point.xz, 0.2, 0.1, time);
	float y_dist = generate_offset(point.xz, 0.5, 0.3, time) * wave_height;
	float z_dist = generate_offset(point.xz, 0.15, 0.2, time);
	return point + vec3(x_dist, y_dist, z_dist);
}

void vertex() {
	VERTEX = apply_distortion(VERTEX, TIME);
}

float to_linear_depth(float val) {
	float near = near_plane;
	float far = far_plane;
	val = 2.0 * val - 1.0;
	val = 2.0 * near * far / (far + near - val * (far - near));
	return val;
}

float calculate_water_depth(sampler2D depth_tex, vec2 screen_uv, float frag_coord_z) {
	float floor_distance = to_linear_depth(texture(depth_tex, screen_uv).r);
	float water_distance = to_linear_depth(frag_coord_z);
	return floor_distance - water_distance;
}

void fragment() {
	// Low-poly
	NORMAL = normalize(cross(dFdx(VERTEX), dFdy(VERTEX)));
  
	// Water color
	ALBEDO = albedo.rgb;
	ROUGHNESS = 0.1;
	SPECULAR = 0.2;
  
	// Transparency
	ALPHA = opacity;
  
	// Shore and light absorption
	float water_depth = calculate_water_depth(DEPTH_TEXTURE, SCREEN_UV, FRAGCOORD.z);
	if (water_depth < 2.0) {
		ALBEDO = shoreline_color.rgb;
	} else if (water_depth < 10.0) {
		float factor = smoothstep(1.0, 0.0, water_depth / 7.0);
		ALBEDO = absorption_color.rgb * factor + albedo.rgb;
	}
}
)");

	Ref<ShaderMaterial> m = memnew(ShaderMaterial);
	m->set_shader(water_shader);
	return m;
}
