// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include <mappalette.h>
#include <noise_graph.h>
#include <biomecomponent.h>
#include <influence_curve.h>

class Ecotope : public BiomeComponent {

	GDCLASS(Ecotope, BiomeComponent);

private:
	// distribution noise function threshold:
	// - A mask of 0 means no content.
	// - 1 means distribution in the entire region.
	// - Intermediate values define the size of "islands" over the area.
	real_t grass_mask = 0.2;
	real_t bush_mask = 0.3;
	real_t tree_mask = 0.35;
	real_t misc_mask = 0.8;

	// Minimun absolute distance between the elementos of each distribution
	// group.
	real_t grass_min_distance = 0.75;
	real_t bush_min_distance = 3.0;
	real_t tree_min_distance = 8.0;
	real_t misc_min_distance = 12.0;

	// Minimun absolute distance between the elementos of each distribution
	// group.
	real_t grass_noise_freq = 0.09;
	real_t bush_noise_freq = 0.02;
	real_t tree_noise_freq = 0.02;
	real_t misc_noise_freq = 0.02;

	real_t grass_group_freq = 0.45;
	real_t bush_group_freq = 0.45;
	real_t tree_group_freq = 0.45;
	real_t misc_group_freq = 0.45;

	Ref<EcotopePalette> palette;
	Ref<NoiseGraph> noise_graph;

	Ref<NoiseGraph> noise_graph_3d;
	Ref<InfluenceCurve> height_curve;
	real_t threshold_3d = 0.0;

	// Always instance this ecotope in the generation.
	bool force_generation = false;

protected:
	static void _bind_methods();

	GDVIRTUAL2RC(Ref<NoiseBuffer>, _generate_noise_region, Rect2i, int)

public:

	Ref<NoiseBuffer> generate_noise_region(const Rect2i &p_rect, const int p_seed);

	void set_grass_mask(const real_t p_grass_mask);
	real_t get_grass_mask() const;
	void set_bush_mask(const real_t p_bush_mask);
	real_t get_bush_mask() const;
	void set_tree_mask(const real_t p_tree_mask);
	real_t get_tree_mask() const;
	void set_misc_mask(const real_t p_misc_mask);
	real_t get_misc_mask() const;

	void set_grass_min_distance(const real_t p_grass_min_distance);
	real_t get_grass_min_distance() const;
	void set_bush_min_distance(const real_t p_bush_min_distance);
	real_t get_bush_min_distance() const;
	void set_tree_min_distance(const real_t p_tree_min_distance);
	real_t get_tree_min_distance() const;
	void set_misc_min_distance(const real_t p_misc_min_distance);
	real_t get_misc_min_distance() const;

	void set_grass_noise_freq(const real_t p_grass_noise_freq);
	real_t get_grass_noise_freq() const;
	void set_bush_noise_freq(const real_t p_bush_noise_freq);
	real_t get_bush_noise_freq() const;
	void set_tree_noise_freq(const real_t p_tree_noise_freq);
	real_t get_tree_noise_freq() const;
	void set_misc_noise_freq(const real_t p_misc_noise_freq);
	real_t get_misc_noise_freq() const;

	void set_grass_group_freq(const real_t p_grass_group_freq);
	real_t get_grass_group_freq() const;
	void set_bush_group_freq(const real_t p_bush_group_freq);
	real_t get_bush_group_freq() const;
	void set_tree_group_freq(const real_t p_tree_group_freq);
	real_t get_tree_group_freq() const;
	void set_misc_group_freq(const real_t p_misc_group_freq);
	real_t get_misc_group_freq() const;


	void set_palette(Ref<EcotopePalette> p_palette);
	Ref<EcotopePalette> get_palette() const;

	void set_noise_graph(Ref<NoiseGraph> p_noise_graph);
	Ref<NoiseGraph> get_noise_graph() const;

	void set_height_curve(Ref<InfluenceCurve> p_height_curve);
	Ref<InfluenceCurve> get_height_curve() const;

	void set_noise_graph_3d(Ref<NoiseGraph> p_noise_graph);
	Ref<NoiseGraph> get_noise_graph_3d() const;

	void set_3d_threshold(const real_t p_threshold);
	real_t get_3d_threshold() const;

	void set_force_generation(const bool p_force_generation);
	bool get_force_generation() const;

	bool is_valid(ErrorLogger &r_err) const override;

	Ecotope();
	virtual ~Ecotope();

};