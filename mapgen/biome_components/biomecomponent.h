// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "scene/main/node.h"
#include <mapgenconstants.h>
#include <vector>

class BiomeComponent : public Node {

	GDCLASS(BiomeComponent, Node);

protected:
	static void _bind_methods();

public:
	bool get_component_enabled() const;
	void set_component_enabled(const bool p_enabled);

	virtual bool is_valid(ErrorLogger &r_err) const;

	BiomeComponent();
	virtual ~BiomeComponent();

private:
	bool component_enabled = true;
};
