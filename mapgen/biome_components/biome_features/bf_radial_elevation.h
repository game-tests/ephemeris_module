// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <biomefeature.h>


class BFRadialElevation : public BiomeFeature {

	GDCLASS(BFRadialElevation, BiomeFeature);

public:
	enum FunctionType {
		FUNCTION_LINEAL,
		FUNCTION_LOGARITHMIC,
	};

private:
	real_t distance_factor = -0.15;
	FunctionType f_type = FunctionType::FUNCTION_LINEAL;
	int length_from_border = -1;


protected:
	static void _bind_methods();

public:
	// Process method
	void process_biome(Matrix<float> &p_height, const int p_seed) override;

	real_t get_distance_factor() const;
	void set_distance_factor(const real_t p_factor);
	FunctionType get_function_type() const;
	void set_function_type(const FunctionType p_type);
	int get_length_from_border() const;
	void set_length_from_border(int p_length);

	bool is_valid(ErrorLogger &r_err) const override;

	BFRadialElevation();
	virtual ~BFRadialElevation();
};

VARIANT_ENUM_CAST(BFRadialElevation::FunctionType)
