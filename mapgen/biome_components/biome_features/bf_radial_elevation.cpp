// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "bf_radial_elevation.h"
#include "core/math/math_defs.h"
#include "core/math/vector2i.h"

void BFRadialElevation::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_distance_factor", "factor"), &BFRadialElevation::set_distance_factor);
	ClassDB::bind_method(D_METHOD("get_distance_factor"), &BFRadialElevation::get_distance_factor);
	ClassDB::bind_method(D_METHOD("set_function_type", "type"), &BFRadialElevation::set_function_type);
	ClassDB::bind_method(D_METHOD("get_function_type"), &BFRadialElevation::get_function_type);
	ClassDB::bind_method(D_METHOD("set_length_from_border", "length"), &BFRadialElevation::set_length_from_border);
	ClassDB::bind_method(D_METHOD("get_length_from_border"), &BFRadialElevation::get_length_from_border);

	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "distance_factor"), "set_distance_factor", "get_distance_factor");
	//ADD_PROPERTY(PropertyInfo(Variant::INT, "function_type", PROPERTY_HINT_ENUM, "Lineal,Logarithmic"), "set_function_type", "get_function_type");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "length_from_border"), "set_length_from_border", "get_length_from_border");
}

void BFRadialElevation::process_biome(Matrix<float> &p_height, const int p_seed) {
	const int l = length_from_border < 0 ? (p_height.rows() / 2) : length_from_border;
	const real_t elevation_offset = l * -distance_factor;

	const Vector2 center = p_height.size_vector() / 2;
	const real_t start_distance = (p_height.rows() / 2) - l;

	for (int x = 0; x < p_height.rows(); x++) {
		for (int y = 0; y < p_height.columns(); y++) {
			// compensate the border elevation of the world
			p_height(x, y) += elevation_offset;

			const real_t d = center.distance_to(Vector2(x, y)) - start_distance;
			if (d >= 0) {
				p_height(x, y) += distance_factor * d;
			}
		}
	}

}

real_t BFRadialElevation::get_distance_factor() const {
	return distance_factor;
}

void BFRadialElevation::set_distance_factor(const real_t p_factor) {
	distance_factor = p_factor;
}

BFRadialElevation::FunctionType BFRadialElevation::get_function_type() const {
	return f_type;
}

void BFRadialElevation::set_function_type(const FunctionType p_type) {
	f_type = p_type;
}

int BFRadialElevation::get_length_from_border() const {
	return length_from_border;
}

void BFRadialElevation::set_length_from_border(int p_length) {
	length_from_border = p_length;
}

bool BFRadialElevation::is_valid(ErrorLogger &r_err) const {
	return true;
}

BFRadialElevation::BFRadialElevation() {}

BFRadialElevation::~BFRadialElevation() {}
