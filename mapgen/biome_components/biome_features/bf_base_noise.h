// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <biomefeature.h>
#include <noise_graph.h>


class BFBaseNoise : public BiomeFeature {

	GDCLASS(BFBaseNoise, BiomeFeature);

private:
	Ref<NoiseGraph> noise_graph;


protected:
	static void _bind_methods();

public:
	// Process method
	void process_biome(Matrix<float> &p_height, const int p_seed) override;

	void set_noise_graph(Ref<NoiseGraph> p_noise_graph);
	Ref<NoiseGraph> get_noise_graph() const;

	bool is_valid(ErrorLogger &r_err) const override;

	BFBaseNoise();
	virtual ~BFBaseNoise();
};
