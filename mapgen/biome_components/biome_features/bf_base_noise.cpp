// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "bf_base_noise.h"
#include "core/math/rect2i.h"
#include "core/math/vector2i.h"

void BFBaseNoise::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_noise_graph", "noise_graph"), &BFBaseNoise::set_noise_graph);
	ClassDB::bind_method(D_METHOD("get_noise_graph"), &BFBaseNoise::get_noise_graph);

	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "noise_graph", PROPERTY_HINT_RESOURCE_TYPE, "NoiseGraph"), "set_noise_graph", "get_noise_graph");
}

void BFBaseNoise::process_biome(Matrix<float> &p_height, const int p_seed) {
	Ref<NoiseBuffer> nb = noise_graph->generate_noise(Rect2i(Vector2i(0,0), p_height.size_vector()), 0, 1, p_seed);

	for (int x = 0; x < p_height.rows(); x++) {
		for (int y = 0; y < p_height.columns(); y++) {
			p_height(x, y) += nb->get_value(x, y);
		}
	}

}

void BFBaseNoise::set_noise_graph(Ref<NoiseGraph> p_noise_graph) {
	noise_graph = p_noise_graph;
}

Ref<NoiseGraph> BFBaseNoise::get_noise_graph() const {
	return noise_graph;
}


bool BFBaseNoise::is_valid(ErrorLogger &r_err) const {
	return true;
}

BFBaseNoise::BFBaseNoise() {}

BFBaseNoise::~BFBaseNoise() {}
