// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <biomecomponent.h>
#include "scene/resources/packed_scene.h"

class AreaOfInterest : public BiomeComponent {

	GDCLASS(AreaOfInterest, BiomeComponent);

private:
	Ref<PackedScene> feature_scene;

	// Offset of the y axis from the floor's y value.
	real_t min_y_offset = 0.0;
	real_t max_y_offset = 0.0;

	real_t reserve_radius = 0.0;
	distribution_layer_t d_layers = ~0;

	// control frecuency and if optional/forced at least one
	int min_instances = 0;
	int max_instances = 1000;

	Vector3 min_rotation = Vector3(0, 0, 0);
	Vector3 max_rotation = Vector3(0, Math_TAU, 0);

	bool scale_with_world = false;

protected:
	static void _bind_methods();

public:
	void set_feature_scene(const Ref<PackedScene> &p_feature_scene);
	Ref<PackedScene> get_feature_scene() const;

	void set_min_y_offset(const real_t p_offset);
	real_t get_min_y_offset() const;

	void set_max_y_offset(const real_t p_offset);
	real_t get_max_y_offset() const;

	void set_min_instances(const int p_instances);
	int get_min_instances() const;

	void set_max_instances(const int p_instances);
	int get_max_instances() const;

	void set_scale_with_world(const bool p_scale);
	bool get_scale_with_world() const;

	void set_reserve_radius(const real_t p_radius);
	real_t get_reserve_radius() const;

	void set_min_rotation(const Vector3 &p_min_rotation);
	void set_max_rotation(const Vector3 &p_max_rotation);
	void set_min_rotation_degrees(const Vector3 &p_min_rotation_degrees);
	void set_max_rotation_degrees(const Vector3 &p_max_rotation_degrees);

	Vector3 get_min_rotation() const;
	Vector3 get_max_rotation() const;
	Vector3 get_min_rotation_degrees() const;
	Vector3 get_max_rotation_degrees() const;

	void set_grass_layer(const bool p_grass_layer);
	bool get_grass_layer() const;

	void set_bush_layer(const bool p_bush_layer);
	bool get_bush_layer() const;

	void set_tree_layer(const bool p_tree_layer);
	bool get_tree_layer() const;

	void set_misc_layer(const bool p_misc_layer);
	bool get_misc_layer() const;

	distribution_layer_t get_distribution_layers() const;

	bool is_valid(ErrorLogger &r_err) const override;

	AreaOfInterest();
};