// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/math/color.h"
#include "core/io/resource.h"

class EcotopePalette : public Resource {
	GDCLASS(EcotopePalette, Resource);
	OBJ_SAVE_TYPE(EcotopePalette);

private:
	Color terrain_color;
	Color water_color;
	Color vegetation_color_1;
	Color vegetation_color_2;

protected:
	static void _bind_methods();

public:
	Color get_terrain_color() const;
	void set_terrain_color(const Color &p_color);
	Color get_water_color() const;
	void set_water_color(const Color &p_color);
	Color get_vegetation_color_1() const;
	void set_vegetation_color_1(const Color &p_color);
	Color get_vegetation_color_2() const;
	void set_vegetation_color_2(const Color &p_color);

	EcotopePalette();
	virtual ~EcotopePalette();
};

class BiomePalette : public Resource {
	GDCLASS(BiomePalette, Resource);
	OBJ_SAVE_TYPE(BiomePalette);

private:
	Color sea_color;
	Color rock_color;
	Color sand_color;

protected:
	static void _bind_methods();

public:
	Color get_sea_color() const;
	void set_sea_color(const Color &p_color);
	Color get_rock_color() const;
	void set_rock_color(const Color &p_color);
	Color get_sand_color() const;
	void set_sand_color(const Color &p_color);

	BiomePalette();
	virtual ~BiomePalette();
};