// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "ecotope.h"


Ref<NoiseBuffer> Ecotope::generate_noise_region(const Rect2i &p_rect, const int p_seed) {

	Ref<NoiseBuffer> res;

	//if (get_script_instance() && get_script_instance()->has_method("generate_noise_region")) {
	//	res = get_script_instance()->call("generate_noise_region", p_rect, p_seed);
	//}

	if (noise_graph.is_valid()) {
		res = noise_graph->generate_noise(p_rect, 0, 1, p_seed);
	}

	if (res.is_null()) {
		res.instantiate();
		res->resize_v(p_rect.size);
	}
	return res;
}

void Ecotope::set_grass_mask(const real_t p_grass_mask) {
	grass_mask = p_grass_mask;
}

real_t Ecotope::get_grass_mask() const {
	return grass_mask;
}

void Ecotope::set_bush_mask(const real_t p_bush_mask) {
	bush_mask = p_bush_mask;
}

real_t Ecotope::get_bush_mask() const {
	return bush_mask;
}

void Ecotope::set_tree_mask(const real_t p_tree_mask) {
	tree_mask = p_tree_mask;
}

real_t Ecotope::get_tree_mask() const {
	return tree_mask;
}

void Ecotope::set_misc_mask(const real_t p_misc_mask) {
	misc_mask = p_misc_mask;
}

real_t Ecotope::get_misc_mask() const {
	return misc_mask;
}

void Ecotope::set_grass_min_distance(const real_t p_grass_min_distance) {
	grass_min_distance = p_grass_min_distance;
}

real_t Ecotope::get_grass_min_distance() const {
	return grass_min_distance;
}

void Ecotope::set_bush_min_distance(const real_t p_bush_min_distance) {
	bush_min_distance = p_bush_min_distance;
}

real_t Ecotope::get_bush_min_distance() const {
	return bush_min_distance;
}

void Ecotope::set_tree_min_distance(const real_t p_tree_min_distance) {
	tree_min_distance = p_tree_min_distance;
}

real_t Ecotope::get_tree_min_distance() const {
	return tree_min_distance;
}

void Ecotope::set_misc_min_distance(const real_t p_misc_min_distance) {
	misc_min_distance = p_misc_min_distance;
}

real_t Ecotope::get_misc_min_distance() const {
	return misc_min_distance;
}

void Ecotope::set_grass_noise_freq(const real_t p_grass_noise_freq) {
	grass_noise_freq = p_grass_noise_freq;
}

real_t Ecotope::get_grass_noise_freq() const {
	return grass_noise_freq;
}

void Ecotope::set_bush_noise_freq(const real_t p_bush_noise_freq) {
	bush_noise_freq = p_bush_noise_freq;
}

real_t Ecotope::get_bush_noise_freq() const {
	return bush_noise_freq;
}

void Ecotope::set_tree_noise_freq(const real_t p_tree_noise_freq) {
	tree_noise_freq = p_tree_noise_freq;
}

real_t Ecotope::get_tree_noise_freq() const {
	return tree_noise_freq;
}

void Ecotope::set_misc_noise_freq(const real_t p_misc_noise_freq) {
	misc_noise_freq = p_misc_noise_freq;
}

real_t Ecotope::get_misc_noise_freq() const {
	return misc_noise_freq;
}

void Ecotope::set_grass_group_freq(const real_t p_grass_group_freq) {
	grass_group_freq = p_grass_group_freq;
}

real_t Ecotope::get_grass_group_freq() const {
	return grass_group_freq;
}

void Ecotope::set_bush_group_freq(const real_t p_bush_group_freq) {
	bush_group_freq = p_bush_group_freq;
}

real_t Ecotope::get_bush_group_freq() const {
	return bush_group_freq;
}

void Ecotope::set_tree_group_freq(const real_t p_tree_group_freq) {
	tree_group_freq = p_tree_group_freq;
}

real_t Ecotope::get_tree_group_freq() const {
	return tree_group_freq;
}

void Ecotope::set_misc_group_freq(const real_t p_misc_group_freq) {
	misc_group_freq = p_misc_group_freq;
}

real_t Ecotope::get_misc_group_freq() const {
	return misc_group_freq;
}

void Ecotope::set_palette(Ref<EcotopePalette> p_palette) {
	palette = p_palette;
}

Ref<EcotopePalette> Ecotope::get_palette() const {
	return palette;
}

void Ecotope::set_noise_graph(Ref<NoiseGraph> p_noise_graph) {
	noise_graph = p_noise_graph;
}

Ref<NoiseGraph> Ecotope::get_noise_graph() const {
	return noise_graph;
}

void Ecotope::set_height_curve(Ref<InfluenceCurve> p_height_curve) {
	height_curve = p_height_curve;
}

Ref<InfluenceCurve> Ecotope::get_height_curve() const {
	return height_curve;
}

void Ecotope::set_noise_graph_3d(Ref<NoiseGraph> p_noise_graph) {
	noise_graph_3d = p_noise_graph;
}

Ref<NoiseGraph> Ecotope::get_noise_graph_3d() const {
	return noise_graph_3d;
}

void Ecotope::set_3d_threshold(const real_t p_threshold) {
	threshold_3d = p_threshold;
}

real_t Ecotope::get_3d_threshold() const {
	return threshold_3d;
}

void Ecotope::set_force_generation(const bool p_force_generation) {
	force_generation = p_force_generation;
}

bool Ecotope::get_force_generation() const {
	return force_generation;
}

bool Ecotope::is_valid(ErrorLogger &r_err) const {
	if (palette.is_null()) {
		r_err.push_contex_error("Empty EcotopePalette");
	}
	if (grass_mask <= 0) {
		r_err.push_contex_error("Grass mask can't be 0 or less");
	}
	if (bush_mask <= 0) {
		r_err.push_contex_error("Bush mask can't be 0 or less");
	}
	if (tree_mask <= 0) {
		r_err.push_contex_error("Tree mask can't be 0 or less");
	}
	if (misc_mask <= 0) {
		r_err.push_contex_error("Misc mask can't be 0 or less");
	}
	if (grass_min_distance <= 0) {
		r_err.push_contex_error("Grass mask can't be 0 or less");
	}
	if (bush_min_distance <= 0) {
		r_err.push_contex_error("Bush mask can't be 0 or less");
	}
	if (tree_min_distance <= 0) {
		r_err.push_contex_error("Tree mask can't be 0 or less");
	}
	if (misc_min_distance <= 0) {
		r_err.push_contex_error("Misc mask can't be 0 or less");
	}
	if (grass_noise_freq <= 0) {
		r_err.push_contex_error("Grass frequency can't be 0 or less");
	}
	if (bush_noise_freq <= 0) {
		r_err.push_contex_error("Bush frequency can't be 0 or less");
	}
	if (tree_noise_freq <= 0) {
		r_err.push_contex_error("Tree frequency can't be 0 or less");
	}
	if (misc_noise_freq <= 0) {
		r_err.push_contex_error("Misc frequency can't be 0 or less");
	}
	r_err.close_context("The " + get_class() + " node \"" + get_name() + "\" has an invalid configuration");
	return !r_err;
}

void Ecotope::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_grass_mask", "grass_mask"), &Ecotope::set_grass_mask);
	ClassDB::bind_method(D_METHOD("get_grass_mask"), &Ecotope::get_grass_mask);
	ClassDB::bind_method(D_METHOD("set_bush_mask", "bush_mask"), &Ecotope::set_bush_mask);
	ClassDB::bind_method(D_METHOD("get_bush_mask"), &Ecotope::get_bush_mask);
	ClassDB::bind_method(D_METHOD("set_tree_mask", "tree_mask"), &Ecotope::set_tree_mask);
	ClassDB::bind_method(D_METHOD("get_tree_mask"), &Ecotope::get_tree_mask);
	ClassDB::bind_method(D_METHOD("set_misc_mask", "misc_mask"), &Ecotope::set_misc_mask);
	ClassDB::bind_method(D_METHOD("get_misc_mask"), &Ecotope::get_misc_mask);

	ClassDB::bind_method(D_METHOD("set_grass_min_distance", "grass_min_distance"), &Ecotope::set_grass_min_distance);
	ClassDB::bind_method(D_METHOD("get_grass_min_distance"), &Ecotope::get_grass_min_distance);
	ClassDB::bind_method(D_METHOD("set_bush_min_distance", "bush_min_distance"), &Ecotope::set_bush_min_distance);
	ClassDB::bind_method(D_METHOD("get_bush_min_distance"), &Ecotope::get_bush_min_distance);
	ClassDB::bind_method(D_METHOD("set_tree_min_distance", "tree_min_distance"), &Ecotope::set_tree_min_distance);
	ClassDB::bind_method(D_METHOD("get_tree_min_distance"), &Ecotope::get_tree_min_distance);
	ClassDB::bind_method(D_METHOD("set_misc_min_distance", "misc_min_distance"), &Ecotope::set_misc_min_distance);
	ClassDB::bind_method(D_METHOD("get_misc_min_distance"), &Ecotope::get_misc_min_distance);

	ClassDB::bind_method(D_METHOD("set_grass_noise_freq", "grass_noise_freq"), &Ecotope::set_grass_noise_freq);
	ClassDB::bind_method(D_METHOD("get_grass_noise_freq"), &Ecotope::get_grass_noise_freq);
	ClassDB::bind_method(D_METHOD("set_bush_noise_freq", "bush_noise_freq"), &Ecotope::set_bush_noise_freq);
	ClassDB::bind_method(D_METHOD("get_bush_noise_freq"), &Ecotope::get_bush_noise_freq);
	ClassDB::bind_method(D_METHOD("set_tree_noise_freq", "tree_noise_freq"), &Ecotope::set_tree_noise_freq);
	ClassDB::bind_method(D_METHOD("get_tree_noise_freq"), &Ecotope::get_tree_noise_freq);
	ClassDB::bind_method(D_METHOD("set_misc_noise_freq", "misc_noise_freq"), &Ecotope::set_misc_noise_freq);
	ClassDB::bind_method(D_METHOD("get_misc_noise_freq"), &Ecotope::get_misc_noise_freq);

	ClassDB::bind_method(D_METHOD("set_grass_group_freq", "grass_group_freq"), &Ecotope::set_grass_group_freq);
	ClassDB::bind_method(D_METHOD("get_grass_group_freq"), &Ecotope::get_grass_group_freq);
	ClassDB::bind_method(D_METHOD("set_bush_group_freq", "bush_group_freq"), &Ecotope::set_bush_group_freq);
	ClassDB::bind_method(D_METHOD("get_bush_group_freq"), &Ecotope::get_bush_group_freq);
	ClassDB::bind_method(D_METHOD("set_tree_group_freq", "tree_group_freq"), &Ecotope::set_tree_group_freq);
	ClassDB::bind_method(D_METHOD("get_tree_group_freq"), &Ecotope::get_tree_group_freq);
	ClassDB::bind_method(D_METHOD("set_misc_group_freq", "misc_group_freq"), &Ecotope::set_misc_group_freq);
	ClassDB::bind_method(D_METHOD("get_misc_group_freq"), &Ecotope::get_misc_group_freq);

	ClassDB::bind_method(D_METHOD("set_palette", "palette"), &Ecotope::set_palette);
	ClassDB::bind_method(D_METHOD("get_palette"), &Ecotope::get_palette);
	ClassDB::bind_method(D_METHOD("set_noise_graph", "noise_graph"), &Ecotope::set_noise_graph);
	ClassDB::bind_method(D_METHOD("get_noise_graph"), &Ecotope::get_noise_graph);
	ClassDB::bind_method(D_METHOD("set_noise_graph_3d", "noise_graph"), &Ecotope::set_noise_graph_3d);
	ClassDB::bind_method(D_METHOD("get_noise_graph_3d"), &Ecotope::get_noise_graph_3d);
	ClassDB::bind_method(D_METHOD("set_height_curve", "curve"), &Ecotope::set_height_curve);
	ClassDB::bind_method(D_METHOD("get_height_curve"), &Ecotope::get_height_curve);
	ClassDB::bind_method(D_METHOD("set_3d_threshold", "threshold"), &Ecotope::set_3d_threshold);
	ClassDB::bind_method(D_METHOD("get_3d_threshold"), &Ecotope::get_3d_threshold);
	ClassDB::bind_method(D_METHOD("set_force_generation", "force_generation"), &Ecotope::set_force_generation);
	ClassDB::bind_method(D_METHOD("get_force_generation"), &Ecotope::get_force_generation);

	GDVIRTUAL_BIND(_generate_noise_region, "rect", "gen_seed");
	
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "force_generation"), "set_force_generation", "get_force_generation");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "palette", PROPERTY_HINT_RESOURCE_TYPE, "EcotopePalette", PROPERTY_USAGE_DEFAULT | PROPERTY_USAGE_EDITOR_INSTANTIATE_OBJECT), "set_palette", "get_palette");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "noise_graph", PROPERTY_HINT_RESOURCE_TYPE, "NoiseGraph", PROPERTY_USAGE_DEFAULT | PROPERTY_USAGE_EDITOR_INSTANTIATE_OBJECT), "set_noise_graph", "get_noise_graph");

	ADD_GROUP("Mask", "");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "grass_mask", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_grass_mask", "get_grass_mask");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "bush_mask", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_bush_mask", "get_bush_mask");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "tree_mask", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_tree_mask", "get_tree_mask");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "misc_mask", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_misc_mask", "get_misc_mask");

	ADD_GROUP("Element Distance", "");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "grass_distance", PROPERTY_HINT_RANGE, "0.01,50,0.01,or_greater"), "set_grass_min_distance", "get_grass_min_distance");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "bush_distance", PROPERTY_HINT_RANGE, "0.1,100,0.1,or_greater"), "set_bush_min_distance", "get_bush_min_distance");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "tree_distance", PROPERTY_HINT_RANGE, "0.2,200,0.2,or_greater"), "set_tree_min_distance", "get_tree_min_distance");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "misc_distance", PROPERTY_HINT_RANGE, "0.1,200,0.1,or_greater"), "set_misc_min_distance", "get_misc_min_distance");

	ADD_GROUP("Noise Frequency", "");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "grass_noise_freq", PROPERTY_HINT_RANGE, "0.001,0.2,0.001,or_greater"), "set_grass_noise_freq", "get_grass_noise_freq");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "bush_noise_freq", PROPERTY_HINT_RANGE, "0.001,0.2,0.001,or_greater"), "set_bush_noise_freq", "get_bush_noise_freq");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "tree_noise_freq", PROPERTY_HINT_RANGE, "0.001,0.2,0.001,or_greater"), "set_tree_noise_freq", "get_tree_noise_freq");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "misc_noise_freq", PROPERTY_HINT_RANGE, "0.001,0.2,0.001,or_greater"), "set_misc_noise_freq", "get_misc_noise_freq");

	ADD_GROUP("Grouping", "");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "grass_group_freq", PROPERTY_HINT_RANGE, "0.001,1,0.001,or_greater"), "set_grass_group_freq", "get_grass_group_freq");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "bush_group_freq", PROPERTY_HINT_RANGE, "0.001,1,0.001,or_greater"), "set_bush_group_freq", "get_bush_group_freq");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "tree_group_freq", PROPERTY_HINT_RANGE, "0.001,1,0.001,or_greater"), "set_tree_group_freq", "get_tree_group_freq");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "misc_group_freq", PROPERTY_HINT_RANGE, "0.001,1,0.001,or_greater"), "set_misc_group_freq", "get_misc_group_freq");

	// TODO
	ADD_GROUP("3D", "");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "noise_graph_3d", PROPERTY_HINT_RESOURCE_TYPE, "NoiseGraph"), "set_noise_graph_3d", "get_noise_graph_3d");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "height_curve", PROPERTY_HINT_RESOURCE_TYPE, "InfluenceCurve"), "set_height_curve", "get_height_curve");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "threshold", PROPERTY_HINT_RANGE, "-1.0,1.0,0.01,or_lesser,or_greater"), "set_3d_threshold", "get_3d_threshold");


}

Ecotope::Ecotope() {}

Ecotope::~Ecotope() {}