// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "aoi.h"

void AreaOfInterest::set_feature_scene(const Ref<PackedScene> &p_feature_scene) {
	feature_scene = p_feature_scene;
}

Ref<PackedScene> AreaOfInterest::get_feature_scene() const {
	return feature_scene;
}

void AreaOfInterest::set_min_y_offset(const real_t p_offset) {
	min_y_offset = MIN(p_offset, max_y_offset);
}

real_t AreaOfInterest::get_min_y_offset() const {
	return min_y_offset;
}

void AreaOfInterest::set_max_y_offset(const real_t p_offset) {
	max_y_offset = MAX(p_offset, min_y_offset);
}

real_t AreaOfInterest::get_max_y_offset() const {
	return max_y_offset;
}

void AreaOfInterest::set_min_instances(const int p_instances) {
	min_instances = MIN(p_instances, max_instances);
}

int AreaOfInterest::get_min_instances() const {
	return min_instances;
}

void AreaOfInterest::set_max_instances(const int p_instances) {
	max_instances = MAX(p_instances, min_instances);
}

int AreaOfInterest::get_max_instances() const {
	return max_instances;
}

void AreaOfInterest::set_scale_with_world(const bool p_scale) {
	scale_with_world = p_scale;
}

bool AreaOfInterest::get_scale_with_world() const {
	return scale_with_world;
}

void AreaOfInterest::set_reserve_radius(const real_t p_radius) {
	reserve_radius = p_radius;
}

real_t AreaOfInterest::get_reserve_radius() const {
	return reserve_radius;
}

void AreaOfInterest::set_min_rotation(const Vector3 &p_min_rotation) {
	min_rotation.x = MIN(p_min_rotation.x, max_rotation.x);
	min_rotation.y = MIN(p_min_rotation.y, max_rotation.y);
	min_rotation.z = MIN(p_min_rotation.z, max_rotation.z);
}

void AreaOfInterest::set_max_rotation(const Vector3 &p_max_rotation) {
	max_rotation.x = MAX(p_max_rotation.x, min_rotation.x);
	max_rotation.y = MAX(p_max_rotation.y, min_rotation.y);
	max_rotation.z = MAX(p_max_rotation.z, min_rotation.z);
}

void AreaOfInterest::set_min_rotation_degrees(const Vector3 &p_min_rotation_degrees) {
	set_min_rotation(Vector3(
		Math::deg2rad(p_min_rotation_degrees.x),
		Math::deg2rad(p_min_rotation_degrees.y),
		Math::deg2rad(p_min_rotation_degrees.z)
	));
}

void AreaOfInterest::set_max_rotation_degrees(const Vector3 &p_max_rotation_degrees) {
	set_max_rotation(Vector3(
		Math::deg2rad(p_max_rotation_degrees.x),
		Math::deg2rad(p_max_rotation_degrees.y),
		Math::deg2rad(p_max_rotation_degrees.z)
	));
}

Vector3 AreaOfInterest::get_min_rotation() const {
	return min_rotation;
}

Vector3 AreaOfInterest::get_max_rotation() const {
	return max_rotation;
}

Vector3 AreaOfInterest::get_min_rotation_degrees() const {
	return Vector3(
		Math::rad2deg(min_rotation.x),
		Math::rad2deg(min_rotation.y),
		Math::rad2deg(min_rotation.z)
	);
}

Vector3 AreaOfInterest::get_max_rotation_degrees() const {
	return Vector3(
		Math::rad2deg(max_rotation.x),
		Math::rad2deg(max_rotation.y),
		Math::rad2deg(max_rotation.z)
	);
}

void AreaOfInterest::set_grass_layer(const bool p_grass_layer) {
	d_layers[0] = p_grass_layer;
}

bool AreaOfInterest::get_grass_layer() const {
	return d_layers[0];
}

void AreaOfInterest::set_bush_layer(const bool p_bush_layer) {
	d_layers[1] = p_bush_layer;
}

bool AreaOfInterest::get_bush_layer() const {
	return d_layers[1];
}

void AreaOfInterest::set_tree_layer(const bool p_tree_layer) {
	d_layers[2] = p_tree_layer;
}

bool AreaOfInterest::get_tree_layer() const {
	return d_layers[2];
}

void AreaOfInterest::set_misc_layer(const bool p_misc_layer) {
	d_layers[3] = p_misc_layer;
}

bool AreaOfInterest::get_misc_layer() const {
	return d_layers[3];
}

distribution_layer_t AreaOfInterest::get_distribution_layers() const {
	return d_layers;
}

void AreaOfInterest::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_feature_scene", "feature_scene"), &AreaOfInterest::set_feature_scene);
	ClassDB::bind_method(D_METHOD("get_feature_scene"), &AreaOfInterest::get_feature_scene);
	ClassDB::bind_method(D_METHOD("set_min_y_offset", "min_y_offset"), &AreaOfInterest::set_min_y_offset);
	ClassDB::bind_method(D_METHOD("get_min_y_offset"), &AreaOfInterest::get_min_y_offset);
	ClassDB::bind_method(D_METHOD("set_max_y_offset", "max_y_offset"), &AreaOfInterest::set_max_y_offset);
	ClassDB::bind_method(D_METHOD("get_max_y_offset"), &AreaOfInterest::get_max_y_offset);
	ClassDB::bind_method(D_METHOD("set_min_instances", "min_instances"), &AreaOfInterest::set_min_instances);
	ClassDB::bind_method(D_METHOD("get_min_instances"), &AreaOfInterest::get_min_instances);
	ClassDB::bind_method(D_METHOD("set_max_instances", "max_instances"), &AreaOfInterest::set_max_instances);
	ClassDB::bind_method(D_METHOD("get_max_instances"), &AreaOfInterest::get_max_instances);
	ClassDB::bind_method(D_METHOD("set_scale_with_world", "scale"), &AreaOfInterest::set_scale_with_world);
	ClassDB::bind_method(D_METHOD("get_scale_with_world"), &AreaOfInterest::get_scale_with_world);
	ClassDB::bind_method(D_METHOD("set_reserve_radius", "radius"), &AreaOfInterest::set_reserve_radius);
	ClassDB::bind_method(D_METHOD("get_reserve_radius"), &AreaOfInterest::get_reserve_radius);
	ClassDB::bind_method(D_METHOD("set_grass_layer", "grass_layer"), &AreaOfInterest::set_grass_layer);
	ClassDB::bind_method(D_METHOD("get_grass_layer"), &AreaOfInterest::get_grass_layer);
	ClassDB::bind_method(D_METHOD("set_bush_layer", "bush_layer"), &AreaOfInterest::set_bush_layer);
	ClassDB::bind_method(D_METHOD("get_bush_layer"), &AreaOfInterest::get_bush_layer);
	ClassDB::bind_method(D_METHOD("set_tree_layer", "tree_layer"), &AreaOfInterest::set_tree_layer);
	ClassDB::bind_method(D_METHOD("get_tree_layer"), &AreaOfInterest::get_tree_layer);
	ClassDB::bind_method(D_METHOD("set_misc_layer", "misc_layer"), &AreaOfInterest::set_misc_layer);
	ClassDB::bind_method(D_METHOD("get_misc_layer"), &AreaOfInterest::get_misc_layer);

	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "feature_scene", PROPERTY_HINT_RESOURCE_TYPE, "PackedScene"), "set_feature_scene", "get_feature_scene");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "min_y_offset", PROPERTY_HINT_RANGE, "-100,100,0.01,or_lesser,or_greater"), "set_min_y_offset", "get_min_y_offset");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "max_y_offset", PROPERTY_HINT_RANGE, "-100,100,0.01,or_lesser,or_greater"), "set_max_y_offset", "get_max_y_offset");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "min_instances", PROPERTY_HINT_RANGE, "0,10000,1,or_greater"), "set_min_instances", "get_min_instances");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "max_instances", PROPERTY_HINT_RANGE, "0,10000,1,or_greater"), "set_max_instances", "get_max_instances");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "scale_with_world"), "set_scale_with_world", "get_scale_with_world");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "reserve_radius", PROPERTY_HINT_RANGE, "0,100,0.01,or_greater"), "set_reserve_radius", "get_reserve_radius");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "grass_layer"), "set_grass_layer", "get_grass_layer");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "bush_layer"), "set_bush_layer", "get_bush_layer");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "tree_layer"), "set_tree_layer", "get_tree_layer");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "misc_layer"), "set_misc_layer", "get_misc_layer");

}

bool AreaOfInterest::is_valid(ErrorLogger &r_err) const {
	if (feature_scene.is_null()) {
		r_err.push_contex_error("Empty Scene");
	} else if (!feature_scene->can_instantiate()) {
		r_err.push_contex_error("The scene doesn't contain nodes");
	}
	r_err.close_context("The " + get_class() + " node \"" + get_name() + "\" has an invalid configuration");
	return !r_err;
}

AreaOfInterest::AreaOfInterest() {}