// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/math/vector2.h"
#include "scene/resources/navigation_mesh.h"
#include "core/io/resource.h"
#include <mapgenconstants.h>
#include <vector>

class CompositionData : public Resource {
	GDCLASS(CompositionData, Resource);
	OBJ_SAVE_TYPE(CompositionData);

private:
	Vector2i world_size = Vector2i(Lod::CHUNK_SIDE_SIZE * 10, Lod::CHUNK_SIDE_SIZE * 10);
	int seed = 127364;
	bool report_disabled_nodes = false;
	Ref<NavigationMesh> nav_mesh;

protected:
	static void _bind_methods();

public:
	bool is_valid(ErrorLogger &r_err) const;

	void print_data() const;

	void set_world_size(const Vector2i &p_world_size);
	Vector2i get_world_size() const;

	void set_seed(const int p_seed);
	int get_seed() const;

	void set_report_disabled_nodes(const bool p_report);
	bool get_report_disabled_nodes() const;

	void set_navigation_mesh(const Ref<NavigationMesh> &p_navmesh);
	Ref<NavigationMesh> get_navigation_mesh() const;

	CompositionData();
};