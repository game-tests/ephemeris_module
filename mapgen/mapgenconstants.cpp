// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "mapgenconstants.h"
#include "core/string/print_string.h"

namespace Lod {
	const int BASE_MESH_SIZE = 32;
	const int BASE_VERTEX_DISTANCE = 2;
	const int NUM_LODS = 3;
	const int LAST_LOD = NUM_LODS -1;
	const int LAST_LOD_SIDE_SIZE = BASE_MESH_SIZE * Math::pow(2.0, static_cast<double>(LAST_LOD));
	const int CHUNK_SIDE_SIZE = LAST_LOD_SIDE_SIZE;
	const int OCTREE_SIDE_SIZE = LAST_LOD_SIDE_SIZE * 2;

	const double UPDATE_FREQ = 0.25;
}

void ErrorLogger::set_prefix(const String &p_prefix) {
	prefix = p_prefix;
}

void ErrorLogger::push_error(const String &p_err) {
	// 
	if (context_list.is_empty() || !context_list.back()->get().context.is_empty()) {
		ContextualErr err;
		err.errors.push_front(p_err);
		context_list.push_back(err);
	} else {
		context_list.back()->get().errors.push_back(p_err);
	}
}

void ErrorLogger::push_contex_error(const String &p_err) {
	tmp_errors.push_back(p_err);
}

void ErrorLogger::close_context(const String &p_context) {
	if (tmp_errors.is_empty()) {
		return;
	}
	ContextualErr err;
	err.context = p_context;
	err.errors = tmp_errors;
	tmp_errors.clear();
	
	context_list.push_back(err);
}

void ErrorLogger::print() const {
	for (const String &error : tmp_errors) {
		print_error(error + ".");
	}
	String error_prefix;
	for (const ContextualErr &c_err : context_list) {
		if (c_err.context.size()) {
			print_error(prefix + ": " + c_err.context + ":");
			error_prefix = "- ";
		} else {
			error_prefix.clear();
		}
		for (const String &err : c_err.errors) {
			print_error(error_prefix + err + ".");
		}
	}
}
