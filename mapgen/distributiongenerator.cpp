// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "distributiongenerator.h"
#include "servers/physics_server_3d.h"
#include "servers/rendering_server.h"


void DistributionGenerator::set_distribution(const DistributionType p_type, const real_t p_distance) {
	std::vector<Vector2> points;
	real_t base_height;
	real_t base_width;

	if (p_type == DistributionType::GRASS) {
		points = {
			{0.5625, 0.5625}, {1.8125, 0.6875}, {2.9375, 0.3125}, {4.3125, 0.0625},
			{1, 1.375}, {2.5625, 1.5625}, {3.6875, 0.9375}, {4.6875, 1.4375},
			{0.5, 2.3125}, {1.6875, 2.0625}, {3.625, 2}, {1.3125, 3.0625},
			{2.875, 2.75}, {4.25, 2.875}, {0.3125, 3.5625}, {1.375, 4.0625},
			{2.3125, 3.6875}, {3.4375, 3.625}
		};
		base_height = 4.25;
		base_width = 4.75;
	} else if (p_type == DistributionType::GENERAL) {
		// TODO change
		points = {
			{0.5625, 0.5625}, {1.8125, 0.6875}, {2.9375, 0.3125}, {4.3125, 0.0625},
			{1, 1.375}, {2.5625, 1.5625}, {3.6875, 0.9375}, {4.6875, 1.4375},
			{0.5, 2.3125}, {1.6875, 2.0625}, {3.625, 2}, {1.3125, 3.0625},
			{2.875, 2.75}, {4.25, 2.875}, {0.3125, 3.5625}, {1.375, 4.0625},
			{2.3125, 3.6875}, {3.4375, 3.625}
		};
		base_height = 4.25;
		base_width = 4.75;
	}

	distribution_type = p_type;

	tile_distance_points = points;
	for (size_t i = 0; i < points.size(); i++) {
		tile_distance_points[i] *= p_distance;
	}
	tile_height = base_height * p_distance;
	tile_width = base_width * p_distance;

}

std::vector<Vector2> DistributionGenerator::get_rect_points(const Rect2 &p_rect) const {
	std::vector<Vector2> res;

	// The top left position where we start tiling the point pattern.
	const real_t begin_w = Math::floor(p_rect.position.width / tile_width) * tile_width;
	const real_t begin_h = Math::floor(p_rect.position.height / tile_height) * tile_height;

	// How many horizontal and vertical tiles we need to add.
	const real_t n_w = Math::ceil(p_rect.size.width / tile_width);
	const real_t n_h = Math::ceil(p_rect.size.height / tile_height);

	res.reserve(tile_distance_points.size() * n_w + tile_distance_points.size() * n_h);

	real_t offset_w = 0;
	real_t offset_h = 0;

	std::vector<Vector2> points;

	for (int i = 0; i < n_w; i++) {
		offset_w = begin_w + i * tile_width;
		for (int j = 0; j < n_h; j++) {
			offset_h = begin_h + j * tile_height;

			points = tile_distance_points;
			// Apply the offset.
			for (size_t p_index = 0; p_index < points.size(); p_index++) {
				points[p_index].height += offset_h;
				points[p_index].width += offset_w;
			}
			// The elements in the border need ot be checked to know if they are inside the area.
			const bool requires_check = (i == 0 || j == 0 || i == (n_w - 1) || j == (n_h - 1));

			if (requires_check) {
				for (size_t p_index = 0; p_index < points.size(); p_index++) {
					if (p_rect.has_point(points[p_index])) {
						res.push_back(points[p_index]);
					}
				}
			} else {

				for (size_t p_index = 0; p_index < points.size(); p_index++) {
					res.push_back(points[p_index]);
				}
			}
		}
	}
	return res;
}

DistributionGenerator::DistributionGenerator() {}
