// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#ifndef EPHEMERIS_REGISTER_TYPES_H
#define EPHEMERIS_REGISTER_TYPES_H

#include "modules/register_module_types.h"

void initialize_ephemeris_module(ModuleInitializationLevel p_level);
void uninitialize_ephemeris_module(ModuleInitializationLevel p_level);

#endif // EPHEMERIS_REGISTER_TYPES_H
