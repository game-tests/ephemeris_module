// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include "core/math/vector2.h"
#include "core/object/ref_counted.h"
#include "core/io/image.h"

struct StarPoint {
	Vector2 pos;
	// (0, 1]
	real_t size;
	real_t time_offset;
};

class StarDataGenerator : public RefCounted {
	GDCLASS(StarDataGenerator, RefCounted)

protected:
	void _init();

	static void _bind_methods();

public:

	StarDataGenerator();

	Image* generate_star_data(const int p_seed);

	void set_texture_size(const float p_texture_size);
	int64_t get_texture_size() const;

	void set_star_radius(const float p_star_radius);
	int64_t get_star_radius() const;

	void set_star_density(const float p_star_density);
	real_t get_star_density() const;

	void set_min_star_size(const float p_min_star_size);
	real_t get_min_star_size() const;

	void set_max_star_size(const float p_max_star_size);
	real_t get_max_star_size() const;

private:
	int64_t m_texture_size = 2048;
	int64_t m_star_radius = 30;
	real_t m_star_density = 0.1;
	real_t m_min_star_size = 0.1;
	real_t m_max_star_size = 0.3;

	std::vector<StarPoint> generate_star_points(const int p_seed) const;

	_FORCE_INLINE_ real_t normalize_coord(const real_t p_coord) const {
		return p_coord / m_texture_size;
	}
	_FORCE_INLINE_ real_t denormalize_coord(const real_t p_coord) const {
		return p_coord * m_texture_size;
	}
};