// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "stardatagenerator.h"
#include "core/math/vector2.h"
#include "scene/resources/texture.h"
#include <matrix_2d.h>

StarDataGenerator::StarDataGenerator() {}

void StarDataGenerator::_init() {}

void StarDataGenerator::_bind_methods() {
	ClassDB::bind_method(D_METHOD("generate_star_data"), &StarDataGenerator::generate_star_data);
	ClassDB::bind_method(D_METHOD("set_texture_size", "size"), &StarDataGenerator::set_texture_size);
	ClassDB::bind_method(D_METHOD("get_texture_size"), &StarDataGenerator::get_texture_size);
	ClassDB::bind_method(D_METHOD("set_star_density", "density"), &StarDataGenerator::set_star_density);
	ClassDB::bind_method(D_METHOD("get_star_density"), &StarDataGenerator::get_star_density);
	ClassDB::bind_method(D_METHOD("set_star_radius", "radius"), &StarDataGenerator::set_star_radius);
	ClassDB::bind_method(D_METHOD("get_star_radius"), &StarDataGenerator::get_star_radius);
	ClassDB::bind_method(D_METHOD("set_min_star_size", "min_star_size"), &StarDataGenerator::set_min_star_size);
	ClassDB::bind_method(D_METHOD("get_min_star_size"), &StarDataGenerator::get_min_star_size);
	ClassDB::bind_method(D_METHOD("set_max_star_size", "max_star_size"), &StarDataGenerator::set_max_star_size);
	ClassDB::bind_method(D_METHOD("get_max_star_size"), &StarDataGenerator::get_max_star_size);

	ADD_PROPERTY(PropertyInfo(Variant::INT, "texture_size"), "set_texture_size", "get_texture_size");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "star_density"), "set_star_density", "get_star_density");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "star_radius"), "set_star_radius", "get_star_radius");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "min_star_size"), "set_min_star_size", "get_min_star_size");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "max_star_size"), "set_max_star_size", "get_max_star_size");
}

/*
r = distance to max radius (in pixels) normalized [0, 1]
g = star size [0, 1]
b = temporal blink offset [0, 1]
*/
Image* StarDataGenerator::generate_star_data(const int p_seed) {
    Image* img = memnew(Image);

    img->create(m_texture_size, m_texture_size, false, Image::FORMAT_RGBF);

	std::vector<StarPoint> star_points = generate_star_points(p_seed);

    const float max_distance = 1.0;
    Matrix<Color> star_data(m_texture_size, m_texture_size, Color(max_distance, 0.0, 0.0));

    #pragma omp parallel for
    for (size_t i = 0; i < star_points.size(); i++) {
        Vector2 pos = star_points[i].pos;
        
        int start_x = pos.x - m_star_radius;
        int end_x = pos.x + m_star_radius;
        int start_y = pos.y - m_star_radius;
        int end_y = pos.y + m_star_radius;

        for (int x = start_x; x <= end_x; ++x) {
            for (int y = start_y; y <= end_y; ++y) {
				real_t dist = pos.distance_to(Vector2(x, y));
				if (dist < m_star_radius) {
					Color c_data(dist / m_star_radius,
						star_points[i].size,
						star_points[i].time_offset);
					star_data(x, y) = c_data;
				}
            }
        }
    }

    for (int x = 0; x < star_data.rows(); ++x) {
		for (int y = 0; y < star_data.columns(); ++y) {
            img->set_pixel(x, y, star_data(x, y));
        }
    }

	return img;
}

void StarDataGenerator::set_texture_size(const float p_texture_size) {
	m_texture_size = p_texture_size;
}

int64_t StarDataGenerator::get_texture_size() const {
	return m_texture_size;
}

void StarDataGenerator::set_star_radius(const float p_star_radius) {
	m_star_radius = p_star_radius;
}

int64_t StarDataGenerator::get_star_radius() const {
	return m_star_radius;
}

void StarDataGenerator::set_star_density(const float p_star_density) {
	m_star_density = p_star_density;
}

real_t StarDataGenerator::get_star_density() const {
	return m_star_density;
}

void StarDataGenerator::set_min_star_size(const float p_min_star_size) {
	m_min_star_size = p_min_star_size;
}
real_t StarDataGenerator::get_min_star_size() const {
	return m_min_star_size;
}

void StarDataGenerator::set_max_star_size(const float p_max_star_size) {
	m_max_star_size = p_max_star_size;
}
real_t StarDataGenerator::get_max_star_size() const {
	return m_max_star_size;
}

std::vector<StarPoint> StarDataGenerator::generate_star_points(const int p_seed) const
{
	RandomPCG pcg(p_seed);

	int64_t num_stars = (m_texture_size * m_texture_size) / 2000 * m_star_density;
	real_t min_distance = m_star_radius * 2;

	real_t min_pos = m_star_radius;
	real_t max_pos = m_texture_size - m_star_radius;

	std::vector<StarPoint> stars;
	stars.reserve(num_stars);

	#pragma omp parallel for
	for (int64_t i = 0; i < num_stars; i++) {
		float x = pcg.random(0, 1);
		float y = pcg.random(0, 1);
		Vector2 point(denormalize_coord(x), denormalize_coord(y));

		bool too_close = point.x < min_pos || point.x > max_pos ||
			point.y < min_pos || point.y > max_pos;
		
		for (size_t j = 0; j < stars.size(); j++) {
			if (stars[j].pos.distance_to(point) < min_distance) {
				too_close = true;
				break;
			}
		}
		if (too_close) {
			continue;
		}
		StarPoint star;
		star.pos = point;

		real_t rand_size_factor = pcg.random(0, 1);
		star.size = m_min_star_size + rand_size_factor * (m_max_star_size - m_min_star_size);

		star.time_offset = pcg.random(0, 1);

		stars.push_back(star);
	}
	return stars;
}
