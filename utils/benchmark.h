// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include "core/string/ustring.h"

class Benchmark {
public:

	enum TimeUnit {
		MICROSECONDS = 0,
		MILLISECONDS = 1,
	};

	void set_loop_target(const size_t p_n);

	void start(const String &p_name, const TimeUnit p_unit);

	uint64_t get_elapsed() const;
	void print_results();

	// for loop integration
	bool is_done() const;
	void next();

	// global
	static void reset_global_accumulator();
	static void print_global_accumulator(const TimeUnit p_unit = TimeUnit::MILLISECONDS);

private:
	String bench_name;
	
	uint64_t start_time;
	TimeUnit unit_factor;
	size_t loop_target = 1;

	std::vector<uint64_t> metrics;

	static uint64_t global_accum;

public:
	Benchmark();
	Benchmark(const String &name, const TimeUnit unit);
	~Benchmark();
};

#ifdef DEBUG_ENABLED
	#define BENCHMARK_MICRO(NAME) \
		for (Benchmark __bench_##__LINE__(NAME, Benchmark::MICROSECONDS); \
				__bench_##__LINE__.is_done() == false; __bench_##__LINE__.next())
#else
    #define BENCHMARK_MICRO(NAME)
#endif

#ifdef DEBUG_ENABLED
	#define BENCHMARK_MILLIS(NAME) \
		for (Benchmark __bench_##__LINE__(NAME, Benchmark::MILLISECONDS); \
				__bench_##__LINE__.is_done() == false; __bench_##__LINE__.next())
#else
    #define BENCHMARK_MILLIS(NAME)
#endif

