// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#include "benchmark.h"
#include "core/os/os.h"

uint64_t Benchmark::global_accum = 0;

real_t unit_conversion_factor[] = {
    1,
    1000,
};

String unit_name[] = {
    "us",
    "ms",
};


void Benchmark::set_loop_target(const size_t p_n) {
	ERR_FAIL_COND(p_n < 1);

	loop_target = p_n;
	metrics.reserve(p_n);

}

void Benchmark::start(const String &p_name, const TimeUnit p_unit) {
	bench_name = p_name;
	unit_factor = p_unit;
	start_time = OS::get_singleton()->get_ticks_usec();
}

uint64_t Benchmark::get_elapsed() const {
	uint64_t end  = OS::get_singleton()->get_ticks_usec();
	return end - start_time;
}

void Benchmark::print_results() {
	
	uint64_t res = 0;

	if (metrics.size() == 0) { // no data
		res = get_elapsed();
	} else if (metrics.size() == 1) { // single value
		res = metrics[0];
	} else { // lowest of multiple
		res = 99999999;
		for (real_t i = 0; i < metrics.size(); i++) {
			if (metrics[i] < res) {
				res = metrics[i];
			}
		}
	}

	global_accum += res;

	res /= unit_conversion_factor[unit_factor];
	
	
	print_line(bench_name + " time: " + String::num_int64(res) + unit_name[unit_factor]);
}

bool Benchmark::is_done() const {
	return loop_target == metrics.size();
}

void Benchmark::next() {
	uint64_t end  = OS::get_singleton()->get_ticks_usec();
	metrics.push_back(end - start_time);
	start_time = end;
}

void Benchmark::reset_global_accumulator() {
	global_accum = 0;
}

void Benchmark::print_global_accumulator(const TimeUnit p_unit) {
	print_line("Global time: " + String::num_int64(global_accum / unit_conversion_factor[p_unit]) + unit_name[p_unit]);
}

Benchmark::Benchmark() : start_time(0), unit_factor(Benchmark::MILLISECONDS), loop_target(1) {
}

Benchmark::Benchmark(const String &p_name, const TimeUnit p_unit) {
	start(p_name, p_unit);
}

Benchmark::~Benchmark() {
	print_results();
}