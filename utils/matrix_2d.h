// Copyright (C)
// 2018-2022 Alejandro Sirgo Rica
//
//  This file is part of Ephemeris.
//
//  Ephemeris is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Ephemeris is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Ephemeris.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "core/math/vector2i.h"
#include "core/string/ustring.h"
#include <vector>

template <class T>
class Matrix {
public:
	Matrix() {}

	Matrix(const int p_rows, const  int p_columns) :
		m_data(p_rows * p_columns), m_rows(p_rows), m_cols(p_columns)
	{}

	Matrix(const int p_rows, const  int p_columns, const T &p_value) :
		m_data(p_rows * p_columns, p_value), m_rows(p_rows), m_cols(p_columns)
	{}

	_FORCE_INLINE_ T& operator() (const int p_row, const int p_col) {
		int position = p_row * m_cols + p_col;
		return m_data[position];
	}

	_FORCE_INLINE_ T operator() (const int p_row, const int p_col) const {
		int position = p_row * m_cols + p_col;
		return m_data[position];
	}

	_FORCE_INLINE_ T& operator() (const int p_idx) {
		return m_data[p_idx];
	}

	_FORCE_INLINE_ T operator() (const int p_idx) const {
		return m_data[p_idx];
	}

	_FORCE_INLINE_ T& operator() (const Point2i &p_point) {
		int position = p_point.x * m_cols + p_point.y;
		return m_data[position];
	}

	_FORCE_INLINE_ T operator() (const Point2i &p_point) const {
		int position = p_point.x * m_cols + p_point.y;
		return m_data[position];
	}

	_FORCE_INLINE_ T& get_value(const int p_row, const int p_col) {
		int position = p_row * m_cols + p_col;
		return m_data[position];
	}

	_FORCE_INLINE_ T get_value(const int p_row, const int p_col) const {
		int position = p_row * m_cols + p_col;
		return m_data[position];
	}

	_FORCE_INLINE_ T& get_value(const int p_idx) {
		return m_data[p_idx];
	}

	_FORCE_INLINE_ T get_value(const int p_idx) const {
		return m_data[p_idx];
	}

	_FORCE_INLINE_ T& get_value(const Point2i &p_point) {
		int position = p_point.x * m_cols + p_point.y;
		return m_data[position];
	}

	_FORCE_INLINE_ T get_value(const Point2i &p_point) const {
		int position = p_point.x * m_cols + p_point.y;
		return m_data[position];
	}

	void resize(const int p_rows, const int p_columns) {
		m_rows = p_rows;
		m_cols = p_columns;
		m_data.resize(p_rows * p_columns);
	}

	_FORCE_INLINE_ void resize(const Vector2i p_v) {
		resize(p_v.x, p_v.y);
	}

	void resize(const int p_rows, const int p_columns, const T &p_value) {
		m_rows = p_rows;
		m_cols = p_columns;
		m_data.resize(p_rows * p_columns, p_value);
	}

	_FORCE_INLINE_ void resize(const Vector2i p_v, const T &p_value) {
		resize(p_v.x, p_v.y, p_value);
	}

	_FORCE_INLINE_ int rows() const {
		return m_rows;
	}

	_FORCE_INLINE_ int columns() const {
		return m_cols;
	}

	_FORCE_INLINE_ int size() const { return m_data.size(); }
	_FORCE_INLINE_ int empty() const { return m_data.empty(); }
	_FORCE_INLINE_ Vector2i size_vector() const { return Vector2i(m_rows, m_cols); }
	_FORCE_INLINE_ typename std::vector<T>::iterator begin() { return m_data.begin(); }
	_FORCE_INLINE_ typename std::vector<T>::iterator end() { return m_data.end(); }
	_FORCE_INLINE_ typename std::vector<T>::const_iterator begin() const { return m_data.begin(); }
	_FORCE_INLINE_ typename std::vector<T>::const_iterator end() const { return m_data.end(); }
	_FORCE_INLINE_ T* data() { return m_data.data(); }

	operator String() const {
		String res;
		for (int i = 0; i < m_rows; i++) {
			for (int j = 0; j < m_cols; j++) {
				res += this->operator() (i, j) + " ";
			}
			res += "\n";
		}
		return res;
	}

private:
	std::vector<T> m_data;

	int m_rows = 0;
	int m_cols = 0;
};
