/////////////////////////////////////////////////////////////////////////////////////
//  IncrementalHalton.cpp
//    
//  Calculates the halton sequence incrementally
//    
//  Andrew Willmott, public domain
/////////////////////////////////////////////////////////////////////////////////////

/*
    Notes:
    
    This is a reference implementation for clarity -- optimizations are possible!
    
    In particular the base 2 part can be done more quickly using bit tricks.
    (E.g., branchless bit reverse followed by or'ing the result into a float
    mantissa.)
*/

#include <stdint.h>
#include <stdio.h>

namespace {
   const float kOneOverThree = float(1.0 / 3.0);
}


/////////////////////////////////////////////////////////////////////////////////////
// HaltonSequence
//

struct HaltonSequence {
/// This calculates the Halton sequence incrementally
    float mX = 0.0;
    float mY = 0.0;

    float get_x() const {
        return mX;
    }

    float get_y() const {
        return mY;
    }
    
    uint32_t mBase2 = 0;
    uint32_t mBase3 = 0;
    
    HaltonSequence() {
        step();
    }
    
    int step();
    ///< Advance to next point in the sequence. Returns the index of this point. 
    void reset();
    ///< Move back to first point in the sequence (i.e. the origin.)
};


int HaltonSequence::step() {
    /////////////////////////////////////
    // base 2
    
    uint32_t oldBase2 = mBase2;
    mBase2++;
    uint32_t diff = mBase2 ^ oldBase2;

    // bottom bit always changes, higher bits
    // change less frequently.
    float s = 0.5f;

    // diff will be of the form 0*1+, i.e. one bits up until the last carry.
    // expected iterations = 1 + 0.5 + 0.25 + ... = 2
    do {
        if (oldBase2 & 1) {
            mX -= s;
        } else {
            mX += s;
        }
        s *= 0.5f;
        
        diff = diff >> 1;
        oldBase2 = oldBase2 >> 1;
    } while (diff);

    
    /////////////////////////////////////
    // base 3: use 2 bits for each base 3 digit.
    
    uint32_t mask = 0x3;  // also the max base 3 digit
    uint32_t add  = 0x1;  // amount to add to force carry once digit==3
    s = kOneOverThree;

    mBase3++;

    // expected iterations: 1.5
    while (1) {
        if ((mBase3 & mask) == mask) {
            mBase3 += add;   // force carry into next 2-bit digit
            mY -= 2 * s;
            
            mask = mask << 2;
            add  = add  << 2;
            
            s *= kOneOverThree;
        }
        else {
            mY += s;     // we know digit n has gone from a to a + 1
            break;
        }
    }

    return mBase2; // return the index of this sequence point
}

void HaltonSequence::reset() {
    mBase2 = 0;
    mBase3 = 0;
    mX = 0.0f;
    mY = 0.0f;

    step();
}
